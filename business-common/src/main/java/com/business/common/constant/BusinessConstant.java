package com.business.common.constant;

public class BusinessConstant {
    /**
     *立项状态
     */
    public static final String PROJECT_NOT_APPROVED = "未立项";

    public static final String PROJECT_UNDER_WAY = "进行中";

    public static final String PROJECT_COMPLETED = "进行中";

    public static final String VARIABLE_NAME_KEY = "name";

    public static final String VARIABLE_VALUE_KEY = "value";

    public static final String DELETE_KEY = "delete";

    /**
     * 审批拒绝
     */
    public static final String OPINION_DISAGREE = "DISAGREE";

    /**
     * 审批通过
     */
    public static final String AGREE = "AGREE";

    /**
     * 审批驳回
     */
    public static final String REFUSE = "REFUSE";

    /**
     * 流程结束状态
     */
    public static final String PROCESS_HISTORY_END = "processIsEnd";


    public static final String ORDER_NOT_APPROVED = "已完成";

    public static final String ORDER_UNDER_WAY = "进行中";

    public static final String ORDER_COMPLETED = "结算单";

    //审批状态
    public static final String PROCESS_SUBMIT_APPROVE = "提交审批";
    public static final String PROCESS_SUBMIT = "已提交";
    public static final String PROCESS_PASS = "已通过";
    public static final String PROCESS_APPROVEING = "审批中";
    public static final String PROCESS_NO_APPROVE = "未开始";
    public static final String PROCESS_REJECT = "已拒绝";
}
