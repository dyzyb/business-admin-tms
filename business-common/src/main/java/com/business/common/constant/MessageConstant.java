package com.business.common.constant;

/**
 * 消息提示信息
 */
public class MessageConstant {

    /**
     *审批消息通知
     */
    public static final String APPROVAL_MSG = "您发起的{0}已{1}，点击查看详情";

    /**
     *参与消息通知
     */
    public static final String PARTICIPATE_MSG = "您参与的{0}已经{1}，点击查看详情";

    /**
     *评论消息通知
     */
    public static final String COMMENT_MSG = "{0}有新的评论，点击查看详情";

    /**
     *变更消息通知
     */
    public static final String CHANGE_MSG = "{0}有新的变更，点击查看详情";

    /**
     * 消息通知结果
     */

    public static final String APPROVAL_PASS = "审批通过";
    public static final String APPROVAL_REJECT = "拒绝";
    public static final String APPROVAL_WITHDRAW = "撤回";
    public static final String APPROVAL_CHANGE = "变更";


    private static final String APPROVAL_BUSINESS_TYPE = "投标申请";
}
