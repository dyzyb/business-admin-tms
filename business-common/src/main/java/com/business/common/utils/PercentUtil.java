package com.business.common.utils;

import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.text.NumberFormat;
import java.util.Map;

/**
 * 计算百分比
 */
@Component
public class PercentUtil {

    /**
     * 使用java.text.NumberFormat实现
     * @param x
     * @param y
     * @return
     */
    public static String getPercent(int x, int y) {
        double d1 = x * 1.0;
        double d2 = y * 1.0;
        NumberFormat percentInstance = NumberFormat.getPercentInstance();
        // 设置保留几位小数，这里设置的是保留两位小数
        percentInstance.setMinimumFractionDigits(2);
        return percentInstance.format(d1 / d2);
    }


}
