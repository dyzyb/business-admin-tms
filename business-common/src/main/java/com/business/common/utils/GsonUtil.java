package com.business.common.utils;

import com.business.common.constant.Constants;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.springframework.util.StringUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author: js
 * @Description: Gson 工具类
 * @date: 2022-12-7 13:29:18
 */
public class GsonUtil {

    //线程安全的
    private static final Gson GSON;
    private static final Gson GSON_NULL; // 不过滤空值
    static {
        GSON = new GsonBuilder().enableComplexMapKeySerialization() //当Map的key为复杂对象时,需要开启该方法
                .setDateFormat("yyyy-MM-dd HH:mm:ss")//序列化日期格式  "yyyy-MM-dd"
                .disableHtmlEscaping() //防止特殊字符出现乱码
                .setLongSerializationPolicy(LongSerializationPolicy.STRING)
                .create();
        GSON_NULL = new GsonBuilder().enableComplexMapKeySerialization() //当Map的key为复杂对象时,需要开启该方法
                .serializeNulls() //当字段值为空或null时，依然对该字段进行转换
                .setDateFormat("yyyy-MM-dd HH:mm:ss")//序列化日期格式  "yyyy-MM-dd"
                .disableHtmlEscaping() //防止特殊字符出现乱码
                .setLongSerializationPolicy(LongSerializationPolicy.STRING)
                .create();
    }

    //获取gson解析器
    public static Gson getGson() {
        return GSON;
    }

    //获取gson解析器 有空值 解析
    public static Gson getWriteNullGson() {
        return GSON_NULL;
    }


    /**
     * 根据对象返回json  过滤空值字段
     */
    public static String toJsonString(Object object) {
        return GSON.toJson(object);
    }

    /**
     * 根据对象返回json  过滤空值字段  value转化成String类型
     */
    public static String toJsonStringValue(Object object) {
        return new GsonBuilder().setLongSerializationPolicy(LongSerializationPolicy.STRING).create().toJson(object);
    }

    /**
     * 根据对象返回json  不过滤空值字段
     */
    public static String toJsonString(Object object, String nullValue) {
        if (Constants.WRITE_MAP_NULL_VALUE.equals(nullValue)){
            return GSON_NULL.toJson(object);
        }
        return GSON.toJson(object);
    }


    /**
     * 将字符串转化对象
     *
     * @param json     源字符串
     * @param classOfT 目标对象类型
     * @param <T>
     * @return
     */
    public static <T> T strToJavaBean(String json, Class<T> classOfT) {
        return GSON.fromJson(json, classOfT);
    }

    /**
     * 将json转化为对应的实体对象
     * new TypeToken<List<T>>() {}.getType()
     * new TypeToken<Map<String, T>>() {}.getType()
     * new TypeToken<List<Map<String, T>>>() {}.getType()
     */
    public static <T> T fromJson(String json, Type typeOfT) {
        return GSON.fromJson(json, typeOfT);
    }

    /**
     * 转成list
     * @param gsonString
     * @param cls
     * @return
     */
    public static <T> List<T> strToList(String gsonString, Class<T> cls) {
        List<T> list = new ArrayList<T>();
        JsonArray array = new JsonParser().parse(gsonString).getAsJsonArray();
        for (final JsonElement elem : array) {
            list.add(GSON.fromJson(elem, cls));
        }
        return list;
    }

    /**
     * 转成list中有map的
     * @param gsonString
     * @return
     */
    public static <T> List<Map<String, T>> strToListMaps(String gsonString) {
        return GSON.fromJson(gsonString, new TypeToken<List<Map<String, String>>>() {
        }.getType());
    }

    /**
     * 转成map
     * @param gsonString
     * @return
     */
    public static <T> Map<String, T> strToMaps(String gsonString) {
        return GSON.fromJson(gsonString, new TypeToken<Map<String, T>>() {
        }.getType());
    }

    /**
     * 转成long类型的list
     * @param gsonString
     * @param cls
     * @return
     */
    public static List<Long> strToLongList(String gsonString, Class<Long> cls) {
        if(!StringUtils.hasText(gsonString)){
            return null;
        }
        return strToList(gsonString, cls);
    }
}