
package com.business.common.core.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Component
public class GetRedisIncreID {

    @Autowired
    private RedisService redisService;

    public  String getId() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();
        String formatDate = sdf.format(date);
        String key = "key" + formatDate;

//        System.out.println(key);
        Long incr = getIncr(key, getCurrent2TodayEndMillisTime());

//        System.out.println(incr);
        if (incr == 0) {
            incr = getIncr(key, getCurrent2TodayEndMillisTime());//从001开始
        }
        DecimalFormat df = new DecimalFormat("000000");//三位序列号
//        System.out.println(formatDate + df.format(incr));
        return formatDate + df.format(incr);
    }

    public  Long getIncr(String key, long liveTime) {
        try {
            RedisAtomicLong entityIdCounter = new RedisAtomicLong(key, redisService.redisTemplate.getConnectionFactory());
            Long increment = entityIdCounter.getAndIncrement();

            if ((null == increment || increment.longValue() == 0) && liveTime > 0) {//初始设置过期时间
                entityIdCounter.expire(liveTime, TimeUnit.MILLISECONDS);//单位毫秒
            }
            return increment;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return Long.valueOf(0);
        }
    }

    //现在到今天结束的毫秒数
    public static Long getCurrent2TodayEndMillisTime() {
        Calendar todayEnd = Calendar.getInstance();
        // Calendar.HOUR 12小时制
        // HOUR_OF_DAY 24小时制
        todayEnd.set(Calendar.HOUR_OF_DAY, 23);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.MILLISECOND, 999);
        return todayEnd.getTimeInMillis() - new Date().getTime();
    }


}

