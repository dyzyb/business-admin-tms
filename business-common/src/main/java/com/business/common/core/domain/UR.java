package com.business.common.core.domain;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 统一结果返回类
 *
 */
@Data
public class UR {

    //@ApiModelProperty(value = "是否成功")
    private Boolean success;

    //@ApiModelProperty(value = "返回码")
    private Integer code;

    //@ApiModelProperty(value = "返回消息")
    private String message;

    //@ApiModelProperty(value = "用户ID")
    private String mid;

    //@ApiModelProperty(value = "返回数据")
    private Map<String,Object> data = new HashMap<>();

    //构造方法私有化
    private UR(){}

    //成功
    public static com.business.common.core.domain.UR ok(){
        com.business.common.core.domain.UR ur = new com.business.common.core.domain.UR();
        ur.setSuccess(true);
        ur.setCode(ResultCode.SUCCESS);
        ur.setMessage("成功");
        return ur;
    }

    //失败
    public static com.business.common.core.domain.UR error(){
        com.business.common.core.domain.UR ur = new com.business.common.core.domain.UR();
        ur.setSuccess(false);
        ur.setCode(ResultCode.ERROR);
        ur.setMessage("失败");
        return ur;
    }

    public com.business.common.core.domain.UR success(Boolean success){
        this.setSuccess(success);
        return this;
    }

    public com.business.common.core.domain.UR mid(String mid){
        this.setMid(mid);
        return this;
    }

    public com.business.common.core.domain.UR message(String message){
        this.setMessage(message);
        return this;
    }

    public com.business.common.core.domain.UR code(Integer code){
        this.setCode(code);
        return this;
    }

    public com.business.common.core.domain.UR data(String key, Object value){
        this.data.put(key,value);
        return this;
    }

    public com.business.common.core.domain.UR data(Map<String,Object>map){
        this.setData(map);
        return this;
    }

}
