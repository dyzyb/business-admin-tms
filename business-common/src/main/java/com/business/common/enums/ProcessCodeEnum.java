package com.business.common.enums;

/**
 * @Author: js
 * @Description: 流程枚举类
 * @date: 2022/12/8 13:33
 */
public enum ProcessCodeEnum {

    /**
     * 流程审批结果
     */
    PROCESS_APPROVE_RESULT_VERIFYING("processApproveResult:verifying", "审核中"),
    PROCESS_APPROVE_RESULT_PASS("processApproveResult:pass", "已通过"),
    PROCESS_APPROVE_RESULT_REJECT("processApproveResult:reject", "已拒绝"),
    PROCESS_APPROVE_RESULT_REFUSE("processApproveResult:refuse", "已驳回"),
    PROCESS_APPROVE_RESULT_WITHDRAW("processApproveResult:withdraw", "已撤回"),
    ;

    private String code;
    private String description;

    private ProcessCodeEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }

    public String toString(){
        return this.getCode();
    }
}
