package com.business.common.exception;

/**
 * @author js
 * @version 1.0
 * @date 2022/12/8
 * @since
 */
public class CoreExceptionMessage extends ExceptionMessage{

	public CoreExceptionMessage(String code, Object message) {
		super(code, message);
	}

	private static final long serialVersionUID = 8239091920220949891L;

	public static final String UNDEFINED_ERROR="COMMON.UNDEFINED_ERROR";


	public static com.business.common.exception.CoreExceptionMessage build(String code, Object message) {
		return new com.business.common.exception.CoreExceptionMessage(code,message);
	}

	public static com.business.common.exception.CoreExceptionMessage build(Object message) {
		return new com.business.common.exception.CoreExceptionMessage(UNDEFINED_ERROR,message);
	}

	public static com.business.common.exception.CoreExceptionMessage build(Throwable exception) {
		return new com.business.common.exception.CoreExceptionMessage(UNDEFINED_ERROR,exception.getMessage());
	}
}
