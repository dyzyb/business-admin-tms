package com.business.common.exception;

/**
 * @author js
 *
 * @Description: 异常提示信息
 * @version 1.0
 * @date 2022/12/8
 * @since
 */
public enum VerificationErrorCode implements ErrorCode {
    //前面二个字母代表工程简称，后面字母代码模块，数字前二位代码是哪个包，后面数字为自增序号，Message为在业务处理的时候可以andMessageIs
    VERIFICATION_TASK_VERIFY_ERROR("TASKVERIFY0001", "审批不通过，原因必填", "审批不通过，原因必填"),
    VERIFICATION_PROCESS_ERROR("CNPE0001","调用新增货源接口异常","调用新增货源接口异常"),
    ;

    private final String code;
    private final Object description;
    private Object message;

    private VerificationErrorCode(String code, Object message, Object description) {
        this.code = code;
        this.message=message;
        this.description = description;

    }

    public com.business.common.exception.VerificationErrorCode andMessageIs(Object message) {
        this.message = message;
        return this;
    }

    public String getCode() {
        return this.code;
    }

    public Object getDescription() {
        return this.description;
    }

    public Object getMessage() {
        return this.message;
    }

    public Object getMessageOrElseDescription() {
        return this.message != null && !"".equals(this.message) ? this.message : this.description;
    }
}

