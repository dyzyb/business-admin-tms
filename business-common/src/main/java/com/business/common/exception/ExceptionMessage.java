package com.business.common.exception;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author js
 * @version 1.0
 * @date 2022/12/8
 * @since
 */
@Getter
@Setter
public abstract class ExceptionMessage implements Serializable {

	private static final long serialVersionUID = -547904528497231913L;

	private String code;

	private Object message;

	public ExceptionMessage(String code, Object message) {
		this.code = code;
		this.message = message;
	}
}
