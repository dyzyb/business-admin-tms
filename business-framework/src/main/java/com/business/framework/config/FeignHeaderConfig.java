package com.business.framework.config;

import com.business.common.core.domain.model.LoginUser;
import com.business.framework.web.service.TokenService;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.apache.http.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

public class FeignHeaderConfig implements RequestInterceptor {

    @Autowired
    private TokenService tokenService;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        if(ObjectUtils.isEmpty(attributes)){
            return;
        }
        HttpServletRequest request = attributes.getRequest();
        LoginUser loginUser = tokenService.getLoginUser(request);
        String accessToken = loginUser.getToken();
        if (Objects.nonNull(accessToken)) {
            requestTemplate.header(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
            return;
        }


        if(StringUtils.hasText(request.getHeader(HttpHeaders.AUTHORIZATION))){
            requestTemplate.header(HttpHeaders.AUTHORIZATION, request.getHeader(HttpHeaders.AUTHORIZATION));
        }
    }
}
