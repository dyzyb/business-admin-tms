package com.business.system.util;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiPredicate;

/**
 * 对象值比较工具类
 */
public class CompareObjUtil {

    public static boolean compareObj(Object beforeObj, Object afterObj) throws Exception{
        boolean result = false;
        if(beforeObj == null) {
            throw new RuntimeException("原对象不能为空");
        }
        if(afterObj == null) {
            throw new RuntimeException("新对象不能为空");
        }
        if(!beforeObj.getClass().isAssignableFrom(afterObj.getClass())){
            throw new RuntimeException("两个对象不相同，无法比较");
        }
        //取出属性
        Field[] beforeFields = beforeObj.getClass().getDeclaredFields();
        Field[] afterFields = afterObj.getClass().getDeclaredFields();
        Field.setAccessible(beforeFields, true);
        Field.setAccessible(afterFields, true);

        //遍历取出差异值
        if(beforeFields != null && beforeFields.length > 0){
            for(int i=0; i<beforeFields.length; i++){
                Object beforeValue = beforeFields[i].get(beforeObj);
                Object afterValue = afterFields[i].get(afterObj);
                if((beforeValue != null && !"".equals(beforeValue) && !beforeValue.equals(afterValue)) || ((beforeValue == null || "".equals(beforeValue)) && afterValue != null)){
                    result = true;
                    break;
                }
            }
        }

        return result;
    }

    /**
     * 判断两个对象是否有属性变更了
     * @param obj1
     * @param obj2
     * @param ignoreFields
     * @return
     * @throws IllegalAccessException
     */
    public static boolean isCompareTwoObject(Object obj1, Object obj2, String[] ignoreFields) throws IllegalAccessException {
        boolean result = false;
        List<String> ignoreFieldList = Arrays.asList(ignoreFields);
        Class<?> clazz1 = obj1.getClass();
        Class<?> clazz2 = obj2.getClass();
        Field[] fields1 = clazz1.getDeclaredFields();
        Field[] fields2 = clazz2.getDeclaredFields();
        BiPredicate biPredicate = new BiPredicate() {
            @Override
            public boolean test(Object object1, Object object2) {
                if (object1 == null && object2 == null) {
                    return true;
                }
                if (object1 == null && object2 != null){
                    return false;
                }
                // 假如还有别的类型需要特殊判断 比如 BigDecimal, 演示，只写BigDecimal示例，其他都相似
                if (object1 instanceof BigDecimal && object2 instanceof BigDecimal) {
                    if (object1 == null && object2 == null) {
                        return true;
                    }
                    if (object1 == null ^ object2 == null) {
                        return false;
                    }
                    return ((BigDecimal) object1).compareTo((BigDecimal) object2) == 0;
                }

                if (object1.equals(object2)) {
                    return true;
                }
                return false;
            }
        };

        for (Field field1 : fields1) {
            for (Field field2 : fields2) {
                if (ignoreFieldList.contains(field1.getName()) || ignoreFieldList.contains(field2.getName())) {
                    continue;
                }
                if (field1.getName().equals(field2.getName())) {
                    field1.setAccessible(true);
                    field2.setAccessible(true);
                    if (!biPredicate.test(field1.get(obj1), field2.get(obj2))) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    /**
     * 判断对象自定义属性是否变更了，返回变更字段
     * @param obj1
     * @param obj2
     * @param ignoreFields
     * @return
     * @throws IllegalAccessException
     */
    public static Map<String, String> compareTwoObject(Object obj1, Object obj2, String[] ignoreFields) throws IllegalAccessException {
        Map<String, String> diffMap = new LinkedHashMap<>();
        List<String> ignoreFieldList = Arrays.asList(ignoreFields);
        Class<?> clazz1 = obj1.getClass();
        Class<?> clazz2 = obj2.getClass();
        Field[] fields1 = clazz1.getDeclaredFields();
        Field[] fields2 = clazz2.getDeclaredFields();
        BiPredicate biPredicate = new BiPredicate() {
            @Override
            public boolean test(Object object1, Object object2) {
                if (object1 == null && object2 == null) {
                    return true;
                }
                if (object1 == null && object2 != null){
                    return false;
                }
                // 假如还有别的类型需要特殊判断 比如 BigDecimal, 演示，只写BigDecimal示例，其他都相似
                if (object1 instanceof BigDecimal && object2 instanceof BigDecimal) {
                    if (object1 == null && object2 == null) {
                        return true;
                    }
                    if (object1 == null ^ object2 == null) {
                        return false;
                    }
                    return ((BigDecimal) object1).compareTo((BigDecimal) object2) == 0;
                }

                if (object1.equals(object2)) {
                    return true;
                }
                return false;
            }
        };

        for (Field field1 : fields1) {
            for (Field field2 : fields2) {
                if (ignoreFieldList.contains(field1.getName()) || ignoreFieldList.contains(field2.getName())) {
                    continue;
                }
                if (field1.getName().equals(field2.getName())) {
                    field1.setAccessible(true);
                    field2.setAccessible(true);
                    if (!biPredicate.test(field1.get(obj1), field2.get(obj2))) {
                        diffMap.put("compare_object: ", obj1 + " vs " + obj2);
                        diffMap.put(obj1 + "_field:" + field1.getName(), field1.get(obj1).toString());
                        diffMap.put(obj2 + "_field:" + field2.getName(), field2.get(obj2).toString());
                    }
                }
            }
        }
        return diffMap;
    }
}
