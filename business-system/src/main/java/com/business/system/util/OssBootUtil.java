package com.business.system.util;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.OSSObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.List;
import java.util.zip.Adler32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Slf4j
@Component
@RefreshScope
public class OssBootUtil {

    //金谷阿里云
    String endpoint = "oss-cn-huhehaote.aliyuncs.com";
    String accessKeyId = "LTAI5t6vud5rdp72ycv1hUgd";
    String accessKeySecret = "GDXnZk5RAw6YbodSaHUqeSZBKx0Fhh";
    String bucketName = "zhongnengxinlian";


    /**
     * @param zipName 压缩文件名称
     * @param paths oss文件列表
     * @return
     */
    public void getZipFromOSSByPaths(String zipName, List<String> paths) {
        //激活oss服务端
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        FileOutputStream fos = null;
        // 压缩文件输出流
        ZipOutputStream zos = null;
        try {
            // 创建zip文件
            fos = new FileOutputStream(zipName);
            // 作用是为任何OutputStream产生校验和
            // 第一个参数是制定产生校验和的输出流，第二个参数是指定Checksum的类型 （Adler32（较快）和CRC32两种）
            CheckedOutputStream cos = new CheckedOutputStream(fos, new Adler32());
            // 用于将数据压缩成zip文件格式
            zos = new ZipOutputStream(cos);

            OSSObject ossObject = null;
            InputStream is = null;
            // 循环根据路径从OSS获得对象，存入临时文件zip中
            for (String path : paths) {
                try {
                    log.info("下载文件路径 {}", path);
                    // 根据path 和 bucket 从OSS获取对象
                    ossObject = ossClient.getObject(bucketName, path);
                    // 获取输出流
                    is = ossObject.getObjectContent();
                    // 将文件放入zip中，并命名不能重复
                    zos.putNextEntry(new ZipEntry(path));
                    // 向压缩文件中写数据
                    int len = 0;
                    while ((len = is.read()) != -1) {
                        zos.write(len);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    log.info("未找到文件 {}", path);
                    continue;
                } finally {
                    is.close();
                    zos.closeEntry();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                // 关闭流
                zos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
