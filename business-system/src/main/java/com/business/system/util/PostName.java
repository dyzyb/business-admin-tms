package com.business.system.util;


import com.business.system.domain.SysPost;
import com.business.system.mapper.SysPostMapper;
import com.business.system.service.ISysPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PostName {

    @Autowired
    ISysPostService sysPostService;

    public String getName(Long userId){
        //获取岗位信息
        List<Long> longs = sysPostService.selectPostListByUserId(userId);
        String postName = "";
        for (Long postId:longs) {
            SysPost sysPost = sysPostService.selectPostById(postId);
            postName = sysPost.getPostName() + "-" + postName;
        }
        return postName;
    }
}
