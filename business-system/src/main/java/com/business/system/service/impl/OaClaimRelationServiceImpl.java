package com.business.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.utils.DateUtils;
import com.business.system.domain.OaClaimRelation;
import com.business.system.domain.vo.OaClaimStatVO;
import com.business.system.mapper.OaClaimRelationMapper;
import com.business.system.service.IOaClaimRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 报销关联流程Service业务层处理
 *
 * @author single
 * @date 2023-12-13
 */
@Service
public class OaClaimRelationServiceImpl extends ServiceImpl<OaClaimRelationMapper, OaClaimRelation> implements IOaClaimRelationService
{
    @Autowired
    private OaClaimRelationMapper oaClaimRelationMapper;

    /**
     * 查询报销关联流程
     *
     * @param id 报销关联流程ID
     * @return 报销关联流程
     */
    @Override
    public OaClaimRelation selectOaClaimRelationById(String id)
    {
        return oaClaimRelationMapper.selectOaClaimRelationById(id);
    }

    /**
     * 查询报销关联流程列表
     *
     * @param oaClaimRelation 报销关联流程
     * @return 报销关联流程
     */
    @Override
    public List<OaClaimRelation> selectOaClaimRelationList(OaClaimRelation oaClaimRelation)
    {
        return oaClaimRelationMapper.selectOaClaimRelationList(oaClaimRelation);
    }

    /**
     * 新增报销关联流程
     *
     * @param oaClaimRelation 报销关联流程
     * @return 结果
     */
    @Override
    public int insertOaClaimRelation(OaClaimRelation oaClaimRelation)
    {
        oaClaimRelation.setCreateTime(DateUtils.getNowDate());
        return oaClaimRelationMapper.insertOaClaimRelation(oaClaimRelation);
    }

    /**
     * 修改报销关联流程
     *
     * @param oaClaimRelation 报销关联流程
     * @return 结果
     */
    @Override
    public int updateOaClaimRelation(OaClaimRelation oaClaimRelation)
    {
        oaClaimRelation.setUpdateTime(DateUtils.getNowDate());
        return oaClaimRelationMapper.updateOaClaimRelation(oaClaimRelation);
    }

    /**
     * 批量删除报销关联流程
     *
     * @param ids 需要删除的报销关联流程ID
     * @return 结果
     */
    @Override
    public int deleteOaClaimRelationByIds(String[] ids)
    {
        return oaClaimRelationMapper.deleteOaClaimRelationByIds(ids);
    }

    /**
     * 删除报销关联流程信息
     *
     * @param id 报销关联流程ID
     * @return 结果
     */
    @Override
    public int deleteOaClaimRelationById(String id)
    {
        return oaClaimRelationMapper.deleteOaClaimRelationById(id);
    }

    @Override
    public OaClaimStatVO sumOaClaimRelation(OaClaimRelation oaClaimRelation) {
        OaClaimStatVO oaClaimStatVO = oaClaimRelationMapper.sumOaClaimRelation(oaClaimRelation);
        return oaClaimStatVO;
    }
}
