package com.business.system.service;

import java.util.List;
import java.util.Map;

import com.business.common.core.domain.UR;
import com.business.system.domain.OaClock;
import com.business.system.domain.dto.OaClockDTO;
import com.business.system.domain.dto.UserClockDTO;

/**
 * 打卡Service接口
 *
 * @author single
 * @date 2023-11-27
 */
public interface IOaClockService
{
    /**
     * 查询打卡
     *
     * @param id 打卡ID
     * @return 打卡
     */
    public OaClock selectOaClockById(String id);

    /**
     * 查询打卡列表
     *
     * @param oaClock 打卡
     * @return 打卡集合
     */
    public List<OaClock> selectOaClockList(OaClock oaClock);

    /**
     * 新增打卡
     *
     * @param oaClock 打卡
     * @return 结果
     */
    public UR insertOaClock(OaClock oaClock);

    /**
     * 修改打卡
     *
     * @param oaClock 打卡
     * @return 结果
     */
    public int updateOaClock(OaClock oaClock);

    /**
     * 批量删除打卡
     *
     * @param ids 需要删除的打卡ID
     * @return 结果
     */
    public int deleteOaClockByIds(String[] ids);

    /**
     * 删除打卡信息
     *
     * @param id 打卡ID
     * @return 结果
     */
    public int deleteOaClockById(String id);

    /**
     * APP打卡统计
     */
    public Map statClock(OaClockDTO oaClockDTO);

    /**
     * APP打卡查询一天打卡数据
     */
    public List<OaClock> statOaClockList(OaClockDTO oaClockDTO);

    public String initClockData();

    /**
     * 以打卡在状态查询每一天数据
     */
    public Map<String,Integer> dayData(OaClock oaClock);

    /**
     * 每一天数据列表
     */
    public List<UserClockDTO> dayDataClock(OaClock oaClock);

    /**
     * 每周数据统计
     */
    public UserClockDTO weeklyData(OaClock oaClock);

    /**
     * 每周打卡数据列表
     */
    public List<UserClockDTO> weeklyDataClock(OaClock oaClock);
}
