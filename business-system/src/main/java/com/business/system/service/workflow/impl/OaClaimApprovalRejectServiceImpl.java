package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.OaClaim;
import com.business.system.domain.OaOther;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IOaClaimApprovalService;
import com.business.system.service.workflow.IOaOtherApprovalService;
import org.springframework.stereotype.Service;


/**
 * 报销申请审批结果:审批拒绝
 */
@Service("oaClaimApprovalContent:processApproveResult:reject")
public class OaClaimApprovalRejectServiceImpl extends IOaClaimApprovalService implements IWorkflowUpdateStatusService {

    @Override
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        this.checkValidate(approvalWorkflowDTO);
        return this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.OACLAIM_STATUS_REJECT.getCode());
    }

    /**
     * 验证数据的有效性
     */
    @Override
    protected OaClaim checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        return super.checkValidate(approvalWorkflowDTO);
    }
}
