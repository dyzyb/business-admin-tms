package com.business.system.service;

import java.util.List;

import com.business.system.domain.BaMessage;

/**
 * 系统消息记录Service接口
 *
 * @author single
 * @date 2023-01-10
 */
public interface IBaMessageService
{
    /**
     * 查询系统消息记录
     *
     * @param msId 系统消息记录ID
     * @return 系统消息记录
     */
    public BaMessage selectBaMessageById(String msId);

    /**
     * 查询系统消息记录列表
     *
     * @param baMessage 系统消息记录
     * @return 系统消息记录集合
     */
    public List<BaMessage> selectBaMessageList(BaMessage baMessage);

    /**
     * 新增系统消息记录
     *
     * @param baMessage 系统消息记录
     * @return 结果
     */
    public int insertBaMessage(BaMessage baMessage);

    /**
     * 修改系统消息记录
     *
     * @param baMessage 系统消息记录
     * @return 结果
     */
    public int updateBaMessage(BaMessage baMessage);

    /**
     * 批量删除系统消息记录
     *
     * @param msIds 需要删除的系统消息记录ID
     * @return 结果
     */
    public int deleteBaMessageByIds(String[] msIds);

    /**
     * 删除系统消息记录信息
     *
     * @param msId 系统消息记录ID
     * @return 结果
     */
    public int deleteBaMessageById(String msId);

    /**
     * 清空
     * @param
     * @return
     */
    public int empty();

    /**
     * 已读
     * @param
     * @return
     */
    public int read();


    public BaMessage selectBaMessage();
}
