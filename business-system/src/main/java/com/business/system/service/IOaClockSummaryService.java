package com.business.system.service;

import java.util.List;

import com.business.system.domain.OaClockSummary;
import com.business.system.domain.dto.DayClockListDTO;

/**
 * 打卡汇总Service接口
 *
 * @author single
 * @date 2023-12-11
 */
public interface IOaClockSummaryService
{
    /**
     * 查询打卡汇总
     *
     * @param id 打卡汇总ID
     * @return 打卡汇总
     */
    public OaClockSummary selectOaClockSummaryById(String id);

    /**
     * 查询打卡汇总列表
     *
     * @param oaClockSummary 打卡汇总
     * @return 打卡汇总集合
     */
    public List<OaClockSummary> selectOaClockSummaryList(OaClockSummary oaClockSummary);

    /**
     * 新增打卡汇总
     *
     * @param oaClockSummary 打卡汇总
     * @return 结果
     */
    public int insertOaClockSummary(OaClockSummary oaClockSummary);

    /**
     * 修改打卡汇总
     *
     * @param oaClockSummary 打卡汇总
     * @return 结果
     */
    public int updateOaClockSummary(OaClockSummary oaClockSummary);

    /**
     * 批量删除打卡汇总
     *
     * @param ids 需要删除的打卡汇总ID
     * @return 结果
     */
    public int deleteOaClockSummaryByIds(String[] ids);

    /**
     * 删除打卡汇总信息
     *
     * @param id 打卡汇总ID
     * @return 结果
     */
    public int deleteOaClockSummaryById(String id);

    /**
     * 获取当月打卡数据统计
     */
    public int sameMonth();

    /**
     * 打卡日统计
     */
    public List<DayClockListDTO> dailyStatistics(DayClockListDTO dayClockListDTO);

}
