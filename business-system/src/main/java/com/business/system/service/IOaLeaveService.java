package com.business.system.service;

import java.util.List;
import com.business.system.domain.OaLeave;

/**
 * 请假申请Service接口
 *
 * @author ljb
 * @date 2023-11-11
 */
public interface IOaLeaveService
{
    /**
     * 查询请假申请
     *
     * @param id 请假申请ID
     * @return 请假申请
     */
    public OaLeave selectOaLeaveById(String id);

    /**
     * 查询请假申请列表
     *
     * @param oaLeave 请假申请
     * @return 请假申请集合
     */
    public List<OaLeave> selectOaLeaveList(OaLeave oaLeave);

    /**
     * 新增请假申请
     *
     * @param oaLeave 请假申请
     * @return 结果
     */
    public int insertOaLeave(OaLeave oaLeave);

    /**
     * 修改请假申请
     *
     * @param oaLeave 请假申请
     * @return 结果
     */
    public int updateOaLeave(OaLeave oaLeave);

    /**
     * 批量删除请假申请
     *
     * @param ids 需要删除的请假申请ID
     * @return 结果
     */
    public int deleteOaLeaveByIds(String[] ids);

    /**
     * 删除请假申请信息
     *
     * @param id 请假申请ID
     * @return 结果
     */
    public int deleteOaLeaveById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);


}
