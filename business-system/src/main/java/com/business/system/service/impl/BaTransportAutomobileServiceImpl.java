package com.business.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.*;
import com.business.system.domain.*;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.dto.ProcessInstancesRuntimeDTO;
import com.business.system.domain.dto.StartProcessInstanceDTO;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaProcessBusinessInstanceRelatedService;
import com.business.system.service.IBaProcessDefinitionRelatedService;
import com.business.system.service.IBaTransportAutomobileService;
import com.business.system.service.ISysUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 汽车运输/货源Service业务层处理
 *
 * @author ljb
 * @date 2023-02-17
 */
@Service
public class BaTransportAutomobileServiceImpl extends ServiceImpl<BaTransportAutomobileMapper, BaTransportAutomobile> implements IBaTransportAutomobileService
{
    private static final Logger log = LoggerFactory.getLogger(BaTransportAutomobileServiceImpl.class);
    @Autowired
    private BaTransportAutomobileMapper baTransportAutomobileMapper;
    @Autowired
    private GetRedisIncreID getRedisIncreID;
    /*@Autowired
    private BaRailwayPlatformMapper baRailwayPlatformMapper;*/
    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;
    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;
    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;
    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private ISysUserService iSysUserService;
    /*@Autowired
    private BaOrderMapper baOrderMapper;*/
    @Autowired
    private BaMessageMapper baMessageMapper;
    @Autowired
    private BaAutomobuleStandardMapper baAutomobuleStandardMapper;
   /* @Autowired
    private BaCheckMapper baCheckMapper;*/
    @Autowired
    private BaShippingOrderMapper baShippingOrderMapper;
    /*@Autowired
    private BaSettlementFreightMapper baSettlementFreightMapper;
    @Autowired
    private BaContractStartMapper baContractStartMapper;*/
    @Autowired
    private DistrictsMapper districtsMapper;
    /*@Autowired
    private BaCompanyMapper baCompanyMapper;*/
    @Autowired
    private BaCounterpartMapper baCounterpartMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private RestTemplateUtil restTemplateUtil;

    /*@Autowired
    private IBaTransportCmstService baTransportCmstService;*/

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

   /* @Autowired
    private BaShippingOrderCmstMapper baShippingOrderCmstMapper;

    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;

    @Autowired
    private BaDepotMapper baDepotMap;

    @Autowired
    private BaDepotLocationMapper baDepotLocationMapper;*/

    /**
     * 查询汽车运输/货源
     *
     * @param id 汽车运输/货源ID
     * @return 汽车运输/货源
     */
    @Override
    public BaTransportAutomobile selectBaTransportAutomobileById(String id)
    {
        BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(id);
        //费用信息
        QueryWrapper<BaAutomobuleStandard> standardQueryWrapper = new QueryWrapper<>();
        standardQueryWrapper.eq("relation_id",baTransportAutomobile.getId());
        List<BaAutomobuleStandard> baAutomobuleStandards = baAutomobuleStandardMapper.selectList(standardQueryWrapper);
        if(baAutomobuleStandards.size() > 0){
            baTransportAutomobile.setRatesItemList(baAutomobuleStandards);
        }
        //发货人
        /*if(StringUtils.isNotEmpty(baTransportAutomobile.getDepName())){
            baTransportAutomobile.setDepName(baCounterpartMapper.selectBaCounterpartById(baTransportAutomobile.getDepName()).getName());
        }*/
        //收货人
        /*if(StringUtils.isNotEmpty(baTransportAutomobile.getDesName())){
            baTransportAutomobile.setDesName(baCounterpartMapper.selectBaCounterpartById(baTransportAutomobile.getDesName()).getName());
        }*/
        //运输托运单位名称
        /*if(StringUtils.isNotEmpty(baTransportAutomobile.getCompanyId())){
           baTransportAutomobile.setCompanyName(baCompanyMapper.selectBaCompanyById(baTransportAutomobile.getCompanyId()).getName());
        }*/
        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("business_id",baTransportAutomobile.getId());
        queryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.TRANSPORT_STATUS_VERIFYING.getCode());
        queryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        //订单编号
       /* if(StringUtils.isNotEmpty(baTransportAutomobile.getOrderId())){
            *//*BaOrder baOrder = baOrderMapper.selectBaOrderById(baTransportAutomobile.getOrderId());
            baTransportAutomobile.setOrderName(baOrder.getOrderName());*//*
            BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(baTransportAutomobile.getOrderId());
            baTransportAutomobile.setOrderName(baContractStart.getName());
        }*/
        baTransportAutomobile.setProcessInstanceId(processInstanceIds);
        //发起人
        baTransportAutomobile.setUser(sysUserMapper.selectUserById(baTransportAutomobile.getUserId()));
        //运单
        QueryWrapper<BaShippingOrder> baShippingOrderQueryWrapper = new QueryWrapper<>();
        baShippingOrderQueryWrapper.eq("supplier_no",baTransportAutomobile.getId());
        List<BaShippingOrder> baShippingOrders = baShippingOrderMapper.selectList(baShippingOrderQueryWrapper);
        /*if(baShippingOrders.size() > 0){
            for (BaShippingOrder shippingOrder:baShippingOrders) {
                //仓库名称
                if(StringUtils.isNotEmpty(shippingOrder.getDepotId())){
                    BaDepot baDepot = baDepotMap.selectBaDepotById(shippingOrder.getDepotId());
                    shippingOrder.setDepotName(baDepot.getName());
                }
                //库位名称
                if(StringUtils.isNotEmpty(shippingOrder.getPositionId())){
                    BaDepotLocation baDepotLocation = baDepotLocationMapper.selectBaDepotLocationById(shippingOrder.getPositionId());
                    shippingOrder.setPositionName(baDepotLocation.getName());
                }
            }
        }*/
        baTransportAutomobile.setShippingOrdeList(baShippingOrders);
        //发货信息
        /*QueryWrapper<BaCheck> checkQueryWrapper = new QueryWrapper<>();
        checkQueryWrapper.eq("relation_id",baTransportAutomobile.getId());
        checkQueryWrapper.eq("type","1");
        BaCheck baCheck = baCheckMapper.selectOne(checkQueryWrapper);
        baTransportAutomobile.setDelivery(baCheck);
        //验收信息
            QueryWrapper<BaCheck> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("relation_id",baTransportAutomobile.getId());
            queryWrapper1.eq("type","2");
            BaCheck baCheck1 = baCheckMapper.selectOne(queryWrapper1);
            baTransportAutomobile.setReceipt(baCheck1);*/
        return baTransportAutomobile;
    }

    /**
     * 查询汽车运输/货源列表
     *
     * @param baTransportAutomobile 汽车运输/货源
     * @return 汽车运输/货源
     */
    @Override
    public List<BaTransportAutomobile> selectBaTransportAutomobileList(BaTransportAutomobile baTransportAutomobile)
    {
        List<BaTransportAutomobile> baTransportAutomobiles = baTransportAutomobileMapper.baTransportAutomobileList(baTransportAutomobile);
        if(baTransportAutomobiles.size() > 0){
            baTransportAutomobiles.forEach(item ->{
                //合同启动名称
               /* if(StringUtils.isNotEmpty(item.getOrderId())){
                    BaContractStart baContractStart = baContractStartMapper.selectBaContractStartById(item.getOrderId());
                    item.setOrderName(baContractStart.getName());
                }*/
                  //发货省
                  if(StringUtils.isNotEmpty(item.getFhs())){
                      Districts districts = districtsMapper.selectDistrictsById(Integer.parseInt(item.getFhs()));
                      item.setFhs(districts.getExtName());
                  }
                  //发货市
                if(StringUtils.isNotEmpty(item.getFhcity())){
                    Districts districts = districtsMapper.selectDistrictsById(Integer.parseInt(item.getFhcity()));
                    item.setFhcity(districts.getExtName());
                }
                  //发货区
                if(StringUtils.isNotEmpty(item.getFhqx())){
                    Districts districts = districtsMapper.selectDistrictsById(Integer.parseInt(item.getFhqx()));
                    item.setFhqx(districts.getExtName());
                }
                  //收货省、市、区
                if(StringUtils.isNotEmpty(item.getShs())){
                    Districts districts = districtsMapper.selectDistrictsById(Integer.parseInt(item.getShs()));
                    item.setShs(districts.getExtName());
                }
                if(StringUtils.isNotEmpty(item.getShcity())){
                    Districts districts = districtsMapper.selectDistrictsById(Integer.parseInt(item.getShcity()));
                    item.setShcity(districts.getExtName());
                }
                if(StringUtils.isNotEmpty(item.getShqx())){
                    Districts districts = districtsMapper.selectDistrictsById(Integer.parseInt(item.getShqx()));
                    item.setShqx(districts.getExtName());
                }
                  //发起人
 //               item.setUserName(sysUserMapper.selectUserById(item.getUserId()).getNickName());
                  //矿发量
               /* QueryWrapper<BaCheck> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("relation_id",item.getId());
                queryWrapper.eq("type","1");
                BaCheck baCheck = baCheckMapper.selectOne(queryWrapper);
                item.setDelivery(baCheck);*/
                //查询货源下所有运单
                QueryWrapper<BaShippingOrder> shippingOrderQueryWrapper = new QueryWrapper<>();
                shippingOrderQueryWrapper.eq("supplier_no",item.getSupplierNo());
                List<BaShippingOrder> baShippingOrders = baShippingOrderMapper.selectList(shippingOrderQueryWrapper);
                BigDecimal sale = new BigDecimal(0);
                for (BaShippingOrder baShippingOrder:baShippingOrders) {
                    if(baShippingOrder.getCcjweight() != null){
                        sale = sale.add(baShippingOrder.getCcjweight());
                    }
                }
                item.setCarNum(baShippingOrders.size());
                //实际发运量
                item.setSaleNum(sale);
                //货主名称
                if(StringUtils.isNotNull(baShippingOrders)&&baShippingOrders.size()>0){
                    item.setConsignorUserName(baShippingOrders.get(0).getConsignorUserName());
                }
                /*//是否全部签收
                if(baShippingOrders.size() > 0){
                    item.setSign(false);
                }else {
                    item.setSign(true);
                }*/
            });
        }
        return baTransportAutomobiles;
    }

    /*@Override
    public List<sourceGoodsDTO> sourceGoodsDTOList(BaTransportAutomobile baTransportAutomobile) {
        List<sourceGoodsDTO> sourceGoodsDTOS = baTransportAutomobileMapper.sourceGoodsDTOList(baTransportAutomobile);
        if(sourceGoodsDTOS.size() > 0){
            sourceGoodsDTOS.stream().forEach(item ->{
                if("1".equals(item.getType()) || "3".equals(item.getType())){
                    //矿发量
                    QueryWrapper<BaCheck> queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq("relation_id",item.getId());
                    queryWrapper.eq("type","1");
                    BaCheck baCheck = baCheckMapper.selectOne(queryWrapper);
                    item.setDelivery(baCheck);
                    //查询货源下所有运单
                    QueryWrapper<BaShippingOrder> shippingOrderQueryWrapper = new QueryWrapper<>();
                    shippingOrderQueryWrapper.eq("supplier_no",item.getSupplierNo());
                    List<BaShippingOrder> baShippingOrders = baShippingOrderMapper.selectList(shippingOrderQueryWrapper);
                    BigDecimal sale = new BigDecimal(0);
                    for (BaShippingOrder baShippingOrder:baShippingOrders) {
                        if(baShippingOrder.getCcjweight() != null){
                            sale = sale.add(baShippingOrder.getCcjweight());
                        }else {
                            sale = sale.add(new BigDecimal(0));
                        }
                    }
                    item.setCarNum(baShippingOrders.size());
                    //实际发运量
                    item.setSaleNum(sale);
                }
                if("2".equals(item.getType())){
                    BaShippingOrderCmst baShippingOrderCmst1 = new BaShippingOrderCmst();
                    baShippingOrderCmst1.setYardId(item.getSupplierNo());
                    List<BaShippingOrderCmst> baShippingOrderCmsts = baShippingOrderCmstMapper.selectBaShippingOrderCmstList(baShippingOrderCmst1);
                    //车数
                    item.setCarNum(baShippingOrderCmsts.size());
                    //实际发运量
                    BigDecimal saleNum = new BigDecimal(0);
                    for (BaShippingOrderCmst baShippingOrderCmst:baShippingOrderCmsts) {
                        if(baShippingOrderCmst.getTonnage() != null){
                            saleNum = saleNum.add(baShippingOrderCmst.getConfirmedDeliveryWeight());
                        }
                    }
                    item.setSaleNum(saleNum);
                    if(baShippingOrderCmsts != null && baShippingOrderCmsts.size() > 0){
                        //货主名称
                        item.setSupplierName(baShippingOrderCmsts.get(0).getConsignorUserName());
                    }
                }
                //终端信息
                if(StringUtils.isNotEmpty(item.getEnterprise())){
                    BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(item.getEnterprise());
                    if(StringUtils.isNotNull(enterprise)){
                        item.setEnterpriseName(enterprise.getName());
                    }
                }
            });
        }
        return sourceGoodsDTOS;
    }*/

    /**
     * 新增汽车运输/货源
     *
     * @param baTransportAutomobile 汽车运输/货源
     * @return 结果
     */
    @Override
    public int insertBaTransportAutomobile(BaTransportAutomobile baTransportAutomobile)
    {
        //当前时间转时间戳
        Long id = DateUtils.getNowDate().getTime();
        baTransportAutomobile.setId("XJ"+String.valueOf(id).substring(0,13));
        //新增货源编号
        baTransportAutomobile.setSupplierNo(baTransportAutomobile.getId());
        baTransportAutomobile.setCreateTime(DateUtils.getNowDate());
        baTransportAutomobile.setUserId(SecurityUtils.getUserId());
        baTransportAutomobile.setDeptId(SecurityUtils.getDeptId());
        baTransportAutomobile.setCreateBy(SecurityUtils.getUsername());
        baTransportAutomobile.setConfirmStatus("1");
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_PRODUCT_AUTOMOBILE.getCode(), SecurityUtils.getCurrComId());
        baTransportAutomobile.setFlowId(flowId);
        Integer result = baTransportAutomobileMapper.insertBaTransportAutomobile(baTransportAutomobile);
        if(result > 0){
            //新增货源中运价
            if(baTransportAutomobile.getRatesItemList().size() > 0){
                baTransportAutomobile.getRatesItemList().forEach(item ->{
                    item.setId(getRedisIncreID.getId());
                    item.setRelationId(baTransportAutomobile.getId());
                    baAutomobuleStandardMapper.insertBaAutomobuleStandard(item);
                });
            }
            BaTransportAutomobile transportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baTransportAutomobile.getId());
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(transportAutomobile, AdminCodeEnum.AUTOMOBILE_APPROVAL_CONTENT_INSERT.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                return 0;
            }else {
                transportAutomobile.setState(AdminCodeEnum.AUTOMOBILE_STATUS_VERIFYING.getCode());
                baTransportAutomobileMapper.updateBaTransportAutomobile(transportAutomobile);
                /*if (StringUtils.isNotEmpty(transportAutomobile.getOrderId())) {
                    BaOrder baOrder = baOrderMapper.selectBaOrderById(transportAutomobile.getOrderId());
                    baOrder.setTransportState(1);
                    baOrderMapper.updateBaOrder(baOrder);
                }*/
            }
        }
        return result;
    }

    @Override
    public int insertAutomobile(JSONObject transportAutomobile) {

        JSONArray ratesItemList = transportAutomobile.getJSONArray("ratesItemList");
        Object o = ratesItemList.get(0);
        JSONObject obj = (JSONObject) JSON.toJSON(o);
        String precision = obj.getString("precision");
        BaTransportAutomobile baTransportAutomobile = JSONObject.toJavaObject((JSONObject) JSON.toJSON(transportAutomobile), BaTransportAutomobile.class);

        //以货源编号为主键
        baTransportAutomobile.setId(baTransportAutomobile.getSupplierNo());
        baTransportAutomobile.setCreateTime(DateUtils.getNowDate());
        baTransportAutomobile.setType("1");
        //新增货源中运价
        if(baTransportAutomobile.getRatesItemList().size() > 0){
            baTransportAutomobile.getRatesItemList().forEach(item ->{
                item.setId(getRedisIncreID.getId());
                item.setRelationId(baTransportAutomobile.getId());
                item.setIsShowPrice(baTransportAutomobile.getIsShowPrice());
                item.setPrecisions(precision);
                baAutomobuleStandardMapper.insertBaAutomobuleStandard(item);
            });
        }
        return baTransportAutomobileMapper.insertBaTransportAutomobile(baTransportAutomobile);
    }

    /**
     * 提交运输审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(BaTransportAutomobile transport, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(transport.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_PRODUCT_AUTOMOBILE.getCode(), AdminCodeEnum.TASK_TYPE_PRODUCT_AUTOMOBILE.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(transport.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_AUTOMOBILE.getCode());
        //获取流程实例关联的业务对象
        BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(transport.getId());
        SysUser sysUser = iSysUserService.selectUserById(baTransportAutomobile.getUserId());
        baTransportAutomobile.setUserName(sysUser.getUserName());
        baTransportAutomobile.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(GsonUtil.toJsonStringValue(baTransportAutomobile));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }

    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.AUTOMOBILE_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }
    /**
     * 修改汽车运输/货源
     *
     * @param baTransportAutomobile 汽车运输/货源
     * @return 结果
     */
    @Override
    public int updateBaTransportAutomobile(BaTransportAutomobile baTransportAutomobile)
    {
        baTransportAutomobile.setUpdateTime(DateUtils.getNowDate());
        baTransportAutomobile.setUpdateBy(SecurityUtils.getUsername());
        //修改费用信息
        if(baTransportAutomobile.getRatesItemList().size() > 0){
            for (BaAutomobuleStandard baAutomobuleStandard:baTransportAutomobile.getRatesItemList()) {
                baAutomobuleStandardMapper.updateBaAutomobuleStandard(baAutomobuleStandard);
            }
        }
        if(baTransportAutomobile.getShippingOrdeList().size() > 0){
            //判断运单是否存在
            for (BaShippingOrder baShippingOrder:baTransportAutomobile.getShippingOrdeList()) {
                QueryWrapper<BaShippingOrder> orderQueryWrapper = new QueryWrapper<>();
                orderQueryWrapper.eq("id",baShippingOrder.getThdno());
                orderQueryWrapper.eq("supplier_no",baTransportAutomobile.getId());
                BaShippingOrder baShippingOrder1 = baShippingOrderMapper.selectOne(orderQueryWrapper);
                if(StringUtils.isNotNull(baShippingOrder1)){
                    baShippingOrder.setId(baShippingOrder.getThdno());
                    baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                }else {
                    orderQueryWrapper = new QueryWrapper<>();
                    orderQueryWrapper.eq("id",baShippingOrder.getThdno());
                    List<BaShippingOrder> baShippingOrders = baShippingOrderMapper.selectList(orderQueryWrapper);
                    if(baShippingOrders.size() > 0){
                        return 0;
                    }
                    baShippingOrder.setId(baShippingOrder.getThdno());
                    baShippingOrder.setCreateTime(DateUtils.getNowDate());
                    baShippingOrder.setSupplierNo(baTransportAutomobile.getId());
                    baShippingOrderMapper.insertBaShippingOrder(baShippingOrder);
                }
            }
        }
        Integer result = baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
      /*  if(result > 0){
            BaTransportAutomobile transportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baTransportAutomobile.getId());
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(transportAutomobile, AdminCodeEnum.AUTOMOBILE_APPROVAL_CONTENT_UPDATE.getCode());
            if(StringUtils.isNull(baProcessInstancesRuntimeVO)){
                return 0;
            }else {
                transportAutomobile.setState(AdminCodeEnum.AUTOMOBILE_STATUS_VERIFYING.getCode());
                //修改创建时间
                transportAutomobile.setCreateTime(baTransportAutomobile.getUpdateTime());
                baTransportAutomobileMapper.updateBaTransportAutomobile(transportAutomobile);
            }
        }*/
        return result;
    }

    @Override
    public int updateAutomobile(JSONObject transportAutomobile) {

        BaTransportAutomobile baTransportAutomobile = JSONObject.toJavaObject((JSONObject) JSON.toJSON(transportAutomobile), BaTransportAutomobile.class);
        //以货源编号为主键
        baTransportAutomobile.setId(baTransportAutomobile.getSupplierNo());
        baTransportAutomobile.setUpdateTime(DateUtils.getNowDate());
        //查询货源运价
        QueryWrapper<BaAutomobuleStandard> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("relation_id",baTransportAutomobile.getId());
        BaAutomobuleStandard baAutomobuleStandard = baAutomobuleStandardMapper.selectOne(queryWrapper);
        if(baTransportAutomobile.getRatesItemList().size() > 0){
            BaAutomobuleStandard baAutomobuleStandard1 = baTransportAutomobile.getRatesItemList().get(0);
            baAutomobuleStandard1.setId(baAutomobuleStandard.getId());
            //修改货源运价信息
            baAutomobuleStandardMapper.updateBaAutomobuleStandard(baAutomobuleStandard1);
        }
        return baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
    }

    /**
     * 批量删除汽车运输/货源
     *
     * @param ids 需要删除的汽车运输/货源ID
     * @return 结果
     */
    @Override
    public int deleteBaTransportAutomobileByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(id);
                baTransportAutomobile.setIsDelete(Long.valueOf(1));
                baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
            }
        }else {
            return 0;
        }
        return 1;
    }

    /**
     * 删除汽车运输/货源信息
     *
     * @param id 汽车运输/货源ID
     * @return 结果
     */
    @Override
    public int deleteBaTransportAutomobileById(String id)
    {
        return baTransportAutomobileMapper.deleteBaTransportAutomobileById(id);
    }

    @Override
    public int revoke(String id) {
        if(StringUtils.isNotEmpty(id)){
            BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(id);
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",baTransportAutomobile.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result", AdminCodeEnum.TRANSPORT_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(baTransportAutomobile.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //运输流程审批状态修改
                baTransportAutomobile.setState(AdminCodeEnum.AUTOMOBILE_STATUS_WITHDRAW.getCode());
                baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
                String userId = String.valueOf(baTransportAutomobile.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(baTransportAutomobile, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_PRODUCT_AUTOMOBILE.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public int delivery(BaShippingOrder baShippingOrder) {
        //装车净重
         BigDecimal ccjweight = new BigDecimal(0.00);
         //入场净重
        BigDecimal rcjweight = new BigDecimal(0.00);
        //运费
        BigDecimal sfratesmoney = new BigDecimal(0.00);
         //车数
        Integer a = 0;
        //查找所有运单
        QueryWrapper<BaShippingOrder> orderQueryWrapper = new QueryWrapper<>();
        orderQueryWrapper.eq("supplier_no",baShippingOrder.getSupplierNo());
        List<BaShippingOrder> baShippingOrders = baShippingOrderMapper.selectList(orderQueryWrapper);
        for (BaShippingOrder baShippingOrder1:baShippingOrders) {
            ccjweight = ccjweight.add(baShippingOrder1.getCcjweight());
            rcjweight = rcjweight.add(baShippingOrder1.getRcjweight());
            sfratesmoney = sfratesmoney.add(baShippingOrder1.getSfratesmoney());
            a++;
        }
        //更新运单卸车净重
        Integer result = baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
        //查询是否有发货信息
        /*QueryWrapper<BaCheck> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("relation_id",baShippingOrder.getSupplierNo());
        queryWrapper.eq("type",3);
        BaCheck baCheck1 = baCheckMapper.selectOne(queryWrapper);
        BaCheck baCheck = new BaCheck();
        if(StringUtils.isNull(baCheck1)){
            //新增发货信息
            baCheck.setId(getRedisIncreID.getId());
            baCheck.setType(3);
            baCheck.setRelationId(baShippingOrder.getSupplierNo());
            baCheck.setCheckTime(DateUtils.getNowDate());
            baCheck.setCarsNum(a);
            baCheck.setCheckCoalNum(rcjweight);
            result = baCheckMapper.insertBaCheck(baCheck);
        }else {
            baCheck1.setCarsNum(a);
            baCheck1.setCheckCoalNum(rcjweight);
            baCheckMapper.updateBaCheck(baCheck1);
        }*/
        //更新结算信息
        /*QueryWrapper<BaSettlementFreight> freightQueryWrapper = new QueryWrapper<>();
        freightQueryWrapper.eq("automobile_id",baShippingOrder.getSupplierNo());
        BaSettlementFreight baSettlementFreight = baSettlementFreightMapper.selectOne(freightQueryWrapper);
        if(StringUtils.isNotNull(baSettlementFreight)){
            baSettlementFreight.setCarNum((long)a);
            baSettlementFreight.setYfratesmoney(sfratesmoney);
            baSettlementFreightMapper.updateBaSettlementFreight(baSettlementFreight);
        }*/
        //货源数据发运状态
        /*if(result > 0){
            BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baShippingOrder.getSupplierNo());
            baTransportAutomobile.setConfirmStatus("3");
            baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
        }else {
            return 0;
        }*/
        return result;
    }

    @Override
    public int receipt(BaTransportAutomobile baTransportAutomobile) {
        int result = 0;
        /*if(StringUtils.isNotNull(baTransportAutomobile.getReceipt())){
            BaCheck baCheck = baTransportAutomobile.getReceipt();
            baCheck.setId(getRedisIncreID.getId());
            baCheck.setType(2);
            result = baCheckMapper.insertBaCheck(baCheck);
        }*/
        if(result > 0){
            baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baTransportAutomobile.getId());
            baTransportAutomobile.setConfirmStatus("4");
            baTransportAutomobile.setQdCountType((long)1);
            baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
        }
        return result;
    }

    @Override
    public int qrCode(BaTransportAutomobile baTransportAutomobile) {

        return baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
    }

    @Override
    public List<BaShippingOrder> shippingList(BaShippingOrder baShippingOrder) {
        List<BaShippingOrder> baShippingOrders = baShippingOrderMapper.selectBaShippingOrderList(baShippingOrder);
        return baShippingOrders;
    }

    @Override
    public int transfer(BaTransportAutomobile baTransportAutomobile) {
        baTransportAutomobile.setConfirmStatus("5");
        return baTransportAutomobileMapper.updateBaTransportAutomobile(baTransportAutomobile);
    }

    @Override
    public UR gain() {
        //本地环境
        //String url = "https://4942h442k4.yicp.fun/system/middleware/uploadShipper";
        //线上环境
        String url = "https://zncy.xkgo.net/prod-api/system/middleware/uploadShipper";
        JSONObject jsonObject = new JSONObject();
        String postData = restTemplateUtil.postMethod(url, JSONObject.toJSONString(jsonObject));
        Object parse = JSONObject.parse(postData);
        JSONObject obj = (JSONObject) JSON.toJSON(parse);
        //获取data对象集合
        JSONArray data = obj.getJSONArray("data");
        List<Object> objects = new ArrayList<>();
        Map<String, String> hashMap = new HashMap<>();
        for (Object user:data) {
            /*Map entity = (Map)user;
            hashMap.put(entity.get("userId").toString(),entity.get("companyName").toString());*/
            objects.add(user);
        }
        return UR.ok().data("list",objects);
    }

    @Override
    public List<BaTransportAutomobile> listAutomobile() {

        List<BaTransportAutomobile> list = new ArrayList<>();
        //查询货源信息
        QueryWrapper<BaTransportAutomobile> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_delete",0);
        List<BaTransportAutomobile> baTransportAutomobiles = baTransportAutomobileMapper.selectList(queryWrapper);
        for (BaTransportAutomobile baTransportAutomobile:baTransportAutomobiles) {
            //查询相关运单
            QueryWrapper<BaShippingOrder> orderQueryWrapper = new QueryWrapper<>();
            orderQueryWrapper.eq("supplier_no",baTransportAutomobile.getSupplierNo());
            orderQueryWrapper.eq("depot_state","0");
            List<BaShippingOrder> shippingOrders = baShippingOrderMapper.selectList(orderQueryWrapper);
            if(StringUtils.isNotNull(shippingOrders)){
                list.add(baTransportAutomobile);
            }
        }
        return list;
    }


    @Override
    public int insertSourceOfGoods(BaTransportAutomobile baTransportAutomobile) {
        //当前时间转时间戳
        Long id = DateUtils.getNowDate().getTime();
        baTransportAutomobile.setId("XJ"+String.valueOf(id).substring(0,13));
        //新增货源编号
        baTransportAutomobile.setSupplierNo(baTransportAutomobile.getId());
        baTransportAutomobile.setCreateTime(DateUtils.getNowDate());
        baTransportAutomobile.setUserId(SecurityUtils.getUserId());
        baTransportAutomobile.setDeptId(SecurityUtils.getDeptId());
        baTransportAutomobile.setCreateBy(SecurityUtils.getUsername());
        baTransportAutomobile.setConfirmStatus("1");
        baTransportAutomobile.setType("3");
        //新增货源中运价
        if(baTransportAutomobile.getRatesItemList().size() > 0){
            baTransportAutomobile.getRatesItemList().forEach(item ->{
                item.setId(getRedisIncreID.getId());
                item.setRelationId(baTransportAutomobile.getId());
                baAutomobuleStandardMapper.insertBaAutomobuleStandard(item);
            });
        }
        return baTransportAutomobileMapper.insertBaTransportAutomobile(baTransportAutomobile);
    }


    /**
     * 插入消息
     * @return
     */
    private void insertMessage(BaTransportAutomobile baTransportAutomobile, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(baTransportAutomobile.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_PRODUCT_AUTOMOBILE.getCode());
        baMessageMapper.insertBaMessage(baMessage);
    }

}
