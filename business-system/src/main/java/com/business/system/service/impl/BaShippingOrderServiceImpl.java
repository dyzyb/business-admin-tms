package com.business.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.RestTemplateUtil;
import com.business.common.utils.StringUtils;
import com.business.system.domain.BaShippingOrder;
import com.business.system.domain.BaTransportAutomobile;
import com.business.system.domain.vo.BaShippingOrderGoodsProportionVO;
import com.business.system.domain.vo.BaShippingOrderRecentCarVO;
import com.business.system.domain.vo.BaShippingOrderVO;
import com.business.system.mapper.BaCounterpartMapper;
import com.business.system.mapper.BaShippingOrderMapper;
import com.business.system.mapper.BaTransportAutomobileMapper;
import com.business.system.service.IBaShippingOrderService;
import com.business.system.service.IBaTransportAutomobileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * 发运单Service业务层处理
 *
 * @author single
 * @date 2023-02-18
 */
@Service
public class BaShippingOrderServiceImpl extends ServiceImpl<BaShippingOrderMapper, BaShippingOrder> implements IBaShippingOrderService
{
    @Autowired
    private BaShippingOrderMapper baShippingOrderMapper;
    @Autowired
    private GetRedisIncreID getRedisIncreID;
    @Autowired
    private BaTransportAutomobileMapper baTransportAutomobileMapper;
    @Autowired
    private IBaTransportAutomobileService baTransportAutomobileService;
    /*@Autowired
    private BaCheckMapper baCheckMapper;*/
    @Autowired
    private RestTemplateUtil restTemplateUtil;
    @Autowired
    private BaCounterpartMapper baCounterpartMapper;
    /*@Autowired
    private BaAutomobileSettlementDetailMapper baAutomobileSettlementDetailMapper;
    @Autowired
    private BaAutomobileSettlementInvoiceMapper baAutomobileSettlementInvoiceMapper;
    @Autowired
    private BaShippingOrderCmstMapper baShippingOrderCmstMapper;
    @Autowired
    private BaEnterpriseMapper baEnterpriseMapper;
    @Autowired
    private BaSampleAssayMapper baSampleAssayMapper;*/

    /**
     * 查询发运单
     *
     * @param id 发运单ID
     * @return 发运单
     */
    @Override
    public BaShippingOrder selectBaShippingOrderById(String id)
    {
        BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(id);
        //发货人
        /*if(StringUtils.isNotEmpty(baShippingOrder.getFhunitname())){
            baShippingOrder.setFhunitname(baCounterpartMapper.selectBaCounterpartById(baShippingOrder.getFhunitname()).getName());
        }*/
        //收货人
        /*if(StringUtils.isNotEmpty(baShippingOrder.getShunitname())){
            baShippingOrder.setShunitname(baCounterpartMapper.selectBaCounterpartById(baShippingOrder.getShunitname()).getName());
        }*/
        //货源信息
        if(StringUtils.isNotEmpty(baShippingOrder.getSupplierNo())){
            BaTransportAutomobile baTransportAutomobile = baTransportAutomobileService.selectBaTransportAutomobileById(baShippingOrder.getSupplierNo());
            if(StringUtils.isNotNull(baTransportAutomobile)){
                baShippingOrder.setSourceName(baTransportAutomobile.getSourceName());
                baShippingOrder.setBaTransportAutomobile(baTransportAutomobile);
            }
        }
        //化验信息
        /*BaSampleAssay sampleAssay = baSampleAssayMapper.selectBaSampleAssayById(baShippingOrder.getExperimentId());
        if(StringUtils.isNotNull(sampleAssay)){
            baShippingOrder.setAssay(sampleAssay);
        }*/
        return baShippingOrder;
    }

    /**
     * 查询发运单列表
     *
     * @param baShippingOrder 发运单
     * @return 发运单
     */
    @Override
    public List<BaShippingOrder> selectBaShippingOrderList(BaShippingOrder baShippingOrder)
    {
        List<BaShippingOrder> baShippingOrders = baShippingOrderMapper.baShippingOrderList(baShippingOrder);
        for (BaShippingOrder shippingOrder:baShippingOrders) {
            if(StringUtils.isNotEmpty(shippingOrder.getSupplierNo())){
                //查询货源信息
                BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(shippingOrder.getSupplierNo());
                shippingOrder.setBaTransportAutomobile(baTransportAutomobile);
            }
            //查询付款明细
            /*QueryWrapper<BaAutomobileSettlementDetail> queryWrapper =new QueryWrapper<>();
            queryWrapper.eq("external_order",shippingOrder.getId());
            List<BaAutomobileSettlementDetail> baAutomobileSettlementDetails = baAutomobileSettlementDetailMapper.selectList(queryWrapper);
            for (BaAutomobileSettlementDetail baAutomobileSettlementDetail:baAutomobileSettlementDetails) {
                if(baAutomobileSettlementDetail.getState().equals("2")){
                    shippingOrder.setPaymentStatus("1");
                }
            }*/
            //开票状态
            /*if(StringUtils.isNotEmpty(shippingOrder.getThdno())){
                BaAutomobileSettlementInvoice baAutomobileSettlementInvoice = baAutomobileSettlementInvoiceMapper.selectBaAutomobileSettlementInvoiceByexternalorder(shippingOrder.getThdno());
                if(StringUtils.isNotNull(baAutomobileSettlementInvoice)){
                    shippingOrder.setCheckFlag(baAutomobileSettlementInvoice.getCheckFlag());
                }

            }*/

        }
        return baShippingOrders;
    }

    /*@Override
    public List<waybillDTO> waybillDTOList(BaShippingOrder baShippingOrder) {
        List<waybillDTO> waybillDTOList = baShippingOrderMapper.waybillDTOList(baShippingOrder);
        for (waybillDTO waybillDTO:waybillDTOList) {
            if(StringUtils.isNotEmpty(waybillDTO.getSupplierNo())){
                //查询货源信息
                BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(waybillDTO.getSupplierNo());
                waybillDTO.setBaTransportAutomobile(baTransportAutomobile);
            }
            if("1".equals(waybillDTO.getType())){
                //开票状态
                if(StringUtils.isNotEmpty(waybillDTO.getThdno())){
                    BaAutomobileSettlementInvoice baAutomobileSettlementInvoice = baAutomobileSettlementInvoiceMapper.selectBaAutomobileSettlementInvoiceByexternalorder(waybillDTO.getThdno());
                    if(StringUtils.isNotNull(baAutomobileSettlementInvoice)){
                        waybillDTO.setCheckFlag(baAutomobileSettlementInvoice.getCheckFlag().toString());
                    }
                }
            }else if("2".equals(waybillDTO.getType())){
                BaShippingOrderCmst baShippingOrderCmst = baShippingOrderCmstMapper.selectBaShippingOrderCmstById(waybillDTO.getId());
                waybillDTO.setCheckFlag(baShippingOrderCmst.getInvoiceType());
                waybillDTO.setSupplierName(baShippingOrderCmst.getConsignorUserName());
            }
            //供应商名称
            if(StringUtils.isNotEmpty(waybillDTO.getEnterprise())){
                BaEnterprise enterprise = baEnterpriseMapper.selectBaEnterpriseById(waybillDTO.getEnterprise());
                waybillDTO.setEnterpriseName(enterprise.getName());
            }
        }
        return waybillDTOList;
    }*/

    /**
     * 新增发运单
     *
     * @param baShippingOrder 发运单
     * @return 结果
     */
    @Override
    public int insertBaShippingOrder(BaShippingOrder baShippingOrder)
    {
        //运费

        //抢单车数统计

        //货源单信息
        /*BaTransportAutomobile baTransportAutomobile = baTransportAutomobileMapper.selectBaTransportAutomobileById(baShippingOrder.getSupplierNo());
        baTransportAutomobile.setQdCount(baTransportAutomobile.getQdCount() + 1);*/

        //baShippingOrder.setId(getRedisIncreID.getId());
        baShippingOrder.setCreateTime(DateUtils.getNowDate());
        baShippingOrder.setSignFor("0");
        Integer result = baShippingOrderMapper.insertBaShippingOrder(baShippingOrder);
        if(result > 0){
            //装车净重
            BigDecimal ccjweight = new BigDecimal(0.00);
            //车数
            Integer a = 0;
            //查找所有运单
            QueryWrapper<BaShippingOrder> orderQueryWrapper = new QueryWrapper<>();
            orderQueryWrapper.eq("supplier_no",baShippingOrder.getSupplierNo());
            List<BaShippingOrder> baShippingOrders = baShippingOrderMapper.selectList(orderQueryWrapper);
            for (BaShippingOrder baShippingOrder1:baShippingOrders) {
                ccjweight = ccjweight.add(baShippingOrder1.getCcjweight());
                a++;
            }
            //查询是否有发货信息
            /*QueryWrapper<BaCheck> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("relation_id",baShippingOrder.getSupplierNo());
            queryWrapper.eq("type",1);
            BaCheck baCheck1 = baCheckMapper.selectOne(queryWrapper);
            BaCheck baCheck = new BaCheck();
            if(StringUtils.isNull(baCheck1)){
                //新增发货信息
                baCheck.setId(getRedisIncreID.getId());
                baCheck.setType(1);
                baCheck.setRelationId(baShippingOrder.getSupplierNo());
                baCheck.setCheckTime(DateUtils.getNowDate());
                baCheck.setCarsNum(a);
                baCheck.setCheckCoalNum(ccjweight);
                result = baCheckMapper.insertBaCheck(baCheck);
            }else {
                baCheck1.setCarsNum(a);
                baCheck1.setCheckCoalNum(ccjweight);
                baCheckMapper.updateBaCheck(baCheck1);
            }*/
        }
        return result;
    }

    @Override
    public int insertOrder(JSONObject object) {
        BaShippingOrder baShippingOrder = JSONObject.toJavaObject((JSONObject) JSON.toJSON(object), BaShippingOrder.class);
        baShippingOrder.setId(getRedisIncreID.getId());
        baShippingOrder.setCreateTime(DateUtils.getNowDate());
        return baShippingOrderMapper.insertBaShippingOrder(baShippingOrder);
    }

    /**
     * 修改发运单
     *
     * @param baShippingOrder 发运单
     * @return 结果
     */
    @Override
    public int updateBaShippingOrder(BaShippingOrder baShippingOrder)
    {
        baShippingOrder.setUpdateTime(DateUtils.getNowDate());
        baShippingOrder.setSignFor("1");
        //对接昕科，返回签收成功标识
        return baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
    }

    @Override
    public int updateShippingOrder(JSONObject shippingOrder) {
        BaShippingOrder baShippingOrder = JSONObject.toJavaObject((JSONObject) JSON.toJSON(shippingOrder), BaShippingOrder.class);
        baShippingOrder.setId(baShippingOrder.getThdno());
        baShippingOrder.setSignFor("1");
        baShippingOrder.setUpdateTime(DateUtils.getNowDate());
        return baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
    }

    /**
     * 批量删除发运单
     *
     * @param ids 需要删除的发运单ID
     * @return 结果
     */
    @Override
    public int deleteBaShippingOrderByIds(String[] ids)
    {
        return baShippingOrderMapper.deleteBaShippingOrderByIds(ids);
    }

    /**
     * 删除发运单信息
     *
     * @param id 发运单ID
     * @return 结果
     */
    @Override
    public int deleteBaShippingOrderById(String id)
    {
        return baShippingOrderMapper.deleteBaShippingOrderById(id);
    }

    @Override
    public int signFor(String[] ids) {
        //本地环境
        //String url = "https://4942h442k4.yicp.fun/business/middleware/transSign";
        //线上环境
        String url = "https://zncy.xkgo.net/prod-api/business/middleware/transSign";
        List<String> transNo = new ArrayList<String>();

        for (String id:ids) {
            transNo.add(id);
        }
        String s = JSONObject.toJSONString(transNo);
        System.out.println(s);
        Map<String, List<String>> map = new HashMap<>();
        map.put("transNo",transNo);
        String postData = restTemplateUtil.postMethod(url, JSONObject.toJSONString(map));
        Object parse = JSONObject.parse(postData);
        JSONObject obj = (JSONObject) JSON.toJSON(parse);
        System.out.println("++++++++++++++++"+obj.getString("msg"));
        if(obj.getString("code").equals("200")){
            Object data = obj.getJSONObject("data");
            JSONObject obj1 = (JSONObject) JSON.toJSON(data);
            //签收成功
            String success = obj1.getString("success");
            if(StringUtils.isNotEmpty(success)){
                String[] split = success.split(",");
                for (String id:split) {
                    BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(id);
                    baShippingOrder.setSignFor("1");
                    baShippingOrder.setErrorText("");
                    baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                }
            }
            //签收失败
            String failed = obj1.getString("failed");
            if(StringUtils.isNotEmpty(failed)){
                String[] split = failed.split(",");
                for (String id:split) {
                    BaShippingOrder baShippingOrder = baShippingOrderMapper.selectBaShippingOrderById(id);
                    baShippingOrder.setSignFor("2");
                    baShippingOrder.setErrorText(obj.getString("msg"));
                    baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
                }
                return 0;
            }
            return 200;
        }
        return 0;
    }

    /**
     * 发运单签收回调
     * @param transNos
     * @return
     */
    @Override
    public int editSignForCallback(List<String> transNos) {
        if(!CollectionUtils.isEmpty(transNos)){
            transNos.stream().forEach(item -> {
                BaShippingOrder baShippingOrder = new BaShippingOrder();
                baShippingOrder.setId(item);
                baShippingOrder.setSignFor("1");
                baShippingOrderMapper.updateBaShippingOrder(baShippingOrder);
            });
        }
        return 0;
    }

    @Override
    public Map<String, BaShippingOrderRecentCarVO> recentCars() {
        List<BaShippingOrderVO> baShippingOrderVOList = baShippingOrderMapper.recentCars();
        Map<String, BaShippingOrderRecentCarVO> recentCarsMap = new LinkedHashMap<>();
        BaShippingOrderRecentCarVO baShippingOrderRecentCarVO = null;
        //在途车辆数
        int inTransit = 0;
        //装车车辆数
        int loadVehicle = 0;
        //卸车车辆数
        int unloadVehicle = 0;
        if(!CollectionUtils.isEmpty(baShippingOrderVOList)){
            for(BaShippingOrderVO baShippingOrderVO : baShippingOrderVOList){
                //判断网货平台
                if("昕科".equals(baShippingOrderVO.getPlatform())){
                    if(StringUtils.isNotEmpty(baShippingOrderVO.getVidpic2())){         //上传卸车磅单
                        unloadVehicle = unloadVehicle + 1;
                    } else if(StringUtils.isNotEmpty(baShippingOrderVO.getVidpic1())){  //上传装车磅单
                        loadVehicle = loadVehicle + 1;
                    } else {
                        inTransit = inTransit + 1;
                    }
                } else if("中储智运".equals(baShippingOrderVO.getPlatform())){
                    //摘单
                    if("5".equals(baShippingOrderVO.getConsignorState())){
                        unloadVehicle = unloadVehicle + 1;
                    } else if("6".equals(baShippingOrderVO.getConsignorState())){ //确认发货
                        loadVehicle = loadVehicle + 1;
                    } else { //确认收货
                        inTransit = inTransit + 1;
                    }
                }
                if(recentCarsMap.containsKey(baShippingOrderVO.getCreateTime())){
                    baShippingOrderRecentCarVO = recentCarsMap.get(baShippingOrderVO.getCreateTime());
                    baShippingOrderRecentCarVO.setInTransit(inTransit);
                    baShippingOrderRecentCarVO.setLoadVehicle(loadVehicle);
                    baShippingOrderRecentCarVO.setUnloadVehicle(unloadVehicle);
                    recentCarsMap.put(baShippingOrderVO.getCreateTime(), baShippingOrderRecentCarVO);
                } else {
                    baShippingOrderRecentCarVO = new BaShippingOrderRecentCarVO();
                    baShippingOrderRecentCarVO.setInTransit(inTransit);
                    baShippingOrderRecentCarVO.setLoadVehicle(loadVehicle);
                    baShippingOrderRecentCarVO.setUnloadVehicle(unloadVehicle);
                    recentCarsMap.put(baShippingOrderVO.getCreateTime(), baShippingOrderRecentCarVO);
                }
            }
        }
        return recentCarsMap;
    }

    @Override
    public List<BaShippingOrderGoodsProportionVO> selectGoodsProportion() {
        return baShippingOrderMapper.selectGoodsProportion();
    }
}
