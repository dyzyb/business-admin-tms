package com.business.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.business.system.domain.BaProcessInstanceRelated;
import com.business.system.domain.vo.BaProcessBusinessInstanceRelatedVO;

import java.util.List;
import java.util.Map;

/**
 * @Author: js
 * @Description: 业务与流程实例关联关系接口 服务类
 * @date: 2022/12/5 15:10
 */
public interface IBaProcessBusinessInstanceRelatedService extends IService<BaProcessInstanceRelated> {

    /**
     * 封装业务流程实例关联数据接口
     * @param businessKeys
     * @return Map<String, BaProcessBusinessInstanceRelatedVO>
     */
    Map<String, BaProcessBusinessInstanceRelatedVO> packageBusinessProcessInstanceRelatedMap(List<String> businessKeys);

    /**
     * 根据流程实例ID查询业务数据
     * @param processInstanceId
     * @return
     */
    BaProcessInstanceRelated getApplyBusinessDataByProcessInstanceId(String processInstanceId);
}