package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.OaEgress;
import com.business.system.domain.OaOther;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IOaEgressApprovalService;
import com.business.system.service.workflow.IOaOtherApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 其他申请审批结果:审批通过
 */
@Service("oaOtherApprovalContent:processApproveResult:pass")
@Slf4j
public class OaOtherApprovalAgreeServiceImpl extends IOaOtherApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        OaOther oaOther = this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.OAOTHER_STATUS_PASS.getCode());
        return result;
    }
}
