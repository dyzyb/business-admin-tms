package com.business.system.service.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.OaAttendanceGroup;
import com.business.system.domain.OaCalendar;
import com.business.system.domain.OaClock;
import com.business.system.domain.dto.DayClockListDTO;
import com.business.system.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.domain.OaClockSummary;
import com.business.system.service.IOaClockSummaryService;

/**
 * 打卡汇总Service业务层处理
 *
 * @author single
 * @date 2023-12-11
 */
@Service
public class OaClockSummaryServiceImpl extends ServiceImpl<OaClockSummaryMapper, OaClockSummary> implements IOaClockSummaryService
{
    @Autowired
    private OaClockSummaryMapper oaClockSummaryMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private OaClockMapper oaClockMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private OaAttendanceGroupMapper oaAttendanceGroupMapper;

    @Autowired
    private OaCalendarMapper oaCalendarMapper;

    /**
     * 查询打卡汇总
     *
     * @param id 打卡汇总ID
     * @return 打卡汇总
     */
    @Override
    public OaClockSummary selectOaClockSummaryById(String id)
    {
        return oaClockSummaryMapper.selectOaClockSummaryById(id);
    }

    /**
     * 查询打卡汇总列表
     *
     * @param oaClockSummary 打卡汇总
     * @return 打卡汇总
     */
    @Override
    public List<OaClockSummary> selectOaClockSummaryList(OaClockSummary oaClockSummary)
    {
        //日期格式化
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String date = format.format(new Date());
        //获取年月
        int index = date.lastIndexOf("-");
        String result = date.substring(0,index);
        //格式化时分
        SimpleDateFormat time = new SimpleDateFormat("HH:mm");

        List<OaClockSummary> oaClockSummaries = oaClockSummaryMapper.selectOaClockSummaryList(oaClockSummary);
        for (OaClockSummary summary:oaClockSummaries) {
            //员工名称
             summary.setUserName(sysUserMapper.selectUserById(Long.valueOf(summary.getUserId())).getNickName());
             //员工部门
            if(StringUtils.isNotEmpty(summary.getDeptId())){
                summary.setDeptName(sysDeptMapper.selectDeptById(Long.valueOf(summary.getDeptId())).getDeptName());
            }
             //查看日历
            OaCalendar oaCalendar = new OaCalendar();
            if(StringUtils.isNotEmpty(oaClockSummary.getSummaryDate())){
                oaCalendar.setDate(oaClockSummary.getSummaryDate());
            }else {
                oaCalendar.setDate(result);
            }
            List<OaCalendar> oaCalendars = oaCalendarMapper.selectOaCalendarList(oaCalendar);
            //本月执行范围
            List<OaCalendar> list = new ArrayList<>();
            for (OaCalendar calendar:oaCalendars) {
                //查看是否本月数据
                if(StringUtils.isNotEmpty(oaClockSummary.getSummaryDate())){
                    if(result.equals(oaClockSummary.getSummaryDate())){
                        if(calendar.getDate().compareTo(date) < 0){
                            list.add(calendar);
                            //查看打卡信息
                            OaClock oaClock = new OaClock();
                            oaClock.setEmployeeId(Long.valueOf(summary.getUserId()));
                            oaClock.setQueryTime(calendar.getDate());
                            oaClock.setType("1");
                            List<OaClock> oaClockList = oaClockMapper.selectOaClockList(oaClock);
                            String thisClock = "";
                            for (OaClock clock:oaClockList) {
                                if("1".equals(clock.getClockType()) && !"0".equals(clock.getClockState())){
                                    String compare = compare(clock.getClockState());
                                    if(clock.getClockTime() != null){
                                        thisClock = thisClock+","+compare + ";" + time.format(clock.getClockTime());
                                    }else {
                                        thisClock = thisClock+","+compare;
                                    }
                                }else if("1".equals(clock.getClockType()) && clock.getFlag() == 1 && "0".equals(clock.getClockState())){
                                    thisClock = "上班缺卡";
                                }
                                if("2".equals(clock.getClockType()) && !"0".equals(clock.getClockState())){
                                    String compare = compare(clock.getClockState());
                                    if(clock.getClockTime() != null){
                                        thisClock = thisClock+","+compare + ";" + time.format(clock.getClockTime());
                                    }else {
                                        thisClock = thisClock+","+compare;
                                    }
                                }else if("2".equals(clock.getClockType()) && clock.getFlag() == 1 && "0".equals(clock.getClockState())){
                                    thisClock = thisClock+","+"下班缺卡";
                                }
                            }
                            if(StringUtils.isNotEmpty(thisClock)){
                                if(thisClock.charAt(0) == ','){
                                    thisClock = thisClock.replaceFirst(",", "");
                                }
                            }
                            calendar.setThisClock(thisClock);
                        }
                    }else {
                        list.add(calendar);
                        //查看打卡信息
                        OaClock oaClock = new OaClock();
                        oaClock.setEmployeeId(Long.valueOf(summary.getUserId()));
                        oaClock.setQueryTime(calendar.getDate());
                        oaClock.setType("1");
                        List<OaClock> oaClockList = oaClockMapper.selectOaClockList(oaClock);
                        String thisClock = "";
                        for (OaClock clock:oaClockList) {
                            if("1".equals(clock.getClockType()) && !"0".equals(clock.getClockState())){
                                String compare = compare(clock.getClockState());
                                if(clock.getClockTime() != null){
                                    thisClock = thisClock+","+compare + ";" + time.format(clock.getClockTime());
                                }else {
                                    thisClock = thisClock+","+compare;
                                }
                            }else if("1".equals(clock.getClockType()) && clock.getFlag() == 1 && "0".equals(clock.getClockState())){
                                thisClock = "上班缺卡";
                            }
                            if("2".equals(clock.getClockType()) && !"0".equals(clock.getClockState())){
                                String compare = compare(clock.getClockState());
                                if(clock.getClockTime() != null){
                                    thisClock = thisClock+","+compare + ";" + time.format(clock.getClockTime());
                                }else {
                                    thisClock = thisClock+","+compare;
                                }
                            }else if("2".equals(clock.getClockType()) && clock.getFlag() == 1 && "0".equals(clock.getClockState())){
                                thisClock = thisClock+","+"下班缺卡";
                            }
                        }
                        if(StringUtils.isNotEmpty(thisClock)){
                            if(thisClock.charAt(0) == ','){
                                thisClock = thisClock.replaceFirst(",", "");
                            }
                        }
                        calendar.setThisClock(thisClock);
                    }
                }else {
                    if(calendar.getDate().compareTo(date) < 0){
                        list.add(calendar);
                        //查看打卡信息
                        OaClock oaClock = new OaClock();
                        oaClock.setEmployeeId(Long.valueOf(summary.getUserId()));
                        oaClock.setQueryTime(calendar.getDate());
                        oaClock.setType("1");
                        List<OaClock> oaClockList = oaClockMapper.selectOaClockList(oaClock);
                        String thisClock = "";
                        for (OaClock clock:oaClockList) {
                            if("1".equals(clock.getClockType()) && !"0".equals(clock.getClockState())){
                                String compare = compare(clock.getClockState());
                                if(clock.getClockTime() != null){
                                    thisClock = thisClock+","+compare + ";" + time.format(clock.getClockTime());
                                }else {
                                    thisClock = thisClock+","+compare;
                                }
                            }else if("1".equals(clock.getClockType()) && clock.getFlag() == 1 && "0".equals(clock.getClockState())){
                                thisClock = "上班缺卡";
                            }
                            if("2".equals(clock.getClockType()) && !"0".equals(clock.getClockState())){
                                String compare = compare(clock.getClockState());
                                if(clock.getClockTime() != null){
                                    thisClock = thisClock+","+compare + ";" + time.format(clock.getClockTime());
                                }else {
                                    thisClock = thisClock+","+compare;
                                }
                            }else if("2".equals(clock.getClockType()) && clock.getFlag() == 1 && "0".equals(clock.getClockState())){
                                thisClock = thisClock+","+"下班缺卡";
                            }
                        }
                        if(StringUtils.isNotEmpty(thisClock)){
                            if(thisClock.charAt(0) == ','){
                                thisClock = thisClock.replaceFirst(",", "");
                            }
                        }
                        calendar.setThisClock(thisClock);
                    }
                }

            }
            summary.setOaCalendars(list);
        }
        return oaClockSummaries;
    }

    /**
     * 新增打卡汇总
     *
     * @param oaClockSummary 打卡汇总
     * @return 结果
     */
    @Override
    public int insertOaClockSummary(OaClockSummary oaClockSummary)
    {
        oaClockSummary.setId(getRedisIncreID.getId());
        oaClockSummary.setCreateTime(DateUtils.getNowDate());
        return oaClockSummaryMapper.insertOaClockSummary(oaClockSummary);
    }

    /**
     * 修改打卡汇总
     *
     * @param oaClockSummary 打卡汇总
     * @return 结果
     */
    @Override
    public int updateOaClockSummary(OaClockSummary oaClockSummary)
    {
        oaClockSummary.setUpdateTime(DateUtils.getNowDate());
        return oaClockSummaryMapper.updateOaClockSummary(oaClockSummary);
    }

    /**
     * 批量删除打卡汇总
     *
     * @param ids 需要删除的打卡汇总ID
     * @return 结果
     */
    @Override
    public int deleteOaClockSummaryByIds(String[] ids)
    {
        return oaClockSummaryMapper.deleteOaClockSummaryByIds(ids);
    }

    /**
     * 删除打卡汇总信息
     *
     * @param id 打卡汇总ID
     * @return 结果
     */
    @Override
    public int deleteOaClockSummaryById(String id)
    {
        return oaClockSummaryMapper.deleteOaClockSummaryById(id);
    }

    @Override
    public int sameMonth() {
        //日期格式化
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
        String month = format.format(new Date());
        //当月汇总数据
        OaClockSummary oaClockSummary = new OaClockSummary();
        oaClockSummary.setSummaryDate(month);
        oaClockSummary.setAttendanceName("all");
        List<OaClockSummary> oaClockSummaries = oaClockSummaryMapper.selectOaClockSummaryList(oaClockSummary);
        for (OaClockSummary clockSummary:oaClockSummaries) {
            //考勤组信息
            SysUser user = sysUserMapper.selectUserById(Long.valueOf(clockSummary.getUserId()));
            //查看部门信息
            SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
            if(StringUtils.isNotNull(dept)){
                if(!"0".equals(dept.getAttendanceLock())){
                    OaAttendanceGroup attendanceGroup = oaAttendanceGroupMapper.selectOaAttendanceGroupById(dept.getAttendanceLock());
                    clockSummary.setAttendanceName(attendanceGroup.getAttendanceName());
                    clockSummary.setAttendanceId(attendanceGroup.getId());
                }
            }
            //迟到次数
            int latenessNum = 0;
            //迟到时长
            BigDecimal latenessTimes = new BigDecimal(0);
            //早退次数
            int leaveEarlyNum = 0;
            //早退时长
            BigDecimal leaveEarlyTimes = new BigDecimal(0);
            //旷工天数
            int absenteeismNum = 0;
            //出差天数
            int evectionNum = 0;
            //外出天数
            int egressNum = 0;
            //请假天数
            int leaveNum = 0;
            //出勤
            int attendance = 0;
            //休息
            int rest = 0;
            //查询用户打卡当月数据
            OaClock oaClock = new OaClock();
            oaClock.setThisMonth(month);
            oaClock.setEmployeeId(Long.valueOf(clockSummary.getUserId()));
            oaClock.setType("1");
            List<OaClock> oaClockList = oaClockMapper.selectOaClockList(oaClock);
            for (OaClock clock:oaClockList) {
                //出勤天数
                if (!"5".equals(clock.getClockState()) && !"4".equals(clock.getClockState()) && !"7".equals(clock.getClockState()) && !"8".equals(clock.getClockState()) && !"11".equals(clock.getClockState())){
                    attendance++;
                }
                //休息天数
                if ("5".equals(clock.getClockState())){
                    rest++;
                }
                //迟到次数
                if("2".equals(clock.getClockState()) || "6".equals(clock.getClockState())){
                    latenessNum++;
                    //迟到时长
                    if(clock.getDuration() != null){
                        latenessTimes = latenessTimes.add(new BigDecimal(clock.getDuration()));
                    }
                }
               //早退次数
                if("3".equals(clock.getClockState())){
                    leaveEarlyNum++;
                    //早退时长
                    if(clock.getDuration() != null){
                        leaveEarlyTimes = leaveEarlyTimes.add(new BigDecimal(clock.getDuration()));
                    }
                }
                //旷工天数
                if("7".equals(clock.getClockState()) || "8".equals(clock.getClockState()) || "11".equals(clock.getClockState())){
                    absenteeismNum++;
                }
                //出差天数
                if("9".equals(clock.getClockState())){
                    evectionNum++;
                }
                //外出天数
                if("10".equals(clock.getClockState())){
                    egressNum++;
                }
                //请假天数
                if("4".equals(clock.getClockState())){
                    leaveNum++;
                }
            }
            clockSummary.setAttendanceDays(new BigDecimal(attendance*0.5));
            clockSummary.setRestDays(new BigDecimal(rest*0.5));
            clockSummary.setLatenessNum(Long.valueOf(latenessNum));
            clockSummary.setLatenessTimes(latenessTimes);
            clockSummary.setLeaveEarlyNum(Long.valueOf(leaveEarlyNum));
            clockSummary.setLeaveEarlyTimes(leaveEarlyTimes);
            clockSummary.setAbsenteeismNum(new BigDecimal(absenteeismNum*0.5));
            clockSummary.setEvectionNum(new BigDecimal(evectionNum*0.5));
            clockSummary.setEgressNum(new BigDecimal(egressNum*0.5));
            clockSummary.setLeaveNum(new BigDecimal(leaveNum*0.5));
            oaClockSummaryMapper.updateOaClockSummary(clockSummary);
        }
        return 1;
    }

    @Override
    public List<DayClockListDTO> dailyStatistics(DayClockListDTO dayClockDTO) {
        //日期格式化
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        List<DayClockListDTO> dayClockDTOS = oaClockSummaryMapper.selectDayClockList(dayClockDTO);
        for (DayClockListDTO clockDTO:dayClockDTOS) {
            //上班打卡状态
            if(StringUtils.isNotEmpty(clockDTO.getClockInState())){
                String compare = compare(clockDTO.getClockInState());
                clockDTO.setClockInState(compare);
            }
            //下班打卡状态
            if(StringUtils.isNotEmpty(clockDTO.getClockOutState())){
                String compare = compare(clockDTO.getClockOutState());
                clockDTO.setClockOutState(compare);
            }
            //获取日期组合
            if(StringUtils.isNotEmpty(clockDTO.getClockInCreateTime())){
                try {
                    String week = DateUtils.dateToWeek(clockDTO.getClockInCreateTime().substring(0,10));
                    clockDTO.setDateTime(clockDTO.getClockInCreateTime().substring(0,10)+" "+ week);
                }catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        return dayClockDTOS;
    }

    //两个数组对比拿出不同数据
    public static <T> String compare(String key) {
        Map<String, String> map = new HashMap<>();
        map.put("1","正常");
        map.put("2","迟到");
        map.put("3","早退");
        map.put("4","请假");
        map.put("5","休息");
        map.put("6","严重迟到");
        map.put("7","迟到旷工");
        map.put("8","早退旷工");
        map.put("9","出差");
        map.put("10","外出");
        map.put("11","旷工");
        map.put("12","外勤");
        map.put("0","未打卡");
        for (Map.Entry<String, String> entry:map.entrySet()) {
              if(entry.getKey().equals(key)){
                  return entry.getValue();
              }
        }
        return "";
    }

}
