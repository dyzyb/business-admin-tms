package com.business.system.service.impl;

import com.business.common.constant.Constants;
import com.business.common.constant.UserConstants;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.exception.CustomException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.SysCompany;
import com.business.system.domain.dto.CopyProcessTemplatesDTO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.SysCompanyMapper;
import com.business.system.mapper.SysUserMapper;
import com.business.system.service.ISysCompanyService;
import com.business.system.service.ISysUserService;
import com.business.system.util.KeyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.Date;
import java.util.List;

/**
 * 公司信息Service业务层处理
 *
 * @author leslie1015
 * @date 2020-07-11
 */
@Service
public class SysCompanyServiceImpl implements ISysCompanyService
{
    @Autowired
    private SysCompanyMapper sysCompanyMapper;


    @Autowired
    private SysUserMapper userMapper;

    @Autowired
    private ISysUserService userService;

    @Value("${sys.default.pwd}")
    private String defaultPwd;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    /**
     *  0否1是
     */
    private static final Integer FLAG_NO = 0, FLAG_YES = 1;

    /**
     * 查询公司信息
     *
     * @param id 公司信息ID
     * @return 公司信息
     */
    @Override
    public SysCompany selectSysCompanyById(String id)
    {
        return sysCompanyMapper.selectSysCompanyById(id);
    }

    /**
     * 查询公司信息列表
     *
     * @param sysCompany 公司信息
     * @return 公司信息
     */
    @Override
    public List<SysCompany> selectSysCompanyList(SysCompany sysCompany)
    {
        return sysCompanyMapper.selectSysCompanyList(sysCompany);
    }

    /**
     * 新增公司信息
     *
     * @param sysCompany 公司信息
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertSysCompany(SysCompany sysCompany)
    {
        String companyId = KeyUtils.genUniqueKey();
        sysCompany.setId(companyId);
        sysCompany.setComCode(getComCode());
        sysCompany.setCreateTime(DateUtils.getNowDate());
        if (null == sysCompany.getActiveFlag()) {
            sysCompany.setActiveFlag(FLAG_YES);
        }
        if (StringUtils.isBlank(sysCompany.getPhone())) {
            throw new CustomException("电话不能为空");
        }
        if (null == sysCompany.getTempId()) {
            throw new CustomException("请选择权限模板");
        }
        if (sysCompanyMapper.insertSysCompany(sysCompany) <= 0) {
            throw new CustomException("添加公司失败");
        }
        //初始化流程定义模板
        CopyProcessTemplatesDTO copyProcessTemplatesDTO = new CopyProcessTemplatesDTO();
        copyProcessTemplatesDTO.setTenantId("1706366025829490414");//默认流程模板
        copyProcessTemplatesDTO.setNewTenantId(companyId);
        AjaxResult ajaxResult = workFlowFeignClient.copyTemplates(copyProcessTemplatesDTO);
        String result = "";
        if(!ObjectUtils.isEmpty(ajaxResult)){
            result = String.valueOf(ajaxResult.get("msg"));
            if(!result.equals(Constants.SUCCESS)){
                throw new CustomException("初始化流程定义异常");
            }
        }
        return addDefaultUser(sysCompany);
    }

    /**
     * 添加默认用户
     * @param sysCompany
     */
    private Integer addDefaultUser(SysCompany sysCompany) {
        SysUser user = new SysUser();
        user.setUserName(sysCompany.getUserName());
        user.setPhonenumber(sysCompany.getPhone());
        user.setNickName("管理员" + sysCompany.getComCode());
        user.setEmail(sysCompany.getEmail());
        user.setStatus("0");//0正常1停用
        user.setCreateBy("superAdmin");
        user.setCreateTime(new Date());
        user.setDeptId(new Long(103));
        user.setRemark("公司管理员账号");
        user.setComId(sysCompany.getId());
        user.setAdminFlag(FLAG_YES);//是否管理员0否1是
        user.setSuperAdminFlag(FLAG_NO);//是否平台超级管理员
        user.setPassword(SecurityUtils.encryptPassword(defaultPwd));
        checkUser(user);
        return userMapper.insertUser(user);
    }

    /**
     * 校验用户信息
     * @param user
     */
    private void checkUser(SysUser user) {
        if (UserConstants.NOT_UNIQUE.equals(userService.checkUserNameUnique(user)))
        {
            throw new CustomException("新增用户'" + user.getUserName() + "'失败，该登录账号已存在");
        }
        else if (UserConstants.NOT_UNIQUE.equals(userService.checkPhoneUnique(user)))
        {
            throw new CustomException("新增用户'" + user.getUserName() + "'失败，手机号码已存在");
        }
        else if (UserConstants.NOT_UNIQUE.equals(userService.checkEmailUnique(user)))
        {
            throw new CustomException("新增用户'" + user.getUserName() + "'失败，邮箱账号已存在");
        }
    }

    private synchronized Integer getComCode() {
        Integer comCode = KeyUtils.genComCode();
        Integer count = sysCompanyMapper.countByComCode(comCode);
        if ( null != count && count > 0) {
            return getComCode();
        }
        return comCode;
    }

    /**
     * 修改公司信息
     *
     * @param sysCompany 公司信息
     * @return 结果
     */
    @Override
    public int updateSysCompany(SysCompany sysCompany)
    {
        return sysCompanyMapper.updateSysCompany(sysCompany);
    }

    /**
     * 批量删除公司信息
     *
     * @param ids 需要删除的公司信息ID
     * @return 结果
     */
    @Override
    public int deleteSysCompanyByIds(String[] ids)
    {
        return sysCompanyMapper.deleteSysCompanyByIds(ids);
    }

    /**
     * 删除公司信息信息
     *
     * @param id 公司信息ID
     * @return 结果
     */
    @Override
    public int deleteSysCompanyById(String id)
    {
        return sysCompanyMapper.deleteSysCompanyById(id);
    }
}
