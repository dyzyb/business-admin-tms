package com.business.system.service;

import java.util.List;
import com.business.system.domain.OaAssigned;

/**
 * 交办数据Service接口
 *
 * @author ljb
 * @date 2023-11-21
 */
public interface IOaAssignedService
{
    /**
     * 查询交办数据
     *
     * @param id 交办数据ID
     * @return 交办数据
     */
    public OaAssigned selectOaAssignedById(String id);

    /**
     * 查询交办数据列表
     *
     * @param oaAssigned 交办数据
     * @return 交办数据集合
     */
    public List<OaAssigned> selectOaAssignedList(OaAssigned oaAssigned);

    /**
     * 新增交办数据
     *
     * @param oaAssigned 交办数据
     * @return 结果
     */
    public int insertOaAssigned(OaAssigned oaAssigned);

    /**
     * 修改交办数据
     *
     * @param oaAssigned 交办数据
     * @return 结果
     */
    public int updateOaAssigned(OaAssigned oaAssigned);

    /**
     * 批量删除交办数据
     *
     * @param ids 需要删除的交办数据ID
     * @return 结果
     */
    public int deleteOaAssignedByIds(String[] ids);

    /**
     * 删除交办数据信息
     *
     * @param id 交办数据ID
     * @return 结果
     */
    public int deleteOaAssignedById(String id);


}
