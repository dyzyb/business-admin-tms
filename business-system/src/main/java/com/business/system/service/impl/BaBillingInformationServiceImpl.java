package com.business.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.system.domain.BaBillingInformation;
import com.business.system.mapper.BaBillingInformationMapper;
import com.business.system.service.IBaBillingInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 开票信息Service业务层处理
 *
 * @author single
 * @date 2023-02-28
 */
@Service
public class BaBillingInformationServiceImpl extends ServiceImpl<BaBillingInformationMapper, BaBillingInformation> implements IBaBillingInformationService
{
    @Autowired
    private BaBillingInformationMapper baBillingInformationMapper;

    /**
     * 查询开票信息
     *
     * @param id 开票信息ID
     * @return 开票信息
     */
    @Override
    public BaBillingInformation selectBaBillingInformationById(String id)
    {
        return baBillingInformationMapper.selectBaBillingInformationById(id);
    }

    /**
     * 查询开票信息列表
     *
     * @param baBillingInformation 开票信息
     * @return 开票信息
     */
    @Override
    public List<BaBillingInformation> selectBaBillingInformationList(BaBillingInformation baBillingInformation)
    {
        return baBillingInformationMapper.selectBaBillingInformationList(baBillingInformation);
    }

    /**
     * 新增开票信息
     *
     * @param baBillingInformation 开票信息
     * @return 结果
     */
    @Override
    public int insertBaBillingInformation(BaBillingInformation baBillingInformation)
    {
        return baBillingInformationMapper.insertBaBillingInformation(baBillingInformation);
    }

    /**
     * 修改开票信息
     *
     * @param baBillingInformation 开票信息
     * @return 结果
     */
    @Override
    public int updateBaBillingInformation(BaBillingInformation baBillingInformation)
    {
        return baBillingInformationMapper.updateBaBillingInformation(baBillingInformation);
    }

    /**
     * 批量删除开票信息
     *
     * @param ids 需要删除的开票信息ID
     * @return 结果
     */
    @Override
    public int deleteBaBillingInformationByIds(String[] ids)
    {
        return baBillingInformationMapper.deleteBaBillingInformationByIds(ids);
    }

    /**
     * 删除开票信息信息
     *
     * @param id 开票信息ID
     * @return 结果
     */
    @Override
    public int deleteBaBillingInformationById(String id)
    {
        return baBillingInformationMapper.deleteBaBillingInformationById(id);
    }

}
