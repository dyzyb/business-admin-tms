package com.business.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.domain.UR;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.OaClockDTO;
import com.business.system.domain.vo.OaBeforehandStatVO;
import com.business.system.mapper.*;
import com.business.system.service.IOaBeforehandStatService;
import com.business.system.service.IOaClockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.util.*;

/**
 * 事前申请统计Service业务层处理
 *
 * @author single
 * @date 2023-11-27
 */
@Service
public class OaBeforehandStatServiceImpl extends ServiceImpl<OaBeforehandStatMapper, OaBeforehandStat> implements IOaBeforehandStatService
{

    @Autowired
    private OaLeaveMapper oaLeaveMapper;

    @Autowired
    private OaEvectionMapper oaEvectionMapper;

    @Autowired
    private OaEgressMapper oaEgressMapper;

    @Autowired
    private OaEntertainMapper oaEntertainMapper;

    @Autowired
    private OaFeeMapper oaFeeMapper;

    @Autowired
    private OaMaintenanceExpansionMapper oaMaintenanceExpansionMapper;

    @Autowired
    private OaOtherMapper oaOtherMapper;

    @Autowired
    private OaSupplyClockMapper oaSupplyClockMapper;

    @Autowired
    private OaBeforehandStatMapper oaBeforehandStatMapper;

    @Override
    public Map beforehandStat(OaBeforehandStat oaBeforehandStat) {
        Map<String, Long> beforehandMap = new HashMap();
        //统计请假申请
        OaLeave oaLeave = new OaLeave();
        oaLeave.setState(AdminCodeEnum.OALEAVE_STATUS_PASS.getCode());
        if(null != oaBeforehandStat.getEmployeeId()){
            oaLeave.setUserId(oaBeforehandStat.getEmployeeId());
        }
        oaLeave.setCreateTimeSearch(oaBeforehandStat.getCreateTimeSearch());
        //租户ID
        oaLeave.setTenantId(SecurityUtils.getCurrComId());
        beforehandMap.put("oaLeave", oaLeaveMapper.countOaLeaveList(oaLeave));
        //统计出差申请
        OaEvection oaEvection = new OaEvection();
        oaEvection.setState(AdminCodeEnum.OAEVECTION_STATUS_PASS.getCode());
        if(null != oaBeforehandStat.getEmployeeId()) {
            oaEvection.setUserId(oaBeforehandStat.getEmployeeId());
        }
        oaEvection.setCreateTimeSearch(oaBeforehandStat.getCreateTimeSearch());
        //租户ID
        oaEvection.setTenantId(SecurityUtils.getCurrComId());
        beforehandMap.put("oaEvection", oaEvectionMapper.countOaEvectionList(oaEvection));
        //统计外出申请
        OaEgress oaEgress = new OaEgress();
        oaEgress.setState(AdminCodeEnum.OAEGRESS_STATUS_PASS.getCode());
        if(null != oaBeforehandStat.getEmployeeId()) {
            oaEgress.setUserId(oaBeforehandStat.getEmployeeId());
        }
        oaEgress.setCreateTimeSearch(oaBeforehandStat.getCreateTimeSearch());
        //租户ID
        oaEgress.setTenantId(SecurityUtils.getCurrComId());
        beforehandMap.put("oaEgress", oaEgressMapper.countOaEgressList(oaEgress));
        //统计招待申请
        OaEntertain oaEntertain = new OaEntertain();
        oaEntertain.setState(AdminCodeEnum.OAENTERTAIN_STATUS_PASS.getCode());
        if(null != oaBeforehandStat.getEmployeeId()) {
            oaEntertain.setUserId(oaBeforehandStat.getEmployeeId());
        }
        oaEntertain.setCreateTimeSearch(oaBeforehandStat.getCreateTimeSearch());
        //租户ID
        oaEntertain.setTenantId(SecurityUtils.getCurrComId());
        beforehandMap.put("oaEntertain", oaEntertainMapper.countOaEntertainList(oaEntertain));
        //统计费用申请
        OaFee oaFee = new OaFee();
        oaFee.setState(AdminCodeEnum.OAFEE_STATUS_PASS.getCode());
        if(null != oaBeforehandStat.getEmployeeId()) {
            oaFee.setUserId(oaBeforehandStat.getEmployeeId());
        }
        oaFee.setCreateTimeSearch(oaBeforehandStat.getCreateTimeSearch());
        //租户ID
        oaFee.setTenantId(SecurityUtils.getCurrComId());
        beforehandMap.put("oaFee", oaFeeMapper.countOaFeeList(oaFee));
        //统计业务维护拓展申请
        OaMaintenanceExpansion oaMaintenanceExpansion = new OaMaintenanceExpansion();
        oaMaintenanceExpansion.setState(AdminCodeEnum.OAMAINTENANCEEXPANSION_STATUS_PASS.getCode());
        if(null != oaBeforehandStat.getEmployeeId()) {
            oaMaintenanceExpansion.setUserId(oaBeforehandStat.getEmployeeId());
        }
        oaMaintenanceExpansion.setCreateTimeSearch(oaBeforehandStat.getCreateTimeSearch());
        //租户ID
        oaMaintenanceExpansion.setTenantId(SecurityUtils.getCurrComId());
        beforehandMap.put("oaMaintenanceExpansion", oaMaintenanceExpansionMapper.countOaMaintenanceExpansionList(oaMaintenanceExpansion));
        //统计其他申请
        OaOther oaOther = new OaOther();
        oaOther.setState(AdminCodeEnum.OAOTHER_STATUS_PASS.getCode());
        if(null != oaBeforehandStat.getEmployeeId()) {
            oaOther.setUserId(oaBeforehandStat.getEmployeeId());
        }
        oaOther.setCreateTimeSearch(oaBeforehandStat.getCreateTimeSearch());
        //租户ID
        oaOther.setTenantId(SecurityUtils.getCurrComId());
        beforehandMap.put("oaOther", oaOtherMapper.countOaOtherList(oaOther));
        //统计补卡申请
        OaSupplyClock oaSupplyClock = new OaSupplyClock();
        oaSupplyClock.setState(AdminCodeEnum.OASUPPLYCLOCK_STATUS_PASS.getCode());
        if(null != oaBeforehandStat.getEmployeeId()) {
            oaSupplyClock.setUserId(oaBeforehandStat.getEmployeeId());
        }
        oaSupplyClock.setCreateTimeSearch(oaBeforehandStat.getCreateTimeSearch());
        //租户ID
        oaSupplyClock.setTenantId(SecurityUtils.getCurrComId());
        beforehandMap.put("supplyClock", oaSupplyClockMapper.countOaSupplyClockList(oaSupplyClock));
        return beforehandMap;
    }

    /**
     * 申请部门分布
     */
    @Override
    public List<OaBeforehandStatVO> applyDeptStat(OaBeforehandStat oaBeforehandStat) {
        List<OaBeforehandStatVO> oaBeforehandStatVOS = oaBeforehandStatMapper.applyDeptStat(oaBeforehandStat);
        List<OaBeforehandStatVO> beforehandStatVOS = new ArrayList<>();
        OaBeforehandStatVO oaBeforehandStatVO = new OaBeforehandStatVO();
        long count = 0L;
        if(!CollectionUtils.isEmpty(oaBeforehandStatVOS)){
            for(int i = 0; i < oaBeforehandStatVOS.size(); i++){
                if(i <= 6){
                    beforehandStatVOS.add(oaBeforehandStatVOS.get(i));
                } else {
                    if(!ObjectUtils.isEmpty(oaBeforehandStatVOS.get(i))){
                        count = count + oaBeforehandStatVOS.get(i).getCount();
                    }
                }
            }
        }
        oaBeforehandStatVO.setDeptName("其他部门");
        oaBeforehandStatVO.setCount(count);
        beforehandStatVOS.add(oaBeforehandStatVO);

        return beforehandStatVOS;
    }
}
