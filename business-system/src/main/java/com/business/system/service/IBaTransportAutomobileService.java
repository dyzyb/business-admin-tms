package com.business.system.service;

import com.alibaba.fastjson.JSONObject;
import com.business.common.core.domain.UR;
import com.business.system.domain.BaShippingOrder;
import com.business.system.domain.BaTransportAutomobile;

import java.util.List;

/**
 * 汽车运输/货源Service接口
 *
 * @author ljb
 * @date 2023-02-17
 */
public interface IBaTransportAutomobileService
{
    /**
     * 查询汽车运输/货源
     *
     * @param id 汽车运输/货源ID
     * @return 汽车运输/货源
     */
    public BaTransportAutomobile selectBaTransportAutomobileById(String id);

    /**
     * 查询汽车运输/货源列表
     *
     * @param baTransportAutomobile 汽车运输/货源
     * @return 汽车运输/货源集合
     */
    public List<BaTransportAutomobile> selectBaTransportAutomobileList(BaTransportAutomobile baTransportAutomobile);

    //public List<sourceGoodsDTO> sourceGoodsDTOList(BaTransportAutomobile baTransportAutomobile);


    /**
     * 新增汽车运输/货源
     *
     * @param baTransportAutomobile 汽车运输/货源
     * @return 结果
     */
    public int insertBaTransportAutomobile(BaTransportAutomobile baTransportAutomobile);

    /**
     * 昕科推送货源信息
     * @param transportAutomobile
     * @return
     */
    public int insertAutomobile(JSONObject transportAutomobile);

    /**
     * 修改汽车运输/货源
     *
     * @param baTransportAutomobile 汽车运输/货源
     * @return 结果
     */
    public int updateBaTransportAutomobile(BaTransportAutomobile baTransportAutomobile);

    /**
     * 昕科修改货源信息
     * @param transportAutomobile
     * @return
     */
    public int updateAutomobile(JSONObject transportAutomobile);

    /**
     * 批量删除汽车运输/货源
     *
     * @param ids 需要删除的汽车运输/货源ID
     * @return 结果
     */
    public int deleteBaTransportAutomobileByIds(String[] ids);

    /**
     * 删除汽车运输/货源信息
     *
     * @param id 汽车运输/货源ID
     * @return 结果
     */
    public int deleteBaTransportAutomobileById(String id);

    /**
     * 撤销按钮
     * @param id
     * @return
     */
    public int revoke(String id);

    /**
     * 发货
     * @param baShippingOrder
     * @return
     */
    public int delivery(BaShippingOrder baShippingOrder);

    /**
     * 收货
     * @param baTransportAutomobile
     * @return
     */
    public int receipt(BaTransportAutomobile baTransportAutomobile);

    /**
     * 获取司机接单二维码
     * @param baTransportAutomobile
     * @return
     */
    public int qrCode(BaTransportAutomobile baTransportAutomobile);

    /**
     * 签收
     */
    public List<BaShippingOrder> shippingList(BaShippingOrder baShippingOrder);

    /**
     * 货转按钮
     * @param baTransportAutomobile
     * @return
     */
    public int transfer(BaTransportAutomobile baTransportAutomobile);

    /**
     * 获取货主信息
     */
    public UR gain();

    /**
     * 货源下拉框
     * @return
     */
    public List<BaTransportAutomobile> listAutomobile();

    /**
     * 线下新增货源信息
     */
    public int insertSourceOfGoods(BaTransportAutomobile baTransportAutomobile);

    /**
     * 出入库下拉
     * @param baTransportAutomobile
     * @return
     */
    //public List<sourceGoodsDTO> listSourceGoods(BaTransportAutomobile baTransportAutomobile);

    /**
     * 出入库下拉
     * @param baTransportAutomobile
     * @return
     */
    //public List<sourceGoodsDTO> sampleList(BaTransportAutomobile baTransportAutomobile);



}
