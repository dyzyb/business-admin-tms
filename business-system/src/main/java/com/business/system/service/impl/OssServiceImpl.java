package com.business.system.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.*;
import com.business.system.domain.FileUploadResult;
import com.business.system.service.OssService;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.entity.ContentType;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.zip.Adler32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 文件上传ServiceImpl
 *
 * @author jiahe
 * @date 2020.7
 */
@Service
public class OssServiceImpl implements OssService {


    //允许上传的文件格式
    private static final String[] IMAGE_TYPE = new String[]{".bmp", ".jpg",".doc",".docx",".htm",".html",
            ".jpeg", ".gif", ".png", ".xlsx", ".xls", ".pdf", ".txt"};
    //非客阿里云
    /*String endpoint = "oss-cn-qingdao.aliyuncs.com";
    String accessKeyId = "LTAI4FjZTuSNry6qCs8fzrCB";
    String accessKeySecret = "vnx8Jt3oqlMbzpygvPhqH47HE28oCg";
    String bucketName = "feikeyoujia";*/

    //金谷阿里云
    String endpoint = "oss-cn-huhehaote.aliyuncs.com";
    String accessKeyId = "LTAI5t6vud5rdp72ycv1hUgd";
    String accessKeySecret = "GDXnZk5RAw6YbodSaHUqeSZBKx0Fhh";
    String bucketName = "zhongnengxinlian";

    //上传文件到oss
    @Override
    public FileUploadResult uploadFile(MultipartFile file, HttpServletRequest request) {
        //激活oss服务端
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        //校验图片格式
        boolean isLegal = false;
        for (String type : IMAGE_TYPE) {
            if (StringUtils.endsWithIgnoreCase(file.getOriginalFilename(),
                    type)) {
                isLegal = true;
                break;
            }
        }
        //封装Result对象，并且将文件的byte数组放置到result对象中
        FileUploadResult fileUploadResult = new FileUploadResult();
        if (!isLegal) {
            fileUploadResult.setStatus("error");
            return fileUploadResult;
        }
        //文件新路径
        String fileName = file.getOriginalFilename();
        String filePath = getFilePath(fileName,request);
        // 上传到阿里云
        try {
            ossClient.putObject(bucketName, filePath, new
                    ByteArrayInputStream(file.getBytes()));
            ossClient.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
            //上传失败
            fileUploadResult.setStatus("error");
            return fileUploadResult;
        }
        fileUploadResult.setStatus("done");
        fileUploadResult.setResponse("success");
        //this.aliyunConfig.getUrlPrefix() + filePath 文件路径需要保存到数据库
        fileUploadResult.setName(filePath);
        fileUploadResult.setUid(String.valueOf(System.currentTimeMillis()));
        return fileUploadResult;
    }

    //上传app
    @Override
    public FileUploadResult uploadApp(MultipartFile file, HttpServletRequest request) {
        //激活oss服务端
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        //封装Result对象，并且将文件的byte数组放置到result对象中
        FileUploadResult fileUploadResult = new FileUploadResult();
        //文件原始名称
        String fileName = file.getOriginalFilename();
//        System.out.println(fileName);
        //文件新路径
        String filePath = "app/"
                + fileName ;
//        System.out.println(filePath);
        // 上传到阿里云
        try {
            ossClient.putObject(bucketName, filePath, new
                    ByteArrayInputStream(file.getBytes()));
            ossClient.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
            //上传失败
            fileUploadResult.setStatus("error");
            return fileUploadResult;
        }
        fileUploadResult.setStatus("done");
        fileUploadResult.setResponse("success");
        //拼接下载路径保存到结果集里
        //String downLoadName = "https://feikeyoujia.oss-cn-qingdao.aliyuncs.com/"+filePath;
        String downLoadName = "https://zhongnengxinlian.oss-cn-huhehaote.aliyuncs.com/"+filePath;
        fileUploadResult.setName(downLoadName);
        fileUploadResult.setUid(String.valueOf(System.currentTimeMillis()));
        return fileUploadResult;
    }

    //上传图片
    @Override
    public FileUploadResult Certification(MultipartFile file, String type, HttpServletRequest request) {
        //激活oss服务端
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        //校验图片格式
        boolean isLegal = false;
        for (String type1 : IMAGE_TYPE) {
            if (StringUtils.endsWithIgnoreCase(file.getOriginalFilename(),
                    type1)) {
                isLegal = true;
                break;
            }
        }
        file.getContentType();
        ContentType contentTypeTextUtf8 = ContentType.create( file.getContentType(), StandardCharsets.UTF_8.toString());
        //System.out.println(contentTypeTextUtf8);
        file.hashCode();
        InputStreamReader reader;
        String buffer = "";
        if(file.getOriginalFilename().endsWith(".txt")){
            try {
                reader = new InputStreamReader((file.getInputStream()),"utf-8");
                BufferedReader bufferedReader = new BufferedReader(reader);
                buffer = bufferedReader.readLine();
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //封装Result对象，并且将文件的byte数组放置到result对象中
        FileUploadResult fileUploadResult = new FileUploadResult();
        if (!isLegal) {
            fileUploadResult.setStatus("error");
            return fileUploadResult;
        }
        //文件新路径
        String fileName = file.getOriginalFilename();
        String fileType = type+"/";
        String filePath = fileType + fileName.substring(0,fileName.lastIndexOf(".")) +
                RandomUtils.nextInt(100, 9999) + "." +
                StringUtils.substringAfterLast(fileName, ".");
        //String filePath = fileType + fileName;
        // 上传到阿里云
        try {
            //txt文件特殊处理
            if(file.getOriginalFilename().endsWith(".txt")){
                //设置HTTP头
                ObjectMetadata objectMetadata = new ObjectMetadata();
                objectMetadata.setContentType("txt/plain;charset=utf-8");
                ossClient.putObject(bucketName, filePath, new
                        ByteArrayInputStream(buffer.getBytes()),objectMetadata);
            }else {
                ossClient.putObject(bucketName, filePath, new
                        ByteArrayInputStream(file.getBytes()));
            }
            ossClient.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
            //上传失败
            fileUploadResult.setStatus("error");
            return fileUploadResult;
        }
        fileUploadResult.setStatus("done");
        fileUploadResult.setResponse("success");
        //String downLoadName = "https://feikeyoujia.oss-cn-qingdao.aliyuncs.com/"+filePath;
        String downLoadName = "https://zhongnengxinlian.oss-cn-huhehaote.aliyuncs.com/"+filePath;
        fileUploadResult.setName(downLoadName);
        fileUploadResult.setUid(String.valueOf(System.currentTimeMillis()));
        return fileUploadResult;
    }
    /*
     * 生成路径以及文件名
     * 例如:
     * images/2019/04/28/15564277465972939.jpg
     */
    private String getFilePath(String sourceFileName, HttpServletRequest request) {
        return "member/" +"/" + System.currentTimeMillis() +
                RandomUtils.nextInt(100, 9999) + "." +
                StringUtils.substringAfterLast(sourceFileName, ".");
    }

    /*
     * 查看文件列表
     */
    public List<OSSObjectSummary> list() {
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        // 设置最大个数。
        final int maxKeys = 200;
        // 列举文件。
        ObjectListing objectListing = ossClient.listObjects(new ListObjectsRequest(bucketName).withMaxKeys(maxKeys));
        ossClient.shutdown();
        return objectListing.getObjectSummaries();
    }

    /*
     * 删除文件
     */
    public FileUploadResult delete(String objectName) {
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        // 根据BucketName,objectName删除文件
        ossClient.deleteObject(bucketName, objectName);
        FileUploadResult fileUploadResult = new FileUploadResult();
        fileUploadResult.setName(objectName);
        fileUploadResult.setStatus("removed");
        fileUploadResult.setResponse("success");
        ossClient.shutdown();
        return fileUploadResult;
    }

    //下载文件
    public void downloadOssFile(OutputStream os, String objectName) throws IOException {
        String path = "E:\\";
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        // ossObject包含文件所在的存储空间名称、文件名称、文件元信息以及一个输入流。
        OSSObject ossObject = ossClient.getObject(bucketName, objectName);
        // 读取文件内容。
        BufferedInputStream in = new BufferedInputStream(ossObject.getObjectContent());
        BufferedOutputStream out = new BufferedOutputStream(os);
        byte[] buffer = new byte[1024];
        int lenght = 0;
        File tempFile = new File(path);
        if (!tempFile.exists()) {
            tempFile.mkdirs();
        }
        String[] split = objectName.split("/");
        System.out.println(split.length-1);
        os = new FileOutputStream(tempFile.getPath() + File.separator + split[split.length - 1]);
        while ((lenght = in.read(buffer)) != -1) {
            out.write(buffer, 0, lenght);
            os.write(buffer,0,lenght);
        }
        out.flush();
        os.close();
        out.close();
        in.close();
        ossClient.shutdown();
    }

    /**
     * Title: getZipFromOSSByPaths
     * Description: 从OSS批量下载文件存入压缩包， 分别传入压缩包名(zipName)，临时压缩包文件(zipFile)，
     * 				http请求request，http请求response，以及需要下载的文件的地址(imagePths)
     * Date: 2019年7月29日
     * boolean
     */
    public void getZipFromOSSByPaths(String zipName, File zipFile, HttpServletRequest request,
                                            HttpServletResponse response, List<String> paths) throws Exception {

        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);

        FileOutputStream fos = new FileOutputStream(zipFile);

        // 作用是为任何OutputStream产生校验和
        // 第一个参数是制定产生校验和的输出流，第二个参数是指定Checksum的类型 （Adler32（较快）和CRC32两种）
        CheckedOutputStream cos = new CheckedOutputStream(fos, new Adler32());

        // 用于将数据压缩成zip文件格式
        ZipOutputStream zos = new ZipOutputStream(cos);

        // 循环根据路径从OSS获得对象，存入临时文件zip中
        for (String path : paths) {

            // 根据path 和 bucket 从OSS获取对象
            OSSObject ossObject = ossClient.getObject(bucketName, path);

            InputStream is = ossObject.getObjectContent();

            // 将文件放入zip中，并命名不能重复
            zos.putNextEntry(new ZipEntry(path));

            // 向压缩文件中写数据
            int len = 0;
            while ((len = is.read()) != -1) {
                zos.write(len);
            }
            is.close();
            // 当前文件写完，关闭
            zos.closeEntry();
        }
        zos.close();

        String header = request.getHeader("User-Agent").toUpperCase();
        if (header.contains("MSIE") || header.contains("TRIDENT") || header.contains("EDGE")) {
            // IE下载文件名空格变+号问题
            zipName = URLEncoder.encode(zipName, "utf-8");
            zipName = zipName.replace("+", "%20");
        } else {
            zipName = new String(zipName.getBytes(), "ISO8859-1");
        }


        // 下载流封装
        FileInputStream fis = new FileInputStream(zipFile);
        BufferedInputStream bis = new BufferedInputStream(fis);
        BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());

        // 下载zip文件
        FileCopyUtils.copy(bis, bos);

        // 关闭流
        fis.close();
        bis.close();
        bos.close();

        ossClient.shutdown();
        // 删除临时zip文件
        zipFile.delete();

    }

}
