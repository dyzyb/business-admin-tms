package com.business.system.service.workflow.impl;

import com.business.common.enums.AdminCodeEnum;
import com.business.system.domain.OaOther;
import com.business.system.domain.OaSupplyClock;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import com.business.system.service.workflow.IOaOtherApprovalService;
import com.business.system.service.workflow.IOaSupplyClockApprovalService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 补卡申请审批结果:审批通过
 */
@Service("oaSupplyClockApprovalContent:processApproveResult:pass")
@Slf4j
public class OaSupplyClockApprovalAgreeServiceImpl extends IOaSupplyClockApprovalService implements IWorkflowUpdateStatusService {

    @Override
    @Transactional
    public Object acceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        OaSupplyClock oaSupplyClock = this.checkValidate(approvalWorkflowDTO);
        Object result = this.doAcceptApprovalResult(approvalWorkflowDTO, AdminCodeEnum.OASUPPLYCLOCK_STATUS_PASS.getCode());
        return result;
    }
}
