package com.business.system.service;


import com.business.system.domain.JrAppVersion;

import java.util.List;

/**
 * app版本Service接口
 *
 * @author single
 * @date 2022-04-27
 */
public interface IJrAppVersionService
{
    /**
     * 查询app版本
     *
     * @param versionId app版本ID
     * @return app版本
     */
    public JrAppVersion selectJrAppVersionById(String versionId);

    /**
     * 查询app版本列表
     *
     * @param jrAppVersion app版本
     * @return app版本集合
     */
    public List<JrAppVersion> selectJrAppVersionList(JrAppVersion jrAppVersion);

    /**
     * 新增app版本
     *
     * @param jrAppVersion app版本
     * @return 结果
     */
    public int insertJrAppVersion(JrAppVersion jrAppVersion);

    /**
     * 修改app版本
     *
     * @param jrAppVersion app版本
     * @return 结果
     */
    public int updateJrAppVersion(JrAppVersion jrAppVersion);

    /**
     * 批量删除app版本
     *
     * @param versionIds 需要删除的app版本ID
     * @return 结果
     */
    public int deleteJrAppVersionByIds(String[] versionIds);

    /**
     * 删除app版本信息
     *
     * @param versionId app版本ID
     * @return 结果
     */
    public int deleteJrAppVersionById(String versionId);


}
