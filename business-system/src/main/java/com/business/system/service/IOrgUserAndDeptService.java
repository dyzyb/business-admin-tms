package com.business.system.service;

import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysRole;
import com.business.system.domain.dto.SysDeptDTO;
import com.business.system.domain.dto.SysRoleDTO;
import com.business.system.domain.dto.SysUserDTO;
import com.business.system.domain.vo.AddressBook;
import com.business.system.domain.vo.OrgTreeVo;

import java.util.List;

/**
 * 封装工作流审批节点组织架构树
 *
 * @author js
 */
public interface IOrgUserAndDeptService
{
    /**
     * 查询组织架构树
     * @param sysDeptDTO 部门
     * @return 组织架构树数据
     */
    List<OrgTreeVo> getOrgTreeData(SysDeptDTO sysDeptDTO);

    /**
     * 模糊搜索用户
     * @param sysUserDTO 用户
     * @return 匹配到的用户
     */
    List<OrgTreeVo> getOrgTreeUser(SysUserDTO sysUserDTO);

    /**
     * 查询组织架构树
     * @param sysDeptDTO 部门
     * @return 组织架构树数据
     */
    List<OrgTreeVo> getOrgTree(SysDeptDTO sysDeptDTO);

    /**
     * 查询客户信息
     * @param
     * @return 组织架构树数据
     */
    //List<AddressBook> getAddressBook();

    /**
     * 查询组织架构树
     * @param sysDeptDTO 部门
     * @return 组织架构树数据
     */
    List<OrgTreeVo> getAppOrgTree(SysDeptDTO sysDeptDTO);


    /**
     * 查询角色列表
     * @param sysRole
     * @return
     */
    List<OrgTreeVo> getRoleTreeData(SysRole sysRole);
}
