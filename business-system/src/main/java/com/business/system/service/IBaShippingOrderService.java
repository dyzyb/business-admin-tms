package com.business.system.service;

import com.alibaba.fastjson.JSONObject;
import com.business.system.domain.BaShippingOrder;
import com.business.system.domain.vo.BaShippingOrderGoodsProportionVO;

import java.util.List;
import java.util.Map;

/**
 * 发运单Service接口
 *
 * @author single
 * @date 2023-02-18
 */
public interface IBaShippingOrderService
{
    /**
     * 查询发运单
     *
     * @param id 发运单ID
     * @return 发运单
     */
    public BaShippingOrder selectBaShippingOrderById(String id);

    /**
     * 查询发运单列表
     *
     * @param baShippingOrder 发运单
     * @return 发运单集合
     */
    public List<BaShippingOrder> selectBaShippingOrderList(BaShippingOrder baShippingOrder);

    /**
     * 全局运单接口
     * @param baShippingOrder
     * @return
     */
    //public List<waybillDTO> waybillDTOList(BaShippingOrder baShippingOrder);

    /**
     * 新增发运单
     *
     * @param baShippingOrder 发运单
     * @return 结果
     */
    public int insertBaShippingOrder(BaShippingOrder baShippingOrder);

    /**
     * 中储新增发运单
     *
     * @param object 发运单
     * @return 结果
     */
    public int insertOrder(JSONObject object);

    /**
     * 修改发运单
     *
     * @param baShippingOrder 发运单
     * @return 结果
     */
    public int updateBaShippingOrder(BaShippingOrder baShippingOrder);

    /**
     *
     * @param shippingOrder
     * @return
     */
    public int updateShippingOrder(JSONObject shippingOrder);

    /**
     * 批量删除发运单
     *
     * @param ids 需要删除的发运单ID
     * @return 结果
     */
    public int deleteBaShippingOrderByIds(String[] ids);

    /**
     * 删除发运单信息
     *
     * @param id 发运单ID
     * @return 结果
     */
    public int deleteBaShippingOrderById(String id);

    /**
     *签收
     * @return
     */
    public int signFor(String[] ids);

    /**
     * 发运单签收回调
     */
    int editSignForCallback(List<String> transNos);

    /**
     * 统计最近个日期网货平台（在途车辆、装车车辆、卸车车辆）对比数据
     * @return
     */
    public Map recentCars();

    /**
     * 拉运货物占比
     */
    public List<BaShippingOrderGoodsProportionVO> selectGoodsProportion();
}
