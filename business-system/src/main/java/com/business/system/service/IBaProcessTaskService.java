package com.business.system.service;

import com.alibaba.fastjson2.JSONObject;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.PageDomain;
import com.business.common.core.page.TableDataInfo;
import com.business.common.core.page.TableSupport;
import com.business.system.domain.BaProcessInstanceRelated;
import com.business.system.domain.dto.*;
import com.business.system.domain.vo.BaProcessBusinessInstanceRelatedVO;
import com.business.system.domain.vo.ProcessTaskVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.Map;

/**
 * @Author: js
 * @Description: 审批任务Service接口
 * @Date: 2022/12/6 13:42
 */
public interface IBaProcessTaskService {

    /**
     * 查询所有我的待办审批任务列表
     * @param queryDTO
     * @return
     */
    TableDataInfo listRuntimeProcessTask(ProcessTaskQueryDTO queryDTO);

    /**
     * 获取首页待办审批任务列表
     * @param queryDTO
     * @return
     */
    TableDataInfo listHomeRuntimeProcessTask(ProcessTaskQueryDTO queryDTO);

    /**
     * 办理待办任务
     * @param processTaskDTO
     */
    void completeTask(ProcessTaskDTO processTaskDTO);

    /**
     * 设置发起人ID
     * @param queryDTO
     */
    void setStartUserId(ProcessTaskQueryDTO queryDTO);

    /**
     * 查询所有我的已办审批任务列表
     * @param queryDTO
     * @return
     */
    TableDataInfo listHistoryProcessTask(ProcessTaskQueryDTO queryDTO);

    /**
     * 统计各个业务我的待办总数
     * @return
     */
    Map<String, Integer> countRuntimeTask();

    /**
     * 统计某个业务我的已办总数
     * @return
     */
    Map<String, Integer> countHistoryProcessTask();

    /**
     * 统计某个业务我的已完结总数
     * @return
     */
    Map<String, Integer> countHistoryProcessIsEndTask();

    /**
     * 查询我的申请
     * @param queryDTO
     * @return
     */
    List<BaProcessInstanceRelated> listMyApply(BaProcessInstanceRelatedQueryDTO queryDTO);

    /**
     * 统计我的待办任务
     * @param queryDTO
     * @return
     */
    long countRuntimeTask(ProcessTaskQueryDTO queryDTO);

    /**
     * 统计我的已办任务
     * @param queryDTO
     * @return
     */
    int countHistoryProcess(ProcessTaskQueryDTO queryDTO);


    public TableDataInfo myApplyList(BaProcessInstanceRelatedQueryDTO queryDTO);

    /**
     * 分页查询抄送给我的
     * @param queryDTO
     * @return
     */
    public TableDataInfo listRuntimeProcessTaskSend(ProcessTaskQueryDTO queryDTO);

    /**
     * 根据业务获取名称
     */
    String getBusiName(String businessType, JSONObject jsonObject);

    /**
     * 驳回
     * @param processTaskDTO
     * @return
     */
    void refuse(ProcessTaskDTO processTaskDTO);

    TableDataInfo supervisingHistoryProcess(ProcessTaskQueryDTO queryDTO);

    /**
     * APP查询所有我的待办审批任务列表
     * @param queryDTO
     * @return
     */
    TableDataInfo listRuntimeProcessTaskApp(ProcessTaskQueryDTO queryDTO);

    /**
     * APP查询我的已办任务
     * @param queryDTO
     * @return
     */
    TableDataInfo listHistoryProcessTaskApp(ProcessTaskQueryDTO queryDTO);

    /**
     * APP分页查询抄送给我的
     * @param queryDTO
     * @return
     */
    TableDataInfo listRuntimeProcessTaskSendApp(ProcessTaskQueryDTO queryDTO);
}
