package com.business.system.service;

import java.util.List;
import com.business.system.domain.Districts;

/**
 * 地区Service接口
 *
 * @author ljb
 * @date 2022-11-28
 */
public interface IDistrictsService
{
    /**
     * 查询地区
     *
     * @param id 地区ID
     * @return 地区
     */
    public Districts selectDistrictsById(Integer id);

    /**
     * 查询地区列表
     *
     * @param districts 地区
     * @return 地区集合
     */
    public List<Districts> selectDistrictsList(Districts districts);

    List<Districts> getAllList();

    /**
     * 新增地区
     *
     * @param districts 地区
     * @return 结果
     */
    public int insertDistricts(Districts districts);

    /**
     * 修改地区
     *
     * @param districts 地区
     * @return 结果
     */
    public int updateDistricts(Districts districts);

    /**
     * 批量删除地区
     *
     * @param ids 需要删除的地区ID
     * @return 结果
     */
    public int deleteDistrictsByIds(Integer[] ids);

    /**
     * 删除地区信息
     *
     * @param id 地区ID
     * @return 结果
     */
    public int deleteDistrictsById(Integer id);


}
