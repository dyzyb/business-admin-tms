package com.business.system.service;

import com.business.system.domain.BaProcessDefinitionRelated;
import com.business.system.domain.dto.BaProcessDefinitionRelatedDeleteDTO;
import com.business.system.domain.dto.BaProcessDefinitionRelatedQueryDTO;
import com.business.system.domain.dto.BaProcessDefinitionRelatedQueryOneDTO;
import com.business.system.domain.dto.BaProcessDefinitionRelatedSaveDTO;
import com.business.system.domain.vo.BaProcessDefinitionRelatedVO;

import java.util.List;

/**
 * @Author: js
 * @Description: 业务与流程定义关联关系接口
 * @date: 2022/12/1 17:44
 */
public interface IBaProcessDefinitionRelatedService {

    /**
     * 按条件查询业务与流程定义关联关系列表
     *
     * @param businessType 业务类型
     * @return 业务与流程定义关联关系集合
     */
    List<BaProcessDefinitionRelated> listProcessBusinessDefinitionByType(String businessType, String tenantId);

    /**
     * 获取审批流程配置列表
     * @param queryDTO 查询对象
     * @return ProcessDefinitionRelatedVO 流程配制对象
     */
    List<BaProcessDefinitionRelatedVO> listProcessDefinitionPage(BaProcessDefinitionRelatedQueryDTO queryDTO);

    /**
     * 新增流程配置
     * @param saveDTO
     */
    int saveProcessDefinition(BaProcessDefinitionRelatedSaveDTO saveDTO);

    /**
     * 批量删除
     * @param deleteDTO
     */
    int deleteProcessDefinition(BaProcessDefinitionRelatedDeleteDTO deleteDTO);

    /**
     * 删除用印信息
     *
     * @param id 用印ID
     * @return 结果
     */
    public int deleteProcessDefinitionById(String id);

    /**
     * 根据流程类型查询流程id
     * @param businessType
     * @return
     */
    String getFlowId(String businessType, String tenantId);

    /**
     * 根据模板ID查询模板数据
     * @param templateId
     * @return
     */
    BaProcessDefinitionRelatedVO getProcessDefinitionRelated(String templateId);
}
