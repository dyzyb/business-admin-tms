package com.business.system.service;

import java.util.List;
import com.business.system.domain.OaClockRecord;

/**
 * 打卡记录Service接口
 *
 * @author single
 * @date 2023-11-27
 */
public interface IOaClockRecordService
{
    /**
     * 查询打卡记录
     *
     * @param id 打卡记录ID
     * @return 打卡记录
     */
    public OaClockRecord selectOaClockRecordById(String id);

    /**
     * 查询打卡记录列表
     *
     * @param oaClockRecord 打卡记录
     * @return 打卡记录集合
     */
    public List<OaClockRecord> selectOaClockRecordList(OaClockRecord oaClockRecord);

    /**
     * 新增打卡记录
     *
     * @param oaClockRecord 打卡记录
     * @return 结果
     */
    public int insertOaClockRecord(OaClockRecord oaClockRecord);

    /**
     * 修改打卡记录
     *
     * @param oaClockRecord 打卡记录
     * @return 结果
     */
    public int updateOaClockRecord(OaClockRecord oaClockRecord);

    /**
     * 批量删除打卡记录
     *
     * @param ids 需要删除的打卡记录ID
     * @return 结果
     */
    public int deleteOaClockRecordByIds(String[] ids);

    /**
     * 删除打卡记录信息
     *
     * @param id 打卡记录ID
     * @return 结果
     */
    public int deleteOaClockRecordById(String id);


}
