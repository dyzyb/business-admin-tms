package com.business.system.service.impl;

import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysRole;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.SysPost;
import com.business.system.domain.dto.SysDeptDTO;
import com.business.system.domain.dto.SysUserDTO;
import com.business.system.domain.vo.AddressBook;
import com.business.system.domain.vo.OrgTreeVo;
import com.business.system.mapper.SysDeptMapper;
import com.business.system.mapper.SysPostMapper;
import com.business.system.mapper.SysRoleMapper;
import com.business.system.mapper.SysUserMapper;
import com.business.system.service.IOrgUserAndDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 部门管理 服务实现
 *
 * @author js
 */
@Service
public class OrgUserAndDeptServiceImpl implements IOrgUserAndDeptService
{
    @Resource
    private SysDeptMapper sysDeptMapper;
    @Resource
    private SysUserMapper sysUserMapper;
    @Resource
    private SysRoleMapper sysRoleMapper;
    @Resource
    private SysPostMapper sysPostMapper;
    /*@Resource
    private BaSupplierMapper baSupplierMapper;
    @Resource
    private BaEnterpriseMapper baEnterpriseMapper;
    @Autowired
    private BaCompanyGroupMapper baCompanyGroupMapper;
    @Autowired
    private BaCompanyMapper companyMapper;*/
    /**
     * 查询组织架构树
     * @param sysDeptDTO 部门
     * @return 组织架构树数据
     */
    @Override
    public List<OrgTreeVo> getOrgTreeData(SysDeptDTO sysDeptDTO) {
        List<OrgTreeVo> orgTreeVos = new LinkedList<>();
        SysDept sysDept = new SysDept();
        SysUser sysUser = new SysUser();
        if(!ObjectUtils.isEmpty(sysDeptDTO.getParentId())){
            BeanUtils.copyBeanProp(sysDept, sysDeptDTO);
            sysDept.setTenantId(SecurityUtils.getCurrComId());
            List<SysDept> sysDepts = sysDeptMapper.selectTreeDeptList(sysDept);
            if(!CollectionUtils.isEmpty(sysDepts)){
                sysDepts.forEach(dept -> {
                    orgTreeVos.add(OrgTreeVo.builder()
                            .id(dept.getDeptId())
                            .name(dept.getDeptName())
                            .selected(false)
                            .type("dept")
                            .build());
                });
            }
            sysUser.setDeptId(sysDeptDTO.getParentId());
            sysUser.setComId(SecurityUtils.getCurrComId());
            List<SysUser> sysUsers = sysUserMapper.selectTreeUserList(sysUser);
            if(!CollectionUtils.isEmpty(sysUsers)) {
                sysUsers.forEach(user -> {
                    orgTreeVos.add(OrgTreeVo.builder()
                            .id(user.getUserId())
                            .name(user.getUserName())
                            .avatar(user.getAvatar())
                            .sex(user.getSex())
                            .type("user")
                            .selected(false)
                            .nickName(user.getNickName())
                            .build());
                });
            }
        }
        return orgTreeVos;
    }

    /**
     * 模糊搜索用户
     * @param sysUserDTO 用户
     * @return 匹配到的用户
     */
    @Override
    public List<OrgTreeVo> getOrgTreeUser(SysUserDTO sysUserDTO) {
        List<OrgTreeVo> list = new LinkedList<>();
        if(!ObjectUtils.isEmpty(sysUserDTO.getUserName()) || !ObjectUtils.isEmpty(sysUserDTO.getNickName())){
        SysUser sysUser = new SysUser();
        BeanUtils.copyBeanProp(sysUser, sysUserDTO);
            sysUser.setComId(SecurityUtils.getCurrComId());
        List<SysUser> sysUsers = sysUserMapper.selectUserList(sysUser);
        sysUsers.forEach(user ->{
            list.add(OrgTreeVo.builder().type("user")
                    .sex(user.getSex())
                    .avatar(user.getAvatar())
                    .name(user.getUserName())
                    .nickName(user.getNickName())
                    .id(user.getUserId())
                    .selected(false).build()
            );
        });
        }
        return list;
    }
    /*@Override
    public List<AddressBook> getAddressBook() {
        List<AddressBook> AddressBooks = new LinkedList<>();
        //供应商
        AddressBook addressBook=new AddressBook();
        addressBook.setName("供应商");
        //查询列表
        BaSupplier baSupplier=new BaSupplier();
        baSupplier.setState(AdminCodeEnum.SUPPLIER_STATUS_PASS.getCode());
        //租户ID
        baSupplier.setTenantId(SecurityUtils.getCurrComId());
        List<BaSupplier> baSuppliers = baSupplierMapper.selectBaSupplierList(baSupplier);
        List<AddressBook> AddressBooks1 = new LinkedList<>();
        if (!CollectionUtils.isEmpty(baSuppliers)) {
            baSuppliers.forEach(supplier -> {
                AddressBooks1.add(AddressBook.builder()
                        .id(supplier.getId())
                        .name(supplier.getName())
                        .address(supplier.getAddress())
                        .type("user")
                        .build());
            });
        }
        addressBook.setNumer(baSuppliers.size());
        addressBook.setChildren(AddressBooks1);
        AddressBooks.add(addressBook);
        AddressBook addressBook1=new AddressBook();
        addressBook1.setName("终端企业");
        BaEnterprise baEnterprise=new BaEnterprise();
        baEnterprise.setState(AdminCodeEnum.ENTERPRISE_STATUS_PASS.getCode());
        //租户ID
        baEnterprise.setTenantId(SecurityUtils.getCurrComId());
        List<BaEnterprise> baEnterprises = baEnterpriseMapper.selectBaEnterpriseList(baEnterprise);
        List<AddressBook> AddressBooks2 = new LinkedList<>();
        if (!CollectionUtils.isEmpty(baEnterprises)) {
            baEnterprises.forEach(enterprise -> {
                AddressBooks2.add(AddressBook.builder()
                        .id(enterprise.getId())
                        .name(enterprise.getName())
                        .address(enterprise.getAddress())
                        .type("user")
                        .build());
            });
        }
        addressBook1.setNumer(baEnterprises.size());
        addressBook1.setChildren(AddressBooks2);
        AddressBooks.add(addressBook1);
        AddressBook addressBook2=new AddressBook();
        addressBook2.setName("公路运输类公司");
        BaCompany baCompany=new BaCompany();
        baCompany.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        baCompany.setCompanyType("汽运公司");
        //租户ID
        baCompany.setTenantId(SecurityUtils.getCurrComId());
        List<BaCompany> baCompanies = companyMapper.selectBaCompanyList(baCompany);
        List<AddressBook> AddressBooks3 = new LinkedList<>();
        if (!CollectionUtils.isEmpty(baCompanies)) {
            baCompanies.forEach(company -> {
                AddressBooks3.add(AddressBook.builder()
                        .id(company.getId())
                        .name(company.getName())
                        .address(company.getAddress())
                        .type("user")
                        .build());
            });
        }
        addressBook2.setNumer(baCompanies.size());
        addressBook2.setChildren(AddressBooks3);
        AddressBooks.add(addressBook2);
        AddressBook addressBook3=new AddressBook();
        addressBook3.setName("托盘公司");
        BaCompany baCompany1=new BaCompany();
        baCompany1.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        baCompany1.setCompanyType("托盘公司");
        //租户ID
        baCompany1.setTenantId(SecurityUtils.getCurrComId());
        List<BaCompany> baCompanies1 = companyMapper.selectBaCompanyList(baCompany1);
        List<AddressBook> AddressBooks4 = new LinkedList<>();
        if (!CollectionUtils.isEmpty(baCompanies1)) {
            baCompanies1.forEach(company1 -> {
                AddressBooks4.add(AddressBook.builder()
                        .id(company1.getId())
                        .name(company1.getName())
                        .address(company1.getAddress())
                        .type("user")
                        .build());
            });
        }
        addressBook3.setNumer(baCompanies1.size());
        addressBook3.setChildren(AddressBooks4);
        AddressBooks.add(addressBook3);

        AddressBook addressBook4=new AddressBook();
        addressBook4.setName("下游贸易商");
        BaCompany baCompany2=new BaCompany();
        baCompany2.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        baCompany2.setCompanyType("下游贸易商");
        //租户ID
        baCompany2.setTenantId(SecurityUtils.getCurrComId());
        List<BaCompany> baCompanies2 = companyMapper.selectBaCompanyList(baCompany2);
        List<AddressBook> AddressBooks5 = new LinkedList<>();
        if (!CollectionUtils.isEmpty(baCompanies2)) {
            baCompanies2.forEach(company2 -> {
                AddressBooks5.add(AddressBook.builder()
                        .id(company2.getId())
                        .name(company2.getName())
                        .address(company2.getAddress())
                        .type("user")
                        .build());
            });
        }
        addressBook4.setNumer(baCompanies2.size());
        addressBook4.setChildren(AddressBooks5);
        AddressBooks.add(addressBook4);
        AddressBook addressBook5=new AddressBook();
        addressBook5.setName("集团公司");
        BaCompanyGroup baCompanyGroup=new BaCompanyGroup();
        baCompanyGroup.setType("1");
        //租户ID
        baCompanyGroup.setTenantId(SecurityUtils.getCurrComId());
        List<BaCompanyGroup> baCompanyGroups = baCompanyGroupMapper.selectBaCompanyGroupList(baCompanyGroup);
        List<AddressBook> AddressBooks6 = new LinkedList<>();
        if (!CollectionUtils.isEmpty(baCompanyGroups)) {
            baCompanyGroups.forEach(companygroup -> {
                AddressBooks6.add(AddressBook.builder()
                        .id(companygroup.getId())
                        .name(companygroup.getCompanyName())
                        .type("user")
                        .build());
            });
        }
        addressBook5.setNumer(baCompanyGroups.size());
        addressBook5.setChildren(AddressBooks6);
        AddressBooks.add(addressBook5);
        AddressBook addressBook6=new AddressBook();
        addressBook6.setName("采购公司");
        BaCompany baCompany3=new BaCompany();
        baCompany3.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        baCompany3.setCompanyType("采购公司");
        //租户ID
        baCompany3.setTenantId(SecurityUtils.getCurrComId());
        List<BaCompany> baCompanies3 = companyMapper.selectBaCompanyList(baCompany3);
        List<AddressBook> AddressBooks7 = new LinkedList<>();
        if (!CollectionUtils.isEmpty(baCompanies3)) {
            baCompanies3.forEach(company3 -> {
                AddressBooks7.add(AddressBook.builder()
                        .id(company3.getId())
                        .name(company3.getName())
                        .address(company3.getAddress())
                        .type("user")
                        .build());
            });
        }
        addressBook6.setNumer(baCompanies3.size());
        addressBook6.setChildren(AddressBooks7);
        AddressBooks.add(addressBook6);
        AddressBook addressBook7=new AddressBook();
        addressBook7.setName("装卸服务有限公司");
        BaCompany baCompany4=new BaCompany();
        baCompany4.setState(AdminCodeEnum.COMPANY_STATUS_PASS.getCode());
        baCompany4.setCompanyType("装卸服务公司");
        //租户ID
        baCompany4.setTenantId(SecurityUtils.getCurrComId());
        List<BaCompany> baCompanies4 = companyMapper.selectBaCompanyList(baCompany4);
        List<AddressBook> AddressBooks8 = new LinkedList<>();
        if (!CollectionUtils.isEmpty(baCompanies4)) {
            baCompanies4.forEach(company4 -> {
                AddressBooks8.add(AddressBook.builder()
                        .id(company4.getId())
                        .name(company4.getName())
                        .address(company4.getAddress())
                        .type("user")
                        .build());
            });
        }
        addressBook7.setNumer(baCompanies4.size());
        addressBook7.setChildren(AddressBooks8);
        AddressBooks.add(addressBook7);
       *//* AddressBook addressBook9=new AddressBook();
        addressBook9.setName("客户");
        addressBook9.setChildren(AddressBooks);
        List<AddressBook> AddressBooks10 = new LinkedList<>();
        AddressBooks10.add(addressBook9);
        return  AddressBooks10;*//*
        return  AddressBooks;
    }*/

    @Override
    public List<OrgTreeVo> getOrgTree(SysDeptDTO sysDeptDTO) {
        //一级部门与用户
        List<OrgTreeVo> orgTreeVos = new LinkedList<>();
        SysDept sysDept = new SysDept();
        SysUser sysUser = new SysUser();
        if(!ObjectUtils.isEmpty(sysDeptDTO.getParentId())) {
            BeanUtils.copyBeanProp(sysDept, sysDeptDTO);
            sysDept.setTenantId(SecurityUtils.getCurrComId());
            List<SysDept> sysDepts = sysDeptMapper.selectTreeDeptList(sysDept);
            List<SysDept> deptList = new ArrayList<>();
            for (SysDept dept:sysDepts) {
                if("1".equals(dept.getDeptType()) && dept.getDeptId() == sysDeptDTO.getParentId()){
                    deptList.add(dept);
                }
                if("2".equals(dept.getDeptType()) || "1".equals(dept.getDeptType())){
                    deptList.add(dept);
                }
            }
            if (!CollectionUtils.isEmpty(deptList)) {
                deptList.forEach(dept -> {
                    orgTreeVos.add(OrgTreeVo.builder()
                            .id(dept.getDeptId())
                            .name(dept.getDeptName())
                            .deptType(dept.getDeptType())
                            .selected(false)
                            .type("dept")
                            .build());
                });
            }
            for (OrgTreeVo orgTreeVo:orgTreeVos) {
                //二级部门与用户
                List<OrgTreeVo> orgTreeVoList = new LinkedList<>();
                //下级部门
                sysDeptDTO.setParentId(orgTreeVo.getId());
                BeanUtils.copyBeanProp(sysDept, sysDeptDTO);
                sysDepts = sysDeptMapper.selectTreeDeptList(sysDept);
                if (!CollectionUtils.isEmpty(sysDepts)) {
                    sysDepts.forEach(dept -> {
                        orgTreeVoList.add(OrgTreeVo.builder()
                                .id(dept.getDeptId())
                                .name(dept.getDeptName())
                                .selected(false)
                                .type("dept")
                                .build());
                    });
                }
                sysUser.setDeptId(sysDeptDTO.getParentId());
                sysUser.setStatus("0");
                List<SysUser> sysUsers = sysUserMapper.selectTreeUserList(sysUser);
                if(!CollectionUtils.isEmpty(sysUsers)) {
                    sysUsers.forEach(user -> {
                        List<SysPost> sysPosts = sysPostMapper.selectPostsByUserName(user.getUserName(), SecurityUtils.getCurrComId());
                        StringBuilder sysPostName = new StringBuilder();
                        if(!CollectionUtils.isEmpty(sysPosts)){
                            for(SysPost sysPost : sysPosts){
                                if(sysPostName.length() > 0){
                                    sysPostName.append(",").append(sysPost.getPostName());
                                } else {
                                    sysPostName.append(sysPost.getPostName());
                                }
                            }
                        }
                        if(StringUtils.isNotEmpty(sysPostName)){
                            user.setNickName(user.getNickName() + "-" + sysPostName);
                        }
                        //查询用户岗位
                        orgTreeVoList.add(OrgTreeVo.builder()
                                .id(user.getUserId())
                                .name(user.getNickName())
                                .avatar(user.getAvatar())
                                .sex(user.getSex())
                                .userName(user.getUserName())
                                .type("user")
                                .selected(false)
                                .nickName(user.getNickName())
                                .build());
                    });
                }
                //封装部门剔除用户
                List<OrgTreeVo> list = new ArrayList<>();
                for (OrgTreeVo orgTreeVo1:orgTreeVoList) {
                    if("dept".equals(orgTreeVo1.getType())){
                        list.add(orgTreeVo1);
                    }
                }
                //三级信息
                for (OrgTreeVo orgTreeVo1:list) {
                    //三级部门与用户
                    List<OrgTreeVo> orgTreeVo3 = new LinkedList<>();
                    //下级部门
                    sysDeptDTO.setParentId(orgTreeVo1.getId());
                    BeanUtils.copyBeanProp(sysDept, sysDeptDTO);
                    sysDepts = sysDeptMapper.selectTreeDeptList(sysDept);
                    if (!CollectionUtils.isEmpty(sysDepts)) {
                        sysDepts.forEach(dept -> {
                            orgTreeVo3.add(orgTreeVo1.builder()
                                    .id(dept.getDeptId())
                                    .name(dept.getDeptName())
                                    .selected(false)
                                    .type("dept")
                                    .build());
                        });
                    }
                    sysUser.setDeptId(sysDeptDTO.getParentId());
                    sysUser.setStatus("0");
                    sysUsers = sysUserMapper.selectTreeUserList(sysUser);
                    if (!CollectionUtils.isEmpty(sysUsers)) {
                        sysUsers.forEach(user -> {
                            List<SysPost> sysPosts = sysPostMapper.selectPostsByUserName(user.getUserName(), SecurityUtils.getCurrComId());
                            StringBuilder sysPostName = new StringBuilder();
                            if(!CollectionUtils.isEmpty(sysPosts)){
                                for(SysPost sysPost : sysPosts){
                                    if(sysPostName.length() > 0){
                                        sysPostName.append(",").append(sysPost.getPostName());
                                    } else {
                                        sysPostName.append(sysPost.getPostName());
                                    }
                                }
                            }
                            if(StringUtils.isNotEmpty(sysPostName)){
                                user.setNickName(user.getNickName() + "-" + sysPostName);
                            }
                            orgTreeVo3.add(orgTreeVo1.builder()
                                    .id(user.getUserId())
                                    .name(user.getNickName())
                                    .avatar(user.getAvatar())
                                    .sex(user.getSex())
                                    .userName(user.getUserName())
                                    .type("user")
                                    .selected(false)
                                    .nickName(user.getNickName())
                                    .build());
                        });
                    }
                    //封装部门剔除用户
                    List<OrgTreeVo> list1 = new ArrayList<>();
                    for (OrgTreeVo orgTreeVo2:orgTreeVo3) {
                        if("dept".equals(orgTreeVo2.getType())){
                            list1.add(orgTreeVo2);
                        }
                    }
                    //三级包四级
                    for (OrgTreeVo orgTreeVo2:list1) {
                        List<OrgTreeVo> orgTreeVo4 = new LinkedList<>();
                        //下级部门
                        sysDeptDTO.setParentId(orgTreeVo2.getId());
                        BeanUtils.copyBeanProp(sysDept, sysDeptDTO);
                        sysDepts = sysDeptMapper.selectTreeDeptList(sysDept);
                        if (!CollectionUtils.isEmpty(sysDepts)) {
                            sysDepts.forEach(dept -> {
                                orgTreeVo4.add(orgTreeVo2.builder()
                                        .id(dept.getDeptId())
                                        .name(dept.getDeptName())
                                        .selected(false)
                                        .type("dept")
                                        .build());
                            });
                        }
                        sysUser.setDeptId(sysDeptDTO.getParentId());
                        sysUser.setStatus("0");
                        sysUsers = sysUserMapper.selectTreeUserList(sysUser);
                        if (!CollectionUtils.isEmpty(sysUsers)) {
                            sysUsers.forEach(user -> {
                                List<SysPost> sysPosts = sysPostMapper.selectPostsByUserName(user.getUserName(), SecurityUtils.getCurrComId());
                                StringBuilder sysPostName = new StringBuilder();
                                if(!CollectionUtils.isEmpty(sysPosts)){
                                    for(SysPost sysPost : sysPosts){
                                        if(sysPostName.length() > 0){
                                            sysPostName.append(",").append(sysPost.getPostName());
                                        } else {
                                            sysPostName.append(sysPost.getPostName());
                                        }
                                    }
                                }
                                if(StringUtils.isNotEmpty(sysPostName)){
                                    user.setNickName(user.getNickName() + "-" + sysPostName);
                                }
                                orgTreeVo4.add(orgTreeVo2.builder()
                                        .id(user.getUserId())
                                        .name(user.getNickName())
                                        .avatar(user.getAvatar())
                                        .sex(user.getSex())
                                        .userName(user.getUserName())
                                        .type("user")
                                        .selected(false)
                                        .nickName(user.getNickName())
                                        .build());
                            });
                        }
                        //三级部门包四级
                        orgTreeVo2.setOrgTreeVoList(orgTreeVo4);
                    }
                    //二级部门包三级
                    orgTreeVo1.setOrgTreeVoList(orgTreeVo3);
                }
                //一级部门包二级部门
                orgTreeVo.setOrgTreeVoList(orgTreeVoList);
            }
        }
        return orgTreeVos;
    }
    @Override
    public List<OrgTreeVo> getAppOrgTree(SysDeptDTO sysDeptDTO) {
        //一级部门与用户
      /*  //查询一级部门及用户
        List<OrgTreeVo> orgTreeVos = new LinkedList<>();
        SysDept sysDept = new SysDept();
        SysUser sysUser = new SysUser();
        if(!ObjectUtils.isEmpty(sysDeptDTO.getParentId())) {
            BeanUtils.copyBeanProp(sysDept, sysDeptDTO);
            List<SysDept> sysDepts = sysDeptMapper.selectTreeDeptList(sysDept);
            //查询部门
            for(SysDept sysDept1:sysDepts){
                OrgTreeVo orgTreeVo=new OrgTreeVo();
                orgTreeVo.setType("dept");
                orgTreeVo.setSelected(false);
                orgTreeVo.setId(sysDept1.getDeptId());
                orgTreeVo.setName(sysDept1.getDeptName());
                orgTreeVos.add(orgTreeVo);
                //查询二级列表
                List<OrgTreeVo> orgTreeVoList = new LinkedList<>();
                //下级部门
                sysDeptDTO.setParentId(orgTreeVo.getId());
                BeanUtils.copyBeanProp(sysDept, sysDeptDTO);
                List<SysDept> sysDepts1 = sysDeptMapper.selectTreeDeptList(sysDept);
                for (SysDept sysDept2:sysDepts1){
                    OrgTreeVo orgTreeVo1=new OrgTreeVo();
                    orgTreeVo1.setType("dept");
                    orgTreeVo1.setSelected(false);
                    orgTreeVo1.setId(sysDept2.getDeptId());
                    orgTreeVo1.setName(sysDept2.getDeptName());
                    orgTreeVoList.add(orgTreeVo1);
                    sysUser.setDeptId(sysDept1.getDeptId());
                    //查询二级列表
                    List<OrgTreeVo> orgTreeVoList2 = new LinkedList<>();
                    sysUser.setDeptId(sysDept2.getDeptId());
                    List<SysUser> sysUsers = sysUserMapper.selectTreeUserList(sysUser);
                    if (!CollectionUtils.isEmpty(sysUsers)) {
                        sysUsers.forEach(user -> {
                            List<SysPost> sysPosts = sysPostMapper.selectPostsByUserName(user.getUserName());
                            StringBuilder sysPostName = new StringBuilder();
                            if (!CollectionUtils.isEmpty(sysPosts)) {
                                for (SysPost sysPost : sysPosts) {
                                    if (sysPostName.length() > 0) {
                                        sysPostName.append(",").append(sysPost.getPostName());
                                    } else {
                                        sysPostName.append(sysPost.getPostName());
                                    }
                                }
                            }
                            if (StringUtils.isNotEmpty(sysPostName)) {
                                //user.setNickName(user.getNickName() + "-" + sysPostName);
                                user.setPostName(sysPostName.toString());
                            }
                        });
                    }
                    for (SysUser sysUser1 : sysUsers) {
                        OrgTreeVo orgTreeVo2 = new OrgTreeVo();
                        orgTreeVo2.setType("dept");
                        orgTreeVo2.setSelected(false);
                        orgTreeVo2.setName(sysUser1.getNickName());
                        orgTreeVo2.setSex(sysUser1.getSex());
                        orgTreeVo2.setUserName(sysUser1.getUserName());
                        orgTreeVo2.setNickName(sysUser1.getNickName());
                        orgTreeVo2.setPhonenumber(sysUser1.getPhonenumber());
                        orgTreeVo2.setPostName(sysUser1.getPostName());
                        orgTreeVoList2.add(orgTreeVo2);
                    }
                    orgTreeVo1.setChildren(orgTreeVoList2);
                    //查询二级列表
                    List<OrgTreeVo> orgTreeVoList3 = new LinkedList<>();
                    //下级部门
                    sysDeptDTO.setParentId(orgTreeVo.getId());
                    BeanUtils.copyBeanProp(sysDept, sysDeptDTO);
                    List<SysDept> sysDepts2 = sysDeptMapper.selectTreeDeptList(sysDept);
                    for (SysDept sysDept3:sysDepts2){
                        OrgTreeVo orgTreeVo2=new OrgTreeVo();
                        orgTreeVo1.setType("dept");
                        orgTreeVo1.setSelected(false);
                        orgTreeVo1.setId(sysDept3.getDeptId());
                        orgTreeVo1.setName(sysDept3.getDeptName());
                        orgTreeVoList.add(orgTreeVo1);
                        sysUser.setDeptId(sysDept1.getDeptId());
                        //查询二级列表
                        List<OrgTreeVo> orgTreeVoList2 = new LinkedList<>();
                        sysUser.setDeptId(sysDept2.getDeptId());
                        List<SysUser> sysUsers = sysUserMapper.selectTreeUserList(sysUser);
                        if (!CollectionUtils.isEmpty(sysUsers)) {
                            sysUsers.forEach(user -> {
                                List<SysPost> sysPosts = sysPostMapper.selectPostsByUserName(user.getUserName());
                                StringBuilder sysPostName = new StringBuilder();
                                if (!CollectionUtils.isEmpty(sysPosts)) {
                                    for (SysPost sysPost : sysPosts) {
                                        if (sysPostName.length() > 0) {
                                            sysPostName.append(",").append(sysPost.getPostName());
                                        } else {
                                            sysPostName.append(sysPost.getPostName());
                                        }
                                    }
                                }
                                if (StringUtils.isNotEmpty(sysPostName)) {
                                    //user.setNickName(user.getNickName() + "-" + sysPostName);
                                    user.setPostName(sysPostName.toString());
                                }
                            });
                        }
                        for (SysUser sysUser1 : sysUsers) {
                            OrgTreeVo orgTreeVo2 = new OrgTreeVo();
                            orgTreeVo2.setType("dept");
                            orgTreeVo2.setSelected(false);
                            orgTreeVo2.setName(sysUser1.getNickName());
                            orgTreeVo2.setSex(sysUser1.getSex());
                            orgTreeVo2.setUserName(sysUser1.getUserName());
                            orgTreeVo2.setNickName(sysUser1.getNickName());
                            orgTreeVo2.setPhonenumber(sysUser1.getPhonenumber());
                            orgTreeVo2.setPostName(sysUser1.getPostName());
                            orgTreeVoList2.add(orgTreeVo2);
                        }
                        orgTreeVo1.setChildren(orgTreeVoList2);
                    }
                }

                   sysUser.setDeptId(sysDept1.getDeptId());
                   List<SysUser> sysUsers = sysUserMapper.selectTreeUserList(sysUser);
                   if (!CollectionUtils.isEmpty(sysUsers)) {
                       sysUsers.forEach(user -> {
                           List<SysPost> sysPosts = sysPostMapper.selectPostsByUserName(user.getUserName());
                           StringBuilder sysPostName = new StringBuilder();
                           if (!CollectionUtils.isEmpty(sysPosts)) {
                               for (SysPost sysPost : sysPosts) {
                                   if (sysPostName.length() > 0) {
                                       sysPostName.append(",").append(sysPost.getPostName());
                                   } else {
                                       sysPostName.append(sysPost.getPostName());
                                   }
                               }
                           }
                           if (StringUtils.isNotEmpty(sysPostName)) {
                               //user.setNickName(user.getNickName() + "-" + sysPostName);
                               user.setPostName(sysPostName.toString());
                           }
                       });
                   }
                for (SysUser sysUser1 : sysUsers) {
                    OrgTreeVo orgTreeVo2 = new OrgTreeVo();
                    orgTreeVo2.setType("dept");
                    orgTreeVo2.setSelected(false);
                    orgTreeVo2.setName(sysUser1.getNickName());
                    orgTreeVo2.setSex(sysUser1.getSex());
                    orgTreeVo2.setUserName(sysUser1.getUserName());
                    orgTreeVo2.setNickName(sysUser1.getNickName());
                    orgTreeVo2.setPhonenumber(sysUser1.getPhonenumber());
                    orgTreeVo2.setPostName(sysUser1.getPostName());
                    orgTreeVoList.add(orgTreeVo2);
                }
                orgTreeVo.setChildren(orgTreeVoList);
            }

            //查询用户
            for(SysDept sysDept4:sysDepts) {
                sysUser.setDeptId(sysDeptDTO.getParentId());
                List<SysUser> sysUsers = sysUserMapper.selectTreeUserList(sysUser);
                if (!CollectionUtils.isEmpty(sysUsers)) {
                    sysUsers.forEach(user -> {
                        List<SysPost> sysPosts = sysPostMapper.selectPostsByUserName(user.getUserName());
                        StringBuilder sysPostName = new StringBuilder();
                        if (!CollectionUtils.isEmpty(sysPosts)) {
                            for (SysPost sysPost : sysPosts) {
                                if (sysPostName.length() > 0) {
                                    sysPostName.append(",").append(sysPost.getPostName());
                                } else {
                                    sysPostName.append(sysPost.getPostName());
                                }
                            }
                        }
                        if (StringUtils.isNotEmpty(sysPostName)) {
                            //user.setNickName(user.getNickName() + "-" + sysPostName);
                            user.setPostName(sysPostName.toString());
                        }
                        for (SysUser sysUser1 : sysUsers) {
                            OrgTreeVo orgTreeVo = new OrgTreeVo();
                            orgTreeVo.setType("dept");
                            orgTreeVo.setSelected(false);
                            orgTreeVo.setName(sysUser1.getNickName());
                            orgTreeVo.setSex(sysUser1.getSex());
                            orgTreeVo.setUserName(sysUser1.getUserName());
                            orgTreeVo.setNickName(sysUser1.getNickName());
                            orgTreeVo.setPhonenumber(sysUser1.getPhonenumber());
                            orgTreeVo.setPostName(sysUser1.getPostName());
                            orgTreeVos.add(orgTreeVo);
                        }

                    });
                }
            }

        }
        return orgTreeVos;*/
       List<OrgTreeVo> orgTreeVos = new LinkedList<>();
        SysDept sysDept = new SysDept();
        SysUser sysUser = new SysUser();
        if(!ObjectUtils.isEmpty(sysDeptDTO.getParentId())) {
            BeanUtils.copyBeanProp(sysDept, sysDeptDTO);
            //租户ID
            sysDept.setTenantId(SecurityUtils.getCurrComId());
            List<SysDept> sysDepts = sysDeptMapper.selectTreeDeptList(sysDept);
            if (!CollectionUtils.isEmpty(sysDepts)) {
                sysDepts.forEach(dept -> {
                    orgTreeVos.add(OrgTreeVo.builder()
                            .id(dept.getDeptId())
                            .name(dept.getDeptName())
                            .deptType(dept.getDeptType())
                            .selected(false)
                            .type("dept")
                            .build());
                });
            }
            //2级部门人数
           // Integer number1=0;
            for (OrgTreeVo orgTreeVo:orgTreeVos) {
                //二级部门与用户
                List<OrgTreeVo> orgTreeVoList = new LinkedList<>();
                //下级部门
                sysDeptDTO.setParentId(orgTreeVo.getId());
                BeanUtils.copyBeanProp(sysDept, sysDeptDTO);
                sysDepts = sysDeptMapper.selectTreeDeptList(sysDept);
                if (!CollectionUtils.isEmpty(sysDepts)) {
                    sysDepts.forEach(dept -> {
                        orgTreeVoList.add(OrgTreeVo.builder()
                                .id(dept.getDeptId())
                                .name(dept.getDeptName())
                                .deptType(dept.getDeptType())
                                .selected(false)
                                .type("dept")
                                .build());
                    });
                }
                sysUser.setDeptId(sysDeptDTO.getParentId());
                sysUser.setStatus("0");
                List<SysUser> sysUsers = sysUserMapper.selectTreeUserList(sysUser);
                //List<SysUser> sysUsers1 = sysUserMapper.userList(sysUser);
                orgTreeVo.setNumer(sysUsers.size());
                if(!CollectionUtils.isEmpty(sysUsers)) {
                    sysUsers.forEach(user -> {
                       List<SysPost> sysPosts = sysPostMapper.selectPostsByUserName(user.getUserName(), SecurityUtils.getCurrComId());
                        StringBuilder sysPostName = new StringBuilder();
                        if(!CollectionUtils.isEmpty(sysPosts)){
                            for(SysPost sysPost : sysPosts){
                                if(sysPostName.length() > 0){
                                    sysPostName.append(",").append(sysPost.getPostName());
                                } else {
                                    sysPostName.append(sysPost.getPostName());
                                }
                            }
                        }
                        if(StringUtils.isNotEmpty(sysPostName)){
                            //user.setNickName(user.getNickName() + "-" + sysPostName);
                            user.setPostName(sysPostName.toString());
                        }
                        //查询用户岗位
                        orgTreeVoList.add(OrgTreeVo.builder()
                                .id(user.getUserId())
                                .name(user.getNickName())
                                .avatar(user.getAvatar())
                                .sex(user.getSex())
                                .userName(user.getUserName())
                                .type("user")
                                .selected(false)
                                .nickName(user.getNickName())
                                .phonenumber(user.getPhonenumber())
                                .postName(user.getPostName())
                                .build());
                    });
                }
                //封装部门剔除用户
                List<OrgTreeVo> list = new ArrayList<>();
                for (OrgTreeVo orgTreeVo1:orgTreeVoList) {
                    if("dept".equals(orgTreeVo1.getType())){
                        list.add(orgTreeVo1);
                    }
                }
                //2级部门人数
                //Integer number2=0;
                //三级信息
                for (OrgTreeVo orgTreeVo1:list) {
                    //三级部门与用户
                    List<OrgTreeVo> orgTreeVo3 = new LinkedList<>();
                    //下级部门
                    sysDeptDTO.setParentId(orgTreeVo1.getId());
                    BeanUtils.copyBeanProp(sysDept, sysDeptDTO);
                    sysDepts = sysDeptMapper.selectTreeDeptList(sysDept);
                    if (!CollectionUtils.isEmpty(sysDepts)) {
                        sysDepts.forEach(dept -> {
                            orgTreeVo3.add(orgTreeVo1.builder()
                                    .id(dept.getDeptId())
                                    .name(dept.getDeptName())
                                    .deptType(dept.getDeptType())
                                    .selected(false)
                                    .type("dept")
                                    .build());
                        });
                    }
                    sysUser.setDeptId(sysDeptDTO.getParentId());
                    sysUser.setStatus("0");
                    sysUsers = sysUserMapper.selectTreeUserList(sysUser);
                    //List<SysUser> sysUsers2 = sysUserMapper.userList(sysUser);
                    orgTreeVo1.setNumer(sysUsers.size());
                    if (!CollectionUtils.isEmpty(sysUsers)) {
                        sysUsers.forEach(user -> {
                            List<SysPost> sysPosts = sysPostMapper.selectPostsByUserName(user.getUserName(), SecurityUtils.getCurrComId());
                            StringBuilder sysPostName = new StringBuilder();
                            if(!CollectionUtils.isEmpty(sysPosts)){
                                for(SysPost sysPost : sysPosts){
                                    if(sysPostName.length() > 0){
                                        sysPostName.append(",").append(sysPost.getPostName());
                                    } else {
                                        sysPostName.append(sysPost.getPostName());
                                    }
                                }
                            }
                            if(StringUtils.isNotEmpty(sysPostName)){
                                //user.setNickName(user.getNickName() + "-" + sysPostName);
                                user.setPostName(sysPostName.toString());
                            }
                            orgTreeVo3.add(orgTreeVo1.builder()
                                    .id(user.getUserId())
                                    .name(user.getNickName())
                                    .avatar(user.getAvatar())
                                    .sex(user.getSex())
                                    .userName(user.getUserName())
                                    .type("user")
                                    .selected(false)
                                    .nickName(user.getNickName())
                                    .phonenumber(user.getPhonenumber())
                                    .postName(user.getPostName())
                                    .build());
                        });
                    }
                    //封装部门剔除用户
                    List<OrgTreeVo> list1 = new ArrayList<>();
                    for (OrgTreeVo orgTreeVo2:orgTreeVo3) {
                        if("dept".equals(orgTreeVo2.getType())){
                            list1.add(orgTreeVo2);
                        }
                    }
                    //三级包四级
                    int num3;
                    for (OrgTreeVo orgTreeVo2:list1) {
                        List<OrgTreeVo> orgTreeVo4 = new LinkedList<>();
                        //下级部门
                        sysDeptDTO.setParentId(orgTreeVo2.getId());
                        BeanUtils.copyBeanProp(sysDept, sysDeptDTO);
                        sysDepts = sysDeptMapper.selectTreeDeptList(sysDept);
                        if (!CollectionUtils.isEmpty(sysDepts)) {
                            sysDepts.forEach(dept -> {
                                orgTreeVo4.add(orgTreeVo2.builder()
                                        .id(dept.getDeptId())
                                        .name(dept.getDeptName())
                                        .selected(false)
                                        .type("dept")
                                        .build());
                            });
                        }
                        sysUser.setDeptId(sysDeptDTO.getParentId());
                        sysUser.setStatus("0");
                        sysUsers = sysUserMapper.selectTreeUserList(sysUser);
                        //List<SysUser> sysUsers3 = sysUserMapper.userList(sysUser);
                        orgTreeVo2.setNumer(sysUsers.size());
                        if (!CollectionUtils.isEmpty(sysUsers)) {
                            sysUsers.forEach(user -> {
                                List<SysPost> sysPosts = sysPostMapper.selectPostsByUserName(user.getUserName(), SecurityUtils.getCurrComId());
                                StringBuilder sysPostName = new StringBuilder();
                                if(!CollectionUtils.isEmpty(sysPosts)){
                                    for(SysPost sysPost : sysPosts){
                                        if(sysPostName.length() > 0){
                                            sysPostName.append(",").append(sysPost.getPostName());
                                        } else {
                                            sysPostName.append(sysPost.getPostName());
                                        }
                                    }
                                }
                                if(StringUtils.isNotEmpty(sysPostName)){
                                    //user.setNickName(user.getNickName() + "-" + sysPostName);
                                    user.setPostName(sysPostName.toString());
                                }
                                orgTreeVo4.add(orgTreeVo2.builder()
                                        .id(user.getUserId())
                                        .name(user.getNickName())
                                        .avatar(user.getAvatar())
                                        .sex(user.getSex())
                                        .userName(user.getUserName())
                                        .type("user")
                                        .selected(false)
                                        .nickName(user.getNickName())
                                        .phonenumber(user.getPhonenumber())
                                        .postName(user.getPostName())
                                        .build());



                            });
                        }
                        //封装部门剔除用户
                        List<OrgTreeVo> list2 = new ArrayList<>();
                        for (OrgTreeVo orgTreeVo6:orgTreeVo4) {
                            if("dept".equals(orgTreeVo6.getType())){
                                list2.add(orgTreeVo6);
                            }
                        }
                        for (OrgTreeVo orgTreeVo6:list2) {
                            List<OrgTreeVo> orgTreeVo5 = new LinkedList<>();
                            //下级部门
                            sysDeptDTO.setParentId(orgTreeVo6.getId());
                            BeanUtils.copyBeanProp(sysDept, sysDeptDTO);
                            sysDepts = sysDeptMapper.selectTreeDeptList(sysDept);
                            if (!CollectionUtils.isEmpty(sysDepts)) {
                                sysDepts.forEach(dept -> {
                                    orgTreeVo5.add(orgTreeVo6.builder()
                                            .id(dept.getDeptId())
                                            .name(dept.getDeptName())
                                            .selected(false)
                                            .type("dept")
                                            .build());
                                });
                            }
                            sysUser.setDeptId(sysDeptDTO.getParentId());
                            sysUser.setStatus("0");
                            sysUsers = sysUserMapper.selectTreeUserList(sysUser);
                            //List<SysUser> sysUsers3 = sysUserMapper.userList(sysUser);
                            orgTreeVo2.setNumer(sysUsers.size());
                            if (!CollectionUtils.isEmpty(sysUsers)) {
                                sysUsers.forEach(user -> {
                                    List<SysPost> sysPosts = sysPostMapper.selectPostsByUserName(user.getUserName(), SecurityUtils.getCurrComId());
                                    StringBuilder sysPostName = new StringBuilder();
                                    if(!CollectionUtils.isEmpty(sysPosts)){
                                        for(SysPost sysPost : sysPosts){
                                            if(sysPostName.length() > 0){
                                                sysPostName.append(",").append(sysPost.getPostName());
                                            } else {
                                                sysPostName.append(sysPost.getPostName());
                                            }
                                        }
                                    }
                                    if(StringUtils.isNotEmpty(sysPostName)){
                                        //user.setNickName(user.getNickName() + "-" + sysPostName);
                                        user.setPostName(sysPostName.toString());
                                    }
                                    orgTreeVo5.add(orgTreeVo6.builder()
                                            .id(user.getUserId())
                                            .name(user.getNickName())
                                            .avatar(user.getAvatar())
                                            .sex(user.getSex())
                                            .userName(user.getUserName())
                                            .type("user")
                                            .selected(false)
                                            .nickName(user.getNickName())
                                            .phonenumber(user.getPhonenumber())
                                            .postName(user.getPostName())
                                            .build());



                                });

                            }
                            //三级部门包四级
                            orgTreeVo6.setChildren(orgTreeVo5);
                        }
                        //三级部门包四级
                        orgTreeVo2.setChildren(orgTreeVo4);
                    }

                    //二级部门包三级
                    orgTreeVo1.setChildren(orgTreeVo3);
                }
                //一级部门包二级部门
                orgTreeVo.setChildren(orgTreeVoList);
            }
        }
        List orgTreeVos1 = new LinkedList<>();
        for(OrgTreeVo orgTreeVo:orgTreeVos){
            orgTreeVos1=orgTreeVo.getChildren();
        }
        return orgTreeVos1;
        /*return orgTreeVoList;*/
    }


    @Override
    public List<OrgTreeVo> getRoleTreeData(SysRole sysRole) {
        List<OrgTreeVo> orgTreeVos = new LinkedList<>();
        if(!ObjectUtils.isEmpty(sysRole)){
            sysRole.setTenantId(SecurityUtils.getCurrComId());
            List<SysRole> sysRoles = sysRoleMapper.selectRoleList(sysRole);
            if(!CollectionUtils.isEmpty(sysRoles)){
                sysRoles.forEach(role -> {
                    orgTreeVos.add(OrgTreeVo.builder()
                            .id(role.getRoleId())
                            .name(role.getRoleName())
                            .selected(false)
                            .type("role")
                            .roleKey(role.getRoleKey())
                            .build());
                });
            }
        }
        return orgTreeVos;
    }
}
