package com.business.system.service.workflow;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.*;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 报销申请审批接口 服务类
 * @date: 2023/11/11 16:38
 */
@Service
public class IOaClaimApprovalService {

    @Autowired
    protected OaClaimMapper oaClaimMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private OaClaimRelationMapper oaClaimRelationMapper;

    @Autowired
    private OaClaimDetailMapper oaClaimDetailMapper;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        OaClaim oaClaim = new OaClaim();
        oaClaim.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        oaClaim = oaClaimMapper.selectOaClaimById(oaClaim.getId());
        oaClaim.setState(status);
        String userId = String.valueOf(oaClaim.getUserId());
        if(AdminCodeEnum.OACLAIM_STATUS_PASS.getCode().equals(status)){
            OaClaimRelation oaClaimRelation = null;
            //修改报销状态
            QueryWrapper<OaClaimDetail> claimDetailQueryWrapper = new QueryWrapper<>();
            claimDetailQueryWrapper.eq("claim_id", oaClaim.getId());
            List<OaClaimDetail> oaClaimDetails = oaClaimDetailMapper.selectList(claimDetailQueryWrapper);
            for (OaClaimDetail claimDetail : oaClaimDetails) {
                oaClaimRelation = new OaClaimRelation();
                oaClaimRelation.setId(claimDetail.getRelationId());
                oaClaimRelation = oaClaimRelationMapper.selectOaClaimRelationById(claimDetail.getRelationId());
                if(!ObjectUtils.isEmpty(oaClaimRelation)){
                    oaClaimRelation.setClaimState(3); //已报销
                    oaClaimRelationMapper.updateOaClaimRelation(oaClaimRelation);
                }

                claimDetail.setRelationId(claimDetail.getRelationId());
                claimDetail.setClaimState(3); //已报销
                claimDetail.setCostTime(DateUtils.getNowDate());
//                claimDetail.setCreateTime(DateUtils.getNowDate());
                claimDetail.setClaimId(oaClaim.getId());
                oaClaimDetailMapper.updateOaClaimDetail(claimDetail);
            }

            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(oaClaim, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_OA_CLAIM.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.OACLAIM_STATUS_REJECT.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(oaClaim, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_OA_CLAIM.getDescription(), MessageConstant.APPROVAL_REJECT));

            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            AjaxResult result = workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            OaClaimRelation oaClaimRelation = null;
            //解绑报销明细
            QueryWrapper<OaClaimDetail> claimDetailQueryWrapper = new QueryWrapper<>();
            claimDetailQueryWrapper.eq("claim_id", oaClaim.getId());
            List<OaClaimDetail> oaClaimDetails = oaClaimDetailMapper.selectList(claimDetailQueryWrapper);
            for (OaClaimDetail claimDetail : oaClaimDetails) {
                oaClaimRelation = new OaClaimRelation();
                oaClaimRelation.setId(claimDetail.getRelationId());
                oaClaimRelation = oaClaimRelationMapper.selectOaClaimRelationById(claimDetail.getRelationId());
                if(!ObjectUtils.isEmpty(oaClaimRelation)){
                    oaClaimRelation.setClaimState(1); //未报销
                    oaClaimRelationMapper.updateOaClaimRelation(oaClaimRelation);
                }

                claimDetail.setRelationId(claimDetail.getRelationId());
                claimDetail.setClaimState(1); //未报销
                claimDetail.setCostTime(DateUtils.getNowDate());
//                claimDetail.setCreateTime(DateUtils.getNowDate());
                claimDetail.setClaimId(oaClaim.getId());
                oaClaimDetailMapper.updateOaClaimDetail(claimDetail);
            }

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if (!ObjectUtils.isEmpty(ajaxResult)) {
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for (LinkedHashMap map : data) {
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if (!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)) {
                            if ("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())) {
                                this.insertMessage(oaClaim, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_OA_CLAIM.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
        }
        oaClaimMapper.updateById(oaClaim);
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(OaClaim oaClaim, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(oaClaim.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_CLAIM.getCode());
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected OaClaim checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        OaClaim oaClaim = oaClaimMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (oaClaim == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_OACLAIM_001);
        }
        if (!AdminCodeEnum.OACLAIM_STATUS_VERIFYING.getCode().equals(oaClaim.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_OACLAIM_002);
        }
        return oaClaim;
    }
}
