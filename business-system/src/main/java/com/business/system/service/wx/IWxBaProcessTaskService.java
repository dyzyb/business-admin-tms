package com.business.system.service.wx;

import com.business.system.domain.dto.BaProcessInstanceRelatedQueryDTO;
import com.business.system.domain.vo.BaProcessBusinessInstanceRelatedVO;

import java.util.List;

/**
 * @Author: js
 * @Description: 小程序审批任务Service接口
 * @Date: 2023/04/25 16:44
 */
public interface IWxBaProcessTaskService {

    List<BaProcessBusinessInstanceRelatedVO> myApplyList(BaProcessInstanceRelatedQueryDTO queryDTO);
}
