package com.business.system.service.impl;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.business.system.mapper.OaAttendanceAddressMapper;
import com.business.system.domain.OaAttendanceAddress;
import com.business.system.service.IOaAttendanceAddressService;

/**
 * 考勤组地点关联Service业务层处理
 *
 * @author single
 * @date 2023-11-24
 */
@Service
public class OaAttendanceAddressServiceImpl extends ServiceImpl<OaAttendanceAddressMapper, OaAttendanceAddress> implements IOaAttendanceAddressService
{
    @Autowired
    private OaAttendanceAddressMapper oaAttendanceAddressMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    /**
     * 查询考勤组地点关联
     *
     * @param id 考勤组地点关联ID
     * @return 考勤组地点关联
     */
    @Override
    public OaAttendanceAddress selectOaAttendanceAddressById(String id)
    {
        return oaAttendanceAddressMapper.selectOaAttendanceAddressById(id);
    }

    /**
     * 查询考勤组地点关联列表
     *
     * @param oaAttendanceAddress 考勤组地点关联
     * @return 考勤组地点关联
     */
    @Override
    public List<OaAttendanceAddress> selectOaAttendanceAddressList(OaAttendanceAddress oaAttendanceAddress)
    {
        return oaAttendanceAddressMapper.selectOaAttendanceAddressList(oaAttendanceAddress);
    }

    /**
     * 新增考勤组地点关联
     *
     * @param oaAttendanceAddress 考勤组地点关联
     * @return 结果
     */
    @Override
    public int insertOaAttendanceAddress(OaAttendanceAddress oaAttendanceAddress)
    {
        oaAttendanceAddress.setId(getRedisIncreID.getId());
        oaAttendanceAddress.setCreateTime(DateUtils.getNowDate());
        return oaAttendanceAddressMapper.insertOaAttendanceAddress(oaAttendanceAddress);
    }

    /**
     * 修改考勤组地点关联
     *
     * @param oaAttendanceAddress 考勤组地点关联
     * @return 结果
     */
    @Override
    public int updateOaAttendanceAddress(OaAttendanceAddress oaAttendanceAddress)
    {
        oaAttendanceAddress.setUpdateTime(DateUtils.getNowDate());
        return oaAttendanceAddressMapper.updateOaAttendanceAddress(oaAttendanceAddress);
    }

    /**
     * 批量删除考勤组地点关联
     *
     * @param ids 需要删除的考勤组地点关联ID
     * @return 结果
     */
    @Override
    public int deleteOaAttendanceAddressByIds(String[] ids)
    {
        return oaAttendanceAddressMapper.deleteOaAttendanceAddressByIds(ids);
    }

    /**
     * 删除考勤组地点关联信息
     *
     * @param id 考勤组地点关联ID
     * @return 结果
     */
    @Override
    public int deleteOaAttendanceAddressById(String id)
    {
        return oaAttendanceAddressMapper.deleteOaAttendanceAddressById(id);
    }

}
