package com.business.system.service.wx.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.business.common.constant.BusinessConstant;
import com.business.common.constant.Constants;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.VerificationErrorCode;
import com.business.common.exception.VerificationException;
import com.business.common.utils.BeanUtil;
import com.business.common.utils.SecurityUtils;
import com.business.system.adapter.WorkflowUpdateStatusAdapter;
import com.business.system.domain.*;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.BaProcessInstanceRelatedQueryDTO;
import com.business.system.domain.dto.ProcessTaskDTO;
import com.business.system.domain.dto.ProcessTaskQueryDTO;
import com.business.system.domain.vo.BaProcessBusinessInstanceRelatedVO;
import com.business.system.domain.vo.HistoryTaskVO;
import com.business.system.domain.vo.ProcessTaskVO;
import com.business.system.domain.vo.RuntimeTaskVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.*;
import com.business.system.service.wx.IWxBaProcessTaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Author: js
 * @Description: 小程序审批任务实现类
 * @date: 2023/04/25 16:44
 */
@Service
@Slf4j
public class WxBaProcessTaskServiceImpl implements IWxBaProcessTaskService {


    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private BeanUtil beanUtil;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private ISysDeptService iSysDeptService;

    @Autowired
    private ISysPostService sysPostService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;


    @Override
    public List<BaProcessBusinessInstanceRelatedVO> myApplyList(BaProcessInstanceRelatedQueryDTO queryDTO) {
        BaProcessInstanceRelated baProcessInstanceRelated = new BaProcessInstanceRelated();
        baProcessInstanceRelated.setBusinessType(queryDTO.getBusinessType());
        baProcessInstanceRelated.setApproveResult(queryDTO.getApproveResult());
        baProcessInstanceRelated.setCreateBy(queryDTO.getCreateBy());
        //过滤未开发类型转List
        if(StringUtils.hasText(queryDTO.getFilterConditionNotEqual())){
            baProcessInstanceRelated.setFilterConditionNotEqual(Arrays.asList(queryDTO.getFilterConditionNotEqual().split(",")));
        }
        //查询指定类型转List
        if(StringUtils.hasText(queryDTO.getFilterConditionEqual())){
            baProcessInstanceRelated.setFilterConditionEqual(Arrays.asList(queryDTO.getFilterConditionEqual().split(",")));
        }
        //增加删除条件过滤
        baProcessInstanceRelated.setFlag(0);
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.listProcessInstanceOrderByCreatetimeWx(baProcessInstanceRelated);
        List<BaProcessBusinessInstanceRelatedVO> baProcessBusinessInstanceRelatedVOS = beanUtil.listConvert(baProcessInstanceRelateds, BaProcessBusinessInstanceRelatedVO.class);
        if(!CollectionUtils.isEmpty(baProcessBusinessInstanceRelatedVOS)){
            baProcessBusinessInstanceRelatedVOS.stream().forEach(item -> {
                SysUser sysUser = iSysUserService.selectUserByUserName(item.getCreateBy());
                item.setSysUser(sysUser);
                item.setSysDept(iSysDeptService.selectDeptById(sysUser.getDeptId()));

                if(!ObjectUtils.isEmpty(item) && StringUtils.hasText(item.getBusinessData())){
                    //处理业务数据
                    JSONObject jsonObject = (JSONObject) JSON.parse(item.getBusinessData());
                    //配置待办任务业务名称
                   /* item.setBusiName(baProcessTaskService.getBusiName(item.getBusinessType(), jsonObject));*/
                }
            });
        }
        for (BaProcessBusinessInstanceRelatedVO baProcessBusinessInstanceRelatedVO:baProcessBusinessInstanceRelatedVOS) {
            if(!ObjectUtils.isEmpty(baProcessBusinessInstanceRelatedVO.getSysUser())){
                List<Long> longs = sysPostService.selectPostListByUserId(baProcessBusinessInstanceRelatedVO.getSysUser().getUserId());
                //岗位信息
                List<SysPost> postList = new ArrayList<>();
                for (Long postId:longs) {
                    SysPost sysPost = sysPostService.selectPostById(postId);
                    postList.add(sysPost);
                }
                baProcessBusinessInstanceRelatedVO.setSysPost(postList);
            }
        }
        return baProcessBusinessInstanceRelatedVOS;
    }

}
