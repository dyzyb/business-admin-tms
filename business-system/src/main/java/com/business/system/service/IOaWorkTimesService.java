package com.business.system.service;

import java.util.List;
import com.business.system.domain.OaWorkTimes;

/**
 * 班次Service接口
 *
 * @author single
 * @date 2023-11-24
 */
public interface IOaWorkTimesService
{
    /**
     * 查询班次
     *
     * @param id 班次ID
     * @return 班次
     */
    public OaWorkTimes selectOaWorkTimesById(String id);

    /**
     * 查询班次列表
     *
     * @param oaWorkTimes 班次
     * @return 班次集合
     */
    public List<OaWorkTimes> selectOaWorkTimesList(OaWorkTimes oaWorkTimes);

    /**
     * 新增班次
     *
     * @param oaWorkTimes 班次
     * @return 结果
     */
    public int insertOaWorkTimes(OaWorkTimes oaWorkTimes);

    /**
     * 修改班次
     *
     * @param oaWorkTimes 班次
     * @return 结果
     */
    public int updateOaWorkTimes(OaWorkTimes oaWorkTimes);

    /**
     * 批量删除班次
     *
     * @param ids 需要删除的班次ID
     * @return 结果
     */
    public int deleteOaWorkTimesByIds(String[] ids);

    /**
     * 删除班次信息
     *
     * @param id 班次ID
     * @return 结果
     */
    public int deleteOaWorkTimesById(String id);

    //查询对应班次
    public OaWorkTimes dayWorkTime(String day);
}
