package com.business.system.service.impl;

import java.text.MessageFormat;
import java.util.*;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.enums.ProcessCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.dto.ProcessInstancesRuntimeDTO;
import com.business.system.domain.dto.StartProcessInstanceDTO;
import com.business.system.domain.dto.json.UserInfo;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.domain.vo.BaProcessInstancesRuntimeVO;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.*;
import com.business.system.service.IBaProcessBusinessInstanceRelatedService;
import com.business.system.service.IBaProcessDefinitionRelatedService;
import com.business.system.service.ISysUserService;
import com.business.system.util.PostName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.business.system.service.IOaEvectionService;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

/**
 * 出差申请Service业务层处理
 *
 * @author ljb
 * @date 2023-11-11
 */
@Service
public class OaEvectionServiceImpl extends ServiceImpl<OaEvectionMapper, OaEvection> implements IOaEvectionService
{
    private static final Logger log = LoggerFactory.getLogger(OaEvectionServiceImpl.class);
    @Autowired
    private OaEvectionMapper oaEvectionMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private OaEvectionTripMapper oaEvectionTripMapper;

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private IBaProcessBusinessInstanceRelatedService iBaProcessBusinessInstanceRelatedService;

    @Autowired
    private BaProcessInstanceRelatedMapper baProcessInstanceRelatedMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Value(value = "${user.login.lockTime}")
    private int lockTime;

    @Autowired
    private PostName getName;

    /**
     * 查询出差申请
     *
     * @param id 出差申请ID
     * @return 出差申请
     */
    @Override
    public OaEvection selectOaEvectionById(String id)
    {
        OaEvection oaEvection = oaEvectionMapper.selectOaEvectionById(id);
        List<String> processInstanceIds = new ArrayList<>();
        //流程实例ID
        QueryWrapper<BaProcessInstanceRelated> sealQueryWrapper = new QueryWrapper<>();
        sealQueryWrapper.eq("business_id",oaEvection.getId());
        sealQueryWrapper.eq("flag",0);
        //queryWrapper.ne("approve_result",AdminCodeEnum.BID_STATUS_VERIFYING.getCode());
        sealQueryWrapper.orderByDesc("create_time");
        List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(sealQueryWrapper);
        for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
            processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
        }
        oaEvection.setProcessInstanceId(processInstanceIds);
        //行程信息
        QueryWrapper<OaEvectionTrip> tripQueryWrapper = new QueryWrapper<>();
        tripQueryWrapper.eq("evection_id",oaEvection.getId());
        List<OaEvectionTrip> oaEvectionTrips = oaEvectionTripMapper.selectList(tripQueryWrapper);
        oaEvection.setOaEvectionTripList(oaEvectionTrips);
        //申请人名称
        oaEvection.setUserName(sysUserMapper.selectUserById(oaEvection.getUserId()).getNickName());
        //申请人部门
        oaEvection.setDeptName(sysDeptMapper.selectDeptById(oaEvection.getDeptId()).getDeptName());
        //同行人
        if(StringUtils.isNotEmpty(oaEvection.getPeers())){
            String peersName = "";
            String[] split = oaEvection.getPeers().split(";");
            for (String userId:split) {
                String[] split1 = userId.split(",");
                peersName = sysUserMapper.selectUserById(Long.valueOf(split1[split1.length-1])).getNickName() + "," + peersName;
            }
            oaEvection.setPeersName(peersName.substring(0,peersName.length()-1));
        }
        return oaEvection;
    }

    /**
     * 查询出差申请列表
     *
     * @param oaEvection 出差申请
     * @return 出差申请
     */
    @Override
    public List<OaEvection> selectOaEvectionList(OaEvection oaEvection)
    {
        List<OaEvection> oaEvections = oaEvectionMapper.selectOaEvectionList(oaEvection);
        for (OaEvection evection:oaEvections) {
            //申请人
            if(evection.getUserId() != null){
                SysUser user = sysUserMapper.selectUserById(evection.getUserId());
                evection.setUserName(user.getNickName());
            }
            //部门名称
            if(evection.getDeptId() != null){
                SysDept dept = sysDeptMapper.selectDeptById(evection.getDeptId());
                evection.setDeptName(dept.getDeptName());
            }
            //同行人
            if(StringUtils.isNotEmpty(evection.getPeers())){
                String peersName = "";
                String[] split = evection.getPeers().split(";");
                for (String userId:split) {
                    String[] split1 = userId.split(",");
                    peersName = sysUserMapper.selectUserById(Long.valueOf(split1[split1.length-1])).getNickName() + "," + peersName;
                }
                evection.setPeersName(peersName.substring(0,peersName.length()-1));
            }
            //行程信息
            QueryWrapper<OaEvectionTrip> tripQueryWrapper = new QueryWrapper<>();
            tripQueryWrapper.eq("evection_id",evection.getId());
            List<OaEvectionTrip> oaEvectionTrips = oaEvectionTripMapper.selectList(tripQueryWrapper);
            evection.setOaEvectionTripList(oaEvectionTrips);
            //创建时间
            evection.setCreationTime(evection.getCreateTime());
        }
        return oaEvections;
    }

    /**
     * 新增出差申请
     *
     * @param oaEvection 出差申请
     * @return 结果
     */
    @Override
    public int insertOaEvection(OaEvection oaEvection)
    {
        //判断是否为保存数据
        if(StringUtils.isNotEmpty(oaEvection.getId()) && "0".equals(oaEvection.getSubmitState())){
            if(oaEvection.getOaEvectionTripList().size() > 0){
                //删除原有行程数据
                QueryWrapper<OaEvectionTrip> tripQueryWrapper = new QueryWrapper<>();
                tripQueryWrapper.eq("evection_id",oaEvection.getId());
                List<OaEvectionTrip> oaEvectionTrips = oaEvectionTripMapper.selectList(tripQueryWrapper);
                for (OaEvectionTrip trip:oaEvectionTrips) {
                    oaEvectionTripMapper.deleteOaEvectionTripById(trip.getId());
                }
                //绑定新行程信息
                for (OaEvectionTrip evectionTrip:oaEvection.getOaEvectionTripList()) {
                        evectionTrip.setId(getRedisIncreID.getId());
                        evectionTrip.setEvectionId(oaEvection.getId());
                        evectionTrip.setCreateTime(DateUtils.getNowDate());
                        oaEvectionTripMapper.insertOaEvectionTrip(evectionTrip);
                }
            }
            return oaEvectionMapper.updateOaEvection(oaEvection);
        }
        //判断是否为保存数据提交
        if(StringUtils.isNotEmpty(oaEvection.getId()) && "1".equals(oaEvection.getSubmitState())){
            OaEvection evection = oaEvectionMapper.selectOaEvectionById(oaEvection.getId());
            evection.setFlag(1L);
            oaEvectionMapper.updateOaEvection(evection);
        }
        oaEvection.setId(getRedisIncreID.getId());
        oaEvection.setCreateTime(DateUtils.getNowDate());
        //租户ID
        oaEvection.setTenantId(SecurityUtils.getCurrComId());
        //多行程绑定
        if(oaEvection.getOaEvectionTripList().size() > 0){
            for (OaEvectionTrip oaEvectionTrip:oaEvection.getOaEvectionTripList()) {
                oaEvectionTrip.setId(getRedisIncreID.getId());
                oaEvectionTrip.setEvectionId(oaEvection.getId());
                oaEvectionTrip.setCreateTime(DateUtils.getNowDate());
                oaEvectionTripMapper.insertOaEvectionTrip(oaEvectionTrip);
            }
        }
        //流程实例ID
        String flowId = iBaProcessDefinitionRelatedService.getFlowId(AdminCodeEnum.TASK_TYPE_OA_EVECTION.getCode(),SecurityUtils.getCurrComId());
        oaEvection.setFlowId(flowId);
        int result = oaEvectionMapper.insertOaEvection(oaEvection);
        if(result > 0 && "1".equals(oaEvection.getSubmitState())){
            OaEvection evection = oaEvectionMapper.selectOaEvectionById(oaEvection.getId());
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(evection, AdminCodeEnum.OAEVECTION_APPROVAL_CONTENT_INSERT.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                oaEvectionMapper.deleteOaEvectionById(evection.getId());
                return 0;
            }else {
                evection.setState(AdminCodeEnum.OAEVECTION_STATUS_VERIFYING.getCode());
                oaEvectionMapper.updateOaEvection(evection);
            }
        }
        return result;
    }

    /**
     * 提交出差申请审核
     */
    public BaProcessInstancesRuntimeVO doRuntimeProcessInstances(OaEvection evection, String taskContent) {
        //流程实例启动对象
        ProcessInstancesRuntimeDTO instancesRuntimeDTO = new ProcessInstancesRuntimeDTO();
        instancesRuntimeDTO.setProcessDefinitionKey(evection.getFlowId());
        Map<String, Object> map = new HashMap<>();
        map.put(AdminCodeEnum.TASK_TYPE_OA_EVECTION.getCode(), AdminCodeEnum.TASK_TYPE_OA_EVECTION.getCode());
        instancesRuntimeDTO.setVariables(map);
        //业务与流程实例关联关系对象
        BaProcessInstanceRelated relatedDO = new BaProcessInstanceRelated();
        relatedDO.setBusinessId(evection.getId());
        relatedDO.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_EVECTION.getCode());
        //获取流程实例关联的业务对象
        //BaContract baContract1 = baContractMapper.selectBaContractById(baContract.getId());
        OaEvection oaEvection = this.selectOaEvectionById(evection.getId());
        SysUser sysUser = iSysUserService.selectUserById(oaEvection.getUserId());
        oaEvection.setUserName(sysUser.getNickName());
        oaEvection.setDeptName(sysUser.getDept().getDeptName());
        relatedDO.setBusinessData(JSONObject.toJSONString(oaEvection, SerializerFeature.WriteMapNullValue));
        relatedDO.setApproveType(taskContent);
        instancesRuntimeDTO.setBaProcessInstanceRelated(relatedDO);


        BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO;
        try {
            baProcessInstancesRuntimeVO = runtimeProcessInstances(instancesRuntimeDTO);
        } catch (Exception e) {
            log.error("提交审核异常，原因：{}", e.getMessage(), e);
            throw new ServiceException("启动流程实例异常");
        }
        if (ObjectUtils.isEmpty(baProcessInstancesRuntimeVO)) {
            log.error("提交审核异常，原因：返回结果为空");
            throw new ServiceException("启动流程实例异常");
        }
        return baProcessInstancesRuntimeVO;
    }


    /**
     * 启动流程实例
     */
    public BaProcessInstancesRuntimeVO runtimeProcessInstances(ProcessInstancesRuntimeDTO processInstancesRuntime) {
        //流程实例启动展示对象
        BaProcessInstanceRelated baProcessInstanceRelated = null;
        StartProcessInstanceDTO startProcessInstanceDTO = new StartProcessInstanceDTO();
        startProcessInstanceDTO.setProcessDefinitionKey(processInstancesRuntime.getProcessDefinitionKey());
        //设置发起人
        UserInfo userInfo = iSysUserService.getCurrentUserInfo();
        startProcessInstanceDTO.setStartUserInfo(userInfo);
        startProcessInstanceDTO.setTenantId(SecurityUtils.getCurrComId());
        //设置流程启动业务变量
        startProcessInstanceDTO.setFormData(JSONObject.parseObject(JSON.toJSONString(processInstancesRuntime.getVariables())));
        //设置抄送人
//       startProcessInstanceDTO.setInvolvedUsers(processInstancesRuntime.getInvolvedUsers());
        BaProcessInstancesRuntimeVO data = null;
        try {
            redisCache.lock(Constants.LOGIN_USER, SecurityUtils.getLoginUser(), lockTime);
            AjaxResult baProcessInstancesRuntimeResult = workFlowFeignClient.start(startProcessInstanceDTO);
            if(!ObjectUtils.isEmpty(baProcessInstancesRuntimeResult)){
                redisCache.unLock(Constants.LOGIN_USER);
                data = new BaProcessInstancesRuntimeVO();
                data.setId(baProcessInstancesRuntimeResult.get("data").toString());
            }
        } catch (Exception e) {
            redisCache.unLock(Constants.LOGIN_USER);
            log.info("启动流程实例调用接口异常:{}", e.getMessage());
            return null;
        }
        if(data != null){
            baProcessInstanceRelated = processInstancesRuntime.getBaProcessInstanceRelated();
            baProcessInstanceRelated.setProcessInstanceId(data.getId());
            baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_VERIFYING.getCode());
            if(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType().equals(AdminCodeEnum.OAEVECTION_APPROVAL_CONTENT_UPDATE.getCode())){
                baProcessInstanceRelated.setUpdateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setUpdateBy(SecurityUtils.getUsername());
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }else {
                baProcessInstanceRelated.setCreateTime(DateUtils.getNowDate());
                baProcessInstanceRelated.setCreateBy(SecurityUtils.getUsername());
            }
            data.setApproveType(processInstancesRuntime.getBaProcessInstanceRelated().getApproveType());
        }


        // 保存业务数据与流程实例关系
        if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
            iBaProcessBusinessInstanceRelatedService.save(baProcessInstanceRelated);
        }

        return data;
    }

    /**
     * 修改出差申请
     *
     * @param oaEvection 出差申请
     * @return 结果
     */
    @Override
    public int updateOaEvection(OaEvection oaEvection)
    {
        //原始数据
        OaEvection oaEvection1 = oaEvectionMapper.selectOaEvectionById(oaEvection.getId());

        oaEvection.setUpdateTime(DateUtils.getNowDate());
        //保存数据修改行程
        if("0".equals(oaEvection.getSubmitState())){
            if(oaEvection.getOaEvectionTripList().size() > 0){
                //删除原有行程数据
                QueryWrapper<OaEvectionTrip> tripQueryWrapper = new QueryWrapper<>();
                tripQueryWrapper.eq("evection_id",oaEvection.getId());
                List<OaEvectionTrip> oaEvectionTrips = oaEvectionTripMapper.selectList(tripQueryWrapper);
                for (OaEvectionTrip trip:oaEvectionTrips) {
                    oaEvectionTripMapper.deleteOaEvectionTripById(trip.getId());
                }
                for (OaEvectionTrip oaEvectionTrip:oaEvection.getOaEvectionTripList()) {
                        oaEvectionTrip.setId(getRedisIncreID.getId());
                        oaEvectionTrip.setEvectionId(oaEvection.getId());
                        oaEvectionTrip.setCreateTime(DateUtils.getNowDate());
                        oaEvectionTripMapper.insertOaEvectionTrip(oaEvectionTrip);
                }
            }
        }
        int result = oaEvectionMapper.updateOaEvection(oaEvection);
        if(result > 0 && "1".equals(oaEvection.getSubmitState())){
            OaEvection evection = oaEvectionMapper.selectOaEvectionById(oaEvection.getId());
            //清除流程与实例关系表
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id", evection.getId());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            if (baProcessInstanceRelateds.size() > 0) {
                for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setFlag(1);
                    baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                }
            }
            //启动流程实例
            BaProcessInstancesRuntimeVO baProcessInstancesRuntimeVO = doRuntimeProcessInstances(evection, AdminCodeEnum.OAEVECTION_APPROVAL_CONTENT_UPDATE.getCode());
            if (StringUtils.isNull(baProcessInstancesRuntimeVO)) {
                oaEvectionMapper.updateOaEvection(oaEvection1);
                return 0;
            }else {
                //修改行程信息
                if(oaEvection.getOaEvectionTripList().size() > 0){
                    //删除原有行程数据
                    QueryWrapper<OaEvectionTrip> tripQueryWrapper = new QueryWrapper<>();
                    tripQueryWrapper.eq("evection_id",evection.getId());
                    List<OaEvectionTrip> oaEvectionTrips = oaEvectionTripMapper.selectList(tripQueryWrapper);
                    for (OaEvectionTrip trip:oaEvectionTrips) {
                        oaEvectionTripMapper.deleteOaEvectionTripById(trip.getId());
                    }
                    for (OaEvectionTrip oaEvectionTrip:oaEvection.getOaEvectionTripList()) {
                            oaEvectionTrip.setId(getRedisIncreID.getId());
                            oaEvectionTrip.setEvectionId(oaEvection.getId());
                            oaEvectionTrip.setCreateTime(DateUtils.getNowDate());
                            oaEvectionTripMapper.insertOaEvectionTrip(oaEvectionTrip);
                    }
                }
                evection.setState(AdminCodeEnum.OAEVECTION_STATUS_VERIFYING.getCode());
                oaEvectionMapper.updateOaEvection(evection);
            }
        }
        return result;
    }

    /**
     * 批量删除出差申请
     *
     * @param ids 需要删除的出差申请ID
     * @return 结果
     */
    @Override
    public int deleteOaEvectionByIds(String[] ids)
    {
        if(StringUtils.isNotEmpty(ids)){
            for (String id:ids) {
                OaEvection oaEvection = oaEvectionMapper.selectOaEvectionById(id);
                oaEvection.setFlag(1L);
                oaEvectionMapper.updateOaEvection(oaEvection);
                //查询流程与实例关系表
                QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("business_id", id);
                List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
                if (baProcessInstanceRelateds.size() > 0) {
                    for (BaProcessInstanceRelated baProcessInstanceRelated : baProcessInstanceRelateds) {
                        baProcessInstanceRelated.setFlag(1);
                        baProcessInstanceRelatedMapper.updateInstanceRelated(baProcessInstanceRelated);
                    }
                }
            }
        }
        return oaEvectionMapper.deleteOaEvectionByIds(ids);
    }

    /**
     * 删除出差申请信息
     *
     * @param id 出差申请ID
     * @return 结果
     */
    @Override
    public int deleteOaEvectionById(String id)
    {
        return oaEvectionMapper.deleteOaEvectionById(id);
    }

    @Override
    public int revoke(String id) {
        //判断id不为空
        if(StringUtils.isNotEmpty(id)){
            OaEvection evection = oaEvectionMapper.selectOaEvectionById(id);
            //baBid.getFlowId();
            QueryWrapper<BaProcessInstanceRelated> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("business_id",evection.getId());
            queryWrapper.eq("flag",0);
            queryWrapper.ne("approve_result",AdminCodeEnum.OAEVECTION_STATUS_WITHDRAW.getCode());
            List<BaProcessInstanceRelated> baProcessInstanceRelateds = baProcessInstanceRelatedMapper.selectList(queryWrapper);
            //实例ID集合
            List<String> processInstanceIds = new ArrayList<>();
            for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                processInstanceIds.add(baProcessInstanceRelated.getProcessInstanceId());
            }
            //新建撤回流程实例对象
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            //当前用户信息
            SysUser sysUser = sysUserMapper.selectUserById(evection.getUserId());
            cancelProcessInstanceDTO.setUserId(sysUser.getUserId().toString());
            cancelProcessInstanceDTO.setUserName(sysUser.getUserName());
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIds);
            //撤回流程实例
            AjaxResult result =  workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);
            if(result.get("code").toString().equals("200")){
                //业务与流程实例关联表的审核结果
                for (BaProcessInstanceRelated baProcessInstanceRelated:baProcessInstanceRelateds) {
                    baProcessInstanceRelated.setApproveResult(ProcessCodeEnum.PROCESS_APPROVE_RESULT_WITHDRAW.getCode());
                    baProcessInstanceRelatedMapper.updateById(baProcessInstanceRelated);
                    //释放redis
                    redisCache.deleteObject(Constants.LOGIN_USER + "_" + baProcessInstanceRelated.getProcessInstanceId());
                }
                //修改业务中的审批状态
                evection.setState(AdminCodeEnum.OAEVECTION_STATUS_WITHDRAW.getCode());
                oaEvectionMapper.updateOaEvection(evection);
                String userId = String.valueOf(evection.getUserId());
                //给所有已审批的用户发消息
                if(!CollectionUtils.isEmpty(baProcessInstanceRelateds)){
                    BaProcessInstanceRelated baProcessInstanceRelated = baProcessInstanceRelateds.get(0);
                    if(!ObjectUtils.isEmpty(baProcessInstanceRelated)){
                        ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
                        processInstanceApproveLinkDTO.setStartUserId(userId);
                        processInstanceApproveLinkDTO.setProcessInstanceId(baProcessInstanceRelated.getProcessInstanceId());
                        AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
                        if(!ObjectUtils.isEmpty(ajaxResult)){
                            List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                            for(LinkedHashMap map : data){
                                ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                                try {
                                    approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                                    if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                                        if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                            this.insertMessage(evection, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_OA_EVECTION.getDescription(), MessageConstant.APPROVAL_WITHDRAW));
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new ServiceException("无法获取审批链路信息");
                                }
                            }
                        }
                    }
                }
            }else {
                return 0;
            }
        }
        return 1;
    }

    @Override
    public List<OaEvectionTrip> selectOaEvectionTrip(OaEvectionTrip oaEvectionTrip) {
        List<OaEvectionTrip> oaEvectionTrips = oaEvectionTripMapper.selectOaEvectionTrip(oaEvectionTrip);
        for (OaEvectionTrip evectionTrip:oaEvectionTrips) {
            //同行人
            if(StringUtils.isNotEmpty(evectionTrip.getPeers())){
                String peersName = "";
                String[] split = evectionTrip.getPeers().split(";");
                for (String userId:split) {
                    String[] split1 = userId.split(",");
                    peersName = sysUserMapper.selectUserById(Long.valueOf(split1[split1.length-1])).getNickName() + "," + peersName;
                }
                evectionTrip.setPeersName(peersName.substring(0,peersName.length()-1));
            }
        }
        return oaEvectionTrips;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(OaEvection oaEvection, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(oaEvection.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_EVECTION.getCode());
        baMessageMapper.insertBaMessage(baMessage);
    }

}
