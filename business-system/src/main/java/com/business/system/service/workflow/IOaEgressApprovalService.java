package com.business.system.service.workflow;

import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.BeanUtil;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.BaMessage;
import com.business.system.domain.OaClock;
import com.business.system.domain.OaEgress;
import com.business.system.domain.OaEvection;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.BaMessageMapper;
import com.business.system.mapper.OaClockMapper;
import com.business.system.mapper.OaEgressMapper;
import com.business.system.mapper.OaEvectionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 外出申请审批接口 服务类
 * @date: 2023/11/11 16:38
 */
@Service
public class IOaEgressApprovalService {

    @Autowired
    protected OaEgressMapper oaEgressMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private OaClockMapper oaClockMapper;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        OaEgress oaEgress = new OaEgress();
        oaEgress.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        oaEgress = oaEgressMapper.selectOaEgressById(oaEgress.getId());
        oaEgress.setState(status);
        String userId = String.valueOf(oaEgress.getUserId());

        OaClock clock = new OaClock();
        clock.setEmployeeId(oaEgress.getUserId());
        if(AdminCodeEnum.OAEGRESS_STATUS_PASS.getCode().equals(status)){

            //请假更新考勤数据
            if(!ObjectUtils.isEmpty(oaEgress.getEveryDay())){
                //查看每次行程每一天
                String[] split = oaEgress.getEveryDay().split(",");
                //数组转list
                List<String> list1 = Arrays.asList(split);
                if(list1.size() == 1){
                    //查询打卡信息
                    clock.setQueryTime(list1.get(0));
                    //clock.setFlag(1L);
                    List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                    for (OaClock oaClock:oaClocks) {
                        if("1".equals(oaEgress.getStartCycle())){
                            //修改上班打卡数据
                            if(oaClock.getClockType().equals("1")){
                                //打卡状态为外出
                                oaClock.setClockState("10");
                                oaClockMapper.updateOaClock(oaClock);
                            }
                        }else if("2".equals(oaEgress.getStartCycle())){
                            //修改下班打卡数据
                            if(oaClock.getClockType().equals("2")){
                                //打卡状态为外出
                                oaClock.setClockState("10");
                                oaClockMapper.updateOaClock(oaClock);
                            }
                        }
                        if("1".equals(oaEgress.getEndCycle())){
                            //修改上班打卡数据
                            if("1".equals(oaClock.getClockType())){
                                //打卡状态为外出
                                oaClock.setClockState("10");
                                oaClockMapper.updateOaClock(oaClock);
                            }
                        }else if("2".equals(oaEgress.getEndCycle())){
                            //修改上班打卡数据
                            if("2".equals(oaClock.getClockType())){
                                //打卡状态为外出
                                oaClock.setClockState("10");
                                oaClockMapper.updateOaClock(oaClock);
                            }
                        }
                    }
                }else {
                    for (String day:list1) {
                        //拿到第一个
                        if(day.equals(list1.get(0))){
                            //查询打卡信息
                            clock.setQueryTime(day);
                            //clock.setFlag(1L);
                            List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                            for (OaClock oaClock:oaClocks) {
                                if("1".equals(oaEgress.getStartCycle())){
                                    //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                        //打卡状态为外出
                                        oaClock.setClockState("10");
                                        oaClockMapper.updateOaClock(oaClock);
                                }else if("2".equals(oaEgress.getStartCycle())){
                                    //修改下班打卡数据
                                    if("2".equals(oaClock.getClockType())){
                                        //打卡状态为外出
                                        oaClock.setClockState("10");
                                        oaClockMapper.updateOaClock(oaClock);
                                    }
                                }
                            }
                        }else if(day.equals(list1.get(list1.size()-1))){
                            //最后一天做对比
                            clock.setQueryTime(day);
                            //clock.setFlag(1L);
                            List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                            for (OaClock oaClock:oaClocks) {
                                if("1".equals(oaEgress.getEndCycle())){
                                    //修改上班打卡数据
                                    if("1".equals(oaClock.getClockType())){
                                        //打卡状态为出差
                                        oaClock.setClockState("10");
                                        oaClockMapper.updateOaClock(oaClock);
                                    }
                                }else if("2".equals(oaEgress.getEndCycle())){
                                    //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                        //打卡状态为外出
                                        oaClock.setClockState("10");
                                        oaClockMapper.updateOaClock(oaClock);
                                }
                            }
                        }else {
                            clock.setQueryTime(day);
                            //clock.setFlag(1L);
                            List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                            //上班下班都要更改
                            for (OaClock oaClock:oaClocks) {
                                //打卡状态为出差
                                oaClock.setClockState("10");
                                oaClockMapper.updateOaClock(oaClock);
                            }
                        }
                    }
                }
            }

            oaEgress.setSubmitTime(DateUtils.getNowDate());

            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(oaEgress, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_OA_EGRESS.getDescription(), MessageConstant.APPROVAL_PASS));
        } else if(AdminCodeEnum.OAEGRESS_STATUS_REJECT.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(oaEgress, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_OA_EGRESS.getDescription(), MessageConstant.APPROVAL_REJECT));

            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(oaEgress, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_OA_EGRESS.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
        }
        oaEgressMapper.updateById(oaEgress);
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(OaEgress oaEgress, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(oaEgress.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_EGRESS.getCode());
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected OaEgress checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        OaEgress oaEgress = oaEgressMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (oaEgress == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_OAEGRESS_001);
        }
        if (!AdminCodeEnum.OAEGRESS_STATUS_VERIFYING.getCode().equals(oaEgress.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_OAEVECTION_002);
        }
        return oaEgress;
    }
}
