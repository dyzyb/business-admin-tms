package com.business.system.service;

import java.util.List;
import com.business.system.domain.OaAttendanceAddress;

/**
 * 考勤组地点关联Service接口
 *
 * @author single
 * @date 2023-11-24
 */
public interface IOaAttendanceAddressService
{
    /**
     * 查询考勤组地点关联
     *
     * @param id 考勤组地点关联ID
     * @return 考勤组地点关联
     */
    public OaAttendanceAddress selectOaAttendanceAddressById(String id);

    /**
     * 查询考勤组地点关联列表
     *
     * @param oaAttendanceAddress 考勤组地点关联
     * @return 考勤组地点关联集合
     */
    public List<OaAttendanceAddress> selectOaAttendanceAddressList(OaAttendanceAddress oaAttendanceAddress);

    /**
     * 新增考勤组地点关联
     *
     * @param oaAttendanceAddress 考勤组地点关联
     * @return 结果
     */
    public int insertOaAttendanceAddress(OaAttendanceAddress oaAttendanceAddress);

    /**
     * 修改考勤组地点关联
     *
     * @param oaAttendanceAddress 考勤组地点关联
     * @return 结果
     */
    public int updateOaAttendanceAddress(OaAttendanceAddress oaAttendanceAddress);

    /**
     * 批量删除考勤组地点关联
     *
     * @param ids 需要删除的考勤组地点关联ID
     * @return 结果
     */
    public int deleteOaAttendanceAddressByIds(String[] ids);

    /**
     * 删除考勤组地点关联信息
     *
     * @param id 考勤组地点关联ID
     * @return 结果
     */
    public int deleteOaAttendanceAddressById(String id);


}
