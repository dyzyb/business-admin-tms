package com.business.system.service.workflow;

import com.business.common.constant.Constants;
import com.business.common.constant.MessageConstant;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.core.redis.RedisCache;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.exception.ServiceException;
import com.business.common.exception.VerificationException;
import com.business.common.utils.BeanUtil;
import com.business.common.utils.DateUtils;
import com.business.common.utils.MapUtil;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.BaMessage;
import com.business.system.domain.OaClock;
import com.business.system.domain.OaSupplyClock;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.domain.dto.CancelProcessInstanceDTO;
import com.business.system.domain.dto.OaClockDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.domain.vo.ApproveLinkHistoryTaskVO;
import com.business.system.exception.BusinessVerificationErrorCode;
import com.business.system.feign.WorkFlowFeignClient;
import com.business.system.mapper.BaMessageMapper;
import com.business.system.mapper.OaClockMapper;
import com.business.system.mapper.OaSupplyClockMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @Author: js
 * @Description: 补卡申请审批接口 服务类
 * @date: 2023/11/27 16:38
 */
@Service
public class IOaSupplyClockApprovalService {

    @Autowired
    protected OaSupplyClockMapper oaSupplyClockMapper;

    @Autowired
    protected BeanUtil beanUtil;

    @Autowired
    private WorkFlowFeignClient workFlowFeignClient;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private OaClockMapper oaClockMapper;

    protected Object doAcceptApprovalResult(AcceptApprovalWorkflowDTO approvalWorkflowDTO, String status) {
        OaSupplyClock oaSupplyClock = new OaSupplyClock();
        oaSupplyClock.setId(String.valueOf(approvalWorkflowDTO.getTargetId()));
        oaSupplyClock = oaSupplyClockMapper.selectOaSupplyClockById(oaSupplyClock.getId());
        oaSupplyClock.setState(status);
        String userId = String.valueOf(oaSupplyClock.getUserId());
        if(AdminCodeEnum.OASUPPLYCLOCK_STATUS_PASS.getCode().equals(status)){

            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(oaSupplyClock, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_OA_SUPPLYCLOCK.getDescription(), MessageConstant.APPROVAL_PASS));

            //打卡查询条件
            if(!ObjectUtils.isEmpty(oaSupplyClock)){
                OaClockDTO oaClockDTO = new OaClockDTO();
                oaClockDTO.setEmployeeId(oaSupplyClock.getUserId());
                oaClockDTO.setSearchTime(DateUtils.dateTime(oaSupplyClock.getClockDate())); // 打卡日期
                oaClockDTO.setType("1"); //内勤
                oaClockDTO.setClockType(oaSupplyClock.getClockType()); //上班打卡、下班打卡
                List<OaClock> oaClocks = oaClockMapper.statOaClockList(oaClockDTO);
                if(!CollectionUtils.isEmpty(oaClocks)){
                    //查询打卡数据,进行补卡
                    OaClock clock = oaClocks.get(0);
                    if(!ObjectUtils.isEmpty(clock)) {
                        clock.setClockDate(oaSupplyClock.getClockDate()); //补卡日期
                        clock.setClockTime(oaSupplyClock.getClockTime()); //补卡时间
                        clock.setFlag(new Long(0)); //删除标识
                        //弹性工作制
                        if("2".equals(clock.getAttendanceType())){
                            if("2".equals(clock.getClockType())) {
                                OaClockDTO oaClockDTO1 = new OaClockDTO();
                                oaClockDTO1.setEmployeeId(oaSupplyClock.getUserId());
                                oaClockDTO1.setSearchTime(DateUtils.dateTime(oaSupplyClock.getClockDate())); // 打卡日期
                                oaClockDTO1.setType("1"); //内勤
                                oaClockDTO1.setClockType("1"); //查询上班打卡
                                List<OaClock> oaClocks1 = oaClockMapper.statOaClockList(oaClockDTO1);
                                oaClockDTO1.setClockType("2"); //查询下班打卡
                                List<OaClock> oaClocks2 = oaClockMapper.statOaClockList(oaClockDTO1);
                                if (!CollectionUtils.isEmpty(oaClocks1) && !CollectionUtils.isEmpty(oaClocks2)) {
                                    //查询打卡数据,进行补卡
                                    OaClock clock1 = oaClocks1.get(0);
                                    OaClock clock2 = oaClocks2.get(0);
                                    //已打卡
                                    if (clock1.getFlag().intValue() == 0) {
                                        Calendar calendar = Calendar.getInstance();
                                        calendar.setTime(clock1.getClockTime()); //打卡开始时间
                                        // 打卡日期增加工作时长
                                        calendar.add(Calendar.HOUR_OF_DAY, clock1.getWorkHours().intValue());
                                        //计算下班打卡时间（早退时长）
                                        LocalTime startTime = LocalTime.of(clock1.getInitialTime().getHours(),calendar.getTime().getMinutes());
                                        LocalTime endTime = LocalTime.of(clock1.getClockTime().getHours(),clock1.getClockTime().getMinutes());
                                        Duration between = Duration.between(endTime, startTime);
                                        //下班打卡
                                        Calendar calendar1 = Calendar.getInstance();
                                        calendar1.setTime(oaSupplyClock.getClockTime());
                                        int result = calendar1.getTime().compareTo(calendar.getTime());
                                        if (result < 0) {
                                            //早退时间
                                            clock1.setDuration(between.toMinutes());
                                            clock2.setClockState("3"); //早退
                                        } else {
                                            clock2.setClockState("1"); //正常
                                        }
                                        clock2.setClockTime(calendar1.getTime());
                                    }
                                    clock2.setClockFlag(new Long(2)); //补卡
                                    clock2.setClockDate(oaSupplyClock.getClockDate()); //补卡日期
                                    clock2.setClockTime(oaSupplyClock.getClockTime()); //补卡时间
                                    clock2.setFlag(new Long(0)); //删除标识
                                    oaClockMapper.updateOaClock(clock2);
                                }
                            } else if("1".equals(clock.getClockType())){
                                //已打卡
                                clock.setClockState("1"); //正常
                                clock.setClockFlag(new Long(2)); //补卡
                                clock.setClockDate(oaSupplyClock.getClockDate()); //补卡日期
                                clock.setClockTime(oaSupplyClock.getClockTime()); //补卡时间
                                clock.setFlag(new Long(0)); //删除标识
                                oaClockMapper.updateOaClock(clock);
                            }
                        } else {
                            //上班打卡
                            if ("1".equals(clock.getClockType())) {

                                //判断是否迟到
                                if(clock.getClockTime().compareTo(clock.getInitialTime()) > 0){
                                    //计算上班时间差
                                    LocalTime startTime = LocalTime.of(clock.getInitialTime().getHours(),clock.getInitialTime().getMinutes());
                                    LocalTime endTime = LocalTime.of(clock.getClockTime().getHours(),clock.getClockTime().getMinutes());
                                    Duration between = Duration.between(startTime, endTime);

                                    long l = between.toMinutes();
                                    //判断是否上班迟到(迟到分钟数不等于空其他都为空)
                                    if(clock.getAllowedLate() > 0 && clock.getSeverelyLate() == 0 && clock.getAbsenteeismLate() == 0){
                                        //时间差小于允许迟到分钟数打卡正常
                                        if(between.toMinutes() <= clock.getAllowedLate()){
                                            clock.setClockState("1");
                                        }else {
                                            //迟到时间
                                            clock.setDuration(l);
                                            clock.setClockState("2");
                                        }
                                    }else {
                                        //迟到时间
                                        clock.setDuration(l);
                                        clock.setClockState("2");
                                    }
                                    //允许旷工为领
                                    if(clock.getAllowedLate() > 0 && clock.getSeverelyLate() > 0 && clock.getAbsenteeismLate() == 0){
                                        //时间差小于允许迟到分钟数打卡正常
                                        if(between.toMinutes() <= clock.getAllowedLate()){
                                            clock.setClockState("1");
                                        }else if(clock.getAllowedLate() < between.toMinutes() && between.toMinutes() < clock.getSeverelyLate()){
                                            //迟到时间
                                            clock.setDuration(l);
                                            clock.setClockState("2");
                                        }else if(between.toMinutes() >= clock.getSeverelyLate()){
                                            //迟到时间
                                            clock.setDuration(l);
                                            clock.setClockState("6");
                                        }
                                    }
                                    //迟到旷工
                                    if(clock.getAllowedLate() > 0 && clock.getSeverelyLate() > 0 && clock.getAbsenteeismLate() > 0){
                                        //时间差小于允许迟到分钟数打卡正常
                                        if(between.toMinutes() <= clock.getAllowedLate()){
                                            clock.setClockState("1");
                                        }else if(clock.getAllowedLate() < between.toMinutes() && between.toMinutes() < clock.getSeverelyLate()){
                                            //迟到时间
                                            clock.setDuration(l);
                                            clock.setClockState("2");
                                        }else if(between.toMinutes() >= clock.getSeverelyLate() && between.toMinutes() < clock.getAbsenteeismLate()){
                                            //迟到时间
                                            clock.setDuration(l);
                                            clock.setClockState("6");
                                        }else if(between.toMinutes() >= clock.getAbsenteeismLate()){
                                            clock.setClockState("7");
                                        }
                                    }
                                    //允许迟到分钟数为零
                                    if(clock.getAllowedLate() == 0 && clock.getSeverelyLate() > 0 && clock.getAbsenteeismLate() > 0){
                                        if(clock.getAllowedLate() < between.toMinutes() && between.toMinutes() < clock.getSeverelyLate()){
                                            //迟到时间
                                            clock.setDuration(l);
                                            clock.setClockState("2");
                                        }else if(between.toMinutes() >= clock.getSeverelyLate() && between.toMinutes() < clock.getAbsenteeismLate()){
                                            //迟到时间
                                            clock.setDuration(l);
                                            clock.setClockState("6");
                                        }else if(between.toMinutes() >= clock.getAbsenteeismLate()){
                                            clock.setClockState("7");
                                        }else {
                                            //迟到时间
                                            clock.setDuration(l);
                                            clock.setClockState("2");
                                        }
                                    }
                                    //严重迟到分钟数为零
                                    if(clock.getAllowedLate() > 0 && clock.getSeverelyLate() == 0 && clock.getAbsenteeismLate() > 0){
                                        //时间差小于允许迟到分钟数打卡正常
                                        if(between.toMinutes() < clock.getAllowedLate()){
                                            clock.setClockState("1");
                                        }else if(between.toMinutes() > clock.getSeverelyLate() && between.toMinutes() < clock.getAbsenteeismLate()){
                                            //迟到时间
                                            clock.setDuration(l);
                                            clock.setClockState("6");
                                        }else if(between.toMinutes() > clock.getAbsenteeismLate()){
                                            clock.setClockState("7");
                                        }
                                    }
                                    //只存在严重迟到
                                    if(clock.getAllowedLate() == 0 && clock.getSeverelyLate() > 0 && clock.getAbsenteeismLate() == 0){
                                        if(between.toMinutes() >= clock.getSeverelyLate()){
                                            //迟到时间
                                            clock.setDuration(l);
                                            clock.setClockState("6");
                                        }
                                    }
                                    //只存在迟到旷工
                                    if(clock.getAllowedLate() == 0 && clock.getSeverelyLate() == 0 && clock.getAbsenteeismLate() > 0){
                                        if(between.toMinutes() >= clock.getSeverelyLate()){
                                            clock.setClockState("7");
                                        }
                                    }
                               /* clock1.setClockState("2");
                                clockState = "2";*/
                                }else {
                                    clock.setClockState("1");
                                }
                            } else {
                                //下班是否早退
                                if(clock.getClockTime().compareTo(clock.getInitialTime()) < 0){
                                    //计算上班时间差
                                    LocalTime startTime = LocalTime.of(clock.getInitialTime().getHours(),clock.getInitialTime().getMinutes());
                                    LocalTime endTime = LocalTime.of(clock.getClockTime().getHours(),clock.getClockTime().getMinutes());
                                    Duration between = Duration.between(endTime, startTime);
                                    //早退旷工
                                    if(clock.getAbsenteeismEarly() > 0){
                                        if(between.toMinutes() >= clock.getAbsenteeismEarly()){
                                            clock.setClockState("8");
                                        }else {
                                            //早退
                                            clock.setDuration(between.toMinutes());
                                            clock.setClockState("3");
                                        }
                                    }else {
                                        //早退
                                        clock.setDuration(between.toMinutes());
                                        clock.setClockState("3");
                                    }

                                }else {
                                    clock.setClockState("1");
                                }
                            }
                            clock.setClockFlag(new Long(2)); //补卡
                            oaClockMapper.updateOaClock(clock);
                        }
                    }
                }
            }
        } else if(AdminCodeEnum.OASUPPLYCLOCK_STATUS_REJECT.getCode().equals(status)){
            redisCache.deleteObject(Constants.LOGIN_USER + "_" + approvalWorkflowDTO.getProcessInstanceId());
            //插入消息通知信息
            this.insertMessage(oaSupplyClock, userId, MessageFormat.format(MessageConstant.APPROVAL_MSG, AdminCodeEnum.TASK_TYPE_OA_SUPPLYCLOCK.getDescription(), MessageConstant.APPROVAL_REJECT));

            List<String> processInstanceIdList = new ArrayList<>();
            processInstanceIdList.add(approvalWorkflowDTO.getProcessInstanceId());
            CancelProcessInstanceDTO cancelProcessInstanceDTO = new CancelProcessInstanceDTO();
            cancelProcessInstanceDTO.setProcessInstanceIds(processInstanceIdList);
            cancelProcessInstanceDTO.setReason("拒绝取消流程实例");
            workFlowFeignClient.cancelProcessInstance(cancelProcessInstanceDTO);

            //给所有已审批的用户发消息
            ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO = new ProcessInstanceApproveLinkDTO();
            processInstanceApproveLinkDTO.setStartUserId(userId);
            processInstanceApproveLinkDTO.setProcessInstanceId(approvalWorkflowDTO.getProcessInstanceId());
            AjaxResult ajaxResult = workFlowFeignClient.listInstanceApproveLink(processInstanceApproveLinkDTO);
            if(!ObjectUtils.isEmpty(ajaxResult)){
                List<LinkedHashMap> data = (List<LinkedHashMap>) ajaxResult.get("data");
                for(LinkedHashMap map : data){
                    ApproveLinkHistoryTaskVO approveLinkHistoryTaskVO = null;
                    try {
                        approveLinkHistoryTaskVO = MapUtil.mapToObj(map, ApproveLinkHistoryTaskVO.class);
                        if(!ObjectUtils.isEmpty(approveLinkHistoryTaskVO)){
                            if("已通过".equals(approveLinkHistoryTaskVO.getBusinessStatus())){
                                this.insertMessage(oaSupplyClock, approveLinkHistoryTaskVO.getAssignee(), MessageFormat.format(MessageConstant.PARTICIPATE_MSG, AdminCodeEnum.TASK_TYPE_OA_SUPPLYCLOCK.getDescription(), MessageConstant.APPROVAL_REJECT));
                            }
                        }
                    } catch (Exception e) {
                        throw new ServiceException("无法获取审批链路信息");
                    }
                }
            }
        }
        oaSupplyClockMapper.updateById(oaSupplyClock);
        return true;
    }

    /**
     * 插入消息
     * @return
     */
    private void insertMessage(OaSupplyClock oaSupplyClock, String mId, String msgContent){
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setBusinessId(oaSupplyClock.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType("1"); //审批提醒
        baMessage.setCreateBy(SecurityUtils.getUsername());
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessage.setBusinessType(AdminCodeEnum.TASK_TYPE_OA_SUPPLYCLOCK.getCode());
        baMessageMapper.insertBaMessage(baMessage);
    }

    /**
     * 验证数据的有效性
     */
    protected OaSupplyClock checkValidate(AcceptApprovalWorkflowDTO approvalWorkflowDTO) {
        OaSupplyClock oaSupplyClock = oaSupplyClockMapper.selectById(approvalWorkflowDTO.getTargetId());
        if (oaSupplyClock == null) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_OASUPPLYCLOCK_001);
        }
        if (!AdminCodeEnum.OASUPPLYCLOCK_STATUS_VERIFYING.getCode().equals(oaSupplyClock.getState())) {
            throw new VerificationException(BusinessVerificationErrorCode.VERIFICATION_OASUPPLYCLOCK_002);
        }
        return oaSupplyClock;
    }
}
