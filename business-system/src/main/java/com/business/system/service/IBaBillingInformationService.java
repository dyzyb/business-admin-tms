package com.business.system.service;

import com.business.system.domain.BaBillingInformation;

import java.util.List;

/**
 * 开票信息Service接口
 *
 * @author single
 * @date 2023-02-28
 */
public interface IBaBillingInformationService
{
    /**
     * 查询开票信息
     *
     * @param id 开票信息ID
     * @return 开票信息
     */
    public BaBillingInformation selectBaBillingInformationById(String id);

    /**
     * 查询开票信息列表
     *
     * @param baBillingInformation 开票信息
     * @return 开票信息集合
     */
    public List<BaBillingInformation> selectBaBillingInformationList(BaBillingInformation baBillingInformation);

    /**
     * 新增开票信息
     *
     * @param baBillingInformation 开票信息
     * @return 结果
     */
    public int insertBaBillingInformation(BaBillingInformation baBillingInformation);

    /**
     * 修改开票信息
     *
     * @param baBillingInformation 开票信息
     * @return 结果
     */
    public int updateBaBillingInformation(BaBillingInformation baBillingInformation);

    /**
     * 批量删除开票信息
     *
     * @param ids 需要删除的开票信息ID
     * @return 结果
     */
    public int deleteBaBillingInformationByIds(String[] ids);

    /**
     * 删除开票信息信息
     *
     * @param id 开票信息ID
     * @return 结果
     */
    public int deleteBaBillingInformationById(String id);


}
