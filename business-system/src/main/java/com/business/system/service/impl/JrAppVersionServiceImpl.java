package com.business.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.business.common.utils.DateUtils;
import com.business.system.domain.JrAppVersion;
import com.business.system.mapper.JrAppVersionMapper;
import com.business.system.service.IJrAppVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * app版本Service业务层处理
 *
 * @author single
 * @date 2022-04-27
 */
@Service
public class JrAppVersionServiceImpl extends ServiceImpl<JrAppVersionMapper, JrAppVersion> implements IJrAppVersionService
{
    @Autowired
    private JrAppVersionMapper jrAppVersionMapper;

    /**
     * 查询app版本
     *
     * @param versionId app版本ID
     * @return app版本
     */
    @Override
    public JrAppVersion selectJrAppVersionById(String versionId)
    {
        return jrAppVersionMapper.selectJrAppVersionById(versionId);
    }

    /**
     * 查询app版本列表
     *
     * @param jrAppVersion app版本
     * @return app版本
     */
    @Override
    public List<JrAppVersion> selectJrAppVersionList(JrAppVersion jrAppVersion)
    {
        return jrAppVersionMapper.selectJrAppVersionList(jrAppVersion);
    }

    /**
     * 新增app版本
     *
     * @param jrAppVersion app版本
     * @return 结果
     */
    @Override
    public int insertJrAppVersion(JrAppVersion jrAppVersion)
    {
        jrAppVersion.setCreateTime(DateUtils.getNowDate());
        return jrAppVersionMapper.insertJrAppVersion(jrAppVersion);
    }

    /**
     * 修改app版本
     *
     * @param jrAppVersion app版本
     * @return 结果
     */
    @Override
    public int updateJrAppVersion(JrAppVersion jrAppVersion)
    {
        jrAppVersion.setUpdateTime(DateUtils.getNowDate());
        return jrAppVersionMapper.updateJrAppVersion(jrAppVersion);
    }

    /**
     * 批量删除app版本
     *
     * @param versionIds 需要删除的app版本ID
     * @return 结果
     */
    @Override
    public int deleteJrAppVersionByIds(String[] versionIds)
    {
        return jrAppVersionMapper.deleteJrAppVersionByIds(versionIds);
    }

    /**
     * 删除app版本信息
     *
     * @param versionId app版本ID
     * @return 结果
     */
    @Override
    public int deleteJrAppVersionById(String versionId)
    {
        return jrAppVersionMapper.deleteJrAppVersionById(versionId);
    }

}
