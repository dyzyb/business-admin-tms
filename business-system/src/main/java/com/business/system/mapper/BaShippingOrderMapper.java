package com.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaShippingOrder;
import com.business.system.domain.vo.BaShippingOrderGoodsProportionVO;
import com.business.system.domain.vo.BaShippingOrderVO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 发运单Mapper接口
 *
 * @author single
 * @date 2023-02-18
 */
@Repository
public interface BaShippingOrderMapper extends BaseMapper<BaShippingOrder>
{
    /**
     * 查询发运单
     *
     * @param id 发运单ID
     * @return 发运单
     */
    public BaShippingOrder selectBaShippingOrderById(String id);

    /**
     * 查询发运单列表
     *
     * @param baShippingOrder 发运单
     * @return 发运单集合
     */
    public List<BaShippingOrder> selectBaShippingOrderList(BaShippingOrder baShippingOrder);

    /**
     * 查询发运单列表
     *
     * @param baShippingOrder 发运单
     * @return 发运单集合
     */
    public List<BaShippingOrder> baShippingOrderList(BaShippingOrder baShippingOrder);

    /**
     * 全局运单接口
     * @param baShippingOrder
     * @return
     */
    //public List<waybillDTO> waybillDTOList(BaShippingOrder baShippingOrder);

    /**
     * 新增发运单
     *
     * @param baShippingOrder 发运单
     * @return 结果
     */
    public int insertBaShippingOrder(BaShippingOrder baShippingOrder);

    /**
     * 修改发运单
     *
     * @param baShippingOrder 发运单
     * @return 结果
     */
    public int updateBaShippingOrder(BaShippingOrder baShippingOrder);

    /**
     * 删除发运单
     *
     * @param id 发运单ID
     * @return 结果
     */
    public int deleteBaShippingOrderById(String id);

    /**
     * 批量删除发运单
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaShippingOrderByIds(String[] ids);

    /**
     * 查询统计最近个日期网货平台运单数据
     *
     * @return 发运单
     */
    public List<BaShippingOrderVO> recentCars();

    /**
     * 统计拉运货物占比
     *
     * @return 发运单
     */
    public List<BaShippingOrderGoodsProportionVO> selectGoodsProportion();

    /**
     * 运单列表
     * @param supplierNo
     * @return
     */
    public List<BaShippingOrder> waybillList(String supplierNo);
}
