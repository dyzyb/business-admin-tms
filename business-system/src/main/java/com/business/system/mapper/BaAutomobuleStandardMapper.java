package com.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaAutomobuleStandard;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 货源中运价Mapper接口
 *
 * @author ljb
 * @date 2023-02-17
 */
@Repository
public interface BaAutomobuleStandardMapper extends BaseMapper<BaAutomobuleStandard>
{
    /**
     * 查询货源中运价
     *
     * @param id 货源中运价ID
     * @return 货源中运价
     */
    public BaAutomobuleStandard selectBaAutomobuleStandardById(String id);

    /**
     * 查询货源中运价列表
     *
     * @param baAutomobuleStandard 货源中运价
     * @return 货源中运价集合
     */
    public List<BaAutomobuleStandard> selectBaAutomobuleStandardList(BaAutomobuleStandard baAutomobuleStandard);

    /**
     * 新增货源中运价
     *
     * @param baAutomobuleStandard 货源中运价
     * @return 结果
     */
    public int insertBaAutomobuleStandard(BaAutomobuleStandard baAutomobuleStandard);

    /**
     * 修改货源中运价
     *
     * @param baAutomobuleStandard 货源中运价
     * @return 结果
     */
    public int updateBaAutomobuleStandard(BaAutomobuleStandard baAutomobuleStandard);

    /**
     * 删除货源中运价
     *
     * @param id 货源中运价ID
     * @return 结果
     */
    public int deleteBaAutomobuleStandardById(String id);

    /**
     * 批量删除货源中运价
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaAutomobuleStandardByIds(String[] ids);
}
