package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaSupervisingRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 办理记录Mapper接口
 *
 * @author ljb
 * @date 2023-11-21
 */
@Repository
public interface OaSupervisingRecordMapper extends BaseMapper<OaSupervisingRecord>
{
    /**
     * 查询办理记录
     *
     * @param id 办理记录ID
     * @return 办理记录
     */
    public OaSupervisingRecord selectOaSupervisingRecordById(String id);

    /**
     * 查询办理记录列表
     *
     * @param oaSupervisingRecord 办理记录
     * @return 办理记录集合
     */
    public List<OaSupervisingRecord> selectOaSupervisingRecordList(OaSupervisingRecord oaSupervisingRecord);

    /**
     * 新增办理记录
     *
     * @param oaSupervisingRecord 办理记录
     * @return 结果
     */
    public int insertOaSupervisingRecord(OaSupervisingRecord oaSupervisingRecord);

    /**
     * 修改办理记录
     *
     * @param oaSupervisingRecord 办理记录
     * @return 结果
     */
    public int updateOaSupervisingRecord(OaSupervisingRecord oaSupervisingRecord);

    /**
     * 删除办理记录
     *
     * @param id 办理记录ID
     * @return 结果
     */
    public int deleteOaSupervisingRecordById(String id);

    /**
     * 批量删除办理记录
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaSupervisingRecordByIds(String[] ids);
}
