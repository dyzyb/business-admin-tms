package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaSupplyClock;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 补卡Mapper接口
 *
 * @author single
 * @date 2023-11-27
 */
@Repository
public interface OaSupplyClockMapper extends BaseMapper<OaSupplyClock>
{
    /**
     * 查询补卡
     *
     * @param id 补卡ID
     * @return 补卡
     */
    public OaSupplyClock selectOaSupplyClockById(String id);

    /**
     * 查询补卡列表
     *
     * @param oaSupplyClock 补卡
     * @return 补卡集合
     */
    public List<OaSupplyClock> selectOaSupplyClockList(OaSupplyClock oaSupplyClock);

    /**
     * 统计补卡列表
     *
     * @param oaSupplyClock 补卡
     * @return 补卡集合
     */
    public Long countOaSupplyClockList(OaSupplyClock oaSupplyClock);

    /**
     * 新增补卡
     *
     * @param oaSupplyClock 补卡
     * @return 结果
     */
    public int insertOaSupplyClock(OaSupplyClock oaSupplyClock);

    /**
     * 修改补卡
     *
     * @param oaSupplyClock 补卡
     * @return 结果
     */
    public int updateOaSupplyClock(OaSupplyClock oaSupplyClock);

    /**
     * 删除补卡
     *
     * @param id 补卡ID
     * @return 结果
     */
    public int deleteOaSupplyClockById(String id);

    /**
     * 批量删除补卡
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaSupplyClockByIds(String[] ids);
}
