package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaAttendanceAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 考勤组地点关联Mapper接口
 *
 * @author single
 * @date 2023-11-24
 */
@Repository
public interface OaAttendanceAddressMapper extends BaseMapper<OaAttendanceAddress>
{
    /**
     * 查询考勤组地点关联
     *
     * @param id 考勤组地点关联ID
     * @return 考勤组地点关联
     */
    public OaAttendanceAddress selectOaAttendanceAddressById(String id);

    /**
     * 查询考勤组地点关联列表
     *
     * @param oaAttendanceAddress 考勤组地点关联
     * @return 考勤组地点关联集合
     */
    public List<OaAttendanceAddress> selectOaAttendanceAddressList(OaAttendanceAddress oaAttendanceAddress);

    /**
     * 新增考勤组地点关联
     *
     * @param oaAttendanceAddress 考勤组地点关联
     * @return 结果
     */
    public int insertOaAttendanceAddress(OaAttendanceAddress oaAttendanceAddress);

    /**
     * 修改考勤组地点关联
     *
     * @param oaAttendanceAddress 考勤组地点关联
     * @return 结果
     */
    public int updateOaAttendanceAddress(OaAttendanceAddress oaAttendanceAddress);

    /**
     * 删除考勤组地点关联
     *
     * @param id 考勤组地点关联ID
     * @return 结果
     */
    public int deleteOaAttendanceAddressById(String id);

    /**
     * 批量删除考勤组地点关联
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaAttendanceAddressByIds(String[] ids);
}
