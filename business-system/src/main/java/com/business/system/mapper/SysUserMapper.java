package com.business.system.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.common.core.domain.entity.SysDept;
import com.business.system.domain.dto.*;
import org.apache.ibatis.annotations.Param;
import com.business.common.core.domain.entity.SysUser;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Repository;

/**
 * 用户表 数据层
 *
 * @author ruoyi
 */
@Repository
public interface SysUserMapper extends BaseMapper<SysUser>
{
    /**
     * 根据条件分页查询用户列表
     *
     * @param sysUser 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectUserList(SysUser sysUser);

    /**
     * 根据条件分页查询用户列表
     *
     * @param sysUser 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> userList(SysUser sysUser);

    /**
     * 根据条件分页查询用户列表
     *
     * @param sysUser 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectTreeUserList(SysUser sysUser);

    /**
     * 根据条件分页查询已配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectAllocatedList(SysUser user);

    /**
     * 根据条件分页查询未分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectUnallocatedList(SysUser user);

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    public SysUser selectUserByUserName(@Param("userName") String userName);

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    public SysUser selectUserById(Long userId);

    /**
     * 新增用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    public int insertUser(SysUser user);

    /**
     * 修改用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    public int updateUser(SysUser user);

    /**
     * 修改用户头像
     *
     * @param userName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    public int updateUserAvatar(@Param("userName") String userName, @Param("avatar") String avatar, @Param("comId") String comId);

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    public int resetUserPwd(@Param("userName") String userName, @Param("password") String password, @Param("comId") String comId);

    /**
     * 通过用户ID删除用户
     *
     * @param userId 用户ID
     * @return 结果
     */
    public int deleteUserById(Long userId);

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    public int deleteUserByIds(Long[] userIds);

    /**
     * 校验用户名称是否唯一
     *
     * @param userName 用户名称
     * @return 结果
     */
    public SysUser checkUserNameUnique(@Param("userName") String userName, @Param("comId") String comId);

    /**
     * 校验用户名称是否唯一
     *
     * @param jobNo 用户名称
     * @return 结果
     */
    public SysUser checkJobNoUnique(@Param("jobNo") String jobNo, @Param("comId") String comId);

    /**
     * 校验岗位编码是否唯一
     *
     * @param jobCode 用户名称
     * @return 结果
     */
    public SysUser checkJobCodeUnique(@Param("jobCode") String jobCode, @Param("comId") String comId);

    /**
     * 校验fu岗位编码是否唯一
     *
     * @param deputyPostCode 用户名称
     * @return 结果
     */
    public SysUser checkDeputyPostCodeUnique(@Param("deputyPostCode") String deputyPostCode, @Param("comId") String comId);

    /**
     * 校验手机号码是否唯一
     *
     * @param phonenumber 手机号码
     * @return 结果
     */
    public SysUser checkPhoneUnique(@Param("phonenumber") String phonenumber, @Param("comId") String comId);

    /**
     * 校验email是否唯一
     *
     * @param email 用户邮箱
     * @return 结果
     */
    public SysUser checkEmailUnique(@Param("email") String email, @Param("comId") String comId);

    /**
     * 根据部门ID及角色ID查询用户列表
     *
     * @param sysUserRoleDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectUserByRoleId(SysUserRoleDTO sysUserRoleDTO);

    /**
     * 根据角色ID查询用户列表
     *
     * @param sysUserByRoleIdDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectUserByRole(SysUserByRoleIdDTO sysUserByRoleIdDTO);

    /**
     * 根据角色ID查询自定义数据权限部门列表
     * @param sysDeptRoleDTO
     * @return
     */
    public List<SysDept> selectDeptsByRoleId(SysDeptRoleDTO sysDeptRoleDTO);

    /**
     * 根据部门ID查询用户列表
     *
     * @param sysUserDepDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<String> selectUserByDepId(SysUserDepDTO sysUserDepDTO);

    /**
     * 根据部门ID和角色查询用户列表
     *
     * @param sysUserDepDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<String> selectUserByDepIdAndRole(SysUserDepDTO sysUserDepDTO);

    /**
     * 根据运营查询部门经理列表
     *
     * @param sysUserByDepRoleDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<String> selectUserByRoleIdAndDepRole(SysUserByDepRoleDTO sysUserByDepRoleDTO);

    /**
     * 根据用户ID查询角色查列表
     *
     * @param sysRolesByUserDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<String> selectRolesByUser(SysRolesByUserDTO sysRolesByUserDTO);

    /**
     * 根据openID获取用户信息
     */
    public SysUser selectOpenIdByUser(String openId);

    /**
     * 根据角色ID查询用户ID列表
     *
     * @param sysUserByRoleIdDTO 用户信息
     * @return 用户信息集合信息
     */
    public List<String> selectUserIdsByRole(SysUserByRoleIdDTO sysUserByRoleIdDTO);

    /**
     * 根据用户部门ID集合查询负责部门角色用户列表
     *
     * @param deptIds 部门数组
     * @return 用户信息集合信息
     */
    public List<SysUser> selectRoleUsersByUser(@Param("deptIds") String deptIds, @Param("tenantId") String tenantId);

    /**
     * 根据roleId查询用户列表
     * @param roleId
     * @return
     */
    public List<SysUser> selectRoleUsersByRoleId(@Param("roleId") String roleId, @Param("comId") String comId);

    /**
     * 根据分类查用户ID
     * @param humanoid
     * @return
     */
    public List<String> selectUserIdByAssort(@Param("humanoid") String humanoid, @Param("comId") String comId);

    /**
     * 统计用户
     * @param user
     * @return
     */
    public int countUser(SysUser user);

    /**
     * 根据分类查用户ID
     * @param sysUserDepDTO
     * @return
     */
    public List<String> selectUserByHumanoid(SysUserDepDTO sysUserDepDTO);

    /**
     * 根据部门ID集合查询用户
     *
     * @param deptIds 部门数组
     * @return 用户信息集合信息
     */
    public List<SysUser> selectUsersByDeptIds(@Param("deptIds") Long[] deptIds, @Param("comId") String comId);

    /**
     * 运营专员
     * @return
     */
    public List<SysUser> operateUser(@Param("comId") String comId);
}
