package com.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaTransportAutomobile;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 汽车运输/货源Mapper接口
 *
 * @author ljb
 * @date 2023-02-17
 */
@Repository
public interface BaTransportAutomobileMapper extends BaseMapper<BaTransportAutomobile>
{
    /**
     * 查询汽车运输/货源
     *
     * @param id 汽车运输/货源ID
     * @return 汽车运输/货源
     */
    public BaTransportAutomobile selectBaTransportAutomobileById(String id);

    /**
     * 查询汽车运输/货源列表
     *
     * @param baTransportAutomobile 汽车运输/货源
     * @return 汽车运输/货源集合
     */
    public List<BaTransportAutomobile> selectBaTransportAutomobileList(BaTransportAutomobile baTransportAutomobile);

    public List<BaTransportAutomobile> baTransportAutomobileList(BaTransportAutomobile baTransportAutomobile);

    /**
     * 新增汽车运输/货源
     *
     * @param baTransportAutomobile 汽车运输/货源
     * @return 结果
     */
    public int insertBaTransportAutomobile(BaTransportAutomobile baTransportAutomobile);

    /**
     * 修改汽车运输/货源
     *
     * @param baTransportAutomobile 汽车运输/货源
     * @return 结果
     */
    public int updateBaTransportAutomobile(BaTransportAutomobile baTransportAutomobile);

    /**
     * 删除汽车运输/货源
     *
     * @param id 汽车运输/货源ID
     * @return 结果
     */
    public int deleteBaTransportAutomobileById(String id);

    /**
     * 批量删除汽车运输/货源
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaTransportAutomobileByIds(String[] ids);

    //public List<sourceGoodsDTO> sourceGoodsDTOList(BaTransportAutomobile baTransportAutomobile);
}
