package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaCalendar;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * OA日历Mapper接口
 *
 * @author single
 * @date 2023-12-12
 */
@Repository
public interface OaCalendarMapper extends BaseMapper<OaCalendar>
{
    /**
     * 查询OA日历
     *
     * @param id OA日历ID
     * @return OA日历
     */
    public OaCalendar selectOaCalendarById(String id);

    /**
     * 查询OA日历列表
     *
     * @param oaCalendar OA日历
     * @return OA日历集合
     */
    public List<OaCalendar> selectOaCalendarList(OaCalendar oaCalendar);

    /**
     * 新增OA日历
     *
     * @param oaCalendar OA日历
     * @return 结果
     */
    public int insertOaCalendar(OaCalendar oaCalendar);

    /**
     * 修改OA日历
     *
     * @param oaCalendar OA日历
     * @return 结果
     */
    public int updateOaCalendar(OaCalendar oaCalendar);

    /**
     * 删除OA日历
     *
     * @param id OA日历ID
     * @return 结果
     */
    public int deleteOaCalendarById(String id);

    /**
     * 批量删除OA日历
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaCalendarByIds(String[] ids);
}
