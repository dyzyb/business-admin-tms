package com.business.system.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.business.common.core.domain.entity.SysDept;
import org.springframework.stereotype.Repository;

/**
 * 部门管理 数据层
 *
 * @author ruoyi
 */
@Repository
public interface SysDeptMapper extends BaseMapper<SysDept>
{
    /**
     * 查询部门管理数据
     *
     * @param dept 部门信息
     * @return 部门信息集合
     */
    public List<SysDept> selectDeptList(SysDept dept);

    /**
     * 查询部门数据
     * @param dept
     * @return
     */
    public List<SysDept> deptList(SysDept dept);

    /**
     * 查询树部门管理数据
     *
     * @param dept 部门信息
     * @return 部门信息集合
     */
    public List<SysDept> selectTreeDeptList(SysDept dept);

    /**
     * 根据角色ID查询部门树信息
     *
     * @param roleId 角色ID
     * @param deptCheckStrictly 部门树选择项是否关联显示
     * @return 选中部门列表
     */
    public List<Long> selectDeptListByRoleId(@Param("roleId") String roleId, @Param("deptCheckStrictly") boolean deptCheckStrictly, @Param("tenantId") String tenantId);

    /**
     * 查询所有关联部门
     * @param roleId
     * @return
     */
    public List<Long> selectDeptIdByRoleId(@Param("roleId") Long roleId);

    /**
     * 根据部门ID查询信息
     *
     * @param deptId 部门ID
     * @return 部门信息
     */
    public SysDept selectDeptById(Long deptId);


    /**
     * 根据部门ID查询信息
     *
     * @param tenantId 部门ID
     * @return 部门信息
     */
    public SysDept selectTopDeptBytenantId(String tenantId);

    /**
     * 根据ID查询所有子部门
     *
     * @param deptId 部门ID
     * @return 部门列表
     */
    public List<SysDept> selectChildrenDeptById(@Param("deptId") Long deptId);

    /**
     * 根据ID查询所有子部门（正常状态）
     *
     * @param deptId 部门ID
     * @return 子部门数
     */
    public int selectNormalChildrenDeptById(@Param("deptId") Long deptId);

    /**
     * 是否存在子节点
     *
     * @param deptId 部门ID
     * @return 结果
     */
    public int hasChildByDeptId(@Param("deptId") Long deptId);

    /**
     * 查询部门是否存在用户
     *
     * @param deptId 部门ID
     * @return 结果
     */
    public int checkDeptExistUser(@Param("deptId") Long deptId);

    /**
     * 校验部门名称是否唯一
     *
     * @param deptName 部门名称
     * @param parentId 父部门ID
     * @return 结果
     */
    public SysDept checkDeptNameUnique(@Param("deptName") String deptName, @Param("parentId") Long parentId);

    /**
     * 新增部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    public int insertDept(SysDept dept);

    /**
     * 修改部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    public int updateDept(SysDept dept);

    /**
     * 修改所在部门正常状态
     *
     * @param deptIds 部门ID组
     */
    public void updateDeptStatusNormal(Long[] deptIds);

    /**
     * 修改子元素关系
     *
     * @param depts 子元素
     * @return 结果
     */
    public int updateDeptChildren(@Param("depts") List<SysDept> depts);

    /**
     * 删除部门管理信息
     *
     * @param deptId 部门ID
     * @return 结果
     */
    public int deleteDeptById(Long deptId);

    /**
     * 查询子公司
     *
     * @return 部门列表
     */
    public List<SysDept> selectSubCompanyList(String tenantId);

    /**
     * 查询业务管理中心、运营管理中心、子公司
     *
     * @return 部门列表
     */
    public List<SysDept> selectBusinessCompanyList();

    /**
     * 查询事业部
     *
     * @return 部门列表
     */
    public List<SysDept> selectDepartmentList(SysDept sysDept);

    /**
     * 查询集团公司
     *
     * @return 部门列表
     */
    public List<SysDept> selectGroupCompanyList(String tenantId);

    /**
     * 查询事业部
     *
     * @return 部门列表
     */
    public List<SysDept> selectDepartmentListByParentId(SysDept sysDept);

    /**
     * 根据userId获取对应归属公司信息
     *
     * @param userId 部门信息
     * @return 部门信息集合
     */
    public List<SysDept> selectUserDeptByUserId(@Param("userId") Long userId);

    /**
     * 查询该组顶级部门
     */
    public String tenantIdDeptName(String tenantId);

}
