package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaHoliday;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 节假日Mapper接口
 *
 * @author ljb
 * @date 2023-11-24
 */
@Repository
public interface OaHolidayMapper extends BaseMapper<OaHoliday>
{
    /**
     * 查询节假日
     *
     * @param id 节假日ID
     * @return 节假日
     */
    public OaHoliday selectOaHolidayById(String id);

    /**
     * 查询节假日列表
     *
     * @param oaHoliday 节假日
     * @return 节假日集合
     */
    public List<OaHoliday> selectOaHolidayList(OaHoliday oaHoliday);

    /**
     * 新增节假日
     *
     * @param oaHoliday 节假日
     * @return 结果
     */
    public int insertOaHoliday(OaHoliday oaHoliday);

    /**
     * 修改节假日
     *
     * @param oaHoliday 节假日
     * @return 结果
     */
    public int updateOaHoliday(OaHoliday oaHoliday);

    /**
     * 删除节假日
     *
     * @param id 节假日ID
     * @return 结果
     */
    public int deleteOaHolidayById(String id);

    /**
     * 批量删除节假日
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaHolidayByIds(String[] ids);
}
