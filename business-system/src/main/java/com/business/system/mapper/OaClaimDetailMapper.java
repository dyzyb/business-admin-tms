package com.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.OaClaimDetail;
import com.business.system.domain.vo.OaClaimStatVO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 报销明细Mapper接口
 *
 * @author single
 * @date 2023-12-11
 */
@Repository
public interface OaClaimDetailMapper extends BaseMapper<OaClaimDetail>
{
    /**
     * 查询报销明细
     *
     * @param id 报销明细ID
     * @return 报销明细
     */
    public OaClaimDetail selectOaClaimDetailById(String id);

    /**
     * 查询报销明细列表
     *
     * @param oaClaimDetail 报销明细
     * @return 报销明细集合
     */
    public List<OaClaimDetail> selectOaClaimDetailList(OaClaimDetail oaClaimDetail);

    /**
     * 新增报销明细
     *
     * @param oaClaimDetail 报销明细
     * @return 结果
     */
    public int insertOaClaimDetail(OaClaimDetail oaClaimDetail);

    /**
     * 修改报销明细
     *
     * @param oaClaimDetail 报销明细
     * @return 结果
     */
    public int updateOaClaimDetail(OaClaimDetail oaClaimDetail);

    /**
     * 删除报销明细
     *
     * @param id 报销明细ID
     * @return 结果
     */
    public int deleteOaClaimDetailById(String id);

    /**
     * 批量删除报销明细
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaClaimDetailByIds(String[] ids);

    /**
     * 查询日常报销统计
     *
     * @return 报销明细
     */
    public OaClaimStatVO sumOaClaimDetail();
}
