package com.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.BaCounterpart;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 对接人Mapper接口
 *
 * @author ljb
 * @date 2022-11-29
 */
@Repository
public interface BaCounterpartMapper extends BaseMapper<BaCounterpart>
{
    /**
     * 查询对接人
     *
     * @param id 对接人ID
     * @return 对接人
     */
    public BaCounterpart selectBaCounterpartById(String id);

    /**
     * 查询对接人列表
     *
     * @param baCounterpart 对接人
     * @return 对接人集合
     */
    public List<BaCounterpart> selectBaCounterpartList(BaCounterpart baCounterpart);

    /**
     * 新增对接人
     *
     * @param baCounterpart 对接人
     * @return 结果
     */
    public int insertBaCounterpart(BaCounterpart baCounterpart);

    /**
     * 修改对接人
     *
     * @param baCounterpart 对接人
     * @return 结果
     */
    public int updateBaCounterpart(BaCounterpart baCounterpart);

    /**
     * 删除对接人
     *
     * @param id 对接人ID
     * @return 结果
     */
    public int deleteBaCounterpartById(String id);

    /**
     * 批量删除对接人
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaCounterpartByIds(String[] ids);
}
