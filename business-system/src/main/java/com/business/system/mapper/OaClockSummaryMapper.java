package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaClockSummary;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.business.system.domain.dto.DayClockListDTO;
import org.springframework.stereotype.Repository;

/**
 * 打卡汇总Mapper接口
 *
 * @author single
 * @date 2023-12-11
 */
@Repository
public interface OaClockSummaryMapper extends BaseMapper<OaClockSummary>
{
    /**
     * 查询打卡汇总
     *
     * @param id 打卡汇总ID
     * @return 打卡汇总
     */
    public OaClockSummary selectOaClockSummaryById(String id);

    /**
     * 查询打卡汇总列表
     *
     * @param oaClockSummary 打卡汇总
     * @return 打卡汇总集合
     */
    public List<OaClockSummary> selectOaClockSummaryList(OaClockSummary oaClockSummary);

    /**
     * 新增打卡汇总
     *
     * @param oaClockSummary 打卡汇总
     * @return 结果
     */
    public int insertOaClockSummary(OaClockSummary oaClockSummary);

    /**
     * 修改打卡汇总
     *
     * @param oaClockSummary 打卡汇总
     * @return 结果
     */
    public int updateOaClockSummary(OaClockSummary oaClockSummary);

    /**
     * 删除打卡汇总
     *
     * @param id 打卡汇总ID
     * @return 结果
     */
    public int deleteOaClockSummaryById(String id);

    /**
     * 批量删除打卡汇总
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaClockSummaryByIds(String[] ids);

    /**
     * 获取当月每一天
     */
    public List<String> everyday(String date);

    /**
     * 天数据
     */
    public List<DayClockListDTO> selectDayClockList(DayClockListDTO dayClockListDTO);
}
