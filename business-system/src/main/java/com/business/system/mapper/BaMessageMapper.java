package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.BaMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 系统消息记录Mapper接口
 *
 * @author single
 * @date 2023-01-10
 */
@Repository
public interface BaMessageMapper extends BaseMapper<BaMessage>
{
    /**
     * 查询系统消息记录
     *
     * @param msId 系统消息记录ID
     * @return 系统消息记录
     */
    public BaMessage selectBaMessageById(String msId);

    /**
     * 查询系统消息记录列表
     *
     * @param baMessage 系统消息记录
     * @return 系统消息记录集合
     */
    public List<BaMessage> selectBaMessageList(BaMessage baMessage);

    /**
     * 新增系统消息记录
     *
     * @param baMessage 系统消息记录
     * @return 结果
     */
    public int insertBaMessage(BaMessage baMessage);

    /**
     * 修改系统消息记录
     *
     * @param baMessage 系统消息记录
     * @return 结果
     */
    public int updateBaMessage(BaMessage baMessage);

    /**
     * 删除系统消息记录
     *
     * @param msId 系统消息记录ID
     * @return 结果
     */
    public int deleteBaMessageById(String msId);

    /**
     * 批量删除系统消息记录
     *
     * @param msIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteBaMessageByIds(String[] msIds);
}
