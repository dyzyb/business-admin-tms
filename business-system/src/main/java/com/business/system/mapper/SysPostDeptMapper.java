package com.business.system.mapper;

import com.business.system.domain.SysPost;
import com.business.system.domain.SysPostDept;
import com.business.system.domain.SysUserPost;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 岗位和部门关联表 数据层
 *
 * @author ruoyi
 */
@Repository
public interface SysPostDeptMapper
{
    /**
     * 通过部门ID删除部门和岗位关联
     *
     * @param deptId 部门ID
     * @return 结果
     */
    public int deletePostDeptByDeptId(Long deptId);

    /**
     * 通过部门ID查询岗位使用数量
     *
     * @param deptId 部门ID
     * @return 结果
     */
    public int countPostDeptById(Long deptId);

    /**
     * 批量删除部门和岗位关联
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deletePostDept(Long[] ids);

    /**
     * 批量新增角色部门信息
     *
     * @param postDeptList 角色部门列表
     * @return 结果
     */
    public int batchPostDept(List<SysPostDept> postDeptList);

    public List<SysPostDept> selectPostDept(Long deptId);
}
