package com.business.system.mapper;

import java.util.List;
import com.business.system.domain.OaApprovalOpinions;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 批复意见Mapper接口
 *
 * @author ljb
 * @date 2023-11-22
 */
@Repository
public interface OaApprovalOpinionsMapper extends BaseMapper<OaApprovalOpinions>
{
    /**
     * 查询批复意见
     *
     * @param id 批复意见ID
     * @return 批复意见
     */
    public OaApprovalOpinions selectOaApprovalOpinionsById(String id);

    /**
     * 查询批复意见列表
     *
     * @param oaApprovalOpinions 批复意见
     * @return 批复意见集合
     */
    public List<OaApprovalOpinions> selectOaApprovalOpinionsList(OaApprovalOpinions oaApprovalOpinions);

    /**
     * 新增批复意见
     *
     * @param oaApprovalOpinions 批复意见
     * @return 结果
     */
    public int insertOaApprovalOpinions(OaApprovalOpinions oaApprovalOpinions);

    /**
     * 修改批复意见
     *
     * @param oaApprovalOpinions 批复意见
     * @return 结果
     */
    public int updateOaApprovalOpinions(OaApprovalOpinions oaApprovalOpinions);

    /**
     * 删除批复意见
     *
     * @param id 批复意见ID
     * @return 结果
     */
    public int deleteOaApprovalOpinionsById(String id);

    /**
     * 批量删除批复意见
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteOaApprovalOpinionsByIds(String[] ids);
}
