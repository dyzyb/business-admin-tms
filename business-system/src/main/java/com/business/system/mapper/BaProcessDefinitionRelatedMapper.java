package com.business.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.business.system.domain.BaProcessDefinitionRelated;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
@Repository
public interface BaProcessDefinitionRelatedMapper extends BaseMapper<BaProcessDefinitionRelated> {

    /**
     * 根据流程类型查询
     * @param businessType
     * @return
     */
    List<BaProcessDefinitionRelated> listProcessBusinessDefinitionByType(@Param("businessType") String businessType, @Param("tenantId") String tenantId);

    /**
     * 分页查询列表
     * @param queryDO
     * @return
     */
    List<BaProcessDefinitionRelated> listProcessDefinitionPage(@Param("queryDO")BaProcessDefinitionRelated queryDO);

    /**
     * 根据id批量删除
     * @param ids
     * @return
     */
    int deleteProcessDefinition(@Param("ids") List<Long> ids);

    public int deleteProcessDefinitionById(String id);

    /**
     * 根据模板ID查询模板数据
     * @param templateId
     * @return
     */
    BaProcessDefinitionRelated selectBaProcessDefinitionRelated(@Param("templateId") String templateId, @Param("tenantId") String tenantId);
}
