package com.business.system.adapter;

import com.business.common.utils.StringPool;
import com.business.common.utils.spring.SpringUtils;
import com.business.system.domain.dto.AcceptApprovalWorkflowDTO;
import com.business.system.service.IWorkflowUpdateStatusService;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;

@Service
public class WorkflowUpdateStatusAdapter {

    public void adapterAcceptApprovalResult(AcceptApprovalWorkflowDTO updateStatusDTO) {
        IWorkflowUpdateStatusService updateStatusService;
        String beanName="";
        try {
            beanName = MessageFormat.format("{0}:{1}", updateStatusDTO.getTargetType().split(StringPool.COLON)[0], updateStatusDTO.getApprovalResult());
            updateStatusService = SpringUtils.getBean(beanName);
        } catch (Exception ex) {
            throw new RuntimeException("IWorkflowUpdateStatusService bean error,beanName is "+beanName,ex);
        }
        updateStatusService.acceptApprovalResult(updateStatusDTO);
    }
}
