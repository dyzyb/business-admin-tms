package com.business.system.domain.vo;

import lombok.Data;

import java.util.List;

@Data
public class MaterialTypeVo {

    private transient String value;

    private transient String label;

    private transient String text;

    private transient List<MaterialTypeVo> children;
/*
    public List<MaterialTypeVo> getChildren(){
        if (children == null){
            children = new ArrayList();
        }
        return children;
    };*/

}
