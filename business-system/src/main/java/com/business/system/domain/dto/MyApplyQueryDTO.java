package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author: js
 * @Description: 我的申请查询对象
 * @date: 2023/1/10 11:41
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "MyApplyQueryDTO对象", description = "我的申请查询对象")
public class MyApplyQueryDTO extends BaseDTO {

    private static final long serialVersionUID = 4398596856902643565L;

    @ApiModelProperty("业务类型")
    private String businessType;

    @ApiModelProperty("审批状态")
    private String state;

}
