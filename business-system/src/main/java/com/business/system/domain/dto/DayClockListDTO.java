package com.business.system.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "打卡合并对象DTO",description = "打卡合并对象")
public class DayClockListDTO {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "上班打卡ID")
    private String clockInId;

    @Excel(name = "姓名")
    @ApiModelProperty(value = "用户名称")
    private String nickName;

    @Excel(name = "考勤组")
    @ApiModelProperty(value = "考勤组")
    private String attendanceName;

    @Excel(name = "部门")
    @ApiModelProperty(value = "部门名称")
    private String deptName;

    /** 日期 */
    @Excel(name = "日期")
    @ApiModelProperty(value = "日期")
    private String dateTime;

    @Excel(name = "上班打卡时间")
    @ApiModelProperty(value = "上班打卡时间")
    private String clockInTime;

    @Excel(name = "上班打卡结果")
    @ApiModelProperty(value = "上班打卡员工状态")
    private String clockInState;

    @ApiModelProperty(value = "上班打卡创建时间")
    private String ClockInCreateTime;

    @Excel(name = "上班打卡地址")
    @ApiModelProperty(value = "上班打卡地址")
    private String CheckInAddress;

    @ApiModelProperty(value = "上班打卡日期")
    private String clockInData;

    @ApiModelProperty(value = "上班打卡经度")
    private String clockInLatitude;

    @ApiModelProperty(value = "上班打卡维度")
    private String clockInLongitude;

    @ApiModelProperty(value = "上班打卡员工ID")
    private String CheckInEmployeeId;

    @ApiModelProperty(value = "下班打卡ID")
    private String ClockOutId;

    @Excel(name = "下班打卡时间")
    @ApiModelProperty(value = "下班打卡时间")
    private String clockOutTime;

    @Excel(name = "下班打卡结果")
    @ApiModelProperty(value = "下班打卡员工状态")
    private String clockOutState;

    @ApiModelProperty(value = "下班打卡创建时间")
    private String ClockOutCreateTime;

    @Excel(name = "下班打卡地址")
    @ApiModelProperty(value = "下班打卡地址")
    private String CheckOutAddress;

    @ApiModelProperty(value = "下班打卡日期")
    private String clockOutData;

    @ApiModelProperty(value = "下班打卡经度")
    private String clockOutLatitude;

    @ApiModelProperty(value = "下班打卡维度")
    private String clockOutLongitude;

    @ApiModelProperty(value = "下班打卡员工ID")
    private String CheckOutEmployeeId;


    @ApiModelProperty(value = "查询时间")
    private String queryTime;

    @ApiModelProperty(value = "租户ID")
    private String tenantId;

}
