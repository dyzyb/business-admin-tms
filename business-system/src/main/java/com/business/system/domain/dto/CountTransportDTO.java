package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "CountTransportDTO对象", description = "发运DTO对象")
public class CountTransportDTO {

    private static final long serialVersionUID = 6281016059931188887L;

    /** 立项类型 **/
    @ApiModelProperty(value = "立项类型")
    private String projectType;

    /** 运输类型 **/
    @ApiModelProperty(value = "运输类型")
    private String transportType;

    /** 租户ID **/
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

}
