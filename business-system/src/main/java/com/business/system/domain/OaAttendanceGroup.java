package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

/**
 * 考勤组对象 oa_attendance_group
 *
 * @author single
 * @date 2023-11-24
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "考勤组对象",description = "")
@TableName("oa_attendance_group")
public class OaAttendanceGroup extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 考勤组名称 */
    @Excel(name = "考勤组名称")
    @ApiModelProperty(value = "考勤组名称")
    private String attendanceName;

    /** 考勤组成员 */
    @Excel(name = "考勤组成员")
    @ApiModelProperty(value = "考勤组成员")
    private String attendanceMembers;

    /** 考勤组成员name */
    @ApiModelProperty(value = "考勤组成员name")
    @TableField(exist = false)
    private String attendanceMembersName;

    /** 非考勤成员 */
    @Excel(name = "非考勤成员")
    @ApiModelProperty(value = "非考勤成员")
    private String nonAttendanceMembers;

    /** 非考勤成员name */
    @ApiModelProperty(value = "非考勤成员name")
    @TableField(exist = false)
    private String nonAttendanceMembersName;

    /** 考勤类型 */
    @Excel(name = "考勤类型 1:固定  2：弹性")
    @ApiModelProperty(value = "考勤类型")
    private String attendanceType;

    /** 新增必须打卡日期 */
    @Excel(name = "新增必须打卡日期")
    @ApiModelProperty(value = "新增必须打卡日期")
    private String mustSpecialSettings;

    /** 新增无需打卡日期 */
    @Excel(name = "新增无需打卡日期")
    @ApiModelProperty(value = "新增无需打卡日期")
    private String notSpecialSettings;

    /** 打卡方式 */
    @Excel(name = "打卡方式")
    @ApiModelProperty(value = "打卡方式")
    private String clockMethod;

    /** 外勤打卡 */
    @Excel(name = "外勤打卡")
    @ApiModelProperty(value = "外勤打卡")
    private String fieldClock;

    /** 状态 0:立即生效  1:延迟生效  3:已被覆盖*/
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 审批流程Id */
    @Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 工作日设置 */
    @ApiModelProperty(value = "工作日设置")
    @TableField(exist = false)
    private List<OaWeekday> oaWeekdays;

    /** 考勤时间 */
    @ApiModelProperty(value = "考勤时间")
    @TableField(exist = false)
    private Map<String,String> map;
    /**
     * 办公地点
     */
    @ApiModelProperty(value = "办公地点")
    @TableField(exist = false)
    private List<OaAttendanceAddress> oaAttendanceAddresses;

    /**
     * 必须打卡
     */
    @ApiModelProperty(value = "必须打卡")
    @TableField(exist = false)
    private List<OaWeekday> mustSpecialSettingsList;

    /**
     * 无需打卡
     */
    @ApiModelProperty(value = "无需打卡")
    @TableField(exist = false)
    private List<OaWeekday> notSpecialSettingsList;

    /**
     * 考勤人数
     */
    @ApiModelProperty(value = "考勤人数")
    @TableField(exist = false)
    private Integer attendanceNumber;

    /**
     * 部门树
     */
    @ApiModelProperty(value = "部门树")
    private String deptTree;

    /**
     * 父级ID
     */
    @ApiModelProperty(value = "父级ID")
    private String parentId;


}
