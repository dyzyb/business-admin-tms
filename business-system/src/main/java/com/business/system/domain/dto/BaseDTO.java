package com.business.system.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;

/**
 * @author js
 * @version 1.0
 * @description: DTO基类
 * @date 2022-12-2 14:09
 */
@Data
public class BaseDTO implements Serializable {

    private static final long serialVersionUID = 9127532221382240134L;

    @ApiModelProperty(value = "当前页")
    private Long current;

    @ApiModelProperty(value = "显示个数")
    @Min(1L)
    @Max(200L)
    private Long size;

    public BaseDTO(){
        this.current = 1L;
        this.size = 10L;
    }
}