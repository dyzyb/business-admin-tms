package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @version 1.0
 * @Author: js
 * @Description: 立项统计DTO
 * @Date: 2023/2/24 23:14
 */
@Data
@ApiModel(value = "CountBaProjectDTO对象", description = "立项统计DTO")
public class CountBaProjectDTO implements Serializable {

    private static final long serialVersionUID = -2865020511591145690L;

    @ApiModelProperty(value = "立项类型")
    private String projectType;

    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 立项状态 */
    @ApiModelProperty(value = "立项状态")
    private String projectState;

    @ApiModelProperty(value = "仓库ID")
    private String depotId;

    @ApiModelProperty(value = "租户ID")
    private String tenantId;
}
