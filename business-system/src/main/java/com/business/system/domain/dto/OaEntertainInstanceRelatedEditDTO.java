package com.business.system.domain.dto;

import com.business.system.domain.OaEntertain;
import com.business.system.domain.OaMaintenanceExpansion;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: js
 * @Description: 招待申请与流程实例关联关系审批编辑对象
 * @date: 2023/12/1
 */
@Data
@ApiModel(value = "OaEntertainInstanceRelatedEditDTO对象", description = "招待申请与流程实例关联关系审批编辑对象")
public class OaEntertainInstanceRelatedEditDTO {

    @ApiModelProperty("业务id")
    private String businessId;

    @ApiModelProperty("流程实例ID")
    private String processInstanceId;

    @ApiModelProperty("业务数据")
    private OaEntertain oaEntertain;
}
