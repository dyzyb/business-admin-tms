package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: js
 * @Description: 流程定义查询对象
 * @date: 2022/12/2 15:44
 */
@Data
@ApiModel(value = "BaProcessDefinitionRelatedQueryOneDTO对象", description = "流程定义查询对象")
public class BaProcessDefinitionRelatedQueryOneDTO {

    @ApiModelProperty("流程定义id")
    private String templateId;
}
