package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 统计终端企业发运量排名
 *
 * @author js
 * @date 2023-8-30
 */
@Data
@ApiModel(value="BaEnterpriseVO对象",description="统计终端企业发运量排名")
public class BaEnterpriseVO
{
    /** 终端名称 */
    @ApiModelProperty(value = "终端名称")
    private String name;

    /** 实际发运量 */
    @ApiModelProperty(value = "实际发运量")
    private BigDecimal num;
}
