package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * 班次对象 oa_work_times
 *
 * @author single
 * @date 2023-11-24
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "班次对象",description = "")
@TableName("oa_work_times")
public class OaWorkTimes extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 班次名称 */
    @Excel(name = "班次名称")
    @ApiModelProperty(value = "班次名称")
    private String name;

    /** 上班时间 */
    @JsonFormat(pattern = "HH:mm")
    @Excel(name = "上班班时间", width = 30, dateFormat = "HH:mm")
    @ApiModelProperty(value = "上班时间")
    private Date goWorkTime;

    /** 分钟前开始打卡 */
    @JsonFormat(pattern = "HH:mm")
    @Excel(name = "分钟前开始打卡", width = 30, dateFormat = "HH:mm")
    @ApiModelProperty(value = "分钟前开始打卡")
    private Date goStartClock;

    /** 分钟后结束打卡 */
    @JsonFormat(pattern = "HH:mm")
    @Excel(name = "分钟后结束打卡", width = 30, dateFormat = "HH:mm")
    @ApiModelProperty(value = "分钟后结束打卡")
    private Date goEndClock;

    /** 下班时间 */
    @JsonFormat(pattern = "HH:mm")
    @Excel(name = "下班时间", width = 30, dateFormat = "HH:mm")
    @ApiModelProperty(value = "下班时间")
    private Date offWorkTime;

    /** 考勤类型 */
    @Excel(name = "考勤类型")
    @ApiModelProperty(value = "考勤类型")
    private Long attendanceType;

    /** 分钟前开始打卡 */
    @JsonFormat(pattern = "HH:mm")
    @Excel(name = "分钟前开始打卡", width = 30, dateFormat = "HH:mm")
    @ApiModelProperty(value = "分钟前开始打卡")
    private Date offStartClock;

    /** 分钟后结束打卡 */
    @JsonFormat(pattern = "HH:mm")
    @Excel(name = "分钟后结束打卡", width = 30, dateFormat = "HH:mm")
    @ApiModelProperty(value = "分钟后结束打卡")
    private Date offEndClock;

    /** 严重迟到分钟数 */
    @Excel(name = "严重迟到分钟数")
    @ApiModelProperty(value = "严重迟到分钟数")
    private Long severelyLate;

    /** 是否启用严重迟到分钟数 */
    @Excel(name = "是否启用严重迟到分钟数")
    @ApiModelProperty(value = "是否启用严重迟到分钟数")
    private String isSeverelyLate;

    /** 允许迟到分钟数 */
    @Excel(name = "允许迟到分钟数")
    @ApiModelProperty(value = "允许迟到分钟数")
    private Long allowedLate;

    /** 是否启用允许迟到分钟数 */
    @Excel(name = "是否启用允许迟到分钟数")
    @ApiModelProperty(value = "是否启用允许迟到分钟数")
    private String isAllowedLate;

    /** 迟到几分钟算矿工 */
    @Excel(name = "迟到几分钟算矿工")
    @ApiModelProperty(value = "迟到几分钟算矿工")
    private Long absenteeismLate;

    /** 是否启用迟到几分钟算旷工 */
    @Excel(name = "是否启用迟到几分钟算旷工")
    @ApiModelProperty(value = "是否启用迟到几分钟算旷工")
    private String isAbsenteeismLate;

    /** 早退几分钟算矿工 */
    @Excel(name = "早退几分钟算矿工")
    @ApiModelProperty(value = "早退几分钟算矿工")
    private Long absenteeismEarly;

    /** 是否启用早退几分钟算矿工 */
    @Excel(name = "是否启用早退几分钟算矿工")
    @ApiModelProperty(value = "是否启用早退几分钟算矿工")
    private String isAbsenteeismEarly;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 当前时间 */
    @ApiModelProperty(value = "当前时间")
    @TableField(exist = false)
    private String currentTime;

    /** 是否可删除 */
    @ApiModelProperty(value = "是否可删除")
    @TableField(exist = false)
    private String deletionFlag;



}
