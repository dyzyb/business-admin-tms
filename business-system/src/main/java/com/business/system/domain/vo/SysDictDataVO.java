package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.List;

import java.io.Serializable;

/**
 * 字典数据表
 *
 * @author ruoyi
 */
@Data
@ApiModel(value="SysDictDataVO对象",description="字典数据对象")
public class SysDictDataVO  implements Serializable {

    private static final long serialVersionUID = -1923330790483239484L;

    @ApiModelProperty("字典编码")
    private Long dictCode;

    @ApiModelProperty("字典排序")
    private Long dictSort;

    @ApiModelProperty("字典标签")
    private String dictLabel;

    @ApiModelProperty("字典键值")
    private String dictValue;

    @ApiModelProperty("字典类型")
    private String dictType;

    @ApiModelProperty("下级数据字典")
    private List<SysDictDataVO> sysDictDataVOList;
}
