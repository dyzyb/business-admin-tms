package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

/**
 * 交办数据对象 oa_assigned
 *
 * @author ljb
 * @date 2023-11-21
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "交办数据对象",description = "")
@TableName("oa_assigned")
public class OaAssigned extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 办理人 */
    @Excel(name = "办理人")
    @ApiModelProperty(value = "办理人")
    private String transactors;

    /** 督办ID */
    @Excel(name = "督办ID")
    @ApiModelProperty(value = "督办ID")
    private String supervisingId;

    /** 督办对象 */
    @ApiModelProperty(value = "督办对象")
    @TableField(exist = false)
    private OaSupervising oaSupervising;

    /** 状态 */
    @Excel(name = "状态 0:办理中 1:已办")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 审批流程Id */
    @Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 类型 */
    @Excel(name = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

    /** 批复意见 */
    @Excel(name = "批复意见")
    @ApiModelProperty(value = "批复意见")
    private String approvalOpinions;

    /** 所有批复意见 */
    @ApiModelProperty(value = "所有批复意见")
    @TableField(exist = false)
    private List<Map<String,String>> map;

    /**
     * 已办结
     */
    @ApiModelProperty(value = "已办结")
    @TableField(exist = false)
    private String completion;
}
