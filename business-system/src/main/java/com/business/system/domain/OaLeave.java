package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
/**
 * 请假申请对象 oa_leave
 *
 * @author ljb
 * @date 2023-11-11
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "请假申请对象",description = "")
@TableName("oa_leave")
public class OaLeave extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    //发起人名称
    @Excel(name = "申请人")
    @TableField(exist = false)
    private String userName;

    /** 部门名称 */
    @Excel(name = "部门")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "请假开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "开始时间")
    private Date startTime;

    /** 开始阶段 */
    @Excel(name = "时段", readConverterExp = "1=上半天,2=下半天")
    @ApiModelProperty(value = "开始阶段")
    private String startCycle;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "请假结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "结束时间")
    private Date endTime;

    /** 结束阶段 */
    @Excel(name = "时段", readConverterExp = "1=上半天,2=下半天")
    @ApiModelProperty(value = "结束阶段")
    private String endCycle;

    /** 请假类型 */
    @Excel(name = "请假类型", readConverterExp = "1=事假,2=病假,3=调休,4=产假,5=陪产假,6=丧假,7=婚假")
    @ApiModelProperty(value = "请假类型")
    private String leaveType;

    /** 时长 */
    @Excel(name = "时长(天)")
    @ApiModelProperty(value = "时长")
    private BigDecimal duration;

    /** 状态 */
    @Excel(name = "审批状态", readConverterExp = "oaLeave:verifying=审批中,oaLeave:pass=已通过,oaLeave:reject=已拒绝,oaLeave:withdraw=已撤回")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 请假事由 */
    @ApiModelProperty(value = "请假事由")
    private String reasonLeave;

    /** 附件 */
    @ApiModelProperty(value = "附件")
    private String annex;

    /** 标记 */
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 审批流程Id */
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用户ID */
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    /** 部门ID */
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 提交状态 */
    @ApiModelProperty(value = "提交状态")
    private String submitState;


    /** 提交时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "提交时间")
    private Date submitTime;

    /** 提交日期 */
    @TableField(exist = false)
    private String submitDate;

    /** 每一天 */
    @ApiModelProperty(value = "每一天")
    private String everyDay;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String start;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String end;

    /** 根据年月搜索 */
    @ApiModelProperty(value = "根据年月搜索")
    @TableField(exist = false)
    private String createTimeSearch;

    /** 用户ID集合 */
    @ApiModelProperty(value = "用户ID集合")
    @TableField(exist = false)
    private String userIds;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @TableField(exist = false)
    private Date createDate;

}
