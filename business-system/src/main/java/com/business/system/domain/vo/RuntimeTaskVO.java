package com.business.system.domain.vo;

import com.business.system.domain.dto.json.UserInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: js
 * @Description: 待办任务对象
 * @date: 2022/12/6 13:45
 */
@Data
@ApiModel(value="RuntimeTaskVO对象",description="待办任务对象")
public class RuntimeTaskVO implements Serializable {

    private static final long serialVersionUID = 6865386035647163281L;

    @ApiModelProperty("任务ID")
    private String id;

    @ApiModelProperty("任务处理人")
    private String assignee;

    @ApiModelProperty("任务处理人用户名")
    private String assigneeName;

    @ApiModelProperty("任务处理人姓名")
    private String assigneeNickName;

    @ApiModelProperty("任务名称")
    private String name;

    @ApiModelProperty("任务描述")
    private String description;

    @ApiModelProperty("流程发起时间")
    private String processStartTime;

    @ApiModelProperty("任务创建时间")
    private String createTime;

    @ApiModelProperty("流程节点")
    private String taskDefinitionKey;

    @ApiModelProperty("流程节点名称")
    private String taskDefinitionName;

    @ApiModelProperty("集群id")
    private String tenantId;

    @ApiModelProperty("流程实例id")
    private String processInstanceId;

    @ApiModelProperty("流程定义id")
    private String processDefinitionId;

    @ApiModelProperty("流程定义名称")
    private String processDefinitionName;

    @ApiModelProperty("总页数")
    private Integer totalPages;

    @ApiModelProperty("数据总数")
    private Integer totalElements;

    @ApiModelProperty("每页数量")
    private Integer size;

    @ApiModelProperty("当前页码，从0开始")
    private Integer number;

    @ApiModelProperty("发起人")
    private UserInfo startUser;

    @ApiModelProperty("发起人姓名")
    private String startUserName;

    @ApiModelProperty("发起人姓名-岗位")
    private String startNickName;

    @ApiModelProperty("审批状态")
    private String businessStatus;

    @ApiModelProperty("业务数据")
    private String businessData;

    @ApiModelProperty("业务类型")
    private String businessType;

    @ApiModelProperty("审核类型")
    private String approveType;

    @ApiModelProperty("审核结果")
    private String approveResult;

    @ApiModelProperty("办理编辑状态")
    private String approveEditType;

    @ApiModelProperty("公司类型")
    private String companyType;

    @ApiModelProperty("时间差")
    private String timeDifference;

    @ApiModelProperty("业务名称")
    private String busiName;

}
