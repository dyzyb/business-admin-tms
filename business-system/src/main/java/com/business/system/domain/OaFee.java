package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 费用申请对象 oa_fee
 *
 * @author ljb
 * @date 2023-11-11
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "费用申请对象",description = "")
@TableName("oa_fee")
public class OaFee extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    //发起人名称
    @Excel(name = "申请人")
    @TableField(exist = false)
    private String userName;

    /** 部门名称 */
    @Excel(name = "部门")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 申请日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "申请日期", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "申请日期")
    private Date applicationDate;

    /** 费用类型 */
    @Excel(name = "费用类型", readConverterExp = "1=招待费,2=备用金,3=其他")
    @ApiModelProperty(value = "费用类型")
    private String feeType;

    /** 费用金额 */
    @Excel(name = "申请金额")
    @ApiModelProperty(value = "费用金额")
    private BigDecimal feeSum;

    /**
     * 费用归属类型
     */
    @ApiModelProperty(value = "费用归属类型")
    private String feeIlk;

    /** 子公司简称 **/
    @ApiModelProperty(value = "子公司简称")
    @Excel(name = "费用归属")
    @TableField(exist = false)
    private String subsidiaryAbbreviation;

    /** 状态 */
    @Excel(name = "审批状态", readConverterExp = "oaFee:verifying=审批中,oaFee:pass=已通过,oaFee:reject=已拒绝,oaFee:withdraw=已撤回")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 项目ID */
    @ApiModelProperty(value = "项目ID")
    private String projectId;

    /** 项目名称 */
    @ApiModelProperty(value = "项目名称")
    @TableField(exist = false)
    private String projectName;

    /** 费用明细 */
    @ApiModelProperty(value = "费用明细")
    private String expenseDetails;

    /** 付款方式 */
    @ApiModelProperty(value = "付款方式")
    private String paymentMethod;

    /** 收款人 */
    @ApiModelProperty(value = "收款人")
    private String payee;

    /** 银行账户 */
    @ApiModelProperty(value = "银行账户")
    private String bankAccount;

    /** 开户行 */
    @ApiModelProperty(value = "开户行")
    private String bankName;

    /** 备注 */
    @ApiModelProperty(value = "备注")
    private String notes;

    /** 附件 */
    @ApiModelProperty(value = "附件")
    private String annex;

    /** 标记 */
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 审批流程Id */
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用户ID */
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    //发起人
    @TableField(exist = false)
    private SysUser user;


    /** 部门ID */
    @ApiModelProperty(value = "部门ID")
    private Long deptId;


    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 提交状态 */
    @ApiModelProperty(value = "提交状态")
    private String submitState;


    /** 提交时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "提交时间")
    private Date submitTime;


    /**
     * 归属事业部
     */
    @ApiModelProperty(value = "归属事业部")
    private String belongDept;

    /**
     * 归属事业部名称
     */
    @ApiModelProperty(value = "归属事业部名称")
    @TableField(exist = false)
    private String belongDeptName;

    /** 根据年月搜索 */
    @ApiModelProperty(value = "根据年月搜索")
    @TableField(exist = false)
    private String createTimeSearch;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String start;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String end;

    /** 用户ID集合 */
    @ApiModelProperty(value = "用户ID集合")
    @TableField(exist = false)
    private String userIds;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @TableField(exist = false)
    private Date createDate;

}
