package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author: js
 * @Description: 订单统计对象
 * @date: 2023/2/6 10:56
 */
@Data
@ApiModel(value="OrderStatVO对象",description="订单统计对象")
public class OrderStatVO implements Serializable {

    private static final long serialVersionUID = -5172282199611326213L;

    /**
     * 订单数量
     */
    @ApiModelProperty(value = "订单数量")
    private int orderTotals;

    /**
     * 订单进行中数量
     */
    @ApiModelProperty(value = "订单数量")
    private int runtimeOrderTotals;

    /**
     * 累计采购量
     */
    @ApiModelProperty(value = "累计采购量")
    private String purchaseVolume;

    /**
     * 累计付款金额
     */
    @ApiModelProperty(value = "累计付款金额")
    private BigDecimal purchasePayment;
}
