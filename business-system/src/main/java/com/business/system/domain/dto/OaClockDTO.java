package com.business.system.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 打卡对象 oa_click
 *
 * @author js
 * @date 2023-11-29
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "打卡对象DTO",description = "")
public class OaClockDTO
{
    private static final long serialVersionUID = 1L;

    /** 员工ID */
    @ApiModelProperty(value = "员工ID")
    private Long employeeId;

    /** 打卡统计年月 */
    @ApiModelProperty(value = "打卡统计年月")
    private String searchDate;

    /** 打卡统计日期 */
    @ApiModelProperty(value = "打卡统计日期")
    private String searchTime;

    /** 考勤类型 1 内勤 2 外勤 */
    @ApiModelProperty(value = "考勤类型 1 内勤 2 外勤")
    private String type;

    /** 补卡班次 1 上班补卡 2 下班补卡 */
    @Excel(name = "补卡班次 1 上班补卡 2 下班补卡")
    @ApiModelProperty(value = "补卡班次 1 上班补卡 2 下班补卡")
    private String clockType;
}
