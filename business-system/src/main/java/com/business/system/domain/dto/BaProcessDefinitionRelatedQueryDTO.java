package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: js
 * @Description: 流程定义查询对象
 * @date: 2022/12/2 15:44
 */
@Data
@ApiModel(value = "BaProcessDefinitionRelatedQueryDTO对象", description = "流程定义查询对象")
public class BaProcessDefinitionRelatedQueryDTO {

    @ApiModelProperty("流程定义id")
    private String processDefinitionId;

    @ApiModelProperty("流程定义名称")
    private String name;

    @ApiModelProperty("流程分类")
    private String businessType;

    @ApiModelProperty(value = "开始日期")
    private String startCreateTime;

    @ApiModelProperty(value = "结束日期")
    private String endCreateTime;

    @ApiModelProperty("租户ID")
    private String tenantId;
}
