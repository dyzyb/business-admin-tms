package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "DataLayoutDTO对象", description = "数据呈现")
public class DataLayoutDTO implements Serializable {

    private static final long serialVersionUID = 6281016059931188887L;

    /** 子公司名称 */
    @ApiModelProperty(value = "业务类型")
    private String businessType;

    /** 累计发运 **/
    @ApiModelProperty(value = "累计发运")
    private BigDecimal checkCoalNum;

    /** 累计交易 **/
    @ApiModelProperty(value = "累计交易")
    private BigDecimal tradeAmount;

    /** 累计利润 **/
    @ApiModelProperty(value = "累计利润")
    private BigDecimal accumulatedProfit;

    /** 累计合同额 **/
    @ApiModelProperty(value = "累计合同额")
    private BigDecimal contractAmount;
}
