package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * OA日历对象 oa_calendar
 *
 * @author single
 * @date 2023-12-12
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "OA日历对象",description = "")
@TableName("oa_calendar")
public class OaCalendar extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 日期 */
    @Excel(name = "日期")
    @ApiModelProperty(value = "日期")
    private String date;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    @ApiModelProperty(value = "当天打卡数据拼接")
    @TableField(exist = false)
    private String thisClock;

    @ApiModelProperty(value = "日报状态")
    @TableField(exist = false)
    private String dailyState;

}
