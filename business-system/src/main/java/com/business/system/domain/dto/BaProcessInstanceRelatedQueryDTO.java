package com.business.system.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author: js
 * @Description: 业务与流程实例关联关系查询对象
 * @date: 2023/1/10
 */
@Data
@ApiModel(value = "BaProcessInstanceRelatedQueryDTO对象", description = "流程对象")
public class BaProcessInstanceRelatedQueryDTO extends BaseDTO{

    private static final long serialVersionUID = -250190867006756959L;

    @ApiModelProperty("业务类型")
    private String businessType;

    @ApiModelProperty("审核结果")
    private String approveResult;

    @ApiModelProperty("发起人ID")
    private String createBy;

    @ApiModelProperty("过滤未开发数据")
    private String filterConditionNotEqual;

    @ApiModelProperty("查询OA数据")
    private String filterConditionEqual;

    /**
     * 过滤未开发数据
     */
    @TableField(exist = false)
    private List<String> filterConditionNotEquals;

    /**
     * 查询指定类型数据
     */
    @TableField(exist = false)
    private List<String> filterConditionEquals;

    /**
     * 状态
     */
    private String state;

    /**
     * 标记
     */
    private Integer flag;


    /**
     * 租户ID
     */
    private String tenantId;
}
