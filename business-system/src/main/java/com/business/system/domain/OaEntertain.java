package com.business.system.domain;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 招待申请对象 oa_entertain
 *
 * @author ljb
 * @date 2023-11-13
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "招待申请对象",description = "")
@TableName("oa_entertain")
public class OaEntertain extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 项目ID */
    //@Excel(name = "项目ID")
    @ApiModelProperty(value = "项目ID")
    private String projectId;

    /** 项目名称 */
    @ApiModelProperty(value = "项目ID")
    @TableField(exist = false)
    private String projectName;

    /** 费用明细 */
    //@Excel(name = "费用明细")
    @ApiModelProperty(value = "费用明细")
    private String expenseDetails;

    /** 备注 */
    //@Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String notes;

    /** 附件 */
    //@Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String annex;

    /** 标记 */
    //@Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 审批流程Id */
    //@Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用户ID */
    //@Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    //发起人名称
    @Excel(name = "申请人")
    @TableField(exist = false)
    private String userName;

    /** 部门ID */
    //@Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 申请日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "申请日期", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "申请日期")
    private Date applicationDate;

    /** 费用类型 */
    @Excel(name = "费用类型",readConverterExp = "1=用餐,2=住宿,3=其他")
    @ApiModelProperty(value = "费用类型")
    private String feeType;

    /** 预计金额 */
    @Excel(name = "预计金额")
    @ApiModelProperty(value = "预计金额")
    private String estimatedAmount;

    /** 租户ID */
    //@Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;

    /** 提交状态 */
    //@Excel(name = "提交状态")
    @ApiModelProperty(value = "提交状态")
    private String submitState;


    /** 提交时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@Excel(name = "提交时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "提交时间")
    private Date submitTime;


    /** 实际花费金额 */
    @Excel(name = "实际花费金额")
    @ApiModelProperty(value = "实际花费金额")
    private String spendAmount;


    /**
     * 费用归属类型
     */
    //@Excel(name = "费用归属类型",readConverterExp = "2=项目,3=管理费,4=办事处")
    @ApiModelProperty(value = "费用归属类型")
    private String feeIlk;

    /**
     * 费用归属类型value
     */
    @Excel(name = "费用归属类型")
    @ApiModelProperty(value = "费用归属类型value")
    @TableField(exist = false)
    private String feeIlkName;


    /** 状态 */
    @Excel(name = "审批状态",readConverterExp = "oaEntertain:verifying=审批中,oaEntertain:pass=已通过,oaEntertain:reject=已拒绝,oaEntertain:withdraw=已撤回")
    @ApiModelProperty(value = "状态")
    private String state;

    /**
     * 归属事业部
     */
    //@Excel(name = "归属事业部")
    @ApiModelProperty(value = "归属事业部")
    private String belongDept;

    /**
     * 归属事业部名称
     */
    @ApiModelProperty(value = "归属事业部名称")
    @TableField(exist = false)
    private String belongDeptName;

    /** 发票 */
    //@Excel(name = "发票")
    @ApiModelProperty(value = "发票")
    private String invoice;

    /** 发票上传时间 */
    //@Excel(name = "发票上传时间")
    @ApiModelProperty(value = "发票上传时间")
    private String uploadTime;

    /** 根据年月搜索 */
    @ApiModelProperty(value = "根据年月搜索")
    @TableField(exist = false)
    private String createTimeSearch;

    /** 用户查询 */
    @ApiModelProperty(value = "用户查询")
    @TableField(exist = false)
    private String userIds;

    /** 创建时间 */
    @Excel(name = "创建时间" ,needMerge = true,dateFormat = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    @TableField(exist = false)
    private Date creationTime;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String startTime;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String endTime;

}
