package com.business.system.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @version 1.0
 * @Author: js
 * @Description: 根据部门查询用户DTO
 * @Date: 2023/2/24 23:14
 */
@Data
@ApiModel(value = "SysUserDepDTO对象", description = "根据部门查询用户DTO")
public class SysUserDepDTO implements Serializable {

    private static final long serialVersionUID = 2382932119139701098L;

    @ApiModelProperty("部门ID")
    private String deptId;

    @ApiModelProperty("用戶ID")
    private String userId;

    @ApiModelProperty("人员分类")
    private String humanoid;

    /** 关键词 */
    @ApiModelProperty(value = "关键词")
    private String keyWord;

    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;
}
