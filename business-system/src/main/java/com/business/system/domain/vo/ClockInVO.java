package com.business.system.domain.vo;

import com.business.common.annotation.Excel;
import com.business.system.domain.OaAttendanceAddress;
import com.business.system.domain.OaWorkTimes;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value="打卡规则对象",description="打卡规则对象")
public class ClockInVO {

    /** 是否休假  1:上班  0：休假*/
    @ApiModelProperty(value = "是否休假")
    private String holiday;

    /** 当天班次 */
    @ApiModelProperty(value = "当天班次")
    private OaWorkTimes oaWorkTimes;

    /** 前一天班次 */
    @ApiModelProperty(value = "前一天班次")
    private OaWorkTimes beforeWorkTimes;

    /** 是否打卡 1：无需打卡  0：需要打卡*/
    @ApiModelProperty(value = "是否打卡")
    private String clockIn;

    /** 是否允许外勤 1：允许外勤  2：不允许外勤*/
    @ApiModelProperty(value = "是否允许外勤")
    private String fieldClock;

    /** 考勤类型 1：固定班制   2：弹性工作制*/
    @ApiModelProperty(value = "考勤类型")
    private String attendanceType;

    /** 地址 */
    @ApiModelProperty(value = "地址")
    private List<OaAttendanceAddress> attendanceAddresses;

    /** 考勤开始时间 */
    @ApiModelProperty(value = "考勤开始时间")
    private String attendanceStart;

    /** 工作时长 */
    @ApiModelProperty(value = "工作时长")
    private Long workHours;

    /** 考勤组名称 */
    @ApiModelProperty(value = "考勤组名称")
    private String attendanceName;
}
