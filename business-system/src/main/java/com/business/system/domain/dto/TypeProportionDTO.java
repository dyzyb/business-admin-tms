package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "TypeProportionDTO", description = "数量及金额的统计")
public class TypeProportionDTO implements Serializable {
    /**
     *类型名称
     */
    @ApiModelProperty(value = "类型名称")
    private String typeName;

    /**
     * 类型占比率
     */
    @ApiModelProperty(value = "类型占比率")
    private BigDecimal Proportion;
    /**
     *类型标识
     */
    @ApiModelProperty(value = "类型标识")
    private String typeValue;

}
