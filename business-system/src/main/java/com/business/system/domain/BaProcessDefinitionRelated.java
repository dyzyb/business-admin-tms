package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author: js
 * @Description: 业务与流程定义关联关系对象
 * @date: 2022/12/1
 */
@EqualsAndHashCode(callSuper = true)
@TableName("ba_process_definition_related")
@Data
public class BaProcessDefinitionRelated extends BaseEntity {

    private static final long serialVersionUID = -1265259579265902853L;

    /**
     * 主键
     */
    private String id;

    /**
     * 业务类型
     */
    private String businessType;

    /**
     * 流程模板ID
     */
    private String templateId;

    /**
     * 流程定义ID
     */
    private String processDefinitionId;

    /**
     * 租户ID
     */
    private String tenantId;

    /**
     * 流程定义key
     */
    private String processKey;

    /**
     * 流程定义名称
     */
    private String name;

    /**
     * 流程定义描述
     */
    private String description;

    /**
     * 流程定义版本
     */
    private String customVersion;

    /**
     * 状态
     */
    private String state;

    /**
     * 标记
     */
    private Integer flag;
}
