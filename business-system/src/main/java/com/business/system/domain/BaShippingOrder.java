package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 发运单对象 ba_shipping_order
 *
 * @author single
 * @date 2023-02-18
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "发运单对象",description = "")
@TableName("ba_shipping_order")
public class BaShippingOrder extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 运单编号 */
    @Excel(name = "运单编号")
    @ApiModelProperty(value = "运单编号")
    private String thdno;

    /** 车号 */
    @Excel(name = "车号")
    @ApiModelProperty(value = "车号")
    private String carno;

    /** 车主电话 */
    @Excel(name = "车主电话")
    @ApiModelProperty(value = "车主电话")
    private String cartel;

    /** 原发时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "原发时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "原发时间")
    private Date yfdate;

    /** 进场时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "进场时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "进场时间")
    private Date dhdate;

    /** 装车磅单图片 */
    @Excel(name = "装车磅单图片")
    @ApiModelProperty(value = "装车磅单图片")
    private String vidpic1;

    /** 卸车磅单图片 */
    @Excel(name = "卸车磅单图片")
    @ApiModelProperty(value = "卸车磅单图片")
    private String vidpic2;

    /** 出场净重 */
    @Excel(name = "出场净重")
    @ApiModelProperty(value = "出场净重")
    private BigDecimal ccjweight;

    /** 入场净重 */
    @Excel(name = "入场净重")
    @ApiModelProperty(value = "入场净重")
    private BigDecimal rcjweight;

    /** 磅差 */
    @Excel(name = "磅差")
    @ApiModelProperty(value = "磅差")
    private BigDecimal bangcha;

    /** 涨吨 */
    @Excel(name = "涨吨")
    @ApiModelProperty(value = "涨吨")
    private BigDecimal zhangdun;


    /** 实收吨数 */
    @Excel(name = "实收吨数")
    @ApiModelProperty(value = "实收吨数")
    private BigDecimal ssdun;

    /** 运费结算重量 */
    @Excel(name = "运费结算重量")
    @ApiModelProperty(value = "运费结算重量")
    private BigDecimal yfjsweight;

    /** 货主货源应付运费 */
    @Excel(name = "货主货源应付运费")
    @ApiModelProperty(value = "货主货源应付运费")
    private BigDecimal yfratesmoney;

    /** 货主运费扣款金额 */
    @Excel(name = "货主运费扣款金额")
    @ApiModelProperty(value = "货主运费扣款金额")
    private BigDecimal rateskoumoney;

    /** 涨吨金额 */
    @Excel(name = "涨吨金额")
    @ApiModelProperty(value = "涨吨金额")
    private BigDecimal zdmoney;

    /** 经济人货源运费扣款金额 */
    @Excel(name = "经济人货源运费扣款金额")
    @ApiModelProperty(value = "经济人货源运费扣款金额")
    private BigDecimal rateskoumoneyEconomic;

    /** 货源单编号 */
    @Excel(name = "货源单编号")
    @ApiModelProperty(value = "货源单编号")
    private String supplierNo;

    /** 货主运费单价 */
    @Excel(name = "货主运费单价")
    @ApiModelProperty(value = "货主运费单价")
    private BigDecimal ratesprice;

    /** 货物名称 */
    @Excel(name = "货物名称")
    @ApiModelProperty(value = "货物名称")
    private String coaltypename;

    /** 是否结算运费 */
    @Excel(name = "是否结算运费")
    @ApiModelProperty(value = "是否结算运费")
    private Integer isjsratesflag;

    /** 实际结算金额 */
    @Excel(name = "实际结算金额")
    @ApiModelProperty(value = "实际结算金额")
    private BigDecimal sfratesmoney;

    /** 信息费 */
    @Excel(name = "信息费")
    @ApiModelProperty(value = "信息费")
    private BigDecimal infocost;

    /** 信息费 */
    @Excel(name = "其他扣款")
    @ApiModelProperty(value = "其他扣款")
    private BigDecimal ratesdikoumoney;

    /** 货物名称 */
    @Excel(name = "发货单位")
    @ApiModelProperty(value = "发货单位")
    private String fhunitname;

    /** 货物名称 */
    @Excel(name = "收货单位")
    @ApiModelProperty(value = "收货单位")
    private String shunitname;

    /** 驾驶证号 */
    @Excel(name = "驾驶证号")
    @ApiModelProperty(value = "驾驶证号")
    private String driverno;

    /**
     * 签收状态
     */
    @Excel(name = "签收状态")
    @ApiModelProperty(value = "签收状态")
    private String signFor;

    @Excel(name = "发货详细地址")
    @ApiModelProperty(value = "发货详细地址")
    private String depDetailed;

    @Excel(name = "司机名称")
    @ApiModelProperty(value = "司机名称")
    private String cardriver;

    @ApiModelProperty(value = "货源名称")
    @TableField(exist = false)
    private String sourceName;

    @Excel(name = "司机名称")
    @ApiModelProperty(value = "司机名称")
    private String errorText;

    @Excel(name = "选中状态")
    @ApiModelProperty(value = "选中状态")
    private String depotState;


    /** 开始时间 **/
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String startTime;

    /** 结束时间 **/
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String endTime;

    /** 合同启动名称 **/
    @ApiModelProperty(value = "合同启动名称")
    @TableField(exist = false)
    private String starName;

    /** 货源信息 **/
    @ApiModelProperty(value = "货源信息")
    @TableField(exist = false)
    private BaTransportAutomobile baTransportAutomobile;

    /** 付款状态 **/
    @ApiModelProperty(value = "付款状态")
    @TableField(exist = false)
    private String paymentStatus;

    /** 运输吨数 **/
    @ApiModelProperty(value = "运输吨数")
    @TableField(exist = false)
    private Long transWeight;

    @Excel(name = "0 抢单,1 竞价")
    @ApiModelProperty(value = "0 抢单,1 竞价")
    private String orderModel;

    @Excel(name = "订单号，如果是批量货，就是子单号")
    @ApiModelProperty(value = "订单号，如果是批量货，就是子单号")
    private String orderId;

    @Excel(name = "如果为批量货，就为母单号，普通货为空")
    @ApiModelProperty(value = "如果为批量货，就为母单号，普通货为空")
    private String yardId;

    @Excel(name = "自定义单号")
    @ApiModelProperty(value = "自定义单号")
    private String selfComment;

    @Excel(name = "选中状态")
    @ApiModelProperty(value = "货主名称")
    private String consignorUserName;

    @Excel(name = "货主名称手机号")
    @ApiModelProperty(value = "货主名称手机号")
    private String consignorMobile;

    @Excel(name = "承运状态 货主运单状态:5 摘单,6 确认发货,7 确认收货,8 已终止")
    @ApiModelProperty(value = "承运状态 货主运单状态:5 摘单,6 确认发货,7 确认收货,8 已终止")
    private String consignorState;

    /*@Excel(name = "货物名称")
    @ApiModelProperty(value = "货物名称")
    private String cargoName;*/

    @Excel(name = "承运方姓名")
    @ApiModelProperty(value = "承运方姓名")
    private String carrierName;

    @Excel(name = "承运方手机号")
    @ApiModelProperty(value = "承运方手机号")
    private String carrierMobile;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "摘牌时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "摘牌时间")
    private Date delistTime;

    @Excel(name = "摘单吨位")
    @ApiModelProperty(value = "摘单吨位")
    private BigDecimal weight;

    @Excel(name = "车牌号")
    @TableField(exist = false)
    private String plateNumber;

    @Excel(name = "司机姓名")
    @ApiModelProperty(value = "司机姓名")
    private String driverUserName;

    @Excel(name = "司机手机号")
    @ApiModelProperty(value = "司机手机号")
    private String driverMobile;

    /** 开票审核状态 **/
    @TableField(exist = false)
    @ApiModelProperty(value = "开票审核状态")
    private Integer checkFlag;

    /** 货物名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "品名")
    private String cargoName;

    /** 货物名称 */
    @TableField(exist = false)
    @ApiModelProperty(value = "货源类型")
    private String automobileType;

    @Excel(name = "出入库类型")
    @ApiModelProperty(value = "出入库类型")
    private String status;

    @Excel(name = "出入库状态")
    @ApiModelProperty(value = "出入库状态")
    private String state;

    /** 平台类型 */
    @TableField(exist = false)
    @ApiModelProperty(value = "平台类型")
    private String type;

    /** 试验ID **/
    @Excel(name = "试验ID")
    @ApiModelProperty(value = "试验ID")
    private String experimentId;

    /** 仓库ID **/
    @Excel(name = "仓库ID")
    @ApiModelProperty(value = "仓库ID")
    private String depotId;

    /** 仓库名称 **/
    @ApiModelProperty(value = "仓库名称")
    @TableField(exist = false)
    private String depotName;

    /** 仓位ID **/
    @Excel(name = "仓位ID")
    @ApiModelProperty(value = "仓位ID")
    private String positionId;

    /** 仓位名称 **/
    @ApiModelProperty(value = "仓位名称")
    @TableField(exist = false)
    private String positionName;

    /** 排序类型 */
    @TableField(exist = false)
    @ApiModelProperty(value = "排序类型")
    private String orderCon;
}
