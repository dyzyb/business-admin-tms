package com.business.system.domain.vo;

import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.system.domain.SysPost;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @Author: js
 * @Description: 业务与流程实例关联关系对象
 * @date: 2022/02/14 16:02
 */
@Data
@ApiModel(value="ProcessBusinessInstanceRelatedVO对象",description="业务与流程实例关联关系对象")
public class BaProcessBusinessInstanceRelatedVO implements Serializable {

    private static final long serialVersionUID = -5853940335979829957L;

    @ApiModelProperty(value = "流程定义id")
    private String id;

    @ApiModelProperty(value = "业务key")
    private String businessKey;

    @ApiModelProperty(value = "业务id")
    private String businessId;

    @ApiModelProperty(value = "业务类型")
    private String businessType;

    @ApiModelProperty(value = "流程实例ID")
    private String processInstanceId;

    @ApiModelProperty(value = "业务数据")
    private String businessData;

    @ApiModelProperty(value = "审核类型")
    private String approveType;

    @ApiModelProperty(value = "审核结果")
    private String approveResult;

    @ApiModelProperty(value = "原因")
    private String reason;

    @ApiModelProperty(value = "租户ID")
    private Integer tenantId;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createdTime;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updatedTime;

    @ApiModelProperty(value = "创建者")
    private String createBy;

    @ApiModelProperty(value = "更新者")
    private String updateBy;

    @ApiModelProperty(value = "系统用户")
    private SysUser sysUser;

    @ApiModelProperty(value = "部门")
    private SysDept sysDept;

    @ApiModelProperty(value = "岗位")
    private List<SysPost> sysPost;

    @ApiModelProperty("业务名称")
    private String busiName;
}
