package com.business.system.domain.dto;

import com.business.system.domain.OaMaintenanceExpansion;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserMaintenanceExpansionDTO", description = "用户维护与拓展信息")
public class UserMaintenanceExpansionDTO {

    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID")
    private String userId;

    /**
     * 用户名称
     */
    @ApiModelProperty(value = "用户名称")
    private String userName;

    /**
     * 用户头像
     */
    @ApiModelProperty(value = "用户头像")
    private String avatar;

    /**
     * 用户岗位
     */
    @ApiModelProperty(value = "用户岗位")
    private String postName;

    /**
     * 用户部门
     */
    @ApiModelProperty(value = "用户部门")
    private String deptName;

    /**
     * 总公里数
     */
    @ApiModelProperty(value = "总公里数")
    private BigDecimal realityKilometersTotal;

    /**
     * 维护与拓展信息
     */
    @ApiModelProperty(value = "维护与拓展信息")
    private List<OaMaintenanceExpansion> maintenanceExpansionList;
}
