package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author js
 * @version 1.0
 * @description: DTO基类
 * @date 2022-12-2 14:09
 */

@Data
@Accessors(chain = true)
public class DictTypeDTO implements Serializable {
    private static final long serialVersionUID = -9173679895285748679L;
    /** 业务类型 */
    @ApiModelProperty(value = "业务类型")
    private String businessType;
    /** 业务类型 */
    @ApiModelProperty(value = "业务名称")
    private String businessname;

    /** 月发运数量 **/
    @ApiModelProperty(value = "业务占比")
    private BigDecimal Typenum;


}
