package com.business.system.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

/**
 * 打卡记录对象 oa_clock_record
 *
 * @author single
 * @date 2023-11-27
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "打卡记录对象",description = "")
@TableName("oa_clock_record")
public class OaClockRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 员工ID */
    @ApiModelProperty(value = "员工ID")
    private Long employeeId;

    /** 员工姓名 */
    @Excel(name = "姓名")
    @ApiModelProperty(value = "员工姓名")
    @TableField(exist = false)
    private String employeeName;

    /** 打卡时间 **/
    @Excel(name = "打卡时间")
    @ApiModelProperty(value = "打卡时间")
    @TableField(exist = false)
    private String dateTime;

    /** 经度 */
    @ApiModelProperty(value = "经度")
    private String longitude;

    /** 纬度 */
    @ApiModelProperty(value = "纬度")
    private String latitude;

    /** 类型 1 内勤 2 外勤 */
    //@Excel(name = "类型 1 内勤 2 外勤", readConverterExp = "1=内勤,2=外勤")
    @ApiModelProperty(value = "类型 1 内勤 2 外勤")
    private String type;

    /** 打卡类型 1 上班打卡 2 下班打卡 */
    @Excel(name = "打卡类型",readConverterExp = "0=外勤打卡,1=上班打卡,2=下班打卡")
    @ApiModelProperty(value = "打卡类型 1 上班打卡 2 下班打卡")
    private String clockType;

    /** 打卡日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@Excel(name = "打卡日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "打卡日期")
    private Date clockDate;

    /** 打卡时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@Excel(name = "打卡时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "打卡时间")
    private Date clockTime;

    /** 标记 */
    //@Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 状态 */
    //@Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 用户ID */
    //@Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    //@Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 流程审批ID */
    //@Excel(name = "流程审批ID")
    @ApiModelProperty(value = "流程审批ID")
    private String flowId;

    /** 租户ID */
    //@Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 打卡状态（1：正常  2：迟到  3：早退） */
    @Excel(name = "打卡结果", readConverterExp = "1=正常,2=迟到,3=早退,6=严重迟到,7=迟到旷工,8=早退旷工,12=外勤")
    @ApiModelProperty(value = "打卡结果")
    private String clockState;

    /** 打卡地址 */
    @Excel(name = "打卡地址")
    @ApiModelProperty(value = "打卡地址")
    private String address;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 关联ID */
    //@Excel(name = "关联ID")
    @ApiModelProperty(value = "关联ID")
    private String relevanceId;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String start;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String end;



}
