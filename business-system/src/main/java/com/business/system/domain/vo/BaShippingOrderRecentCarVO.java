package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 统计最近个日期网货平台（在途车辆、装车车辆、卸车车辆）返回统计数据对象
 *
 * @author js
 * @date 2023-6-26
 */
@Data
@ApiModel(value="BaShippingOrderRecentCarVO对象",description="统计最近个日期网货平台（在途车辆、装车车辆、卸车车辆）返回统计数据对象")
public class BaShippingOrderRecentCarVO
{
    /** 在途车辆数 */
    @ApiModelProperty(value = "在途车辆数")
    private Integer inTransit;

    /** 装车车辆数 */
    @ApiModelProperty(value = "装车车辆数")
    private Integer loadVehicle;

    /** 卸车车辆数 */
    @ApiModelProperty(value = "卸车车辆数")
    private Integer unloadVehicle;
}
