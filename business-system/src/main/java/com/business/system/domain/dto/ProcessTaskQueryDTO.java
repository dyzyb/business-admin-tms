package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @Author: js
 * @Description: 审批任务查询对象
 * @date: 2022/12/6 13:39
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "ProcessTaskQueryDTO对象", description = "审批任务查询对象")
public class ProcessTaskQueryDTO extends BaseDTO {

    private static final long serialVersionUID = 2715419589898089846L;

    @ApiModelProperty("受理人id")
    private String userId;

    @ApiModelProperty("发起人ID")
    private String startUserId;

    @ApiModelProperty("发起人名称")
    private String startUserName;

    @ApiModelProperty("流程实例id")
    private String processInstanceId;

    @ApiModelProperty("用户名过滤条件")
    private String userFilterCondition;

    @ApiModelProperty("业务过滤条件，立项业务 taskType:project")
    private String filterCondition;

    @ApiModelProperty("流程结束状态 processIsEnd 已结束")
    private String processIsEnd;

    @ApiModelProperty("今日数据")
    private String todayData;

    @ApiModelProperty("过滤未开发数据")
    private String filterConditionNotEqual;

    @ApiModelProperty("查询指定数据")
    private String filterConditionEqual;

    @ApiModelProperty("移动端标识")
    private String wxType;

    @ApiModelProperty("业务内容")
    private String businessData;

    @ApiModelProperty("流程实例ID集合")
    private String processInstanceIds;

    @ApiModelProperty("流程实例IDList")
    private List<String> processInstanceIdList;


    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;
}
