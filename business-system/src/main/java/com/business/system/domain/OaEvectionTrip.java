package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 出差行程对象 oa_evection_trip
 *
 * @author ljb
 * @date 2023-11-11
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "出差行程对象",description = "")
@TableName("oa_evection_trip")
public class OaEvectionTrip extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 地点 */
    @Excel(name = "地点")
    @ApiModelProperty(value = "地点")
    private String location;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "开始时间")
    private Date startTime;

    /** 开始阶段 */
    @Excel(name = "开始时段",readConverterExp = "1=上半天,2=下半天")
    @ApiModelProperty(value = "开始阶段")
    private String startCycle;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "结束时间")
    private Date endTime;

    /** 结束阶段 */
    @Excel(name = "时段",readConverterExp = "1=上半天,2=下半天")
    @ApiModelProperty(value = "结束阶段")
    private String endCycle;

    /** 时长 */
    @Excel(name = "时长")
    @ApiModelProperty(value = "时长")
    private BigDecimal duration;

    /** 状态 */
    //@Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    //@Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 出差ID */
    //@Excel(name = "出差ID")
    @ApiModelProperty(value = "出差ID")
    private String evectionId;

    /** 每一天 */
    //@Excel(name = "每一天")
    @ApiModelProperty(value = "每一天")
    private String everyDay;

    /** 同行人 */
    @ApiModelProperty(value = "同行人")
    @TableField(exist = false)
    private String peers;

    /** 同行人名称 */
    @ApiModelProperty(value = "同行人名称")
    @TableField(exist = false)
    private String peersName;

    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    @ApiModelProperty(value = "申请人")
    @TableField(exist = false)
    private String userName;

    @ApiModelProperty(value = "申请人查询")
    @TableField(exist = false)
    private String userIds;

    @ApiModelProperty(value = "租户ID")
    @TableField(exist = false)
    private String tenantId;


}
