package com.business.system.domain.vo;

import com.business.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 统计最近个日期网货平台（在途车辆、装车车辆、卸车车辆）对象
 *
 * @author js
 * @date 2023-6-26
 */
@Data
@ApiModel(value="BaShippingOrderVO对象",description="统计最近个日期网货平台（在途车辆、装车车辆、卸车车辆）对象")
public class BaShippingOrderVO
{
    @ApiModelProperty("ID")
    private String id;

    /** 装车磅单图片 */
    @ApiModelProperty(value = "装车磅单图片")
    private String vidpic1;

    /** 卸车磅单图片 */
    @ApiModelProperty(value = "卸车磅单图片")
    private String vidpic2;

    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    private String createTime;

    /** 平台 */
    @ApiModelProperty(value = "平台")
    private String platform;

    /** 承运状态 货主运单状态:5 摘单,6 确认发货,7 确认收货,8 已终止 */
    private String consignorState;
}
