package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 货主档案对象 ba_cargo_owner
 *
 * @author ljb
 * @date 2023-12-22
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "货主档案对象",description = "")
@TableName("ba_cargo_owner")
public class BaCargoOwner extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 货主名称 */
    @Excel(name = "货主名称")
    @ApiModelProperty(value = "货主名称")
    private String name;

    /** 货仓 */
    @Excel(name = "货仓")
    @ApiModelProperty(value = "货仓")
    private String depotId;

    /** 货仓 */
    @ApiModelProperty(value = "货仓")
    @TableField(exist = false)
    private String depotName;

    /** 扣点 */
    @Excel(name = "扣点")
    @ApiModelProperty(value = "扣点")
    private BigDecimal points;

    /** 省 */
    @Excel(name = "省")
    @ApiModelProperty(value = "省")
    private String province;

    /** 市 */
    @Excel(name = "市")
    @ApiModelProperty(value = "市")
    private String city;

    /** 区 */
    @Excel(name = "区")
    @ApiModelProperty(value = "区")
    private String area;

    /** 详细地址 */
    @Excel(name = "详细地址")
    @ApiModelProperty(value = "详细地址")
    private String address;

    /** 联系人 */
    @Excel(name = "联系人")
    @ApiModelProperty(value = "联系人")
    private String contacts;

    /** 联系电话 */
    @Excel(name = "联系电话")
    @ApiModelProperty(value = "联系电话")
    private String phone;

    /** 用户邮箱 */
    @Excel(name = "用户邮箱")
    @ApiModelProperty(value = "用户邮箱")
    private String email;

    /** 纳税人识别号 */
    @Excel(name = "纳税人识别号")
    @ApiModelProperty(value = "纳税人识别号")
    private String taxNum;

    /** 注册资本（万元） */
    @Excel(name = "注册资本", readConverterExp = "万=元")
    @ApiModelProperty(value = "注册资本")
    private BigDecimal registeredCapital;

    /** 营业期限 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "营业期限", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "营业期限")
    private Date businessTerm;

    /** 资质等级 */
    @Excel(name = "资质等级")
    @ApiModelProperty(value = "资质等级")
    private String qualificationGrade;

    /** 结算类型 */
    @Excel(name = "结算类型")
    @ApiModelProperty(value = "结算类型")
    private String settlementType;

    /** 交货周期 */
    @Excel(name = "交货周期")
    @ApiModelProperty(value = "交货周期")
    private Long leadTime;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 审批流程Id */
    @Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    //发起人名称
    @TableField(exist = false)
    private String userName;

    //发起人
    @TableField(exist = false)
    private SysUser user;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 简称 */
    @Excel(name = "简称")
    @ApiModelProperty(value = "简称")
    private String shorterForm;

    /** 编码 */
    @Excel(name = "编码")
    @ApiModelProperty(value = "编码")
    private String idCode;

    /** 长期有效 */
    @Excel(name = "长期有效")
    @ApiModelProperty(value = "长期有效")
    private String longTerm;

    /**
     * 流程实例ID
     */
    @TableField(exist = false)
    private List<String> processInstanceId;


    /** 营业执照 */
    @Excel(name = "营业执照")
    @ApiModelProperty(value = "营业执照")
    private String file;

    /** 法人身份证 */
    @Excel(name = "法人身份证")
    @ApiModelProperty(value = "法人身份证")
    private String corporateCapacityIdCard;

    /** 附件 **/
    @Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String appendix;

    /** 关联ID **/
    @Excel(name = "关联ID")
    @ApiModelProperty(value = "关联ID")
    private String relationId;

    /** 合同附件 **/
    @Excel(name = "合同附件")
    @ApiModelProperty(value = "合同附件")
    private String contractFile;

    /** 授权书 **/
    @Excel(name = "授权书")
    @ApiModelProperty(value = "授权书")
    private String empowerBook;

    /** 身份证反面 **/
    @Excel(name = "身份证反面")
    @ApiModelProperty(value = "身份证反面")
    private String idCard;

    /**
     * 开票信息
     */
    @ApiModelProperty(value = "开票信息")
    @TableField(exist = false)
    private BaBillingInformation billingInformation;

    /** 账号状态 **/
    @ApiModelProperty(value = "账号状态")
    @TableField(exist = false)
    private String status;

    /** 社会信用代码 **/
    @ApiModelProperty(value = "社会信用代码")
    @TableField(exist = false)
    private String socialCreditCode;


    /** 联系方式 **/
    @ApiModelProperty(value = "联系方式")
    @TableField(exist = false)
    private String phonenumber;

    /** 公司名称 **/
    @ApiModelProperty(value = "公司名称")
    @TableField(exist = false)
    private String companyName;

    /** 法人身份证号 **/
    @ApiModelProperty(value = "法人身份证号")
    private String corporateCapacity;


    /** 公司类型 **/
    @ApiModelProperty(value = "公司类型")
    private String companyType;


}
