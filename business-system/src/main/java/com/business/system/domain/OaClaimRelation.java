package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 报销关联流程对象 oa_claim_relation
 *
 * @author single
 * @date 2023-12-13
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "报销关联流程对象",description = "")
@TableName("oa_claim_relation")
public class OaClaimRelation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 报销状态 0 未报销 1 报销中 2 已报销 */
    @Excel(name = "报销状态 0 未报销 1 报销中 2 已报销")
    @ApiModelProperty(value = "报销状态 0 未报销 1 报销中 2 已报销")
    private int claimState;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 业务类型 */
    @Excel(name = "业务类型")
    @ApiModelProperty(value = "业务类型")
    private String businessType;

    /** 申请时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "申请时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "申请时间")
    private Date applyTime;

    /** 实际花费金额 */
    @Excel(name = "实际花费金额")
    @ApiModelProperty(value = "实际花费金额")
    private BigDecimal applyAmount;

    /** 业务ID */
    @Excel(name = "业务ID")
    @ApiModelProperty(value = "业务ID")
    private String businessId;

    /** 流程实例ID */
    @Excel(name = "流程实例ID")
    @ApiModelProperty(value = "流程实例ID")
    private String processInstanceId;

    /** 关联流程名称 */
    @Excel(name = "关联流程名称")
    @ApiModelProperty(value = "关联流程名称")
    private String processName;

    /** 发票 */
    @Excel(name = "发票")
    @ApiModelProperty(value = "发票")
    private String invoice;

    /** 报销ID */
    @Excel(name = "报销ID")
    @ApiModelProperty(value = "报销ID")
    private String claimId;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @TableField(exist = false)
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @TableField(exist = false)
    private Date endTime;

    /** 申请时间 */
    @JsonFormat(pattern = "yyyy-MM")
    @Excel(name = "申请时间", width = 30, dateFormat = "yyyy-MM")
    @ApiModelProperty(value = "申请时间")
    @TableField(exist = false)
    private String applyTimeSearch;

    /** 关联流程ID（报销实际费用申请ID） */
    //@Excel(name = "备注")
    @ApiModelProperty(value = "关联流程ID（报销实际费用申请ID）")
    private String relationId;
}
