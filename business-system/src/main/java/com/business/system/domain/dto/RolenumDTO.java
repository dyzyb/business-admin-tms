package com.business.system.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author js
 * @version 1.0
 * @description: DTO基类
 * @date 2022-12-2 14:09
 */

@Data
@Accessors(chain = true)
public class RolenumDTO implements Serializable {
    private static final long serialVersionUID = -9173679895285748679L;

    /** 业务类型 */
    @ApiModelProperty(value = "人员类型")
    private String type;

    /** 月发运数量 **/
    @ApiModelProperty(value = "数量")
    private int num;


}
