package com.business.system.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 订单对象 ba_order
 *
 * @author ljb
 * @date 2022-12-09
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "订单对象DTO",description = "")
public class BaOrderDTO
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 订单名称 */
    @ApiModelProperty(value = "订单名称")
    private String orderName;

    /** 标记 */
    @ApiModelProperty(value = "标记")
    private Integer flag;

    /** 商品ID */
    @ApiModelProperty(value = "商品ID")
    private String goodsId;

    /** 商品名称 */
    @ApiModelProperty(value = "商品名称")
    private String goodsName;

    /** 关联合同 */
    @ApiModelProperty(value = "关联合同")
    private String companyId;

    /** 到货时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "到货时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "到货时间")
    private Date arrivalTime;

    /** 发货日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "发货日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "发货日期")
    private Date checkTime;

    /** 验收日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "验收日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "验收日期")
    private Date checkDate;

    /** 运输方式 */
    @ApiModelProperty(value = "运输方式")
    private String transportType;

    /** 运输状态 */
    @ApiModelProperty(value = "运输状态")
    private Integer transportState;

    /** 验收煤量/矿发煤量 */
    @ApiModelProperty(value = "验收煤量")
    private BigDecimal checkCoalNum;

    /** 矿发煤量 **/
    @ApiModelProperty(value = "矿发煤量")
    private BigDecimal coalOutput;

    /** 订单类型 */
    @ApiModelProperty(value = "订单类型")
    private String orderType;

    /** 车数 */
    @ApiModelProperty(value = "车数")
    private Integer carsNum;

    /** 运输编号 */
    @ApiModelProperty(value = "运输编号")
    private String transportId;

    /** 本次回款金额 **/
    @ApiModelProperty(value = "本次回款金额")
    private BigDecimal currentCollection;

    /**
     * 认领历史回款编号
     */
    @ApiModelProperty(value = "认领历史回款编号")
    private String historyId;

    /**
     * 累计金额
     */
    @ApiModelProperty(value = "累计金额")
    private BigDecimal accumulatedAmount;

    /**
     * 订单应付金额
     */
    @ApiModelProperty(value = "订单应付金额")
    private BigDecimal orderPay;

    /**
     * 订单应付金额
     */
    @ApiModelProperty(value = "订单应付金额")
    private BigDecimal orderReceive;


    /**
     * 实付金额
     */
    @ApiModelProperty(value = "订单应付金额/应收")
    private BigDecimal actualPayment;

    /**
     * 已付金额
     */
    @ApiModelProperty(value = "订单已付金额/已收")
    private BigDecimal amountPaid;

    /**
     * 结算历史信息编号
     */
    @ApiModelProperty(value = "结算历史信息编号")
    private String detailId;

    /** 结算编号 **/
    @ApiModelProperty(value = "结算编号")
    private String settlementId;
}
