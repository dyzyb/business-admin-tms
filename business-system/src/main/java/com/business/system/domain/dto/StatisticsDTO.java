package com.business.system.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "StatisticsDTO对象", description = "统计业务部排名")
public class StatisticsDTO {

    private static final long serialVersionUID = 6281016059931188887L;

    /** 部门名称 */
    @ApiModelProperty(value = "部门名称")
    private String deptName;

    /** 立项数量 */
    @ApiModelProperty(value = "立项数量")
    private Integer projects;

    /** 合同启动数量 */
    @ApiModelProperty(value = "合同启动数量")
    private Integer contractStarts;

    /** 发运次数 */
    @ApiModelProperty(value = "发运次数")
    private Integer despatchNum;

    /** 发运量 */
    @ApiModelProperty(value = "发运量")
    private BigDecimal trafficVolume;

    /** 累计合同额 */
    @ApiModelProperty(value = "累计合同额")
    @TableField(exist = false)
    private BigDecimal contractAmount;

    /** 累计贸易额 */
    @ApiModelProperty(value = "累计贸易额")
    @TableField(exist = false)
    private BigDecimal tradeAmount;

    /** 累计运输量 */
    @ApiModelProperty(value = "累计运输量")
    @TableField(exist = false)
    private BigDecimal checkCoalNum;
}
