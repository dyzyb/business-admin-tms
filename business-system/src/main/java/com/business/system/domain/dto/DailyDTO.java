package com.business.system.domain.dto;


import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.system.domain.OaCalendar;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@ApiModel(value = "打卡日统计对象DTO",description = "打卡日统计对象")
public class DailyDTO {

    private static final long serialVersionUID = 1L;

    /** 员工名称 */
    @ApiModelProperty(value = "员工名称")
    private String userName;

        /** 部门名称 */
    @ApiModelProperty(value = "员工名称")
    private String deptName;

    /** 日报提交数 */
    @ApiModelProperty(value = "日报提交数")
    private Integer submitNumber;

    /** 日报未提交数 */
    @ApiModelProperty(value = "日报未提交数")
    private Integer notSubmitNumber;

    /**
     * 天数据
     */
    @ApiModelProperty(value = "天数据")
    private List<OaCalendar> oaCalendars;





}
