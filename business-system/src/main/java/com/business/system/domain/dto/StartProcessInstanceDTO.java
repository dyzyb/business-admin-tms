package com.business.system.domain.dto;

import com.alibaba.fastjson.JSONObject;
import com.business.system.domain.dto.json.UserInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0
 * @Author: js
 * @Description: 流程实例前端控制器接口
 * @Date: 2022/12/1 9:32
 */
@Data
@ApiModel(value = "StartProcessInstanceDTO对象", description = "流程实例对象")
public class StartProcessInstanceDTO implements Serializable {

    private static final long serialVersionUID = -651452651806962406L;

    @ApiModelProperty("流程定义Key")
    private String processDefinitionKey;

    @ApiModelProperty("表单数据")
    private JSONObject formData;

    @ApiModelProperty("流程抄送用户")
    private Map<String, List<UserInfo>> processUsers;

    @ApiModelProperty("流程发起人")
    private UserInfo startUserInfo;

    @ApiModelProperty("流程节点处理人用户")
    private Map<String, Object> assigneeMap;

    @ApiModelProperty("租户ID")
    private String tenantId;
}
