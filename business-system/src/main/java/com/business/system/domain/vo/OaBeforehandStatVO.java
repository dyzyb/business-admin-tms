package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: js
 * @Description: 申请部门分布统计
 * @date: 2023/10/9 19:05
 */
@Data
@ApiModel(value="OaBeforehandStatVO对象",description="申请部门分布统计")
public class OaBeforehandStatVO implements Serializable {

    private static final long serialVersionUID = -8637761583617599449L;

    /** 部门 */
    @ApiModelProperty(value = "部门")
    private String deptId;

    /** 部门名称 */
    @ApiModelProperty(value = "部门名称")
    private String deptName;

    /** 申请数量 */
    @ApiModelProperty(value = "申请数量")
    private Long count;
}
