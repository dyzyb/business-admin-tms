package com.business.system.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * @Author: js
 * @Description: 库存统计对象
 * @date: 2024/1/18 17:51
 */
@Data
@ApiModel(value="BaMasterialStockVO对象",description="库存统计对象")
public class BaMasterialStockVO implements Serializable {

    private static final long serialVersionUID = -3106969666616566691L;
    /** 仓库id */
    @Excel(name = "库存id")
    @ApiModelProperty(value = "库存id")
    private String id;

    /** 仓库id */
    @Excel(name = "仓库id")
    @ApiModelProperty(value = "仓库id")
    private String depotId;

    @TableField(exist = false)
    @ApiModelProperty(value = "仓库名称")
    private String depotName;

    /** 库位ID */
    @Excel(name = "库位ID")
    @ApiModelProperty(value = "库位ID")
    private String locationId;

    /** 库位名称 */
    @Excel(name = "库位名称")
    @ApiModelProperty(value = "库位名称")
    private String locationName;

    /** 库位数量 */
    @Excel(name = "库位数量")
    @ApiModelProperty(value = "库位数量")
    private Long locationCount;

    /** 产品id */
    @Excel(name = "产品id")
    @ApiModelProperty(value = "产品id")
    private String goodsId;

    /** 产品名称 */
    @Excel(name = "产品名称")
    @ApiModelProperty(value = "产品名称")
    private String goodsName;

    /** 产品数量 */
    @Excel(name = "产品数量")
    @ApiModelProperty(value = "产品数量")
    private Long goodsCount;

    /** 项目id */
    @Excel(name = "项目id")
    @ApiModelProperty(value = "项目id")
    private String projectId;

    /** 项目名称 */
    @ApiModelProperty(value = "项目名称")
    @TableField(exist = false)
    private String projectName;

    /** 项目数量 */
    @Excel(name = "项目数量")
    @ApiModelProperty(value = "项目数量")
    private int projectCount;

    /** 库存总量（吨） */
    @Excel(name = "库存总量（吨）")
    @ApiModelProperty(value = "库存总量（吨）")
    private BigDecimal inventoryTotal;

    /** 所属公司 */
    @ApiModelProperty(value = "所属公司")
    private String deptName;

    /** 今日入库（吨） */
    @Excel(name = "今日入库（吨）")
    @ApiModelProperty(value = "今日入库（吨）")
    private BigDecimal todayIntoTotal;

    /** 今日出库（吨） */
    @Excel(name = "今日出库（吨）")
    @ApiModelProperty(value = "今日出库（吨）")
    private BigDecimal todayOutputTotal;

    /** 项目状态 */
    @ApiModelProperty(value = "项目状态")
    @TableField(exist = false)
    private String projectState;

    /** 商品类型 */
    @ApiModelProperty(value = "商品类型")
    @TableField(exist = false)
    private String goodsType;

    /** 商品编号 */
    @ApiModelProperty(value = "商品编号")
    @TableField(exist = false)
    private String code;

}
