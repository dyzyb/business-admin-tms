package com.business.system.domain.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 工作流审批结果接收DTO
 *
 * @author js
 * @since 2022-12-8
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "AcceptApprovalWorkflowDTO", description = "工作流审批结果接收DTO")
public class AcceptApprovalWorkflowDTO implements Serializable {

    private static final long serialVersionUID = -9173679187285748679L;

    @ApiModelProperty(value = "目标业务Id")
    private String targetId;

    @ApiModelProperty(value = "目标业务类型(立项申请/投标申请/合同申请/)")
    private String targetType;

    @ApiModelProperty(value = "审批结果(取消/拒绝/通过)")
    private String approvalResult;

    @ApiModelProperty(value = "审批流程实例id")
    private String processInstanceId;

    @ApiModelProperty(value = "判断是编辑审批还是变更审批")
    private String approveFlag;

}
