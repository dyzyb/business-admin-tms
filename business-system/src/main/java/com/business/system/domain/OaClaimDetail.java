package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

/**
 * 报销明细对象 oa_claim_detail
 *
 * @author single
 * @date 2023-12-11
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "报销明细对象",description = "")
@TableName("oa_claim_detail")
public class OaClaimDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 费用发生日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "费用发生日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "费用发生日期")
    private Date costTime;

    /** 报销类型 */
    @Excel(name = "报销类型", readConverterExp = "1=差旅费,2=招待费,3=办公用品采购,4=交通费,5=备用金")
    @ApiModelProperty(value = "报销类型")
    private Long type;

    /** 报销金额 */
    @Excel(name = "报销金额")
    @ApiModelProperty(value = "报销金额")
    private BigDecimal claimAmount;

    /** 报销状态 0 未报销 1 报销中 2 已报销 */
    @Excel(name = "报销状态", readConverterExp = "1=未报销,2=报销中,3=已报销")
    @ApiModelProperty(value = "报销状态 0 未报销 1 报销中 2 已报销")
    private int claimState;

    /** 附件 */
    @ApiModelProperty(value = "附件")
    private String annex;

    /** 状态 */
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 用户ID */
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 租户ID */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 报销ID */
    @ApiModelProperty(value = "报销ID")
    private String claimId;

    /** 业务类型 */
    @ApiModelProperty(value = "业务类型")
    private String businessType;

    /** 申请时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "申请时间")
    private Date applyTime;

    /** 报销金额 */
    @ApiModelProperty(value = "报销金额")
    private BigDecimal applyAmount;

    /** 业务ID */
    @ApiModelProperty(value = "业务ID")
    private String businessId;

    /** 流程实例ID */
    @ApiModelProperty(value = "流程实例ID")
    private String processInstanceId;

    /** 关联流程名称 */
    @ApiModelProperty(value = "关联流程名称")
    private String processName;

    /** 发票 */
    @ApiModelProperty(value = "发票")
    private String invoice;

    /** 备注 */
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 关联流程ID（报销实际费用申请ID） */
    @ApiModelProperty(value = "关联流程ID（报销实际费用申请ID）")
    private String relationId;
}
