package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 组织架构信息统计
 *
 * @author js
 * @date 2023-8-30
 */
@Data
@ApiModel(value="BaUserVO对象",description="组织架构信息统计")
public class BaUserSortVO
{
    /** 岗位编码 */
    @ApiModelProperty(value = "岗位编码")
    private String jobCode;

    /** 人员ID */
    @ApiModelProperty(value = "人员ID")
    private Long userId;

    /** 人员名称 */
    @ApiModelProperty(value = "人员名称")
    private String nickName;
}
