package com.business.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.annotation.Excels;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 打卡汇总对象 oa_clock_summary
 *
 * @author single
 * @date 2023-12-11
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "打卡汇总对象",description = "")
@TableName("oa_clock_summary")
public class OaClockSummary extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 用户ID */
    @ApiModelProperty(value = "用户ID")
    private String userId;

    /** 用户名称 */
    @Excel(name = "用户名称")
    @ApiModelProperty(value = "用户名称")
    private String userName;


    /** 部门ID */
    @ApiModelProperty(value = "部门ID")
    private String deptId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 考勤组 */
    @Excel(name = "考勤组")
    @ApiModelProperty(value = "考勤组")
    private String attendanceId;

    /** 考勤组名称 */
    @Excel(name = "考勤组名称")
    @ApiModelProperty(value = "考勤组名称")
    private String attendanceName;

    /** 出勤天数 */
    @Excel(name = "出勤天数")
    @ApiModelProperty(value = "出勤天数")
    private BigDecimal attendanceDays;

    /** 休息天数 */
    @Excel(name = "休息天数")
    @ApiModelProperty(value = "休息天数")
    private BigDecimal restDays;

    /** 迟到次数 */
    @Excel(name = "迟到次数")
    @ApiModelProperty(value = "迟到次数")
    private Long latenessNum;

    /** 迟到时长 */
    @Excel(name = "迟到时长")
    @ApiModelProperty(value = "迟到时长")
    private BigDecimal latenessTimes;

    /** 早退次数 */
    @Excel(name = "早退次数")
    @ApiModelProperty(value = "早退次数")
    private Long leaveEarlyNum;

    /** 早退时长 */
    @Excel(name = "早退时长")
    @ApiModelProperty(value = "早退时长")
    private BigDecimal leaveEarlyTimes;

    /** 旷工天数 */
    @Excel(name = "旷工天数")
    @ApiModelProperty(value = "旷工天数")
    private BigDecimal absenteeismNum;

    /** 出差天数 */
    @Excel(name = "出差天数")
    @ApiModelProperty(value = "出差天数")
    private BigDecimal evectionNum;

    /** 外出天数 */
    @Excel(name = "外出天数")
    @ApiModelProperty(value = "外出天数")
    private BigDecimal egressNum;

    /** 请假天数 */
    @Excel(name = "请假天数")
    @ApiModelProperty(value = "请假天数")
    private BigDecimal leaveNum;

    /** 考勤结果 */
    @Excel(name = "考勤结果")
    @ApiModelProperty(value = "考勤结果")
    private String attendanceResults;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 汇总日期 */
    @ApiModelProperty(value = "汇总日期")
    private String summaryDate;

    /**
     * 天数据
     */
    @Excel(name = "天数据",needMerge = false)
    @ApiModelProperty(value = "天数据")
    @TableField(exist = false)
    private List<OaCalendar> oaCalendars;

    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID")
    private String tenantId;



}
