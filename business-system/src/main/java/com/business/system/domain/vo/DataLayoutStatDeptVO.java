package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author: js
 * @Description: 数据呈现部门统计
 * @date: 2023/10/9 19:05
 */
@Data
@ApiModel(value="DataLayoutStatVO对象",description="数据呈现部门统计")
public class DataLayoutStatDeptVO implements Serializable {

    private static final long serialVersionUID = -5566043497952968276L;

    /** 集团累计 */
    @ApiModelProperty(value = "集团累计")
    private BigDecimal groupAmount;

    /** 部门累计(万元) */
    @ApiModelProperty(value = "部门累计")
    private BigDecimal deptAmount;
}
