package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: js
 * @Description: 审批任务历史查询对象
 * @date: 2022/12/11 13:39
 */
@Data
@ApiModel(value = "CountHistoryProcessTaskDTO对象", description = "审批任务历史查询对象")
public class CountHistoryProcessTaskDTO extends BaseDTO{

    private static final long serialVersionUID = -4662138323423776931L;

    @ApiModelProperty("受理人id")
    private String userId;

    @ApiModelProperty("过滤条件，任务类型")
    private String filterCondition;

    @ApiModelProperty("流程结束状态 processIsEnd 已结束")
    private String processIsEnd;
}
