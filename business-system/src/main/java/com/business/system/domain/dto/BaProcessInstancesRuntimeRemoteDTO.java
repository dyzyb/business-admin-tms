package com.business.system.domain.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @Author: js
 * @Description: 流程实例启动对象
 * @date: 2022/12/5 15:10
 */
@Data
public class BaProcessInstancesRuntimeRemoteDTO implements Serializable {

    private static final long serialVersionUID = -5546343145203527578L;

    /**
     * 流程定义id
     */
    private String processDefinitionKey;

    /**
     * 抄送用户id集合
     */
    private List<String> involvedUsers;

    /**
     * 一般为表单变量
     */
    private List<Map<String, Object>> variables;

    /**
     * 业务key
     */
    private String businessKey;
}
