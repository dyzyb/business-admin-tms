package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 货源中运价对象 ba_automobule_standard
 *
 * @author ljb
 * @date 2023-02-17
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "货源中运价对象",description = "")
@TableName("ba_automobule_standard")
public class BaAutomobuleStandard
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 隐藏运价 */
    @Excel(name = "隐藏运价")
    @ApiModelProperty(value = "隐藏运价")
    private Integer isShowPrice;

    /** 运费单价 */
    @Excel(name = "运费单价")
    @ApiModelProperty(value = "运费单价")
    private BigDecimal price;

    /** 路耗类型 */
    @Excel(name = "路耗类型")
    @ApiModelProperty(value = "路耗类型")
    private String luhaotype;

    /** 涨吨类型 */
    @Excel(name = "涨吨类型")
    @ApiModelProperty(value = "涨吨类型")
    private String zdtype;

    /** 气卡金额 */
    @Excel(name = "气卡金额")
    @ApiModelProperty(value = "气卡金额")
    private BigDecimal qkPrice;

    /** 结算依据 */
    @Excel(name = "结算依据")
    @ApiModelProperty(value = "结算依据")
    private String jsyjtype;

    /** 合理涨吨标准 */
    @Excel(name = "合理涨吨标准")
    @ApiModelProperty(value = "合理涨吨标准")
    private String zdbz;

    /** 合理路耗标准 */
    @Excel(name = "合理路耗标准")
    @ApiModelProperty(value = "合理路耗标准")
    private String luhaobz;

    /** 路耗扣款标准 */
    @Excel(name = "路耗扣款标准")
    @ApiModelProperty(value = "路耗扣款标准")
    private String kkbz;

    /** 涨吨扣款标准 */
    @Excel(name = "涨吨扣款标准")
    @ApiModelProperty(value = "涨吨扣款标准")
    private String zdkkbz;

    /** 关联ID */
    @Excel(name = "关联ID")
    @ApiModelProperty(value = "关联ID")
    private String ydId;

    /** 执行时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "执行时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "执行时间")
    private Date executetime;

    /** 截止时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "截止时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "截止时间")
    private Date endtime;

    /** 经济人运价 */
    @Excel(name = "经济人运价")
    @ApiModelProperty(value = "经济人运价")
    private BigDecimal agentprice;

    /** 到期期限 */
    @Excel(name = "到期期限")
    @ApiModelProperty(value = "到期期限")
    private BigDecimal dqhour;

    /** 精度 */
    @Excel(name = "精度")
    @ApiModelProperty(value = "精度")
    private String yfprecision;

    /** 精度 */
    @Excel(name = "精度")
    @ApiModelProperty(value = "精度")
    private String precisions;

    /** 信息费 */
    @Excel(name = "信息费")
    @ApiModelProperty(value = "信息费")
    private BigDecimal infocost;

    /** 状态：0-当前；1-往期。 */
    @Excel(name = "状态：0-当前；1-往期。")
    @ApiModelProperty(value = "状态：0-当前；1-往期。")
    private Long status;

    /** 路耗阶梯计划名 */
    @Excel(name = "路耗阶梯计划名")
    @ApiModelProperty(value = "路耗阶梯计划名")
    private String plankd;

    /** 涨吨阶梯计划名 */
    @Excel(name = "涨吨阶梯计划名")
    @ApiModelProperty(value = "涨吨阶梯计划名")
    private String planzd;

    /** 结算时间类型（0-装车皮重时间，1-装车毛重时间） */
    @Excel(name = "结算时间类型", readConverterExp = "0=-装车皮重时间，1-装车毛重时间")
    @ApiModelProperty(value = "结算时间类型")
    private Integer jsdatetype;

    /** 运价修改执行状态（0-立即执行  1-按生效时间执行） */
    @Excel(name = "运价修改执行状态", readConverterExp = "0=-立即执行,1=-按生效时间执行")
    @ApiModelProperty(value = "运价修改执行状态")
    private Long executingState;

    /** 货主新修改的运价 */
    @Excel(name = "货主新修改的运价")
    @ApiModelProperty(value = "货主新修改的运价")
    private BigDecimal newPrice;

    /** 经济人新修改的运价 */
    @Excel(name = "经济人新修改的运价")
    @ApiModelProperty(value = "经济人新修改的运价")
    private BigDecimal newAgentprice;

    /** 运价生效时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "运价生效时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "运价生效时间")
    private Date effectiveTime;

    /** 关联主键 */
    @Excel(name = "关联主键")
    @ApiModelProperty(value = "关联主键")
    private String relationId;




}
