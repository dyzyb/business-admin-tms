package com.business.system.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "NumsAndMoneyDTO", description = "数量及金额的统计")
public class NumsAndMoneyDTO implements Serializable {
    /**
     *统计数量
     */
    @ApiModelProperty(value = "数量")
    private int nums;

    /**
     * 统计金额
     */
    @ApiModelProperty(value = "金额")
    private BigDecimal amounts;
}
