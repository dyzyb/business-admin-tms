package com.business.system.domain;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 督办对象 oa_supervising
 *
 * @author ljb
 * @date 2023-11-20
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "督办对象",description = "")
@TableName("oa_supervising")
public class OaSupervising extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 协办内容 */
    //@Excel(name = "协办内容")
    @ApiModelProperty(value = "协办内容")
    private String content;

    /** 链接名称 */
    //@Excel(name = "链接名称")
    @ApiModelProperty(value = "链接名称")
    private String linkName;

    /** 链接地址 */
    //@Excel(name = "链接地址")
    @ApiModelProperty(value = "链接地址")
    private String linkRoute;

    /** 标记 */
    //@Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 审批流程Id */
    //@Excel(name = "审批流程Id")
    @ApiModelProperty(value = "审批流程Id")
    private String flowId;

    /** 用户ID */
    //@Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 用户名称 */
    @Excel(name = "督办申请人")
    @ApiModelProperty(value = "用户名称")
    @TableField(exist = false)
    private String userName;

    /** 部门ID */
    //@Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @Excel(name = "部门")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    private String deptName;

    /** 创建时间 */
    @Excel(name = "发起时间" ,needMerge = true,dateFormat = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    @TableField(exist = false)
    private Date creationTime;

    /** 标题 */
    @Excel(name = "标题")
    @ApiModelProperty(value = "标题")
    private String title;

    /** 主办人 */
    //@Excel(name = "主办人")
    @ApiModelProperty(value = "主办人")
    private String sponsor;

    /** 主办人名称 */
    @Excel(name = "主办人")
    @ApiModelProperty(value = "主办人名称")
    @TableField(exist = false)
    private String sponsorName;

    /** 协办人 */
    //@Excel(name = "协办人")
    @ApiModelProperty(value = "协办人")
    private String supported;

    /** 协办人名称 */
    @Excel(name = "协办人")
    @ApiModelProperty(value = "协办人名称")
    private String supportedName;

    /** 状态 */
    @Excel(name = "督办状态",readConverterExp = "0=办理中,1=已回告,2=已办结,3=已撤回")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 租户ID */
    //@Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 提交状态 */
    //@Excel(name = "提交状态")
    @ApiModelProperty(value = "提交状态")
    private String submitState;

    /** 提交时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    //@Excel(name = "提交时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "提交时间")
    private Date submitTime;

    /** 附件 */
    //@Excel(name = "附件")
    @ApiModelProperty(value = "附件")
    private String annex;

    /** 办理数 */
    //@Excel(name = "办理数")
    @ApiModelProperty(value = "办理数")
    private Integer handleNum;

    /** 办理记录 */
    @ApiModelProperty(value = "办理记录")
    @TableField(exist = false)
    private List<OaSupervisingRecord> oaSupervisingRecordList;

    /** 交办 */
    @ApiModelProperty(value = "交办ID")
    @TableField(exist = false)
    private String assignedId;

    /** 批复意见 */
    @ApiModelProperty(value = "批复意见")
    @TableField(exist = false)
    private String approvalOpinions;

    /** 所有批复意见 */
    @ApiModelProperty(value = "所有批复意见")
    @TableField(exist = false)
    private List<Map<String,String>> map;

    /** 根据年月搜索 */
    @ApiModelProperty(value = "根据年月搜索")
    @TableField(exist = false)
    private String createTimeSearch;

    /** 统计报表类型参数 */
    @ApiModelProperty(value = "统计报表类型参数")
    @TableField(exist = false)
    private String paramType;

    /** 关联流程名称 */
    //@Excel(name = "关联流程名称")
    @ApiModelProperty(value = "关联流程名称")
    private String processName;

    /** 关联流程内容 */
    //@Excel(name = "关联流程内容")
    @ApiModelProperty(value = "关联流程内容")
    private String processJson;

    /** 汇报开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    //@Excel(name = "汇报开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "汇报开始时间")
    private Date reportStart;

    /** 汇报结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    //@Excel(name = "汇报结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "汇报结束时间")
    private Date reportEnd;

    /** 协办人搜索 */
    //@Excel(name = "协办人")
    @ApiModelProperty(value = "协办人搜索")
    @TableField(exist = false)
    private String supportedIds;

    /** 主办人搜索 */
    //@Excel(name = "主办人")
    @ApiModelProperty(value = "主办人搜索")
    @TableField(exist = false)
    private String sponsorIds;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String startTime;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String endTime;

    @ApiModelProperty(value = "申请人查询")
    @TableField(exist = false)
    private String userIds;


    /** 回复周期 */
    //@Excel(name = "关联流程名称")
    @ApiModelProperty(value = "回复周期")
    private String returningCycle;


    /** 回复时间 */
    @JsonFormat(pattern = "HH:mm")
    //@Excel(name = "提交时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "回复时间")
    private Date recoveryTime;

    /** 提醒时间 */
    //@Excel(name = "关联流程名称")
    @ApiModelProperty(value = "提醒时间")
    private String reminderTime;


}
