package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 考勤组地点关联对象 oa_attendance_address
 *
 * @author single
 * @date 2023-11-24
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "考勤组地点关联对象",description = "")
@TableName("oa_attendance_address")
public class OaAttendanceAddress extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** Wi-Fi打卡名称 */
    @Excel(name = "Wi-Fi打卡名称")
    @ApiModelProperty(value = "Wi-Fi打卡名称")
    private String name;

    /** 纬度 */
    @Excel(name = "纬度")
    @ApiModelProperty(value = "纬度")
    private String latitude;

    /** 经度 */
    @Excel(name = "经度")
    @ApiModelProperty(value = "经度")
    private String longitude;

    /** 地点 */
    @Excel(name = "地点")
    @ApiModelProperty(value = "地点")
    private String address;

    /** 有效范围 */
    @Excel(name = "有效范围")
    @ApiModelProperty(value = "有效范围")
    private Long scope;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 考勤组ID */
    @Excel(name = "考勤组ID")
    @ApiModelProperty(value = "考勤组ID")
    private String relationId;



}
