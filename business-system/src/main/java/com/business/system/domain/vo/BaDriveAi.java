package com.business.system.domain.vo;

import com.business.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 立项管理对象 ba_project
 *
 * @author ljb
 * @date 2022-12-02
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class BaDriveAi extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String businesstype;
    private Double  typenum;


}
