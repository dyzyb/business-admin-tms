package com.business.system.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
@Data
public class DepotHeadDTO {
        private static final long serialVersionUID = 1L;

        /** 主键 */
        private String id;

        /** 类型（出库/入库） */
        @Excel(name = "类型", readConverterExp = "出=库/入库")
        @ApiModelProperty(value = "类型")
        private Long type;

        /** 出入库分类 */
        @Excel(name = "出入库分类")
        @ApiModelProperty(value = "出入库分类")
        private String subType;

        /** 初始票据号 */
        @Excel(name = "初始票据号")
        @ApiModelProperty(value = "初始票据号")
        private String defaultNumber;

        /** 票据号 */
        @Excel(name = "票据号")
        @ApiModelProperty(value = "票据号")
        private String number;

        /** 出入库时间 */
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        @Excel(name = "出入库时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
        @ApiModelProperty(value = "出入库时间")
        private Date operTime;

        /** 供应商 */
        @Excel(name = "供应商")
        @ApiModelProperty(value = "供应商")
        private String supplierId;

        @TableField(exist = false)
        @ApiModelProperty(value = "供应商名称")
        private String supplierName;

        /** 采购订单编号 */
        @Excel(name = "采购订单编号")
        @ApiModelProperty(value = "采购订单编号")
        private String orderId;

        @TableField(exist = false)
        @ApiModelProperty(value = "订单名称")
        private String orderName;

        /** 批次ID */
        @Excel(name = "批次ID")
        @ApiModelProperty(value = "批次ID")
        private String batch;

        @TableField(exist = false)
        @ApiModelProperty(value = "批次号")
        private String batchName;

        /** 商品id */
        @Excel(name = "商品id")
        @ApiModelProperty(value = "商品id")
        private String materialId;

        @TableField(exist = false)
        @ApiModelProperty(value = "商品名称")
        private String materialName;

        @TableField(exist = false)
        @ApiModelProperty(value = "商品编码")
        private String materialCode;

        /** 仓库id */
        @Excel(name = "仓库id")
        @ApiModelProperty(value = "仓库id")
        private String depotId;

        @TableField(exist = false)
        @ApiModelProperty(value = "仓库名称")
        private String depotName;

        /** 附件名称 */
        @Excel(name = "附件名称")
        @ApiModelProperty(value = "附件名称")
        private String fileName;

        /** 状态 */
        @Excel(name = "状态")
        @ApiModelProperty(value = "状态")
        private String status;

        /** 关联加工出库 */
        @Excel(name = "关联加工出库")
        @ApiModelProperty(value = "关联加工出库")
        private String linkNumber;

        /** 加工类型 */
        @Excel(name = "加工类型")
        @ApiModelProperty(value = "加工类型")
        private String machiningType;

        /** 损耗阈值 */
        @Excel(name = "损耗阈值")
        @ApiModelProperty(value = "损耗阈值")
        private String threshold;

        /** 预入库日期 */
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        @Excel(name = "预入库日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
        @ApiModelProperty(value = "预入库日期")
        private Date time;

        /** 条形码(多个条形码) */
        @Excel(name = "条形码(多个条形码)")
        @ApiModelProperty(value = "条形码(多个条形码)")
        private String barCode;

        /** 状态 */
        @Excel(name = "状态")
        @ApiModelProperty(value = "状态")
        private String state;

        /** 标记 */
        @Excel(name = "标记")
        @ApiModelProperty(value = "标记")
        private Long flag;

        /** 类型 */
        @Excel(name = "类型")
        @ApiModelProperty(value = "类型")
        private String types;


        /** 用户ID */
        @Excel(name = "用户ID")
        @ApiModelProperty(value = "用户ID")
        private Long userId;


        /** 部门ID */
        @Excel(name = "部门ID")
        @ApiModelProperty(value = "部门ID")
        private Long deptId;


        /** 出入库业务状态 */
        @Excel(name = "出入库业务状态")
        @ApiModelProperty(value = "出入库业务状态")
        private String headStatus;

        /** 总量 **/
        @Excel(name = "总量")
        @ApiModelProperty(value = "总量")
        private BigDecimal totals;

        /**
         * 出库总量
         */
        @ApiModelProperty(value = "出库总量")
        private BigDecimal outputAll;

        /**
         * 入库总量
         */
        @ApiModelProperty(value = "入库总量")
        private BigDecimal intoAll;

        /**
         * 损耗情况
         */
        private String losses;

        /**
         * 计划入库时间
         */
        private String planTime;
        /**
         * 入库时间
         */
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        @Excel(name = "入库时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
        private Date intoTime;
        /**
         * 入库期限
         */
        private String intoTerm;
        /**
         * 入库单
         */
        private String intoId;

        /**
         * 切换
         */
        private String logo;
        /**
         * 当前日期
         */
        private String currentFlag;

        /** 项目ID */
        @Excel(name = "项目ID")
        @ApiModelProperty(value = "项目ID")
        private String projectId;

        /** 库位ID */
        @Excel(name = "库位ID")
        @ApiModelProperty(value = "库位ID")
        private String locationId;

        /** 商品ID */
        @Excel(name = "商品ID")
        @ApiModelProperty(value = "商品ID")
        private String goodsId;

        /** 租户ID */
        @ApiModelProperty(value = "租户ID")
        private String tenantId;
}

