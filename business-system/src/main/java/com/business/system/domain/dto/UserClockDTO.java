package com.business.system.domain.dto;

import com.business.system.domain.OaClock;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "UserClockDTO", description = "用户打卡信息")
public class UserClockDTO {

    /**
     * 用户名称
     */
    @ApiModelProperty(value = "用户名称")
    private String userName;

    /**
     * 用户名称
     */
    @ApiModelProperty(value = "用户名称")
    private Long userId;

    /**
     * 用户部门
     */
    @ApiModelProperty(value = "用户部门")
    private String deptName;

    /**
     * 总天数
     */
    @ApiModelProperty(value = "总天数")
    private Long totalDays;

    /**
     * 打卡信息
     */
    @ApiModelProperty(value = "打卡信息")
    private List<OaClock> clocks;

    /**
     * 正常
     */
    @ApiModelProperty(value = "正常")
    private Long normal;
    /**
     * 迟到
     */
    @ApiModelProperty(value = "迟到")
    private Long beLate;

    /**
     * 早退
     */
    @ApiModelProperty(value = "早退")
    private Long leaveEarly;

    /**
     * 缺卡
     */
    @ApiModelProperty(value = "缺卡")
    private Long lackCards;

    /**
     * 外勤
     */
    @ApiModelProperty(value = "外勤")
    private Long fieldPersonnel;

    /**
     * 旷工
     */
    @ApiModelProperty(value = "旷工")
    private Long absenteeism;
}
