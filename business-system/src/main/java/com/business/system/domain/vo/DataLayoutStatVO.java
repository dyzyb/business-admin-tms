package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author: js
 * @Description: 数据呈现统计
 * @date: 2023/10/9 19:05
 */
@Data
@ApiModel(value="DataLayoutStatVO对象",description="数据呈现统计")
public class DataLayoutStatVO implements Serializable {

    private static final long serialVersionUID = -5566043497952968276L;

    /** 部门 */
    @ApiModelProperty(value = "部门")
    private String deptId;

    /** 部门名称 */
    @ApiModelProperty(value = "部门名称")
    private String deptName;

    /** 累计合同额(万元) */
    @ApiModelProperty(value = "累计合同额(万元)")
    private BigDecimal contractAmount;

    /** 累计交易额(万元) */
    @ApiModelProperty(value = "累计交易额(万元)")
    private BigDecimal tradeAmount;

    /** 累计发运量(万元) */
    @ApiModelProperty(value = "累计发运量(万元)")
    private BigDecimal checkCoalNum;

    /** 累计利润(亿元) */
    @ApiModelProperty(value = "累计利润(亿元)")
    private BigDecimal profitAccumulate;

    /** 项目个数 */
    @ApiModelProperty(value = "项目个数")
    private Integer projectNum;
}
