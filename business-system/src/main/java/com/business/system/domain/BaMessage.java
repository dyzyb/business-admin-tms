package com.business.system.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;
import com.business.common.core.domain.entity.SysUser;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 系统消息记录对象 ba_message
 *
 * @author single
 * @date 2023-01-10
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "系统消息记录对象",description = "")
@TableName("ba_message")
public class BaMessage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 发送消息ID */
    private String msId;

    /** 业务id */
    @Excel(name = "业务id")
    @ApiModelProperty(value = "业务id")
    private String businessId;

    /** 业务类型 */
    @Excel(name = "业务类型")
    @ApiModelProperty(value = "业务类型")
    private String businessType;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private String mId;

    @ApiModelProperty(value = "用户ID")
    @TableField(exist = false)
    private SysUser user;

    /** 消息标题 */
    @Excel(name = "消息标题")
    @ApiModelProperty(value = "消息标题")
    private String messTitle;

    /** 消息内容 */
    @Excel(name = "消息内容")
    @ApiModelProperty(value = "消息内容")
    private String messContent;

    /** 发送时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "发送时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "发送时间")
    private Date sendTime;

    /** 发送人 */
    @Excel(name = "发送人")
    @ApiModelProperty(value = "发送人")
    private String sendBy;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 消息类型（1：审批提醒，2：评论提醒, 5：督办逾期， 11：预立项提醒） */
    @Excel(name = "消息类型", readConverterExp = "1：审批提醒，2：评论提醒")
    @ApiModelProperty(value = "消息类型")
    private String type;

    /** 状态（1：已读，2：未读） */
    @Excel(name = "状态", readConverterExp = "1=：已读，2：未读")
    @ApiModelProperty(value = "状态")
    private String states;

    /** 弹框状态(默认：0) */
    @Excel(name = "弹框状态(默认：0)")
    @ApiModelProperty(value = "弹框状态(默认：0)")
    private Long boxState;

    /** 删除标识(默认：0) */
    @Excel(name = "删除标识(默认：0)")
    @ApiModelProperty(value = "删除标识(默认：0)")
    private Long flag;

    /** 业务类型 **/
    @TableField(exist = false)
    private String companyType;

    /** 公司类型 **/
    @TableField(exist = false)
    private String corpType;

    /** 流程实例ID */
    @Excel(name = "流程实例ID")
    @ApiModelProperty(value = "流程实例ID")
    private String processInstanceId;

    /** 时间查询 */
    @ApiModelProperty(value = "流程实例ID")
    @TableField(exist = false)
    private String queryTime;


    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 多类型查询 */
    @ApiModelProperty(value = "消息类型")
    @TableField(exist = false)
    private String types;
}
