package com.business.system.domain.vo;

import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * 组织结构
 *
 * @author : js
 * @version : 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HomePageVo {


    private String name;

    //累计合同额
    private BigDecimal contractAmount ;
    //累计贸易额
    private BigDecimal tradeAmount ;
    //累计运输量
    private BigDecimal checkCoalNum ;
}
