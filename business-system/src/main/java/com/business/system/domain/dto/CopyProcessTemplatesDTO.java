package com.business.system.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 复制流程模板对象
 *
 * @version 1.0
 * @Author: js
 * @Date: 2022/11/29 16:50
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "CopyProcessTemplatesDTO对象", description = "复制流程模板对象")
public class CopyProcessTemplatesDTO implements Serializable {

    private static final long serialVersionUID = -8322142310898270407L;

    @ApiModelProperty("租户ID")
    private String tenantId;

    @ApiModelProperty("新租户ID")
    @TableField(exist = false)
    private String newTenantId;

}
