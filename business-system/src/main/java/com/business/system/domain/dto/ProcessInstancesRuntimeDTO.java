package com.business.system.domain.dto;

import com.business.system.domain.BaProcessInstanceRelated;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @Author: js
 * @Description: 流程实例启动对象
 * @date: 2022/12/3 20:52
 */
@Data
public class ProcessInstancesRuntimeDTO implements Serializable {

    private static final long serialVersionUID = 7981894882716424256L;

    /**
     * 流程定义id
     */
    private String processDefinitionKey;

    /**
     * 抄送用户id集合
     */
    private List<String> involvedUsers;

    /**
     * 一般为表单变量
     */
    private Map<String, Object> variables;

    /**
     * 业务数据
     */
    private BaProcessInstanceRelated baProcessInstanceRelated;

    /**
     * 审批人变量
     */
    private Map<String, Object> assigneeVariables;

    @ApiModelProperty("租户ID")
    private String tenantId;
}
