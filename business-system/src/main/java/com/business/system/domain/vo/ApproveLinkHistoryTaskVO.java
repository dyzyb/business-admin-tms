package com.business.system.domain.vo;

import com.business.system.domain.dto.json.UserInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: js
 * @Description: 审批链路任务对象
 * @date: 2022/12/13 13:45
 */
@Data
@ApiModel(value="ApproveLinkHistoryTaskVO对象",description="审批链路任务对象")
public class ApproveLinkHistoryTaskVO implements Serializable {

    private static final long serialVersionUID = -1242120448842116701L;

    @ApiModelProperty("任务ID")
    private String id;

    @ApiModelProperty("任务处理人")
    private String assignee;

    @ApiModelProperty("任务处理人姓名")
    private String assigneeName;

    @ApiModelProperty("任务名称")
    private String name;

    @ApiModelProperty("任务创建时间")
    private String createTime;

    @ApiModelProperty("流程节点")
    private String taskDefinitionKey;

    @ApiModelProperty("流程节点名称")
    private String taskDefinitionName;

    @ApiModelProperty("流程实例id")
    private String processInstanceId;

    @ApiModelProperty("流程定义id")
    private String processDefinitionId;

    @ApiModelProperty("流程定义名称")
    private String processDefinitionName;

    @ApiModelProperty("发起人")
    private UserInfo startUser;

    @ApiModelProperty("审批状态")
    private String businessStatus;

    @ApiModelProperty("审批原因")
    private String reason;

    @ApiModelProperty("时间差")
    private String timeDifference;

    @ApiModelProperty("会签标识")
    private String countersign;

    @ApiModelProperty("审批原因")
    private String userReason;

    @ApiModelProperty("任务处理人")
    private String userAssigneeSort;

    @ApiModelProperty("审批原因填写人")
    private String reasonUser;
}
