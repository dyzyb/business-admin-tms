package com.business.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author: js
 * @Description: 流程实例启动展示对象
 * @date: 2022/12/1 17:09
 */
@Data
@ApiModel(value="BaProcessInstancesRuntimeVO对象",description="流程实例启动展示对象")
public class BaProcessInstancesRuntimeVO implements Serializable {

    private static final long serialVersionUID = -6632459212813554205L;

    @ApiModelProperty("流程实例ID")
    private String id;

    @ApiModelProperty("流程实例名称")
    private String name;

    @ApiModelProperty("业务key")
    private String businessKey;

    @ApiModelProperty("是否已结束")
    private String ended;

    @ApiModelProperty("流程定义id")
    private String processDefinitionId;

    @ApiModelProperty("流程定义名称")
    private String processDefinitionName;

    @ApiModelProperty("流程定义描述")
    private String processDefinitionDescription;

    @ApiModelProperty("流程启动用户id")
    private String startUserId;

    @ApiModelProperty("流程启动时间")
    private String startTime;

    @ApiModelProperty("租户ID")
    private String tenantId;

    @ApiModelProperty("审核类型")
    private String approveType;
}
