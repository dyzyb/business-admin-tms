package com.business.system.domain.vo;

import com.business.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 进行中的业务
 *
 * @author js
 * @date 2023-8-30
 */
@Data
@ApiModel(value="BaOngoingContractStartVO对象",description="进行中的业务")
public class BaOngoingContractStartVO
{

    /** 名称 */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;

    /** 已到货 */
    @ApiModelProperty(value = "已到货")
    private BigDecimal arrived;

    /**
     * 认领收款
     */
    @ApiModelProperty(value = "认领收款")
    private BigDecimal applyCollectionAmount;

    /** 启动日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "启动日期")
    private Date startCreateTime;

    /** 阶段类型 **/
    @ApiModelProperty(value = "阶段类型")
    private String startType;

    /** 事业部名称 **/
    @ApiModelProperty(value = "事业部名称")
    private String deptName;
}
