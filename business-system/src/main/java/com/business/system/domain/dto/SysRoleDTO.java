package com.business.system.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @version 1.0
 * @Author: js
 * @Description: 角色
 * @Date: 2022/11/29 10:02
 */
@Data
@ApiModel(value = "SysRoleDTO对象", description = "角色对象")
public class SysRoleDTO implements Serializable {

    private static final long serialVersionUID = 2881899032626267999L;

    @ApiModelProperty("类型")
    private String type;
}
