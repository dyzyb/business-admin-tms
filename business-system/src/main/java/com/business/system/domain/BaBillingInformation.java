package com.business.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.business.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 开票信息对象 ba_billing_information
 *
 * @author single
 * @date 2023-02-28
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "开票信息对象",description = "")
@TableName("ba_billing_information")
public class BaBillingInformation
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 名称 */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;

    /** 税号 */
    @Excel(name = "税号")
    @ApiModelProperty(value = "税号")
    private String dutyParagraph;

    /** 单位地址 */
    @Excel(name = "单位地址")
    @ApiModelProperty(value = "单位地址")
    private String workAddress;

    /** 开户行 */
    @Excel(name = "开户行")
    @ApiModelProperty(value = "开户行")
    private String bankName;

    /** 银行账号 */
    @Excel(name = "银行账号")
    @ApiModelProperty(value = "银行账号")
    private String account;

    /** 电话 */
    @Excel(name = "电话")
    @ApiModelProperty(value = "电话")
    private String phone;

    /** 关联ID */
    @Excel(name = "关联ID")
    @ApiModelProperty(value = "关联ID")
    private String relevanceId;

    /** 收件人姓名 */
    @Excel(name = "收件人姓名")
    @ApiModelProperty(value = "收件人姓名")
    private String recipientName;

    /** 联系电话 */
    @Excel(name = "联系电话")
    @ApiModelProperty(value = "联系电话")
    private String telephone;

    /** 省 */
    @Excel(name = "省")
    @ApiModelProperty(value = "省")
    private String province;

    /** 市 */
    @Excel(name = "市")
    @ApiModelProperty(value = "市")
    private String city;

    /** 区 */
    @Excel(name = "区")
    @ApiModelProperty(value = "区")
    private String area;

    /** 详细地址 */
    @Excel(name = "详细地址")
    @ApiModelProperty(value = "详细地址")
    private String detailedAddress;

    /** 邮编 */
    @Excel(name = "邮编")
    @ApiModelProperty(value = "邮编")
    private String zipCode;



}
