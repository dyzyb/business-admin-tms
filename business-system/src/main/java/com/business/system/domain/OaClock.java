package com.business.system.domain;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.business.common.annotation.Excel;
import com.business.common.core.domain.BaseEntity;

/**
 * 打卡对象 oa_clock
 *
 * @author single
 * @date 2023-11-27
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
@ApiModel(value = "打卡对象",description = "")
@TableName("oa_clock")
public class OaClock extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 员工ID */
    @Excel(name = "员工ID")
    @ApiModelProperty(value = "员工ID")
    private Long employeeId;

    /** 员工ID */
    @ApiModelProperty(value = "员工ID")
    @TableField(exist = false)
    private String employeeName;

    /** 经度 */
    @Excel(name = "经度")
    @ApiModelProperty(value = "经度")
    private String longitude;

    /** 纬度 */
    @Excel(name = "纬度")
    @ApiModelProperty(value = "纬度")
    private String latitude;

    /** 地址 */
    @Excel(name = "地址")
    @ApiModelProperty(value = "地址")
    private String address;

    /** 考勤类型 1 内勤 2 外勤 */
    @Excel(name = "考勤类型 1 内勤 2 外勤")
    @ApiModelProperty(value = "考勤类型 1 内勤 2 外勤")
    private String type;

    /** 打卡类型 1 上班打卡 2 下班打卡 */
    @Excel(name = "打卡类型 1 上班打卡 2 下班打卡")
    @ApiModelProperty(value = "打卡类型 1 上班打卡 2 下班打卡")
    private String clockType;

    /** 打卡日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "打卡日期", width = 30, dateFormat = "yyyy-MM-dd")
    @ApiModelProperty(value = "打卡日期")
    private Date clockDate;

    /** 打卡时间 */
    @JsonFormat(pattern = "HH:mm:ss")
    @Excel(name = "打卡时间", width = 30, dateFormat = "HH:mm:ss")
    @ApiModelProperty(value = "打卡时间")
    private Date clockTime;

    /** 标记 */
    @Excel(name = "标记")
    @ApiModelProperty(value = "标记")
    private Long flag;

    /** 状态 */
    @Excel(name = "状态")
    @ApiModelProperty(value = "状态")
    private String state;

    /** 用户ID */
    @Excel(name = "用户ID")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /** 部门ID */
    @Excel(name = "部门ID")
    @ApiModelProperty(value = "部门ID")
    private Long deptId;

    /** 部门名称 */
    @ApiModelProperty(value = "部门名称")
    private String deptName;

    /** 流程审批ID */
    @Excel(name = "流程审批ID")
    @ApiModelProperty(value = "流程审批ID")
    private String flowId;

    /** 租户ID */
    @Excel(name = "租户ID")
    @ApiModelProperty(value = "租户ID")
    private String tenantId;

    /** 打卡状态（1：正常  2：迟到  3：早退） */
    @Excel(name = "打卡状态", readConverterExp = "1=：正常,2=：迟到,3=：早退,4=:请假,5=休息,6=严重迟到,7=迟到旷工,8=早退旷工,9=出差,10=外出,11=旷工,12=外勤")
    @ApiModelProperty(value = "打卡状态 1=：正常,2=：迟到,3=：早退")
    private String clockState;

    /** 备注 */
    @Excel(name = "备注")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /** 标记 1 打卡 2 补卡 */
    @Excel(name = "标记 1 打卡 2 补卡")
    @ApiModelProperty(value = "标记 1 打卡 2 补卡")
    private Long clockFlag;

    /** 初始时间 */
    @JsonFormat(pattern = "HH:mm")
    @Excel(name = "初始时间", width = 30, dateFormat = "HH:mm")
    @ApiModelProperty(value = "初始时间")
    private Date initialTime;

    /** 考勤组考勤类型 */
    @Excel(name = "考勤组考勤类型")
    @ApiModelProperty(value = "考勤组考勤类型")
    private String attendanceType;


    /** 考勤开始时间 */
    @Excel(name = "考勤开始时间")
    @ApiModelProperty(value = "考勤开始时间")
    private String attendanceStart;

    /** 工作时长 */
    @Excel(name = "工作时长")
    @ApiModelProperty(value = "工作时长")
    private Long workHours;

    /** 查询时间 */
    @ApiModelProperty(value = "查询时间")
    @TableField(exist = false)
    private String queryTime;

    /** 上班时间 */
    @ApiModelProperty(value = "上班时间")
    @TableField(exist = false)
    private String officeHours;

    /** 请假数据 */
    @ApiModelProperty(value = "请假数据")
    @TableField(exist = false)
    private List<OaLeave> leaveList;

    /** 最晚打卡时间 */
    @JsonFormat(pattern = "HH:mm")
    @Excel(name = "最晚打卡时间", width = 30, dateFormat = "HH:mm")
    @ApiModelProperty(value = "最晚打卡时间")
    private Date endClock;


    /** 是否休假  1:上班  0：休假*/
    @ApiModelProperty(value = "是否休假")
    @TableField(exist = false)
    private String holiday;

    /** 严重迟到分钟数 */
    @Excel(name = "严重迟到分钟数")
    @ApiModelProperty(value = "严重迟到分钟数")
    private Long severelyLate;

    /** 允许迟到分钟数 */
    @Excel(name = "允许迟到分钟数")
    @ApiModelProperty(value = "允许迟到分钟数")
    private Long allowedLate;

    /** 迟到几分钟算矿工 */
    @Excel(name = "迟到几分钟算矿工")
    @ApiModelProperty(value = "迟到几分钟算矿工")
    private Long absenteeismLate;

    /** 早退几分钟算矿工 */
    @Excel(name = "早退几分钟算矿工")
    @ApiModelProperty(value = "早退几分钟算矿工")
    private Long absenteeismEarly;

    /**
     * 时长
     */
    @Excel(name = "时长")
    @ApiModelProperty(value = "时长")
    private Long duration;


    /** 本月数据 */
    @ApiModelProperty(value = "本月数据")
    @TableField(exist = false)
    private String thisMonth;

    /** 实际打卡 */
    @ApiModelProperty(value = "实际打卡")
    @TableField(exist = false)
    private String reality;

    /** 迟到  clock_state 2:迟到  6:严重迟到*/
    @ApiModelProperty(value = "迟到")
    @TableField(exist = false)
    private String beLate;

    /** 是否分组 */
    @ApiModelProperty(value = "是否分组")
    @TableField(exist = false)
    private String group;

    /** 日期拼星期 **/
    @ApiModelProperty(value = "日期拼星期")
    @TableField(exist = false)
    private String dateWeek;

    /**
     * 周时间区间
     */
    @ApiModelProperty(value = "周时间区间")
    @TableField(exist = false)
    private String weekTime;

    /** 开始时间 */
    @ApiModelProperty(value = "开始时间")
    @TableField(exist = false)
    private String start;

    /** 结束时间 */
    @ApiModelProperty(value = "结束时间")
    @TableField(exist = false)
    private String end;

    /** 用户Id */
    @ApiModelProperty(value = "用户Id")
    @TableField(exist = false)
    private String userIds;
}
