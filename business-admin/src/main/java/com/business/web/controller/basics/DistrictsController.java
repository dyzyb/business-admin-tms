package com.business.web.controller.basics;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.Districts;
import com.business.system.service.IDistrictsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 地区Controller
 *
 * @author ljb
 * @date 2022-11-28
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/districts")
@Api(tags ="地区接口")
public class DistrictsController extends BaseController
{
    @Autowired
    private IDistrictsService districtsService;

    /**
     * 查询地区列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:districts:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询地区列表接口")
    public TableDataInfo list(Districts districts)
    {
        startPage();
        List<Districts> list = districtsService.selectDistrictsList(districts);
        return getDataTable(list);
    }

    /**
     * 查询所有区域信息(树状结构)
     */
    @GetMapping("/allList")
    @ApiOperation(value = "查询所有区域信息(树状结构)")
    public AjaxResult allList(){

        return AjaxResult.success(districtsService.getAllList());
    }

    /**
     * 导出地区列表
     */
    @PreAuthorize("@ss.hasPermi('admin:districts:export')")
    //@Log(title = "地区", businessType = BusinessType.EXPORT)
    //@PostMapping("/export")
    @ApiOperation(value = "导出地区列表接口")
    public void export(HttpServletResponse response, Districts districts) throws IOException
    {
        List<Districts> list = districtsService.selectDistrictsList(districts);
        ExcelUtil<Districts> util = new ExcelUtil<Districts>(Districts.class);
        util.exportExcel(response, list, "districts");
    }

    /**
     * 获取地区详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:districts:query')")
    //@GetMapping(value = "/{id}")
    @ApiOperation(value = "获取地区详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(districtsService.selectDistrictsById(id));
    }

    /**
     * 新增地区
     */
    @PreAuthorize("@ss.hasPermi('admin:districts:add')")
    @Log(title = "地区", businessType = BusinessType.INSERT)
    //@PostMapping
    @ApiOperation(value = "新增地区接口")
    public AjaxResult add(@RequestBody Districts districts)
    {
        return toAjax(districtsService.insertDistricts(districts));
    }

    /**
     * 修改地区
     */
    @PreAuthorize("@ss.hasPermi('admin:districts:edit')")
    @Log(title = "地区", businessType = BusinessType.UPDATE)
    //@PutMapping
    @ApiOperation(value = "修改地区接口")
    public AjaxResult edit(@RequestBody Districts districts)
    {
        return toAjax(districtsService.updateDistricts(districts));
    }

    /**
     * 删除地区
     */
    @PreAuthorize("@ss.hasPermi('admin:districts:remove')")
    @Log(title = "地区", businessType = BusinessType.DELETE)
	//@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除地区接口")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(districtsService.deleteDistrictsByIds(ids));
    }
}
