package com.business.web.controller.basics;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaCounterpart;
import com.business.system.service.IBaCounterpartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 对接人Controller
 *
 * @author ljb
 * @date 2022-11-29
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/counterpart")
@Api(tags ="对接人接口")
public class BaCounterpartController extends BaseController
{
    @Autowired
    private IBaCounterpartService baCounterpartService;

    /**
     * 查询对接人列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:counterpart:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询对接人列表接口")
    public TableDataInfo list(BaCounterpart baCounterpart)
    {
        //租户ID
        baCounterpart.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaCounterpart> list = baCounterpartService.selectBaCounterpartList(baCounterpart);
        return getDataTable(list);
    }

    /**
     * 导出对接人列表
     */
    @PreAuthorize("@ss.hasPermi('admin:counterpart:export')")
    @Log(title = "对接人", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出对接人列表接口")
    public void export(HttpServletResponse response, BaCounterpart baCounterpart) throws IOException
    {
        //租户ID
        baCounterpart.setTenantId(SecurityUtils.getCurrComId());
        List<BaCounterpart> list = baCounterpartService.selectBaCounterpartList(baCounterpart);
        ExcelUtil<BaCounterpart> util = new ExcelUtil<BaCounterpart>(BaCounterpart.class);
        util.exportExcel(response, list, "counterpart");
    }

    /**
     * 获取对接人详细信息
     */
    @PreAuthorize("@ss.hasPermi('admin:counterpart:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取对接人详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baCounterpartService.selectBaCounterpartById(id));
    }

    /**
     * 新增对接人
     */
    @PreAuthorize("@ss.hasPermi('admin:counterpart:add')")
    @Log(title = "对接人", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增对接人接口")
    public AjaxResult add(@RequestBody BaCounterpart baCounterpart)
    {
        return toAjax(baCounterpartService.insertBaCounterpart(baCounterpart));
    }

    /**
     * 修改对接人
     */
    @PreAuthorize("@ss.hasPermi('admin:counterpart:edit')")
    @Log(title = "对接人", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改对接人接口")
    public AjaxResult edit(@RequestBody BaCounterpart baCounterpart)
    {
        return toAjax(baCounterpartService.updateBaCounterpart(baCounterpart));
    }

    /**
     * 删除对接人
     */
    @PreAuthorize("@ss.hasPermi('admin:counterpart:remove')")
    @Log(title = "对接人", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除对接人接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baCounterpartService.deleteBaCounterpartByIds(ids));
    }

    /**
     * 对接人审批撤销
     */
    @PreAuthorize("@ss.hasPermi('admin:counterpart:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id)
    {
        Integer result = baCounterpartService.revoke(id);
        if(result == 0){
            return AjaxResult.error("撤销流程失败");
        }
        return toAjax(result);
    }
}
