package com.business.web.controller.task;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.utils.DateUtils;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.common.utils.bean.BeanUtils;
import com.business.system.domain.*;
import com.business.system.domain.dto.SysUserByRoleIdDTO;
import com.business.system.domain.dto.SysUserDepDTO;
import com.business.system.mapper.*;
import com.business.system.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 定时任务调度测试
 *
 * @author ruoyi
 */
@Component("ryTask")
public class RyTask
{
    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private BaMessageMapper baMessageMapper;

    @Autowired
    private GetRedisIncreID getRedisIncreID;

    @Autowired
    private OaHolidayMapper oaHolidayMapper;

    @Autowired
    private OaAttendanceGroupMapper oaAttendanceGroupMapper;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private OaWeekdayMapper oaWeekdayMapper;

    @Autowired
    private OaWorkTimesMapper oaWorkTimesMapper;

    @Autowired
    private OaClockMapper oaClockMapper;

    @Autowired
    private OaEvectionMapper oaEvectionMapper;

    @Autowired
    private OaEvectionTripMapper oaEvectionTripMapper;

    @Autowired
    private OaLeaveMapper oaLeaveMapper;

    @Autowired
    private OaEgressMapper oaEgressMapper;

    @Autowired
    private OaClockSummaryMapper oaClockSummaryMapper;

    @Autowired
    private OaCalendarMapper oaCalendarMapper;

    @Autowired
    private IOaClockSummaryService  oaClockSummaryService;

    @Autowired
    private IOaAssignedService oaAssignedService;

    @Autowired
    private IOaSupervisingService oaSupervisingService;

    @Autowired
    private OaSupervisingMapper oaSupervisingMapper;


    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i)
    {
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void ryParams(String params)
    {
        //日期格式化
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String queryTime = df.format(new Date());

        // 获取当前日期
        Calendar calendar1 = Calendar.getInstance();
        // 将日期减1
        calendar1.add(Calendar.DAY_OF_YEAR, -1);
        String previousDay = df.format(calendar1.getTime());

        System.out.println(params+"-------------");
        //分钟定时任务
        if(params.equals("min")){
            try {
                //获取当前是周几
                String week = DateUtils.dateToWeek(queryTime);
                //获取所有的交办数据
                OaAssigned oaAssigned = new OaAssigned();
                oaAssigned.setState("0");
                oaAssigned.setType("1");
                List<OaAssigned> oaAssigneds = oaAssignedService.selectOaAssignedList(oaAssigned);
                for (OaAssigned assigned:oaAssigneds) {
                    //查看督办数据
                    OaSupervising supervising = oaSupervisingService.selectOaSupervisingById(assigned.getSupervisingId());
                    //判断是否存在回复周期
                    if(StringUtils.isNotEmpty(supervising.getReturningCycle())){
                        String[] split = supervising.getReturningCycle().split(",");
                        for (String s:split) {
                            int hour = calendar1.get(Calendar.HOUR_OF_DAY);
                            int minute = calendar1.get(Calendar.MINUTE);
                            String time = "";
                            if(10 > minute){
                                time = hour+":"+"0"+minute;
                            }else {
                                //当前时间
                                time = hour+":"+minute;
                            }
                            SimpleDateFormat recoveryTime = new SimpleDateFormat("HH:mm");
                            String format = recoveryTime.format(supervising.getRecoveryTime());
                            //当前时间等于回复时间执行
                            if(s.equals(week) && time.equals(format)){
                                //更新逾期时间
                                supervising.setReminderTime(queryTime+" "+time);
                                oaSupervisingMapper.updateOaSupervising(supervising);
                                //推送消息通知交办逾期
                                this.supervisingMessage(assigned.getTransactors(),supervising.getContent(),"5",assigned.getId(),"督办到期提醒");
                            }
                        }
                    }
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
            System.out.println(params+"每分钟执行一次");
        }
        //小时定时任务
        if(params.equals("hour")){
            //获取当前是周几
            try {
                //String week = DateUtils.dateToWeek(queryTime);
                //获取所有的交办数据
                OaAssigned oaAssigned = new OaAssigned();
                oaAssigned.setState("0");
                oaAssigned.setType("1");
                List<OaAssigned> oaAssigneds = oaAssignedService.selectOaAssignedList(oaAssigned);
                for (OaAssigned assigned:oaAssigneds) {
                    //查看督办数据
                    OaSupervising supervising = oaSupervisingService.selectOaSupervisingById(assigned.getSupervisingId());
                   /* String[] split = supervising.getReturningCycle().split(",");
                    for (String s:split) {
                        //当前时间时
                        int hour = calendar1.get(Calendar.HOUR_OF_DAY);
                        //回复时间时
                        SimpleDateFormat recoveryTime = new SimpleDateFormat("HH");
                        String format = recoveryTime.format(supervising.getRecoveryTime());
                        //当前时间等于回复时间执行
                        if(s.equals(week) && hour > Integer.valueOf(format)){
                            //推送消息通知交办逾期
                            this.supervisingMessage(assigned.getTransactors(),supervising.getContent(),"5",assigned.getId(),"督办逾期提醒");
                        }
                    }*/
                    if(StringUtils.isNotEmpty(supervising.getReminderTime())){
                        SimpleDateFormat recoveryTime = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        Date parse = recoveryTime.parse(supervising.getReminderTime());
                        int i = parse.compareTo(new Date());
                        //提醒时间早于当前时间提醒
                        if(i < 0){
                            //推送消息通知交办逾期
                            this.supervisingMessage(assigned.getTransactors(),supervising.getContent(),"5",assigned.getId(),"督办逾期提醒");
                        }

                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            System.out.println(params+"每小时执行一次");
        }
        //合同管理状态修改
        if(params.equals("ry")){

        }
       //每天指定时间8点50执行
        if(params.equals("clock")){

        }
        //每天指定时间17点50执行
        if(params.equals("daily")){

        }
        //每天凌晨判断是否存在当天日报如果没有,存一条未提交数据
        if(params.equals("dailyDay")){

        }
        //每年12月1号执行
        if(params.equals("year")){
            System.out.println("999999999999999");
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);

            String s=DateUtils.sendGet("http://timor.tech/api/holiday/year/"+year+1, null);
            JSONObject jsonObject = JSONObject.parseObject(s);
            JSONObject holiday = jsonObject.getJSONObject("holiday");
            Set<Map.Entry<String,Object>> entrySet = holiday.entrySet();
            for (Map.Entry<String,Object> entry:entrySet) {
                String key = entry.getKey();
                Object value = entry.getValue();
                System.out.println("++++++"+value);
                JSONObject o = (JSONObject) JSON.toJSON(value);
                OaHoliday oaHoliday = JSONObject.toJavaObject((JSONObject) JSON.toJSON(value), OaHoliday.class);
                oaHoliday.setId(getRedisIncreID.getId());
                oaHoliday.setHolidaysDate(o.getString("date"));
                try {
                    String week = DateUtils.dateToWeek(oaHoliday.getHolidaysDate());
                    oaHoliday.setWeek(week);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                oaHoliday.setAffiliationYear(String.valueOf(year+1));
                oaHolidayMapper.insertOaHoliday(oaHoliday);
            }
        }
        //每晚11点执行更改考勤组状态
        if(params.equals("attendance")){
            //立即执行考勤组
            OaAttendanceGroup oaAttendanceGroup = new OaAttendanceGroup();
            oaAttendanceGroup.setState("1");
            List<OaAttendanceGroup> oaAttendanceGroups = oaAttendanceGroupMapper.selectOaAttendanceGroupList(oaAttendanceGroup);
            for (OaAttendanceGroup attendanceGroup:oaAttendanceGroups) {
                //查询原考勤组
                if(StringUtils.isNotEmpty(attendanceGroup.getParentId())){
                    OaAttendanceGroup attendanceGroup1 = oaAttendanceGroupMapper.selectOaAttendanceGroupById(attendanceGroup.getParentId());
                    //现有考勤组部门
                    if(StringUtils.isNotEmpty(attendanceGroup.getAttendanceMembers())){
                        String[] split1 = attendanceGroup.getAttendanceMembers().split(",");
                    //删除释放部门相关打卡初始化数据
                    if(StringUtils.isNotEmpty(attendanceGroup1.getAttendanceMembers())){
                        //释放对应部门
                        String[] split = attendanceGroup1.getAttendanceMembers().split(",");
                        List<String> compare = compare(split, split1);
                        String[] split2 = compare.toArray(new String[compare.size()]);
                        List<Long> l = Arrays.stream(split2).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
                        Long[] deptIds = l.toArray(new Long[l.size()]);
                        //判断是否有需要释放的部门
                        if(deptIds.length > 0){
                            List<SysUser> sysUsers = sysUserMapper.selectUsersByDeptIds(deptIds, SecurityUtils.getCurrComId());
                            for (SysUser user:sysUsers) {
                                OaClock clock = new OaClock();
                                clock.setQueryTime(queryTime);
                                clock.setEmployeeId(user.getUserId());
                                List<OaClock> oaClockList = oaClockMapper.selectOaClockList(clock);
                                oaClockList.stream().forEach(itm ->{
                                    oaClockMapper.deleteOaClockById(itm.getId());
                                });
                            }
                        }
                        for (String deptId:split) {
                            //释放原有数据考勤人员
                            SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(deptId));
                            dept.setAttendanceLock("0");
                            sysDeptMapper.updateDept(dept);
                        }
                      }
                    }
                    //原人员释放
                    if(StringUtils.isNotEmpty(attendanceGroup1.getNonAttendanceMembers())){
                        String[] split = attendanceGroup1.getNonAttendanceMembers().split(",");
                        for (String userId:split) {
                            //删除对应人员当天打卡数据
                            OaClock clock = new OaClock();
                            clock.setQueryTime(queryTime);
                            clock.setEmployeeId(Long.valueOf(userId));
                            List<OaClock> oaClockList = oaClockMapper.selectOaClockList(clock);
                            for (OaClock oaClock:oaClockList) {
                                oaClockMapper.deleteOaClockById(oaClock.getId());
                            }
                            //释放不用打卡用户
                            SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                            user.setNeedNot("0");
                            sysUserMapper.updateUser(user);
                        }
                    }
                    //修改原有考勤组状态
                    attendanceGroup1.setState("3");
                    attendanceGroup1.setFlag(1L);
                    oaAttendanceGroupMapper.updateOaAttendanceGroup(attendanceGroup1);
                }
                 //需生效考勤组锁定对应部门
                if(StringUtils.isNotEmpty(attendanceGroup.getAttendanceMembers())){

                    //锁定对应部门
                    String[] split = attendanceGroup.getAttendanceMembers().split(",");
                    for (String deptId:split) {
                        SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(deptId));
                        dept.setAttendanceLock(attendanceGroup.getId());
                        sysDeptMapper.updateDept(dept);
                    }
                }
                //更新无需打卡人员
                if(StringUtils.isNotEmpty(attendanceGroup.getNonAttendanceMembers())){
                    //标记不用打卡用户
                    String[] split = attendanceGroup.getNonAttendanceMembers().split(",");
                    for (String userId:split) {
                        SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                        user.setNeedNot("1");
                        sysUserMapper.updateUser(user);
                    }
                }
                //立即生效
                attendanceGroup.setState("0");

                //新考勤组成员生成打卡数据
                //新增打卡缓存数据
                //获取所有考勤组成员
                String[] split = attendanceGroup.getAttendanceMembers().split(",");
                List<Long> l = Arrays.stream(split).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
                Long[] deptIds = l.toArray(new Long[l.size()]);
                List<SysUser> sysUsers = sysUserMapper.selectUsersByDeptIds(deptIds, SecurityUtils.getCurrComId());
                //无需打卡人员
            /*String nonAttendanceMembers = oaAttendanceGroup.getNonAttendanceMembers();
            String[] split1 = nonAttendanceMembers.split(",");*/
                for (SysUser user:sysUsers) {
                    {
                        //查询打卡记录表中存在今天数据
                        OaClock clock = new OaClock();
                        clock.setQueryTime(queryTime);
                        clock.setEmployeeId(user.getUserId());
                        //clock.setFlag(1L);
                        List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                        //工作日查询
                        OaWeekday weekday = new OaWeekday();
                        weekday.setRelationId(attendanceGroup.getId());
                        //查询是否必须打卡
                        weekday.setType("2");
                        weekday.setAttendanceStart(queryTime);
                        List<OaWeekday> mustWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                        if(mustWeekdays.size() > 0){
                            if(StringUtils.isNotEmpty(mustWeekdays.get(0).getWorkTimeId())){
                                OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(mustWeekdays.get(0).getWorkTimeId());
                                //必须打卡日期
                                //缓存打卡数据
                                if(oaClocks.size() == 0){
                                    OaClock oaClock = new OaClock();
                                    oaClock.setId(getRedisIncreID.getId());
                                    oaClock.setFlag(1L);
                                    oaClock.setClockType("1");
                                    oaClock.setType("1");
                                    oaClock.setEmployeeId(user.getUserId());
                                    //租户ID
                                    oaClock.setTenantId(user.getComId());
                                    oaClock.setInitialTime(oaWorkTimes.getGoWorkTime());
                                    oaClock.setEndClock(oaWorkTimes.getGoEndClock());
                                    oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                    oaClock.setCreateTime(DateUtils.getNowDate());
                                    oaClock.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                    oaClock.setAllowedLate(oaWorkTimes.getAllowedLate());
                                    oaClock.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                    oaClock.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                    oaClockMapper.insertOaClock(oaClock);
                                    //下班打卡数据
                                    OaClock oaClock1 = new OaClock();
                                    BeanUtils.copyBeanProp(oaClock1, oaClock);
                                    oaClock1.setId(getRedisIncreID.getId());
                                    oaClock1.setClockType("2");
                                    oaClock.setEndClock(oaWorkTimes.getOffEndClock());
                                    oaClock1.setInitialTime(oaWorkTimes.getOffWorkTime());
                                    oaClockMapper.insertOaClock(oaClock1);
                                }else {
                                    //如有上班打卡时间或者下班打卡时间不更新
                                    oaClocks.stream().forEach(itm ->{
                                        if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                            itm.setInitialTime(oaWorkTimes.getGoWorkTime());
                                            itm.setEndClock(oaWorkTimes.getGoEndClock());
                                            itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                            itm.setClockState("0");
                                            itm.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                            itm.setAllowedLate(oaWorkTimes.getAllowedLate());
                                            itm.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                            itm.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                            oaClockMapper.updateOaClock(itm);
                                        }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                            itm.setInitialTime(oaWorkTimes.getOffWorkTime());
                                            itm.setEndClock(oaWorkTimes.getOffEndClock());
                                            itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                            itm.setClockState("0");
                                            itm.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                            itm.setAllowedLate(oaWorkTimes.getAllowedLate());
                                            itm.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                            itm.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                            oaClockMapper.updateOaClock(itm);
                                        }
                                    });
                                }
                            }
                        }
                        //查询是否无需打卡
                        weekday.setType("3");
                        List<OaWeekday> oaWeekdays1 = oaWeekdayMapper.selectOaWeekdayList(weekday);
                        if(oaWeekdays1.size() > 0){
                            if(oaClocks.size() == 0){
                                //缓存打卡数据
                                OaClock oaClock = new OaClock();
                                oaClock.setId(getRedisIncreID.getId());
                                oaClock.setFlag(1L);
                                oaClock.setClockType("1");
                                oaClock.setType("1");
                                oaClock.setEmployeeId(user.getUserId());
                                //租户ID
                                oaClock.setTenantId(user.getComId());
                                oaClock.setClockState("5");
                                oaClock.setAttendanceType("0");
                                oaClock.setCreateTime(DateUtils.getNowDate());
                                oaClockMapper.insertOaClock(oaClock);
                                //下班打卡数据
                                OaClock oaClock1 = new OaClock();
                                BeanUtils.copyBeanProp(oaClock1, oaClock);
                                oaClock1.setId(getRedisIncreID.getId());
                                oaClock1.setClockType("2");
                                oaClockMapper.insertOaClock(oaClock1);
                            }else {
                                oaClocks.stream().forEach(itm ->{
                                    if("1".equals(itm.getClockType())&& itm.getClockTime() == null){
                                        itm.setAttendanceType("0");
                                        itm.setClockState("5");
                                        oaClockMapper.updateOaClock(itm);
                                    }else if("2".equals(itm.getClockType())&& itm.getClockTime() == null){
                                        itm.setAttendanceType("0");
                                        itm.setClockState("5");
                                        oaClockMapper.updateOaClock(itm);
                                    }
                                });
                            }
                        }
                        //必须打卡与无需打卡不存在
                        if(mustWeekdays.size() == 0 && oaWeekdays1.size() == 0) {
                            OaHoliday oaHoliday = new OaHoliday();
                            oaHoliday.setHolidaysDate(queryTime);
                            oaHoliday.setHoliday("true");
                            List<OaHoliday> oaHolidays = oaHolidayMapper.selectOaHolidayList(oaHoliday);
                            if(oaHolidays.size() > 0){
                                if(oaClocks.size() == 0){
                                    //缓存打卡数据
                                    OaClock oaClock = new OaClock();
                                    oaClock.setId(getRedisIncreID.getId());
                                    oaClock.setFlag(1L);
                                    oaClock.setClockType("1");
                                    oaClock.setType("1");
                                    oaClock.setEmployeeId(user.getUserId());
                                    //租户ID
                                    oaClock.setTenantId(user.getComId());
                                    oaClock.setClockState("5");
                                    oaClock.setAttendanceType("0");
                                    oaClock.setCreateTime(DateUtils.getNowDate());
                                    oaClockMapper.insertOaClock(oaClock);
                                    //下班打卡数据
                                    OaClock oaClock1 = new OaClock();
                                    BeanUtils.copyBeanProp(oaClock1, oaClock);
                                    oaClock1.setId(getRedisIncreID.getId());
                                    oaClock1.setClockType("2");
                                    oaClockMapper.insertOaClock(oaClock1);
                                }else {
                                    oaClocks.stream().forEach(itm ->{
                                        if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                            itm.setAttendanceType("0");
                                            itm.setClockState("5");
                                            oaClockMapper.updateOaClock(itm);
                                        }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                            itm.setAttendanceType("0");
                                            itm.setClockState("5");
                                            oaClockMapper.updateOaClock(itm);
                                        }
                                    });
                                }
                            }
                            weekday.setType("1");
                            weekday.setAttendanceStart("");
                            try {
                                //前一天时间转周几
                                weekday.setName(DateUtils.dateToWeek(queryTime));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if(oaHolidays.size() == 0){
                                if("1".equals(attendanceGroup.getAttendanceType())){
                                    //固定班制
                                    List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                                    OaWeekday oaWeekday = oaWeekdays.get(0);
                                    //获取班次信息
                                    if (StringUtils.isNotEmpty(oaWeekday.getWorkTimeId())) {
                                        if (!"0".equals(oaWeekday.getWorkTimeId())) {
                                            OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(oaWeekday.getWorkTimeId());
                                            if(oaClocks.size() == 0){
                                                //缓存打卡数据
                                                OaClock oaClock = new OaClock();
                                                oaClock.setId(getRedisIncreID.getId());
                                                oaClock.setFlag(1L);
                                                oaClock.setType("1");
                                                oaClock.setClockType("1");
                                                oaClock.setEmployeeId(user.getUserId());
                                                //租户ID
                                                oaClock.setTenantId(user.getComId());
                                                oaClock.setInitialTime(oaWorkTimes.getGoWorkTime());
                                                oaClock.setEndClock(oaWorkTimes.getGoEndClock());
                                                oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                oaClock.setCreateTime(DateUtils.getNowDate());
                                                oaClock.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                                oaClock.setAllowedLate(oaWorkTimes.getAllowedLate());
                                                oaClock.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                                oaClock.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                                oaClockMapper.insertOaClock(oaClock);
                                                //下班打卡数据
                                                OaClock oaClock1 = new OaClock();
                                                BeanUtils.copyBeanProp(oaClock1, oaClock);
                                                oaClock1.setId(getRedisIncreID.getId());
                                                oaClock1.setClockType("2");
                                                oaClock1.setInitialTime(oaWorkTimes.getOffWorkTime());
                                                oaClock1.setEndClock(oaWorkTimes.getOffEndClock());
                                                oaClockMapper.insertOaClock(oaClock1);
                                            }else {
                                                oaClocks.stream().forEach(itm ->{
                                                    if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                        itm.setInitialTime(oaWorkTimes.getGoWorkTime());
                                                        itm.setEndClock(oaWorkTimes.getGoEndClock());
                                                        itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                        itm.setClockState("0");
                                                        itm.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                                        itm.setAllowedLate(oaWorkTimes.getAllowedLate());
                                                        itm.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                                        itm.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                                        oaClockMapper.updateOaClock(itm);
                                                    }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                        itm.setInitialTime(oaWorkTimes.getOffWorkTime());
                                                        itm.setEndClock(oaWorkTimes.getOffEndClock());
                                                        itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                        itm.setClockState("0");
                                                        itm.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                                        itm.setAllowedLate(oaWorkTimes.getAllowedLate());
                                                        itm.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                                        itm.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                                        oaClockMapper.updateOaClock(itm);
                                                    }
                                                });
                                            }
                                        }else {
                                            if(oaClocks.size() == 0){
                                                //缓存打卡数据
                                                OaClock oaClock = new OaClock();
                                                oaClock.setId(getRedisIncreID.getId());
                                                oaClock.setFlag(1L);
                                                oaClock.setClockType("1");
                                                oaClock.setType("1");
                                                oaClock.setEmployeeId(user.getUserId());
                                                //租户ID
                                                oaClock.setTenantId(user.getComId());
                                                oaClock.setClockState("5");
                                                oaClock.setAttendanceType("0");
                                                oaClock.setCreateTime(DateUtils.getNowDate());
                                                oaClockMapper.insertOaClock(oaClock);
                                                //下班打卡数据
                                                OaClock oaClock1 = new OaClock();
                                                BeanUtils.copyBeanProp(oaClock1, oaClock);
                                                oaClock1.setId(getRedisIncreID.getId());
                                                oaClock1.setClockType("2");
                                                oaClockMapper.insertOaClock(oaClock1);
                                            }else {
                                                oaClocks.stream().forEach(itm ->{
                                                    if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                        itm.setAttendanceType("0");
                                                        itm.setClockState("5");
                                                        oaClockMapper.updateOaClock(itm);
                                                    }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                        itm.setAttendanceType("0");
                                                        itm.setClockState("5");
                                                        oaClockMapper.updateOaClock(itm);
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }else if("2".equals(oaAttendanceGroup.getAttendanceType())){
                                    //弹性班制
                                    List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                                    //弹性工作日不包含本天设置为休假
                                    if(oaWeekdays.size() == 0){
                                        if(oaClocks.size() == 0){
                                            //缓存打卡数据
                                            OaClock oaClock = new OaClock();
                                            oaClock.setId(getRedisIncreID.getId());
                                            oaClock.setFlag(1L);
                                            oaClock.setClockType("1");
                                            oaClock.setType("1");
                                            oaClock.setEmployeeId(user.getUserId());
                                            //租户ID
                                            oaClock.setTenantId(user.getComId());
                                            oaClock.setClockState("5");
                                            oaClock.setAttendanceType("0");
                                            oaClock.setCreateTime(DateUtils.getNowDate());
                                            oaClockMapper.insertOaClock(oaClock);
                                            //下班打卡数据
                                            OaClock oaClock1 = new OaClock();
                                            BeanUtils.copyBeanProp(oaClock1, oaClock);
                                            oaClock1.setId(getRedisIncreID.getId());
                                            oaClock1.setClockType("2");
                                            oaClockMapper.insertOaClock(oaClock1);
                                        }else {
                                            oaClocks.stream().forEach(itm ->{
                                                if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                    itm.setAttendanceType("0");
                                                    itm.setClockState("5");
                                                    oaClockMapper.updateOaClock(itm);
                                                }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                    itm.setAttendanceType("0");
                                                    itm.setClockState("5");
                                                    oaClockMapper.updateOaClock(itm);
                                                }
                                            });
                                        }
                                    }else {
                                        OaWeekday oaWeekday = oaWeekdays.get(0);
                                        if(oaClocks.size() == 0){
                                            //缓存打卡数据
                                            OaClock oaClock = new OaClock();
                                            oaClock.setId(getRedisIncreID.getId());
                                            oaClock.setFlag(1L);
                                            oaClock.setClockType("1");
                                            oaClock.setType("1");
                                            //租户ID
                                            oaClock.setTenantId(user.getComId());
                                            oaClock.setEmployeeId(user.getUserId());
                                            oaClock.setAttendanceStart(oaWeekday.getAttendanceStart());
                                            oaClock.setWorkHours(oaWeekday.getWorkHours());
                                            oaClock.setCreateTime(DateUtils.getNowDate());
                                            oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                            oaClockMapper.insertOaClock(oaClock);
                                            //下班打卡数据
                                            OaClock oaClock1 = new OaClock();
                                            BeanUtils.copyBeanProp(oaClock1, oaClock);
                                            oaClock1.setId(getRedisIncreID.getId());
                                            oaClock1.setClockType("2");
                                            oaClockMapper.insertOaClock(oaClock1);
                                        }else {
                                            oaClocks.stream().forEach(itm ->{
                                                if("1".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                    itm.setAttendanceStart(oaWeekday.getAttendanceStart());
                                                    itm.setWorkHours(oaWeekday.getWorkHours());
                                                    itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                    itm.setClockState("0");
                                                    oaClockMapper.updateOaClock(itm);
                                                }else if("2".equals(itm.getClockType()) && itm.getClockTime() == null){
                                                    itm.setAttendanceStart(oaWeekday.getAttendanceStart());
                                                    itm.setWorkHours(oaWeekday.getWorkHours());
                                                    itm.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                    itm.setClockState("0");
                                                    oaClockMapper.updateOaClock(itm);
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        }
                        //判断是否有已通过的请假
                        OaLeave leave = new OaLeave();
                        leave.setUserId(user.getUserId());
                        leave.setState(AdminCodeEnum.OALEAVE_STATUS_PASS.getCode());
                        leave.setStart(queryTime);
                        leave.setEnd(queryTime);
                        List<OaLeave> oaLeaves = oaLeaveMapper.selectOaLeave(leave);

                        //判断当天是否有外出
                        OaEgress egress = new OaEgress();
                        egress.setUserId(user.getUserId());
                        egress.setState(AdminCodeEnum.OAEGRESS_STATUS_PASS.getCode());
                        egress.setStart(queryTime);
                        egress.setEnd(queryTime);
                        //外出信息
                        List<OaEgress> oaEgresses = oaEgressMapper.selectOaEgress(egress);

                        //判断是否有已通过的出差信息
                        OaEvection evection = new OaEvection();
                        evection.setUserId(user.getUserId());
                        evection.setState(AdminCodeEnum.OAEVECTION_STATUS_PASS.getCode());
                        evection.setStartTime(queryTime);
                        evection.setEndTime(queryTime);
                        List<OaEvection> oaEvections = oaEvectionMapper.selectOaEvection(evection);

                        //执行类型
                        String executeType = "";
                        //请假为空其余不为空
                        if(oaLeaves.size() == 0 && oaEgresses.size() > 0 && oaEvections.size() > 0){
                            //判断外出与出差哪个先执行（外出时间在出差之后：以外出为准）
                            if(oaEgresses.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                executeType = "egress";
                            }else {
                                executeType = "evection";
                            }
                        }
                        //请假、出差不为空外出为空
                        if(oaLeaves.size() > 0 && oaEgresses.size() > 0 && oaEvections.size() == 0){
                            //请假开始与结束都是当天上午或者下午并且外出不能重复
                            if("1".equals(oaLeaves.get(0).getStartCycle()) && "1".equals(oaLeaves.get(0).getEndCycle()) && !"1".equals(oaEgresses.get(0).getStartCycle()) && !"1".equals(oaEgresses.get(0).getEndCycle())
                                    || "2".equals(oaLeaves.get(0).getStartCycle()) && "2".equals(oaLeaves.get(0).getEndCycle()) && !"2".equals(oaEgresses.get(0).getStartCycle()) && !"2".equals(oaEgresses.get(0).getEndCycle())){
                                for (OaClock clock1:oaClocks) {
                                    if("1".equals(oaLeaves.get(0).getStartCycle())){
                                        if("1".equals(clock1.getClockType())){
                                            clock1.setClockState("4");
                                        }
                                    }else if("2".equals(oaLeaves.get(0).getStartCycle())){
                                        if("2".equals(clock1.getClockType())){
                                            clock1.setClockState("4");
                                        }
                                    }
                                    oaClockMapper.updateOaClock(clock1);
                                }
                            }
                            //外出开始与结束都是当天上午或者下午
                            if("1".equals(oaEgresses.get(0).getStartCycle()) && "1".equals(oaEgresses.get(0).getEndCycle()) && !"1".equals(oaLeaves.get(0).getStartCycle()) && !"1".equals(oaLeaves.get(0).getEndCycle())
                                    || "2".equals(oaEgresses.get(0).getStartCycle()) && "2".equals(oaEgresses.get(0).getEndCycle()) && !"2".equals(oaLeaves.get(0).getStartCycle()) && !"2".equals(oaLeaves.get(0).getEndCycle())){
                                for (OaClock clock1:oaClocks) {
                                    if("1".equals(oaEgresses.get(0).getStartCycle())){
                                        if("1".equals(clock1.getClockType())){
                                            clock1.setClockState("10");
                                        }
                                    }else if("2".equals(oaEgresses.get(0).getStartCycle())){
                                        if("2".equals(clock1.getClockType())){
                                            clock1.setClockState("10");
                                        }
                                    }
                                    oaClockMapper.updateOaClock(clock1);
                                }
                            }
                            //判断请假与外出哪个先执行（请假时间在外出时间之后：以请假为准）
                            if(oaLeaves.get(0).getSubmitTime().compareTo(oaEgresses.get(0).getSubmitTime()) > 0){
                                executeType = "leaves";
                            }else {
                                executeType = "egress";
                            }
                        }
                        //请假、外出不为空出差为空
                        if(oaLeaves.size() > 0 && oaEgresses.size() == 0 && oaEvections.size() > 0){
                            //判断请假与出差哪个先执行（请假时间在出差时间之后：以请假为准）
                            if(oaLeaves.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                executeType = "leaves";
                            }else {
                                executeType = "evection";
                            }
                        }
                        //请假、出差、外出都不为空
                        if(oaLeaves.size() > 0 && oaEgresses.size() > 0 && oaEvections.size() > 0){
                            //请假时间在外出时间之后又在出差之后：以请假为准
                            if(oaLeaves.get(0).getSubmitTime().compareTo(oaEgresses.get(0).getSubmitTime()) > 0 && oaLeaves.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                executeType = "leaves";
                            }
                            //外出时间在请假时间之后又在出差时间之后
                            if(oaEgresses.get(0).getSubmitTime().compareTo(oaLeaves.get(0).getSubmitTime()) > 0 && oaEgresses.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                executeType = "egress";
                            }
                            //出差时间在请假时间之后又在出差时间之后
                            if(oaEvections.get(0).getSubmitTime().compareTo(oaLeaves.get(0).getSubmitTime()) > 0 && oaEvections.get(0).getSubmitTime().compareTo(oaEgresses.get(0).getSubmitTime()) > 0){
                                executeType = "evection";
                            }
                        }
                        //请假不为空其余为空
                        if(oaLeaves.size() > 0 && oaEgresses.size() == 0 && oaEvections.size() == 0){
                            executeType = "leaves";
                        }
                        //请假、外出为空出差不为空
                        if(oaLeaves.size() == 0 && oaEgresses.size() == 0 && oaEvections.size() > 0){
                            executeType = "evection";
                        }
                        //请假、出差为空外出不为空
                        if(oaLeaves.size() == 0 && oaEgresses.size() > 0 && oaEvections.size() == 0){
                            executeType = "egress";
                        }

                        //请假信息
                        if("leave".equals(executeType)){
                            if(oaLeaves.size() > 0){
                                OaLeave oaLeave = oaLeaves.get(0);
                                //查看每次行程每一天
                                String[] leaveSplit = oaLeave.getEveryDay().split(",");
                                //数组转list
                                List<String> list1 = Arrays.asList(leaveSplit);
                                if(list1.size() == 1){
                                    //判断时间为今天
                                    //出差时间为一天
                                    if(list1.get(0).equals(queryTime)){
                                        for (OaClock oaClock:oaClocks) {
                                            if("1".equals(oaLeave.getStartCycle())){
                                                //修改上班打卡数据
                                                if("1".equals(oaClock.getClockType())){
                                                    //打卡状态为请假
                                                    oaClock.setClockState("4");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }else if("2".equals(oaLeave.getStartCycle())){
                                                //修改下班打卡数据
                                                if("2".equals(oaClock.getClockType())){
                                                    //打卡状态为请假
                                                    oaClock.setClockState("4");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }
                                            if("1".equals(oaLeave.getEndCycle())){
                                                //修改上班打卡数据
                                                if(oaClock.getClockType().equals("1")){
                                                    //打卡状态为请假
                                                    oaClock.setClockState("4");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }else if("2".equals(oaLeave.getEndCycle())){
                                                //修改上班打卡数据
                                                if(oaClock.getClockType().equals("2")){
                                                    //打卡状态为请假
                                                    oaClock.setClockState("4");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }
                                        }

                                    }
                                }else {
                                    for (String day:list1) {
                                        if(day.equals(queryTime)){
                                            //拿到第一个
                                            if(day.equals(list1.get(0))){
                                                for (OaClock oaClock:oaClocks) {
                                                    if("1".equals(oaLeave.getStartCycle())){
                                                        //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                                            //打卡状态为请假
                                                            oaClock.setClockState("4");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                    }else if("2".equals(oaLeave.getStartCycle())){
                                                        //修改下班打卡数据
                                                        if("2".equals(oaClock.getClockType())){
                                                            //打卡状态为请假
                                                            oaClock.setClockState("4");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                }
                                            }else if(day.equals(list1.get(list1.size()-1))){
                                                for (OaClock oaClock:oaClocks) {
                                                    if("1".equals(oaLeave.getEndCycle())){
                                                        //修改上班打卡数据
                                                        if("1".equals(oaClock.getClockType())){
                                                            //打卡状态为请假
                                                            oaClock.setClockState("4");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }else if("2".equals(oaLeave.getEndCycle())){
                                                        //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                                            //打卡状态为请假
                                                            oaClock.setClockState("4");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }else {
                                                //上班下班都要更改
                                                for (OaClock oaClock:oaClocks) {
                                                    //打卡状态为出差
                                                    oaClock.setClockState("4");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }


                        //外出信息
                        if("egress".equals(executeType)){
                            if(oaEgresses.size() > 0){
                                OaEgress oaEgress = oaEgresses.get(0);
                                //查看每一天外出
                                String[] egressSplit = oaEgress.getEveryDay().split(",");
                                //数组转list
                                List<String> list1 = Arrays.asList(egressSplit);
                                if(list1.size() == 1){
                                    //判断时间为今天
                                    //出差时间为一天
                                    if(list1.get(0).equals(queryTime)){
                                        for (OaClock oaClock:oaClocks) {
                                            if("1".equals(oaEgress.getStartCycle())){
                                                //修改上班打卡数据
                                                if("1".equals(oaClock.getClockType())){
                                                    //打卡状态为外出
                                                    oaClock.setClockState("10");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }else if("2".equals(oaEgress.getStartCycle())){
                                                //修改下班打卡数据
                                                if("2".equals(oaClock.getClockType())){
                                                    //打卡状态为外出
                                                    oaClock.setClockState("10");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }
                                            if("1".equals(oaEgress.getEndCycle())){
                                                //修改上班打卡数据
                                                if(oaClock.getClockType().equals("1")){
                                                    //打卡状态为外出
                                                    oaClock.setClockState("10");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }else if("2".equals(oaEgress.getEndCycle())){
                                                //修改上班打卡数据
                                                if(oaClock.getClockType().equals("2")){
                                                    //打卡状态为外出
                                                    oaClock.setClockState("10");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }
                                        }

                                    }
                                }else {
                                    for (String day:list1) {
                                        if(day.equals(queryTime)){
                                            //拿到第一个
                                            if(day.equals(list1.get(0))){
                                                for (OaClock oaClock:oaClocks) {
                                                    if("1".equals(oaEgress.getStartCycle())){
                                                        //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                                            //打卡状态为外出
                                                            oaClock.setClockState("10");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                    }else if("2".equals(oaEgress.getStartCycle())){
                                                        //修改下班打卡数据
                                                        if("2".equals(oaClock.getClockType())){
                                                            //打卡状态为外出
                                                            oaClock.setClockState("10");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                }
                                            }else if(day.equals(list1.get(list1.size()-1))){
                                                for (OaClock oaClock:oaClocks) {
                                                    if("1".equals(oaEgress.getEndCycle())){
                                                        //修改上班打卡数据
                                                        if("1".equals(oaClock.getClockType())){
                                                            //打卡状态为外出
                                                            oaClock.setClockState("10");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }else if("2".equals(oaEgress.getEndCycle())){
                                                        //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                                            //打卡状态为外出
                                                            oaClock.setClockState("10");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }else {
                                                //上班下班都要更改
                                                for (OaClock oaClock:oaClocks) {
                                                    //打卡状态为外出
                                                    oaClock.setClockState("10");
                                                    oaClockMapper.updateOaClock(oaClock);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //出差申请
                        if("evection".equals(executeType)){
                            //拿到最后审批通过数据
                            if(oaEvections.size() > 0 ){
                                OaEvection evection1 = oaEvections.get(0);
                                //查询所有行程
                                QueryWrapper<OaEvectionTrip> queryWrapper = new QueryWrapper<>();
                                queryWrapper.eq("evection_id",evection1.getId());
                                List<OaEvectionTrip> oaEvectionTrips = oaEvectionTripMapper.selectList(queryWrapper);
                                for (OaEvectionTrip oaEvectionTrip:oaEvectionTrips) {
                                    //查看每次行程每一天
                                    String[] split1 = oaEvectionTrip.getEveryDay().split(",");
                                    //数组转list
                                    List<String> list1 = Arrays.asList(split1);
                                    if(list1.size() == 1){
                                        //判断时间为今天
                                        //出差时间为一天
                                        if(list1.get(0).equals(queryTime)){
                                            for (OaClock oaClock:oaClocks) {
                                                if("1".equals(oaEvectionTrip.getStartCycle())){
                                                    //修改上班打卡数据
                                                    if("1".equals(oaClock.getClockType())){
                                                        //打卡状态为出差
                                                        oaClock.setClockState("9");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaEvectionTrip.getStartCycle())){
                                                    //修改下班打卡数据
                                                    if("2".equals(oaClock.getClockType())){
                                                        //打卡状态为出差
                                                        oaClock.setClockState("9");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                                if("1".equals(oaEvectionTrip.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("1")){
                                                        //打卡状态为出差
                                                        oaClock.setClockState("9");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaEvectionTrip.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("2")){
                                                        //打卡状态为出差
                                                        oaClock.setClockState("9");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }

                                        }
                                    }else {
                                        for (String day:list1) {
                                            if(day.equals(queryTime)){
                                                //拿到第一个
                                                if(day.equals(list1.get(0))){
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaEvectionTrip.getStartCycle())){
                                                            //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                                                //打卡状态为出差
                                                                oaClock.setClockState("9");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                        }else if("2".equals(oaEvectionTrip.getStartCycle())){
                                                            //修改下班打卡数据
                                                            if("2".equals(oaClock.getClockType())){
                                                                //打卡状态为出差
                                                                oaClock.setClockState("9");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }
                                                    }
                                                }else if(day.equals(list1.get(list1.size()-1))){
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaEvectionTrip.getEndCycle())){
                                                            //修改上班打卡数据
                                                            if("1".equals(oaClock.getClockType())){
                                                                //打卡状态为出差
                                                                oaClock.setClockState("9");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }else if("2".equals(oaEvectionTrip.getEndCycle())){
                                                            //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                                                //打卡状态为出差
                                                                oaClock.setClockState("9");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                }else {
                                                    //上班下班都要更改
                                                    for (OaClock oaClock:oaClocks) {
                                                        //打卡状态为出差
                                                        oaClock.setClockState("9");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                oaAttendanceGroupMapper.updateOaAttendanceGroup(attendanceGroup);
            }
        }
        //当天凌晨十二点执行插入当天打卡数据
        if(params.equals("oaClock")){
            //查询所有人
            List<String> list = sysUserMapper.selectUserByHumanoid(null);
            for (String userId:list) {
                //获取今天打卡班次
                SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                if("0".equals(user.getNeedNot())){
                    SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
                    if(StringUtils.isNotNull(dept)) {
                        if (!"0".equals(dept.getAttendanceLock())) {
                            //查询考勤组
                            OaAttendanceGroup oaAttendanceGroup = oaAttendanceGroupMapper.selectOaAttendanceGroupById(dept.getAttendanceLock());
                            OaWeekday weekday = new OaWeekday();
                            weekday.setRelationId(oaAttendanceGroup.getId());
                            //查询是否必须打卡
                            weekday.setType("2");
                            //昨天
                            //weekday.setAttendanceStart(previousDay);
                            weekday.setAttendanceStart(queryTime);
                            List<OaWeekday> mustWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                            if(mustWeekdays.size() > 0){
                                if(StringUtils.isNotEmpty(mustWeekdays.get(0).getWorkTimeId())){
                                    OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(mustWeekdays.get(0).getWorkTimeId());
                                    //必须打卡日期
                                    //缓存打卡数据
                                    OaClock oaClock = new OaClock();
                                    oaClock.setId(getRedisIncreID.getId());
                                    oaClock.setFlag(1L);
                                    oaClock.setClockType("1");
                                    oaClock.setType("1");
                                    oaClock.setEmployeeId(user.getUserId());
                                    //租户ID
                                    oaClock.setTenantId(user.getComId());
                                    oaClock.setInitialTime(oaWorkTimes.getGoWorkTime());
                                    oaClock.setEndClock(oaWorkTimes.getGoEndClock());
                                    oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                    //昨天数据
                                    //oaClock.setCreateTime(calendar1.getTime());
                                    oaClock.setCreateTime(DateUtils.getNowDate());
                                    oaClock.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                    oaClock.setAllowedLate(oaWorkTimes.getAllowedLate());
                                    oaClock.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                    oaClock.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                    oaClock.setClockState("0");
                                    oaClockMapper.insertOaClock(oaClock);
                                    //下班打卡数据
                                    OaClock oaClock1 = new OaClock();
                                    BeanUtils.copyBeanProp(oaClock1, oaClock);
                                    oaClock1.setId(getRedisIncreID.getId());
                                    oaClock1.setClockType("2");
                                    oaClock.setEndClock(oaWorkTimes.getOffEndClock());
                                    oaClock1.setInitialTime(oaWorkTimes.getOffWorkTime());
                                    oaClockMapper.insertOaClock(oaClock1);
                                }
                            }
                            //无需打卡
                            //查询是否无需打卡
                            weekday.setType("3");
                            List<OaWeekday> oaWeekdays1 = oaWeekdayMapper.selectOaWeekdayList(weekday);
                            if(oaWeekdays1.size() > 0){
                                //缓存打卡数据
                                OaClock oaClock = new OaClock();
                                oaClock.setId(getRedisIncreID.getId());
                                oaClock.setFlag(1L);
                                oaClock.setClockType("1");
                                oaClock.setType("1");
                                oaClock.setEmployeeId(user.getUserId());
                                //租户ID
                                oaClock.setTenantId(user.getComId());
                                oaClock.setClockState("5");
                                oaClock.setAttendanceType("0");
                                //昨天数据
                                //oaClock.setCreateTime(calendar1.getTime());
                                oaClock.setCreateTime(DateUtils.getNowDate());
                                oaClockMapper.insertOaClock(oaClock);
                                //下班打卡数据
                                OaClock oaClock1 = new OaClock();
                                BeanUtils.copyBeanProp(oaClock1, oaClock);
                                oaClock1.setId(getRedisIncreID.getId());
                                oaClock1.setClockType("2");
                                oaClockMapper.insertOaClock(oaClock1);
                            }
                            //必须打卡与无需打卡不存在
                            if(mustWeekdays.size() == 0 && oaWeekdays1.size() == 0){
                                OaHoliday oaHoliday = new OaHoliday();
                                //昨天数据
                                //oaHoliday.setHolidaysDate(previousDay);
                                oaHoliday.setHolidaysDate(queryTime);
                                oaHoliday.setHoliday("true");
                                List<OaHoliday> oaHolidays = oaHolidayMapper.selectOaHolidayList(oaHoliday);
                                if(oaHolidays.size() > 0){
                                    //缓存打卡数据
                                    OaClock oaClock = new OaClock();
                                    oaClock.setId(getRedisIncreID.getId());
                                    oaClock.setFlag(1L);
                                    oaClock.setClockType("1");
                                    oaClock.setType("1");
                                    oaClock.setEmployeeId(user.getUserId());
                                    //租户ID
                                    oaClock.setTenantId(user.getComId());
                                    oaClock.setClockState("5");
                                    oaClock.setAttendanceType("0");
                                    //昨天数据
                                    //oaClock.setCreateTime(calendar1.getTime());
                                    oaClock.setCreateTime(DateUtils.getNowDate());
                                    oaClockMapper.insertOaClock(oaClock);
                                    //下班打卡数据
                                    OaClock oaClock1 = new OaClock();
                                    BeanUtils.copyBeanProp(oaClock1, oaClock);
                                    oaClock1.setId(getRedisIncreID.getId());
                                    oaClock1.setClockType("2");
                                    oaClockMapper.insertOaClock(oaClock1);
                                }
                                //固定工作制打卡
                                weekday.setType("1");
                                weekday.setAttendanceStart("");
                                try {
                                    //前一天时间转周几
                                    //weekday.setName(DateUtils.dateToWeek(previousDay));
                                    weekday.setName(DateUtils.dateToWeek(queryTime));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                if(oaHolidays.size() == 0){
                                    if ("1".equals(oaAttendanceGroup.getAttendanceType())) {
                                        //固定班制
                                        List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                                        OaWeekday oaWeekday = oaWeekdays.get(0);
                                        //获取班次信息
                                        if (StringUtils.isNotEmpty(oaWeekday.getWorkTimeId())) {
                                            if (!"0".equals(oaWeekday.getWorkTimeId())) {
                                                OaWorkTimes oaWorkTimes = oaWorkTimesMapper.selectOaWorkTimesById(oaWeekday.getWorkTimeId());
                                                //缓存打卡数据
                                                OaClock oaClock = new OaClock();
                                                oaClock.setId(getRedisIncreID.getId());
                                                oaClock.setFlag(1L);
                                                oaClock.setClockType("1");
                                                oaClock.setType("1");
                                                oaClock.setEmployeeId(user.getUserId());
                                                //租户ID
                                                oaClock.setTenantId(user.getComId());
                                                oaClock.setInitialTime(oaWorkTimes.getGoWorkTime());
                                                oaClock.setEndClock(oaWorkTimes.getGoEndClock());
                                                oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                                //昨天数据
                                                //oaClock.setCreateTime(calendar1.getTime());
                                                oaClock.setCreateTime(DateUtils.getNowDate());
                                                oaClock.setSeverelyLate(oaWorkTimes.getSeverelyLate());
                                                oaClock.setAllowedLate(oaWorkTimes.getAllowedLate());
                                                oaClock.setAbsenteeismLate(oaWorkTimes.getAbsenteeismLate());
                                                oaClock.setAbsenteeismEarly(oaWorkTimes.getAbsenteeismEarly());
                                                oaClock.setClockState("0");
                                                oaClockMapper.insertOaClock(oaClock);
                                                //下班打卡数据
                                                OaClock oaClock1 = new OaClock();
                                                BeanUtils.copyBeanProp(oaClock1, oaClock);
                                                oaClock1.setId(getRedisIncreID.getId());
                                                oaClock1.setClockType("2");
                                                oaClock1.setEndClock(oaWorkTimes.getOffEndClock());
                                                oaClock1.setInitialTime(oaWorkTimes.getOffWorkTime());
                                                oaClockMapper.insertOaClock(oaClock1);
                                            }else {
                                                //缓存打卡数据
                                                OaClock oaClock = new OaClock();
                                                oaClock.setId(getRedisIncreID.getId());
                                                oaClock.setFlag(1L);
                                                oaClock.setClockType("1");
                                                oaClock.setType("1");
                                                oaClock.setEmployeeId(user.getUserId());
                                                //租户ID
                                                oaClock.setTenantId(user.getComId());
                                                oaClock.setClockState("5");
                                                oaClock.setAttendanceType("0");
                                                //昨天数据
                                                //oaClock.setCreateTime(calendar1.getTime());
                                                oaClock.setCreateTime(DateUtils.getNowDate());
                                                oaClockMapper.insertOaClock(oaClock);
                                                //下班打卡数据
                                                OaClock oaClock1 = new OaClock();
                                                BeanUtils.copyBeanProp(oaClock1, oaClock);
                                                oaClock1.setId(getRedisIncreID.getId());
                                                oaClock1.setClockType("2");
                                                oaClockMapper.insertOaClock(oaClock1);
                                            }
                                        }
                                    } else if ("2".equals(oaAttendanceGroup.getAttendanceType())) {
                                        //弹性班制
                                        List<OaWeekday> oaWeekdays = oaWeekdayMapper.selectOaWeekdayList(weekday);
                                        //弹性工作日不包含本天设置为休假
                                        if(oaWeekdays.size() == 0){
                                                //缓存打卡数据
                                                OaClock oaClock = new OaClock();
                                                oaClock.setId(getRedisIncreID.getId());
                                                oaClock.setFlag(1L);
                                                oaClock.setClockType("1");
                                                oaClock.setType("1");
                                                oaClock.setEmployeeId(user.getUserId());
                                                //租户ID
                                                oaClock.setTenantId(user.getComId());
                                                oaClock.setClockState("5");
                                                oaClock.setAttendanceType("0");
                                                //昨天数据
                                                //oaClock.setCreateTime(calendar1.getTime());
                                                oaClock.setCreateTime(DateUtils.getNowDate());
                                                oaClockMapper.insertOaClock(oaClock);
                                                //下班打卡数据
                                                OaClock oaClock1 = new OaClock();
                                                BeanUtils.copyBeanProp(oaClock1, oaClock);
                                                oaClock1.setId(getRedisIncreID.getId());
                                                oaClock1.setClockType("2");
                                                oaClockMapper.insertOaClock(oaClock1);
                                        }else {
                                            OaWeekday oaWeekday = oaWeekdays.get(0);
                                            //缓存打卡数据
                                            OaClock oaClock = new OaClock();
                                            oaClock.setId(getRedisIncreID.getId());
                                            oaClock.setFlag(1L);
                                            oaClock.setClockType("1");
                                            oaClock.setType("1");
                                            oaClock.setEmployeeId(user.getUserId());
                                            //租户ID
                                            oaClock.setTenantId(user.getComId());
                                            oaClock.setAttendanceStart(oaWeekday.getAttendanceStart());
                                            oaClock.setWorkHours(oaWeekday.getWorkHours());
                                            //昨天数据
                                            //oaClock.setCreateTime(calendar1.getTime());
                                            oaClock.setCreateTime(DateUtils.getNowDate());
                                            oaClock.setAttendanceType(oaAttendanceGroup.getAttendanceType());
                                            oaClock.setClockState("0");
                                            oaClockMapper.insertOaClock(oaClock);
                                            //下班打卡数据
                                            OaClock oaClock1 = new OaClock();
                                            BeanUtils.copyBeanProp(oaClock1, oaClock);
                                            oaClock1.setId(getRedisIncreID.getId());
                                            oaClock1.setClockType("2");
                                            oaClockMapper.insertOaClock(oaClock1);
                                        }
                                    }
                                }
                            }
                            OaClock clock = new OaClock();
                            clock.setEmployeeId(user.getUserId());
                            //判断是否有已通过的请假
                            OaLeave leave = new OaLeave();
                            leave.setUserId(Long.valueOf(userId));
                            leave.setState(AdminCodeEnum.OALEAVE_STATUS_PASS.getCode());
                            leave.setStart(queryTime);
                            leave.setEnd(queryTime);
                            List<OaLeave> oaLeaves = oaLeaveMapper.selectOaLeave(leave);

                            //判断当天是否有外出
                            OaEgress egress = new OaEgress();
                            egress.setUserId(Long.valueOf(userId));
                            egress.setState(AdminCodeEnum.OAEGRESS_STATUS_PASS.getCode());
                            egress.setStart(queryTime);
                            egress.setEnd(queryTime);
                            //外出信息
                            List<OaEgress> oaEgresses = oaEgressMapper.selectOaEgress(egress);

                            //判断是否有已通过的出差信息
                            OaEvection evection = new OaEvection();
                            evection.setUserId(Long.valueOf(userId));
                            evection.setState(AdminCodeEnum.OAEVECTION_STATUS_PASS.getCode());
                            evection.setStartTime(queryTime);
                            evection.setEndTime(queryTime);
                            List<OaEvection> oaEvections = oaEvectionMapper.selectOaEvection(evection);

                            //执行类型
                            String executeType = "";
                            //请假为空其余不为空
                            if(oaLeaves.size() == 0 && oaEgresses.size() > 0 && oaEvections.size() > 0){
                                //判断外出与出差哪个先执行（外出时间在出差之后：以外出为准）
                                if(oaEgresses.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                    executeType = "egress";
                                }else {
                                    executeType = "evection";
                                }
                            }
                            //请假、外出不为空出差为空
                            if(oaLeaves.size() > 0 && oaEgresses.size() > 0 && oaEvections.size() == 0){
                                //查询打卡信息
                                clock.setQueryTime(queryTime);
                                //clock.setFlag(1L);
                                List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                //请假开始与结束都是当天上午或者下午并且外出不能重复
                                if("1".equals(oaLeaves.get(0).getStartCycle()) && "1".equals(oaLeaves.get(0).getEndCycle()) && !"1".equals(oaEgresses.get(0).getStartCycle()) && !"1".equals(oaEgresses.get(0).getEndCycle())
                                        || "2".equals(oaLeaves.get(0).getStartCycle()) && "2".equals(oaLeaves.get(0).getEndCycle()) && !"2".equals(oaEgresses.get(0).getStartCycle()) && !"2".equals(oaEgresses.get(0).getEndCycle())){
                                    for (OaClock clock1:oaClocks) {
                                        if("1".equals(oaLeaves.get(0).getStartCycle())){
                                            if("1".equals(clock1.getClockType())){
                                                clock1.setClockState("4");
                                            }
                                        }else if("2".equals(oaLeaves.get(0).getStartCycle())){
                                            if("2".equals(clock1.getClockType())){
                                                clock1.setClockState("4");
                                            }
                                        }
                                        oaClockMapper.updateOaClock(clock1);
                                    }
                                }
                                //外出开始与结束都是当天上午或者下午
                                if("1".equals(oaEgresses.get(0).getStartCycle()) && "1".equals(oaEgresses.get(0).getEndCycle()) && !"1".equals(oaLeaves.get(0).getStartCycle()) && !"1".equals(oaLeaves.get(0).getEndCycle())
                                        || "2".equals(oaEgresses.get(0).getStartCycle()) && "2".equals(oaEgresses.get(0).getEndCycle()) && !"2".equals(oaLeaves.get(0).getStartCycle()) && !"2".equals(oaLeaves.get(0).getEndCycle())){
                                    for (OaClock clock1:oaClocks) {
                                        if("1".equals(oaEgresses.get(0).getStartCycle())){
                                            if("1".equals(clock1.getClockType())){
                                                clock1.setClockState("10");
                                            }
                                        }else if("2".equals(oaEgresses.get(0).getStartCycle())){
                                            if("2".equals(clock1.getClockType())){
                                                clock1.setClockState("10");
                                            }
                                        }
                                        oaClockMapper.updateOaClock(clock1);
                                    }
                                }
                                //判断请假与外出哪个先执行（请假时间在外出时间之后：以请假为准）
                                if(oaLeaves.get(0).getSubmitTime().compareTo(oaEgresses.get(0).getSubmitTime()) > 0){
                                    executeType = "leaves";
                                }else {
                                    executeType = "egresses";
                                }
                            }
                            //请假、出差不为空外出为空
                            if(oaLeaves.size() > 0 && oaEgresses.size() == 0 && oaEvections.size() > 0){
                                //判断请假与出差哪个先执行（请假时间在出差时间之后：以请假为准）
                                if(oaLeaves.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                    executeType = "leaves";
                                }else {
                                    executeType = "evections";
                                }
                            }
                            //请假、出差、外出都不为空
                            if(oaLeaves.size() > 0 && oaEgresses.size() > 0 && oaEvections.size() > 0){
                                //请假时间在外出时间之后又在出差之后：以请假为准
                                if(oaLeaves.get(0).getSubmitTime().compareTo(oaEgresses.get(0).getSubmitTime()) > 0 && oaLeaves.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                    executeType = "leaves";
                                }
                                //外出时间在请假时间之后又在出差时间之后
                                if(oaEgresses.get(0).getSubmitTime().compareTo(oaLeaves.get(0).getSubmitTime()) > 0 && oaEgresses.get(0).getSubmitTime().compareTo(oaEvections.get(0).getSubmitTime()) > 0){
                                    executeType = "egresses";
                                }
                                //出差时间在请假时间之后又在出差时间之后
                                if(oaEvections.get(0).getSubmitTime().compareTo(oaLeaves.get(0).getSubmitTime()) > 0 && oaEvections.get(0).getSubmitTime().compareTo(oaEgresses.get(0).getSubmitTime()) > 0){
                                    executeType = "evections";
                                }
                            }
                            //请假不为空其余为空
                            if(oaLeaves.size() > 0 && oaEgresses.size() == 0 && oaEvections.size() == 0){
                                executeType = "leaves";
                            }
                            //请假、外出为空出差不为空
                            if(oaLeaves.size() == 0 && oaEgresses.size() == 0 && oaEvections.size() > 0){
                                executeType = "evections";
                            }
                            //请假、出差为空外出不为空
                            if(oaLeaves.size() == 0 && oaEgresses.size() > 0 && oaEvections.size() == 0){
                                executeType = "evections";
                            }

                            //请假信息
                            if("leave".equals(executeType)){
                                if(oaLeaves.size() > 0){
                                    OaLeave oaLeave = oaLeaves.get(0);
                                    //查看每次行程每一天
                                    String[] split = oaLeave.getEveryDay().split(",");
                                    //数组转list
                                    List<String> list1 = Arrays.asList(split);
                                    if(list1.size() == 1){
                                        //判断时间为今天
                                        //请假时间为一天
                                        if(list1.get(0).equals(queryTime)){
                                            //查询打卡信息
                                            clock.setQueryTime(queryTime);
                                            //clock.setFlag(1L);
                                            List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                            for (OaClock oaClock:oaClocks) {
                                                if("1".equals(oaLeave.getStartCycle())){
                                                    //修改上班打卡数据
                                                    if("1".equals(oaClock.getClockType())){
                                                        //打卡状态为请假
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaLeave.getStartCycle())){
                                                    //修改下班打卡数据
                                                    if("2".equals(oaClock.getClockType())){
                                                        //打卡状态为请假
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                                if("1".equals(oaLeave.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("1")){
                                                        //打卡状态为请假
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaLeave.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("2")){
                                                        //打卡状态为请假
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }

                                        }
                                    }else {
                                        for (String day:list1) {
                                            if(day.equals(queryTime)){
                                                //拿到第一个
                                                if(day.equals(list1.get(0))){
                                                    //查询打卡信息
                                                    clock.setQueryTime(queryTime);
                                                    //clock.setFlag(1L);
                                                    List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaLeave.getStartCycle())){
                                                            //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                                                //打卡状态为请假
                                                                oaClock.setClockState("4");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                        }else if("2".equals(oaLeave.getStartCycle())){
                                                            //修改下班打卡数据
                                                            if("2".equals(oaClock.getClockType())){
                                                                //打卡状态为出差
                                                                oaClock.setClockState("4");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }
                                                    }
                                                }else if(day.equals(list1.get(list1.size()-1))){
                                                    //最后一天做对比
                                                    clock.setQueryTime(queryTime);
                                                    //clock.setFlag(1L);
                                                    List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaLeave.getEndCycle())){
                                                            //修改上班打卡数据
                                                            if("1".equals(oaClock.getClockType())){
                                                                //打卡状态为出差
                                                                oaClock.setClockState("4");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }else if("2".equals(oaLeave.getEndCycle())){
                                                            //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                                                //打卡状态为出差
                                                                oaClock.setClockState("4");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                }else {
                                                    clock.setQueryTime(queryTime);
                                                    //clock.setFlag(1L);
                                                    List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                    //上班下班都要更改
                                                    for (OaClock oaClock:oaClocks) {
                                                        //打卡状态为出差
                                                        oaClock.setClockState("4");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }


                            //外出信息
                            if("egress".equals(executeType)){
                                if(oaEgresses.size() > 0){
                                    OaEgress oaEgress = oaEgresses.get(0);
                                    //查看每一天外出
                                    String[] split = oaEgress.getEveryDay().split(",");
                                    //数组转list
                                    List<String> list1 = Arrays.asList(split);
                                    if(list1.size() == 1){
                                        //判断时间为今天
                                        //外出时间为一天
                                        if(list1.get(0).equals(queryTime)){
                                            //查询打卡信息
                                            clock.setQueryTime(queryTime);
                                            //clock.setFlag(1L);
                                            List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                            for (OaClock oaClock:oaClocks) {
                                                if("1".equals(oaEgress.getStartCycle())){
                                                    //修改上班打卡数据
                                                    if("1".equals(oaClock.getClockType())){
                                                        //打卡状态为外出
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaEgress.getStartCycle())){
                                                    //修改下班打卡数据
                                                    if("2".equals(oaClock.getClockType())){
                                                        //打卡状态为外出
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                                if("1".equals(oaEgress.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("1")){
                                                        //打卡状态为外出
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }else if("2".equals(oaEgress.getEndCycle())){
                                                    //修改上班打卡数据
                                                    if(oaClock.getClockType().equals("2")){
                                                        //打卡状态为外出
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }

                                        }
                                    }else {
                                        for (String day:list1) {
                                            if(day.equals(queryTime)){
                                                //拿到第一个
                                                if(day.equals(list1.get(0))){
                                                    //查询打卡信息
                                                    clock.setQueryTime(queryTime);
                                                    //clock.setFlag(1L);
                                                    List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaEgress.getStartCycle())){
                                                            //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                                                //打卡状态为外出
                                                                oaClock.setClockState("10");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                        }else if("2".equals(oaEgress.getStartCycle())){
                                                            //修改下班打卡数据
                                                            if("2".equals(oaClock.getClockType())){
                                                                //打卡状态为外出
                                                                oaClock.setClockState("10");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }
                                                    }
                                                }else if(day.equals(list1.get(list1.size()-1))){
                                                    //最后一天做对比
                                                    clock.setQueryTime(queryTime);
                                                    //clock.setFlag(1L);
                                                    List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                    for (OaClock oaClock:oaClocks) {
                                                        if("1".equals(oaEgress.getEndCycle())){
                                                            //修改上班打卡数据
                                                            if("1".equals(oaClock.getClockType())){
                                                                //打卡状态为外出
                                                                oaClock.setClockState("10");
                                                                oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }else if("2".equals(oaEgress.getEndCycle())){
                                                            //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                                                //打卡状态为外出
                                                                oaClock.setClockState("10");
                                                                oaClockMapper.updateOaClock(oaClock);

                                                        }
                                                    }
                                                }else {
                                                    clock.setQueryTime(queryTime);
                                                    //clock.setFlag(1L);
                                                    List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                    //上班下班都要更改
                                                    for (OaClock oaClock:oaClocks) {
                                                        //打卡状态为出差
                                                        oaClock.setClockState("10");
                                                        oaClockMapper.updateOaClock(oaClock);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            //出差申请
                            if("evection".equals(executeType)){
                                //拿到最后审批通过数据
                                if(oaEvections.size() > 0 ){
                                    OaEvection evection1 = oaEvections.get(0);
                                    //查询所有行程
                                    QueryWrapper<OaEvectionTrip> queryWrapper = new QueryWrapper<>();
                                    queryWrapper.eq("evection_id",evection1.getId());
                                    List<OaEvectionTrip> oaEvectionTrips = oaEvectionTripMapper.selectList(queryWrapper);
                                    for (OaEvectionTrip oaEvectionTrip:oaEvectionTrips) {
                                        //查看每次行程每一天
                                        String[] split = oaEvectionTrip.getEveryDay().split(",");
                                        //数组转list
                                        List<String> list1 = Arrays.asList(split);
                                        if(list1.size() == 1){
                                            //判断时间为今天
                                            //出差时间为一天
                                            if(list1.get(0).equals(queryTime)){
                                                //查询打卡信息
                                                clock.setQueryTime(queryTime);
                                                //clock.setFlag(1L);
                                                List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                for (OaClock oaClock:oaClocks) {
                                                    if("1".equals(oaEvectionTrip.getStartCycle())){
                                                        //修改上班打卡数据
                                                        if("1".equals(oaClock.getClockType())){
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }else if("2".equals(oaEvectionTrip.getStartCycle())){
                                                        //修改下班打卡数据
                                                        if("2".equals(oaClock.getClockType())){
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                    if("1".equals(oaEvectionTrip.getEndCycle())){
                                                        //修改上班打卡数据
                                                        if(oaClock.getClockType().equals("1")){
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }else if("2".equals(oaEvectionTrip.getEndCycle())){
                                                        //修改上班打卡数据
                                                        if(oaClock.getClockType().equals("2")){
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                }

                                            }
                                        }else {
                                            for (String day:list1) {
                                                if(day.equals(queryTime)){
                                                    //拿到第一个
                                                    if(day.equals(list1.get(0))){
                                                        //查询打卡信息
                                                        clock.setQueryTime(queryTime);
                                                        //clock.setFlag(1L);
                                                        List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                        for (OaClock oaClock:oaClocks) {
                                                            if("1".equals(oaEvectionTrip.getStartCycle())){
                                                                //如果跨天开始时间为第一天的上午修改当天上下班打卡数据
                                                                    //打卡状态为出差
                                                                    oaClock.setClockState("9");
                                                                    oaClockMapper.updateOaClock(oaClock);
                                                            }else if("2".equals(oaEvectionTrip.getStartCycle())){
                                                                //修改下班打卡数据
                                                                if("2".equals(oaClock.getClockType())){
                                                                    //打卡状态为出差
                                                                    oaClock.setClockState("9");
                                                                    oaClockMapper.updateOaClock(oaClock);
                                                                }
                                                            }
                                                        }
                                                    }else if(day.equals(list1.get(list1.size()-1))){
                                                        //最后一天做对比
                                                        clock.setQueryTime(queryTime);
                                                        //clock.setFlag(1L);
                                                        List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                        for (OaClock oaClock:oaClocks) {
                                                            if("1".equals(oaEvectionTrip.getEndCycle())){
                                                                //修改上班打卡数据
                                                                if("1".equals(oaClock.getClockType())){
                                                                    //打卡状态为出差
                                                                    oaClock.setClockState("9");
                                                                    oaClockMapper.updateOaClock(oaClock);
                                                                }
                                                            }else if("2".equals(oaEvectionTrip.getEndCycle())){
                                                                //如果跨天结束时间为最后一天的下午修改当天上下班打卡数据
                                                                    //打卡状态为出差
                                                                    oaClock.setClockState("9");
                                                                    oaClockMapper.updateOaClock(oaClock);
                                                            }
                                                        }
                                                    }else {
                                                        clock.setQueryTime(queryTime);
                                                        //clock.setFlag(1L);
                                                        List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                                                        //上班下班都要更改
                                                        for (OaClock oaClock:oaClocks) {
                                                            //打卡状态为出差
                                                            oaClock.setClockState("9");
                                                            oaClockMapper.updateOaClock(oaClock);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //每月1号1点半初始化汇总数据
        if(params.equals("month")){
            //日期格式化
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
            String month = format.format(new Date());
            List<String> userId = sysUserMapper.selectUserByHumanoid(null);
            for (String id:userId){
                SysUser user = sysUserMapper.selectUserById(Long.valueOf(id));
                //月度汇总
                OaClockSummary oaClockSummary = new OaClockSummary();
                oaClockSummary.setUserId(String.valueOf(user.getUserId()));
                //查看部门信息
                SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
                if(StringUtils.isNotNull(dept)){
                    oaClockSummary.setDeptId(String.valueOf(dept.getDeptId()));
                    if(!"0".equals(dept.getAttendanceLock())){
                        OaAttendanceGroup attendanceGroup = oaAttendanceGroupMapper.selectOaAttendanceGroupById(dept.getAttendanceLock());
                        oaClockSummary.setAttendanceId(attendanceGroup.getId());
                        oaClockSummary.setAttendanceName(attendanceGroup.getAttendanceName());
                    }
                }
                oaClockSummary.setId(getRedisIncreID.getId());
                oaClockSummary.setSummaryDate(month);
                //租户ID
                oaClockSummary.setTenantId(user.getComId());
                oaClockSummaryMapper.insertOaClockSummary(oaClockSummary);
            }
            //OA日历
            String date = month+"-"+"01";
            List<String> everyday = oaClockSummaryMapper.everyday(date);
            for (String day:everyday) {
                OaCalendar oaCalendar = new OaCalendar();
                oaCalendar.setId(getRedisIncreID.getId());
                oaCalendar.setDate(day);
                oaCalendar.setCreateTime(DateUtils.getNowDate());
                oaCalendarMapper.insertOaCalendar(oaCalendar);
            }
        }
        //每天更新打卡汇总数据(1点半执行)
        if(params.equals("clockSummary")){
            oaClockSummaryService.sameMonth();
        }

        //每天凌晨23点59上下班均无打卡数据状态更新为旷工
        if(params.equals("neglectWork")){
            //查询所有人
            List<String> list = sysUserMapper.selectUserByHumanoid(null);
            for (String userId:list) {
                //获取今天打卡班次
                SysUser user = sysUserMapper.selectUserById(Long.valueOf(userId));
                if("0".equals(user.getNeedNot())){
                    SysDept dept = sysDeptMapper.selectDeptById(user.getDeptId());
                    if(StringUtils.isNotNull(dept)) {
                        if (!"0".equals(dept.getAttendanceLock())) {
                            //查询打卡记录表中存在今天数据
                            OaClock clock = new OaClock();
                            clock.setQueryTime(queryTime);
                            clock.setEmployeeId(user.getUserId());
                            clock.setFlag(1L);
                            clock.setClockState("0");
                            List<OaClock> oaClocks = oaClockMapper.selectOaClockList(clock);
                            //判断上下班都未打卡改为旷工状态
                            if(oaClocks.size() == 2){
                                for (OaClock oaClock:oaClocks) {
                                    oaClock.setFlag(0L);
                                    oaClock.setClockState("11");
                                    oaClockMapper.updateOaClock(oaClock);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void jkParams(String params)
    {
        if(params.equals("min")){

        }
    }

    public void hkParams(){

        System.out.println("123132132456465456");
    }

    /**
     * 插入消息
     *
     * @return
     */
    private void insertMessage(String mId, String msgContent,String type) {
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType(type);
        baMessage.setCreateBy("定时任务");
        baMessage.setCreateTime(DateUtils.getNowDate());
        baMessageMapper.insertBaMessage(baMessage);
    }


    /**
     * 督办消息提醒
     *
     * @return
     */
    private void supervisingMessage(String mId, String msgContent,String type,String businessId,String messTitle) {
        BaMessage baMessage = new BaMessage();
        baMessage.setMsId("TZ" + getRedisIncreID.getId());
        baMessage.setMId(mId);
        baMessage.setMessContent(msgContent);
        baMessage.setSendTime(DateUtils.getNowDate());
        baMessage.setType(type);
        baMessage.setMessTitle(messTitle);
        baMessage.setCreateBy("定时任务");
        baMessage.setCreateTime(DateUtils.getNowDate());
        if(StringUtils.isNotEmpty(businessId)){
            baMessage.setBusinessId(businessId);
        }
        baMessageMapper.insertBaMessage(baMessage);
    }

    //两个数组对比拿出不同数据
    public static <T> List<T> compare(T[] t1, T[] t2) {
        List<T> list1 = Arrays.asList(t1);
        List<T> list2 = new ArrayList<T>();
        for (T t : t2) {
            if (!list1.contains(t)) {
                list2.add(t);
            }
        }
        return list2;
    }

    public void ryNoParams()
    {
        System.out.println("执行无参方法");
    }
}
