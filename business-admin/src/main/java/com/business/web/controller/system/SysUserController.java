package com.business.web.controller.system;

import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Anonymous;
import com.business.system.domain.dto.SysRoleUserByDeptDTO;
import com.business.system.domain.dto.SysUserRoleDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.business.common.annotation.Log;
import com.business.common.constant.UserConstants;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.domain.entity.SysRole;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.StringUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.service.ISysDeptService;
import com.business.system.service.ISysPostService;
import com.business.system.service.ISysRoleService;
import com.business.system.service.ISysUserService;

/**
 * 用户信息
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/system/user")
@Api(tags = "用户信息接口")
public class SysUserController extends BaseController
{
    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysDeptService deptService;

    @Autowired
    private ISysPostService postService;

    /**
     * 获取用户列表
     */
    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysUser user)
    {
        user.setComId(SecurityUtils.getCurrComId());
        startPage();
        List<SysUser> list = userService.selectUserList(user);
        return getDataTable(list);
    }

    /**
     * 组织结构获取用户列表
     */
    @GetMapping("/userList")
    @ApiOperation("组织结构获取用户列表")
    @Anonymous
    public TableDataInfo userList(SysUser user)
    {
        user.setComId(SecurityUtils.getCurrComId());
        startPage();
        List<SysUser> list = userService.userList(user);
        return getDataTable(list);
    }

    /**
     * 获取用户列表
     */
    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @GetMapping("/allUserList")
    public TableDataInfo allUserList(SysUser user)
    {
        startPage();
        List<SysUser> list = userService.selectAllUserList(user);
        return getDataTable(list);
    }

    @Log(title = "用户管理", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('system:user:export')")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysUser user)
    {
        List<SysUser> list = userService.selectUserList(user);
        ExcelUtil<SysUser> util = new ExcelUtil<SysUser>(SysUser.class);
        util.exportExcel(response, list, "用户数据");
    }

    @Log(title = "用户管理", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('system:user:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<SysUser> util = new ExcelUtil<SysUser>(SysUser.class);
        List<SysUser> userList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = userService.importUser(userList, updateSupport, operName);
        return success(message);
    }

    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<SysUser> util = new ExcelUtil<SysUser>(SysUser.class);
        util.importTemplateExcel(response, "用户数据");
    }

    /**
     * 根据用户编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:user:query')")
    @GetMapping(value = { "/", "/{userId}" })
    public AjaxResult getInfo(@PathVariable(value = "userId", required = false) Long userId)
    {
        userService.checkUserDataScope(userId);
        AjaxResult ajax = AjaxResult.success();
        List<SysRole> roles = roleService.selectRoleAll();
        ajax.put("roles", SysUser.isAdmin(userId) ? roles : roles.stream().filter(r -> !r.isAdmin()).collect(Collectors.toList()));
        if (StringUtils.isNotNull(userId))
        {
            SysUser sysUser = userService.selectUserById(userId);
            ajax.put(AjaxResult.DATA_TAG, sysUser);
            ajax.put("postIds", postService.selectPostListByUserId(userId));
            ajax.put("roleIds", sysUser.getRoles().stream().map(SysRole::getRoleId).collect(Collectors.toList()));
        }
        return ajax;
    }

    /**
     * 新增用户
     */
    @PreAuthorize("@ss.hasPermi('system:user:add')")
    @Log(title = "用户管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody SysUser user)
    {
        if (UserConstants.NOT_UNIQUE.equals(userService.checkUserNameUnique(user)))
        {
            return error("新增用户'" + user.getUserName() + "'失败，登录账号已存在");
        }else if(StringUtils.isNotEmpty(user.getJobNo())
                && UserConstants.NOT_UNIQUE.equals(userService.checkJobNoUnique(user)))
        {
            return error("新增用户'" + user.getUserName() + "'失败，工号已存在");
        }else if(StringUtils.isNotEmpty(user.getJobCode())
                && UserConstants.NOT_UNIQUE.equals(userService.checkJobCodeUnique(user))){
            return error("新增用户'" + user.getUserName() + "'失败，岗位编码已存在");
        }else if(StringUtils.isNotEmpty(user.getDeputyPostCode())
                && UserConstants.NOT_UNIQUE.equals(userService.checkDeputyPostCodeUnique(user))){
            return error("新增用户'" + user.getUserName() + "'失败，副岗位编码已存在");
        }
//        else if (StringUtils.isNotEmpty(user.getPhonenumber())
//                && UserConstants.NOT_UNIQUE.equals(userService.checkPhoneUnique(user)))
//        {
//            return error("新增用户'" + user.getUserName() + "'失败，手机号码已存在");
//        }
        else if (StringUtils.isNotEmpty(user.getEmail())
                && UserConstants.NOT_UNIQUE.equals(userService.checkEmailUnique(user)))
        {
            return error("新增用户'" + user.getUserName() + "'失败，邮箱账号已存在");
        }
        user.setCreateBy(getUsername());
        user.setPassword(SecurityUtils.encryptPassword(user.getPassword()));
        user.setComId(SecurityUtils.getCurrComId());
        user.setAdminFlag(0);
        return toAjax(userService.insertUser(user));
    }

    /**
     * 修改用户
     */
    @PreAuthorize("@ss.hasPermi('system:user:edit')")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody SysUser user)
    {
        userService.checkUserAllowed(user);
        userService.checkUserDataScope(user.getUserId());
        if (UserConstants.NOT_UNIQUE.equals(userService.checkUserNameUnique(user)))
        {
            return error("修改用户'" + user.getUserName() + "'失败，登录账号已存在");
        }else if(StringUtils.isNotEmpty(user.getJobNo())
                && UserConstants.NOT_UNIQUE.equals(userService.checkJobNoUnique(user)))
        {
            return error("修改用户'" + user.getUserName() + "'失败，工号已存在");
        }else if(StringUtils.isNotEmpty(user.getJobCode())
                && UserConstants.NOT_UNIQUE.equals(userService.checkJobCodeUnique(user))){
            return error("新增用户'" + user.getUserName() + "'失败，岗位编码已存在");
        }else if(StringUtils.isNotEmpty(user.getDeputyPostCode())
                && UserConstants.NOT_UNIQUE.equals(userService.checkDeputyPostCodeUnique(user))){
            return error("新增用户'" + user.getUserName() + "'失败，副岗位编码已存在");
        }
//        else if (StringUtils.isNotEmpty(user.getPhonenumber())
//                && UserConstants.NOT_UNIQUE.equals(userService.checkPhoneUnique(user)))
//        {
//            return error("修改用户'" + user.getUserName() + "'失败，手机号码已存在");
//        }
        else if (StringUtils.isNotEmpty(user.getEmail())
                && UserConstants.NOT_UNIQUE.equals(userService.checkEmailUnique(user)))
        {
            return error("修改用户'" + user.getUserName() + "'失败，邮箱账号已存在");
        }
        user.setUpdateBy(getUsername());
        return toAjax(userService.updateUser(user));
    }

    /**
     * 删除用户
     */
    @PreAuthorize("@ss.hasPermi('system:user:remove')")
    @Log(title = "用户管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable Long[] userIds)
    {
        if (ArrayUtils.contains(userIds, getUserId()))
        {
            return error("当前用户不能删除");
        }
        return toAjax(userService.deleteUserByIds(userIds));
    }

    /**
     * 重置密码
     */
    @PreAuthorize("@ss.hasPermi('system:user:resetPwd')")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PutMapping("/resetPwd")
    public AjaxResult resetPwd(@RequestBody SysUser user)
    {
        userService.checkUserAllowed(user);
        userService.checkUserDataScope(user.getUserId());
        user.setPassword(SecurityUtils.encryptPassword(user.getPassword()));
        user.setUpdateBy(getUsername());
        return toAjax(userService.resetPwd(user));
    }

    /**
     * 状态修改
     */
    @PreAuthorize("@ss.hasPermi('system:user:edit')")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public AjaxResult changeStatus(@RequestBody SysUser user)
    {
        userService.checkUserAllowed(user);
        userService.checkUserDataScope(user.getUserId());
        user.setUpdateBy(getUsername());
        return toAjax(userService.updateUserStatus(user));
    }

    /**
     * 根据用户编号获取授权角色
     */
    @PreAuthorize("@ss.hasPermi('system:user:query')")
    @GetMapping("/authRole/{userId}")
    public AjaxResult authRole(@PathVariable("userId") Long userId)
    {
        AjaxResult ajax = AjaxResult.success();
        SysUser user = userService.selectUserById(userId);
        List<SysRole> roles = roleService.selectRolesByUserId(userId);
        ajax.put("user", user);
        ajax.put("roles", SysUser.isAdmin(userId) ? roles : roles.stream().filter(r -> !r.isAdmin()).collect(Collectors.toList()));
        return ajax;
    }

    /**
     * 用户授权角色
     */
    @PreAuthorize("@ss.hasPermi('system:user:edit')")
    @Log(title = "用户管理", businessType = BusinessType.GRANT)
    @PutMapping("/authRole")
    public AjaxResult insertAuthRole(Long userId, Long[] roleIds)
    {
        userService.checkUserDataScope(userId);
        userService.insertUserAuth(userId, roleIds);
        return success();
    }

    /**
     * 获取部门树列表
     */
    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @GetMapping("/deptTree")
    public AjaxResult deptTree(SysDept dept)
    {
        dept.setTenantId(SecurityUtils.getCurrComId());
        return success(deptService.selectDeptTreeList(dept));
    }

    /**
     * 获取部门树Map
     */
    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @GetMapping("/deptTreeMap")
    public AjaxResult deptTreeMap(SysDept dept)
    {
        dept.setTenantId(SecurityUtils.getCurrComId());
        return success(deptService.selectDeptTreeMap(dept));
    }

    /**
     * 根据username获取用户信息
     */
    @ApiOperation("根据username获取用户信息")
//    @PreAuthorize("@ss.hasPermi('system:user:query')")
    @GetMapping(value = { "/userInfo/{userName}" })
    public AjaxResult getUserInfoByUserName(@PathVariable(value = "userName") String userName)
    {
        return success(userService.selectUserByUserName(userName));
    }

    /**
     * 根据userId获取用户信息
     */
    @ApiOperation("根据userId获取用户信息")
//    @PreAuthorize("@ss.hasPermi('system:user:query')")
    @GetMapping(value = { "/userInfo/{userId}" })
    public AjaxResult getUserInfoByUserId(@PathVariable(value = "userId") String userId)
    {
        return success(userService.selectUserById(Long.valueOf(userId)));
    }

    /**
     * 根据userId获取用户信息
     */
    @ApiOperation("根据Id获取用户信息")
//    @PreAuthorize("@ss.hasPermi('system:user:query')")
    @GetMapping(value = { "/userByUserId/{userId}" })
    @Anonymous
    public AjaxResult getUserByUserId(@PathVariable(value = "userId") String userId)
    {
        return success(userService.selectUserById(Long.valueOf(userId)));
    }

    /**
     * 根据用户部门ID集合查询负责部门角色用户列表
     */
    @ApiOperation("根据用户部门ID集合查询负责部门角色用户列表")
//    @PreAuthorize("@ss.hasPermi('system:user:query')")
    @PostMapping(value = { "/selectRoleUser" })
    public AjaxResult selectRoleUsersByUser(@RequestBody SysRoleUserByDeptDTO sysRoleUserByDeptDTO)
    {
        return success(userService.selectRoleUsersByUser(sysRoleUserByDeptDTO));
    }

    /**
     * 根据部门获取岗位信息
     */
    @PreAuthorize("@ss.hasPermi('system:user:query')")
    @GetMapping(value = {"/getPostByDeptId/{deptId}"})
    public AjaxResult getPostByDeptId(@PathVariable(value = "deptId", required = false) Long deptId)
    {
        return AjaxResult.success(postService.selectPostsByDeptId(deptId));
    }

    /**
     * 根据部门ID集合查询用户
     */
    @ApiOperation("根据Id获取用户信息")
//    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @PostMapping("/selectUsersByDeptIds")
    public AjaxResult selectUsersByDeptIds(@RequestBody Long[] deptIds) {
        return AjaxResult.success(userService.selectUsersByDeptIds(deptIds));
    }

    /**
     * 运营专员
     */
    @GetMapping(value = {"/operateUser"})
    @ApiOperation("运营专员")
    @Anonymous
    public AjaxResult operateUser(String comId)
    {
        return AjaxResult.success(userService.operateUser(comId));
    }
}
