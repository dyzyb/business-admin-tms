package com.business.web.controller.oa;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.OaSupplyClock;
import com.business.system.service.IOaSupplyClockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 补卡Controller
 *
 * @author js
 * @date 2023-11-27
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/supplyClock")
@Api(tags ="补卡接口")
public class OaSupplyClockController extends BaseController
{
    @Autowired
    private IOaSupplyClockService oaSupplyClockService;

    /**
     * 查询补卡列表
     */
//    @PreAuthorize("@ss.hasPermi('admin:supplyClock:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询补卡列表接口")
    public TableDataInfo list(OaSupplyClock oaSupplyClock)
    {
        //租户ID
        oaSupplyClock.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<OaSupplyClock> list = oaSupplyClockService.selectOaSupplyClockList(oaSupplyClock);
        return getDataTable(list);
    }

    /**
     * 导出补卡列表
     */
    @PreAuthorize("@ss.hasPermi('admin:supplyClock:export')")
    @Log(title = "补卡", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出补卡列表接口")
    public void export(HttpServletResponse response, OaSupplyClock oaSupplyClock) throws IOException
    {
        //租户ID
        oaSupplyClock.setTenantId(SecurityUtils.getCurrComId());
        List<OaSupplyClock> list = oaSupplyClockService.selectOaSupplyClockList(oaSupplyClock);
        ExcelUtil<OaSupplyClock> util = new ExcelUtil<OaSupplyClock>(OaSupplyClock.class);
        util.exportExcel(response, list, "supplyClock");
    }

    /**
     * 获取补卡详细信息
     */
//    @PreAuthorize("@ss.hasPermi('admin:supplyClock:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取补卡详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaSupplyClockService.selectOaSupplyClockById(id));
    }

    /**
     * 新增补卡
     */
//    @PreAuthorize("@ss.hasPermi('admin:supplyClock:add')")
//    @Log(title = "补卡", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增补卡接口")
    public AjaxResult add(@RequestBody OaSupplyClock oaSupplyClock)
    {
        int result = oaSupplyClockService.insertOaSupplyClock(oaSupplyClock);
        if(result == -1){
            return AjaxResult.error(-1,"请先进行上班补卡");
        } else if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 修改补卡
     */
//    @PreAuthorize("@ss.hasPermi('admin:supplyClock:edit')")
//    @Log(title = "补卡", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改补卡接口")
    public AjaxResult edit(@RequestBody OaSupplyClock oaSupplyClock)
    {
        int result = oaSupplyClockService.updateOaSupplyClock(oaSupplyClock);
        if(result == -1){
            return AjaxResult.error(-1,"请先进行上班补卡");
        } else if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 删除补卡
     */
//    @PreAuthorize("@ss.hasPermi('admin:supplyClock:remove')")
//    @Log(title = "补卡", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除补卡接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(oaSupplyClockService.deleteOaSupplyClockByIds(ids));
    }

    /**
     * 撤销按钮
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaOther:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = oaSupplyClockService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }
}
