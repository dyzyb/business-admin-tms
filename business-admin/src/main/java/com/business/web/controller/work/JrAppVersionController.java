package com.business.web.controller.work;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.system.domain.JrAppVersion;
import com.business.system.service.IJrAppVersionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * app版本Controller
 *
 * @author single
 * @date 2022-04-27
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/version")
@Api(tags ="app版本接口")
public class JrAppVersionController extends BaseController
{
    @Autowired
    private IJrAppVersionService jrAppVersionService;

    /**
     * 查询app版本列表
     */
    @GetMapping("/list")
    @ApiOperation(value = "查询app版本列表接口")
    public TableDataInfo list(JrAppVersion jrAppVersion)
    {
        startPage();
        List<JrAppVersion> list = jrAppVersionService.selectJrAppVersionList(jrAppVersion);
        return getDataTable(list);
    }

    /**
     * 导出app版本列表
     */
    /*@Log(title = "app版本", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出app版本列表接口")
    public void export(HttpServletResponse response, JrAppVersion jrAppVersion) throws IOException
    {
        List<JrAppVersion> list = jrAppVersionService.selectJrAppVersionList(jrAppVersion);
        ExcelUtil<JrAppVersion> util = new ExcelUtil<JrAppVersion>(JrAppVersion.class);
        util.exportExcel(response, list, "version");
    }*/

    /**
     * 获取app版本详细信息
     */
    @GetMapping(value = "/{versionId}")
    @ApiOperation(value = "获取app版本详细信息接口")
    public AjaxResult getInfo(@PathVariable("versionId") String versionId)
    {
        return AjaxResult.success(jrAppVersionService.selectJrAppVersionById(versionId));
    }

    /**
     * 新增app版本
     */
    @Log(title = "app版本", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增app版本接口")
    public AjaxResult add(@RequestBody JrAppVersion jrAppVersion)
    {
        return toAjax(jrAppVersionService.insertJrAppVersion(jrAppVersion));
    }

    /**
     * 修改app版本
     */
    @Log(title = "app版本", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改app版本接口")
    public AjaxResult edit(@RequestBody JrAppVersion jrAppVersion)
    {
        return toAjax(jrAppVersionService.updateJrAppVersion(jrAppVersion));
    }

    /**
     * 删除app版本
     */
    /*@Log(title = "app版本", businessType = BusinessType.DELETE)
	@DeleteMapping("/{versionIds}")
    @ApiOperation(value = "删除app版本接口")
    public AjaxResult remove(@PathVariable String[] versionIds)
    {
        return toAjax(jrAppVersionService.deleteJrAppVersionByIds(versionIds));
    }*/
}
