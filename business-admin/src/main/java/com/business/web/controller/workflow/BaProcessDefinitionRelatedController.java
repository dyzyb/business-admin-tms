package com.business.web.controller.workflow;

import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.AdminCodeEnum;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.dto.BaProcessDefinitionRelatedDeleteDTO;
import com.business.system.domain.dto.BaProcessDefinitionRelatedQueryDTO;
import com.business.system.domain.dto.BaProcessDefinitionRelatedSaveDTO;
import com.business.system.domain.vo.BaProcessDefinitionRelatedVO;
import com.business.system.service.IBaProcessDefinitionRelatedService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: js
 * @Description: 业务与流程定义关联前端控制类
 * @date: 2022-12-2 14:08:18
 */
@RestController
@RequestMapping("/workflow/definition")
@Api(tags = "审批流程配置接口")
public class BaProcessDefinitionRelatedController extends BaseController {

    @Autowired
    private IBaProcessDefinitionRelatedService iBaProcessDefinitionRelatedService;

    @ApiOperation("分页查询流程定义列表")
    @GetMapping("/listPage")
    public TableDataInfo listProcessDefinitionPage(BaProcessDefinitionRelatedQueryDTO queryDTO) {
        queryDTO.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaProcessDefinitionRelatedVO> baProcessDefinitionRelatedVOS = iBaProcessDefinitionRelatedService.listProcessDefinitionPage(queryDTO);
        return getDataTable(baProcessDefinitionRelatedVOS);
    }

    @ApiOperation("业务绑定流程保存")
    @PostMapping("/saveProcessDefinition")
    public AjaxResult saveProcessDefinition(@RequestBody BaProcessDefinitionRelatedSaveDTO saveDTO) {
        return success(iBaProcessDefinitionRelatedService.saveProcessDefinition(saveDTO));
    }

    @ApiOperation("删除流程")
    @DeleteMapping("/deleteProcessDefinition")
    public void deleteProcessDefinition(BaProcessDefinitionRelatedDeleteDTO deleteDTO) {
        iBaProcessDefinitionRelatedService.deleteProcessDefinition(deleteDTO);
    }

    @GetMapping("/remove/{id}")
    @ApiOperation(value = "根据模板ID删除流程")
    public AjaxResult remove(@PathVariable("id") String id)
    {
        return toAjax(iBaProcessDefinitionRelatedService.deleteProcessDefinitionById(id));
    }

    @ApiOperation("根据模板ID查询模板数据")
    @GetMapping("/getProcessDefinitionRelated/{templateId}")
    public AjaxResult getProcessDefinitionRelated(@PathVariable("templateId") String templateId) {
        BaProcessDefinitionRelatedVO baProcessDefinitionRelatedVO = iBaProcessDefinitionRelatedService.getProcessDefinitionRelated(templateId);
        return success(baProcessDefinitionRelatedVO);
    }
}
