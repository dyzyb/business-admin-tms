package com.business.web.controller.work;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.business.common.annotation.Anonymous;
import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.system.domain.BaShippingOrder;
import com.business.system.service.IBaShippingOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * 发运单Controller
 *
 * @author single
 * @date 2023-02-18
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/shippingOrder")
@Api(tags ="发运单接口")
public class BaShippingOrderController extends BaseController
{
    @Autowired
    private IBaShippingOrderService baShippingOrderService;


    /**
     * 查询发运单列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:shippingOrder:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询发运单列表接口")
    public TableDataInfo list(BaShippingOrder baShippingOrder)
    {
        startPage();
        List<BaShippingOrder> list = baShippingOrderService.selectBaShippingOrderList(baShippingOrder);
        return getDataTable(list);
    }


    /**
     * 导出发运单列表
     */
    /*@PreAuthorize("@ss.hasPermi('admin:shippingOrder:export')")
    @Log(title = "发运单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出发运单列表接口")
    public void export(HttpServletResponse response, BaShippingOrder baShippingOrder) throws IOException
    {
        List<BaShippingOrder> list = baShippingOrderService.selectBaShippingOrderList(baShippingOrder);
        ExcelUtil<BaShippingOrder> util = new ExcelUtil<BaShippingOrder>(BaShippingOrder.class);
        util.exportExcel(response, list, "shippingOrder");
    }*/

    /**
     * 获取发运单详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:shippingOrder:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取发运单详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baShippingOrderService.selectBaShippingOrderById(id));
    }

    /**
     * 新增发运单
     */
    //@PreAuthorize("@ss.hasPermi('admin:shippingOrder:add')")
    //@Log(title = "发运单", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增发运单接口")
    @Anonymous
    public AjaxResult add(@RequestBody JSONObject object)
    {
        System.out.println("****运单信息**** : " + JSONObject.toJSONString(object));
        /*Date date = new Date(object.getLong("yfdate"));
        System.out.println("****时间**** : " + date);*/
        BaShippingOrder baShippingOrder = new BaShippingOrder();
        //实收吨数（存在）
        baShippingOrder.setSsdun(object.getBigDecimal("ssdun"));
        //运单编号（存在）
        baShippingOrder.setThdno(object.getString("thdno"));
        //发货详细地址（存在）
        baShippingOrder.setDepDetailed(object.getString("depDetailed"));
        //运单id
        baShippingOrder.setId(object.getString("thdno"));
        //车号（存在）
        baShippingOrder.setCarno(object.getString("carno"));
        //车主电话（存在）
        baShippingOrder.setCartel(object.getString("cartel"));
        //原发时间（存在）
        baShippingOrder.setYfdate(object.getDate("yfdate"));
        //进场时间（存在）
        baShippingOrder.setDhdate(object.getDate("dhdate"));
        //装车磅单图片（存在）
        baShippingOrder.setVidpic1(object.getString("vidpic1"));
        //卸车磅单图片（存在）
        baShippingOrder.setVidpic2(object.getString("vidpic2"));
        //出场净重（存在）
        baShippingOrder.setCcjweight(object.getBigDecimal("ccjweight"));
        //入场净重（存在）
        baShippingOrder.setRcjweight(object.getBigDecimal("rcjweight"));
        //磅差（存在）
        baShippingOrder.setBangcha(object.getBigDecimal("bangcha"));
        //涨吨
        baShippingOrder.setZhangdun(object.getBigDecimal("zhangdun"));
        //实收吨数（存在）
        baShippingOrder.setSsdun(object.getBigDecimal("ssdun"));
        //运费结算重量（存在）
        baShippingOrder.setYfjsweight(object.getBigDecimal("yfjsweight"));
        //货主货源应付运费（存在）
        baShippingOrder.setYfratesmoney(object.getBigDecimal("yfratesmoney"));
        //货主运费扣款金额（存在）
        baShippingOrder.setRateskoumoney(object.getBigDecimal("rateskoumoney"));
        //涨吨金额（存在）
        baShippingOrder.setZdmoney(object.getBigDecimal("zdmoney"));
        //经济人货源运费扣款金额（存在）
        baShippingOrder.setRateskoumoneyEconomic(object.getBigDecimal("rateskoumoneyEconomic"));
        //货源单编号（存在）
        baShippingOrder.setSupplierNo(object.getString("vidpic8"));
        //货主运费单价（存在）
        baShippingOrder.setRatesprice(object.getBigDecimal("ratesprice"));
        //货物名称（存在）
        baShippingOrder.setCoaltypename(object.getString("coaltypename"));
        //是否结算运费（存在）
        baShippingOrder.setIsjsratesflag(object.getInteger("isjsratesflag"));
        //实际结算金额（存在）
        baShippingOrder.setSfratesmoney(object.getBigDecimal("sfratesmoney"));
        //信息费（存在）
        baShippingOrder.setInfocost(object.getBigDecimal("infocost"));
        //其他扣款（存在）
        baShippingOrder.setRatesdikoumoney(object.getBigDecimal("ratesdikoumoney"));
        //收货单位（存在）
        baShippingOrder.setFhunitname(object.getString("fhunitname"));
        //发货单位（存在）
        baShippingOrder.setShunitname(object.getString("shunitname"));
        //驾驶号（存在）
        baShippingOrder.setDriverno(object.getString("driverno"));
        //司机名称
        baShippingOrder.setCardriver(object.getString("cardriver"));
        return toAjax(baShippingOrderService.insertBaShippingOrder(baShippingOrder));
    }


    /**
     * 修改发运单
     */
    @PreAuthorize("@ss.hasPermi('admin:shippingOrder:edit')")
    @Log(title = "发运单", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改发运单接口")
    public AjaxResult edit(@RequestBody BaShippingOrder baShippingOrder)
    {
        return toAjax(baShippingOrderService.updateBaShippingOrder(baShippingOrder));
    }

    /**
     * 签收同时修改运单
     * @param jsonObject
     * @return
     */
    @PostMapping("/editShippingOrder")
    @ApiOperation(value = "昕科修改发运单接口")
    @Anonymous
    public AjaxResult editShippingOrder(@RequestBody JSONObject jsonObject)
    {
        return toAjax(baShippingOrderService.updateShippingOrder(jsonObject));
    }


    /**
     * 删除发运单
     */
    @PreAuthorize("@ss.hasPermi('admin:shippingOrder:remove')")
    @Log(title = "发运单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除发运单接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baShippingOrderService.deleteBaShippingOrderByIds(ids));
    }

    /**
     * 运单签收
     */
    @PreAuthorize("@ss.hasPermi('admin:shippingOrder:sign')")
    @GetMapping("/sign/{ids}")
    @ApiOperation(value = "签收")
    public AjaxResult sign(@PathVariable String[] ids){
        int result = baShippingOrderService.signFor(ids);
        if(result == 0){
            return AjaxResult.error("未入住网货平台",result);
        }
        return toAjax(result);
    }

    /**
     * 发运单签收回调
     */
    //@PreAuthorize("@ss.hasPermi('admin:shippingOrder:edit')")
    @Log(title = "发运单签收回调", businessType = BusinessType.UPDATE)
    @PostMapping("/editSignForCallback")
    @ApiOperation(value = "发运单签收回调")
    @Anonymous
    public AjaxResult editSignForCallback(@RequestBody List<String> transNos)
    {
        return toAjax(baShippingOrderService.editSignForCallback(transNos));
    }

}
