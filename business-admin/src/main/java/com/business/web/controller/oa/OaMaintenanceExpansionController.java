package com.business.web.controller.oa;

import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.OaMaintenanceExpansion;
import com.business.system.domain.dto.OaMaintenanceExpansionInstanceRelatedEditDTO;
import com.business.system.domain.dto.UserMaintenanceExpansionDTO;
import com.business.system.service.IOaMaintenanceExpansionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.business.common.annotation.Log;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 业务维护/拓展Controller
 *
 * @author ljb
 * @date 2023-11-13
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/maintenanceExpansion")
@Api(tags ="业务维护/拓展接口")
public class OaMaintenanceExpansionController extends BaseController
{
    @Autowired
    private IOaMaintenanceExpansionService oaMaintenanceExpansionService;

    /**
     * 查询业务维护/拓展列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:maintenanceExpansion:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询业务维护/拓展列表接口")
    public TableDataInfo list(OaMaintenanceExpansion oaMaintenanceExpansion)
    {
        //租户ID
        oaMaintenanceExpansion.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<OaMaintenanceExpansion> list = oaMaintenanceExpansionService.selectOaMaintenanceExpansionList(oaMaintenanceExpansion);
        return getDataTable(list);
    }

    /**
     * 导出业务维护/拓展列表
     */
    @PreAuthorize("@ss.hasPermi('admin:maintenanceExpansion:export')")
    @Log(title = "业务维护/拓展", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出业务维护/拓展列表接口")
    public void export(HttpServletResponse response, OaMaintenanceExpansion oaMaintenanceExpansion) throws IOException
    {
        //租户ID
        oaMaintenanceExpansion.setTenantId(SecurityUtils.getCurrComId());
        List<OaMaintenanceExpansion> list = oaMaintenanceExpansionService.selectOaMaintenanceExpansionList(oaMaintenanceExpansion);
        ExcelUtil<OaMaintenanceExpansion> util = new ExcelUtil<OaMaintenanceExpansion>(OaMaintenanceExpansion.class);
        util.exportExcel(response, list, "maintenanceExpansion");
    }

    /**
     * 获取业务维护/拓展详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:maintenanceExpansion:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取业务维护/拓展详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaMaintenanceExpansionService.selectOaMaintenanceExpansionById(id));
    }

    /**
     * 新增业务维护/拓展
     */
   /* @PreAuthorize("@ss.hasPermi('admin:maintenanceExpansion:add')")
    @Log(title = "业务维护/拓展", businessType = BusinessType.INSERT)*/
    @PostMapping
    @ApiOperation(value = "新增业务维护/拓展接口")
    public AjaxResult add(@RequestBody OaMaintenanceExpansion oaMaintenanceExpansion)
    {
        int result = oaMaintenanceExpansionService.insertOaMaintenanceExpansion(oaMaintenanceExpansion);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 修改业务维护/拓展
     */
    /*@PreAuthorize("@ss.hasPermi('admin:maintenanceExpansion:edit')")
    @Log(title = "业务维护/拓展", businessType = BusinessType.UPDATE)*/
    @PutMapping
    @ApiOperation(value = "修改业务维护/拓展接口")
    public AjaxResult edit(@RequestBody OaMaintenanceExpansion oaMaintenanceExpansion)
    {
        int result = oaMaintenanceExpansionService.updateOaMaintenanceExpansion(oaMaintenanceExpansion);
        if(result == 0){
            return AjaxResult.error(0,"流程启动失败");
        }
        return toAjax(result);
    }

    /**
     * 删除业务维护/拓展
     */
    /*@PreAuthorize("@ss.hasPermi('admin:maintenanceExpansion:remove')")
    @Log(title = "业务维护/拓展", businessType = BusinessType.DELETE)*/
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除业务维护/拓展接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(oaMaintenanceExpansionService.deleteOaMaintenanceExpansionByIds(ids));
    }

    /**
     * 撤销按钮
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaLeave:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = oaMaintenanceExpansionService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回流程实例失败");
        }
        return toAjax(result);
    }

    @ApiOperation("审批办理保存编辑业务数据")
    @PostMapping("/saveApproveBusinessData")
    public AjaxResult saveApproveBusinessData(@RequestBody OaMaintenanceExpansionInstanceRelatedEditDTO oaMaintenanceExpansionInstanceRelatedEditDTO) {
        int result = oaMaintenanceExpansionService.updateProcessBusinessData(oaMaintenanceExpansionInstanceRelatedEditDTO);
        if(result == 0){
            return AjaxResult.error(0,"任务办理失败");
        }
        return toAjax(result);
    }

    //统计
    @GetMapping("/submitUser")
    @ApiOperation(value = "统计")
    public TableDataInfo submitUser(OaMaintenanceExpansion oaMaintenanceExpansion)
    {
        //租户ID
        oaMaintenanceExpansion.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<UserMaintenanceExpansionDTO> list = oaMaintenanceExpansionService.submitUser(oaMaintenanceExpansion);
        return getDataTable(list);
    }
}
