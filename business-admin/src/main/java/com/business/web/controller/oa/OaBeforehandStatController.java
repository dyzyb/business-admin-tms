package com.business.web.controller.oa;

import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.OaBeforehandStat;
import com.business.system.domain.dto.OaClockDTO;
import com.business.system.service.IOaBeforehandStatService;
import com.business.system.service.IOaClockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 事前申请统计Controller
 *
 * @author js
 * @date 2023-12-15
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/beforehand")
@Api(tags ="打卡统计接口")
public class OaBeforehandStatController extends BaseController
{
    @Autowired
    private IOaBeforehandStatService oaBeforehandStatService;

    /**
     * 事前申请统计
     */
//    @PreAuthorize("@ss.hasPermi('admin:clock:query')")
    @PostMapping(value = "/beforehandStat")
    @ApiOperation(value = "事前申请统计")
    public AjaxResult beforehandStat(@RequestBody OaBeforehandStat oaBeforehandStat)
    {
        return AjaxResult.success(oaBeforehandStatService.beforehandStat(oaBeforehandStat));
    }

    /**
     * 申请部门分布
     */
//    @PreAuthorize("@ss.hasPermi('admin:clock:query')")
    @PostMapping(value = "/applyDeptStat")
    @ApiOperation(value = "申请部门分布")
    public AjaxResult applyDeptStat(@RequestBody OaBeforehandStat oaBeforehandStat)
    {
        //租户ID
        oaBeforehandStat.setTenantId(SecurityUtils.getCurrComId());
        return AjaxResult.success(oaBeforehandStatService.applyDeptStat(oaBeforehandStat));
    }
}
