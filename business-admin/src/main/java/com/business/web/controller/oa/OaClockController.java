package com.business.web.controller.oa;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.UR;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.OaClock;
import com.business.system.service.IOaClockService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 打卡Controller
 *
 * @author single
 * @date 2023-11-27
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/clock")
@Api(tags ="打卡接口")
public class OaClockController extends BaseController
{
    @Autowired
    private IOaClockService oaClockService;

    /**
     * 查询打卡列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:clock:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询打卡列表接口")
    public TableDataInfo list(OaClock oaClock)
    {
        //租户ID
        oaClock.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<OaClock> list = oaClockService.selectOaClockList(oaClock);
        return getDataTable(list);
    }

    /**
     * 导出打卡列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:clock:export')")
    @Log(title = "打卡", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出打卡列表接口")
    public void export(HttpServletResponse response, OaClock oaClock) throws IOException
    {
        //租户ID
        oaClock.setTenantId(SecurityUtils.getCurrComId());
        List<OaClock> list = oaClockService.selectOaClockList(oaClock);
        ExcelUtil<OaClock> util = new ExcelUtil<OaClock>(OaClock.class);
        util.exportExcel(response, list, "clock");
    }

    /**
     * 获取打卡详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:clock:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取打卡详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaClockService.selectOaClockById(id));
    }

    /**
     * 新增打卡
     */
    //@PreAuthorize("@ss.hasPermi('admin:clock:add')")
    @Log(title = "打卡", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增打卡接口")
    public UR add(@RequestBody OaClock oaClock)
    {
        UR ur = oaClockService.insertOaClock(oaClock);
        return ur;
    }

    /**
     * 修改打卡
     */
    //@PreAuthorize("@ss.hasPermi('admin:clock:edit')")
    @Log(title = "打卡", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改打卡接口")
    public AjaxResult edit(@RequestBody OaClock oaClock)
    {
        return toAjax(oaClockService.updateOaClock(oaClock));
    }

    /**
     * 删除打卡
     */
    //@PreAuthorize("@ss.hasPermi('admin:clock:remove')")
    @Log(title = "打卡", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除打卡接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(oaClockService.deleteOaClockByIds(ids));
    }
}
