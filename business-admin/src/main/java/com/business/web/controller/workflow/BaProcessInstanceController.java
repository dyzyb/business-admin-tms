package com.business.web.controller.workflow;

import com.business.common.core.domain.AjaxResult;
import com.business.system.domain.dto.BaProcessDefinitionDTO;
import com.business.system.domain.dto.ProcessInstanceApproveLinkDTO;
import com.business.system.service.IBaProcessInstanceService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static com.business.common.core.domain.AjaxResult.success;

/**
 * @version 1.0
 * @Author: js
 * @Description: 流程实例前端控制器接口
 * @Date: 2022/12/9 18:16
 */
@RestController
@RequestMapping("/process/instance")
public class BaProcessInstanceController {

    @Autowired
    private IBaProcessInstanceService iBaProcessInstanceService;

    @ApiOperation("查询流程实例审批链路信息")
    @GetMapping("/listInstanceApproveLink")
    public AjaxResult listInstanceApproveLink(@ModelAttribute ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO) {
        return success(iBaProcessInstanceService.listInstanceApproveLink(processInstanceApproveLinkDTO));
    }

    @ApiOperation("查询流程定义审批链路信息")
    @GetMapping("/listDefinitionApproveLink")
    public AjaxResult listDefinitionApproveLink(@ModelAttribute BaProcessDefinitionDTO baProcessDefinitionDTO) {
        return success(iBaProcessInstanceService.listDefinitionApproveLink(baProcessDefinitionDTO));
    }

    @ApiOperation("查询流程当前节点审批链路信息")
    @GetMapping("/getCurrentNodeApproveLink")
    public AjaxResult getCurrentNodeApproveLink(@ModelAttribute ProcessInstanceApproveLinkDTO processInstanceApproveLinkDTO) {
        return success(iBaProcessInstanceService.getCurrentNodeApproveLink(processInstanceApproveLinkDTO));
    }
}
