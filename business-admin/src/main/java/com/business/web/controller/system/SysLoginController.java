package com.business.web.controller.system;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import com.business.common.core.domain.entity.SysDept;
import com.business.common.core.redis.RedisCache;
import com.business.common.utils.StringUtils;
import com.business.system.domain.SysCompany;
import com.business.system.mapper.SysCompanyMapper;
import com.business.system.mapper.SysDeptMapper;
import com.business.system.mapper.SysUserMapper;
import com.business.system.service.ISysUserService;
import com.business.system.service.impl.SysUserServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.business.common.constant.Constants;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysMenu;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.domain.model.LoginBody;
import com.business.common.utils.SecurityUtils;
import com.business.framework.web.service.SysLoginService;
import com.business.framework.web.service.SysPermissionService;
import com.business.system.service.ISysMenuService;

/**
 * 登录验证
 *
 * @author ruoyi
 */
@RestController
@Api(tags ="登录验证")
public class SysLoginController
{

    @Value("${name:default_name}")
    private String name;

    @Autowired
    private SysLoginService loginService;

    @Autowired
    private ISysMenuService menuService;

    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private SysDeptMapper sysDeptMapper;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private SysCompanyMapper sysCompanyMapper;

    @Autowired
    private ISysUserService userService;

    @GetMapping("/refresh")
    public String getConfigName(){
        return "name is :" + name;
    }

    /**
     * 登录方法
     *
     * @param loginBody 登录信息
     * @return 结果
     */
    @PostMapping("/login")
    public AjaxResult login(@RequestBody LoginBody loginBody)
    {
        System.out.println("*********** name is :" + name);
        //用户名称获取用户信息
        SysUser user = userService.selectUserByUserName(loginBody.getUsername());
        if(StringUtils.isNotNull(user)){
            if(StringUtils.isNotEmpty(user.getPlateMark())){
                String[] split = user.getPlateMark().split(",");
                List<String> list = Arrays.asList(split);
                if(!list.contains("1")){
                    loginBody.setUsername("123456");
                }
            }
        }
        AjaxResult ajax = AjaxResult.success();
        // 生成令牌
        String token = loginService.login(loginBody.getUsername(), loginBody.getPassword(), loginBody.getCode(),
                loginBody.getUuid());
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }

    /**
     * 登录方法
     *
     * @param loginBody 登录信息
     * @return 结果
     */
    @PostMapping("/appLogin")
    public AjaxResult appLogin(@RequestBody LoginBody loginBody)
    {
        System.out.println("*********** name is :" + name);
        AjaxResult ajax = AjaxResult.success();
        // 生成令牌
        String token = loginService.appLogin(loginBody.getUsername(), loginBody.getPassword(), loginBody.getCode(),
                loginBody.getUuid());
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }

    /**
     * 绕过验证码登录方法
     *
     * @param loginBody 登录信息
     * @return 结果
     */
    @PostMapping("/getToken")
    public AjaxResult getToken(@RequestBody LoginBody loginBody)
    {
        System.out.println("*********** name is :" + name);
        AjaxResult ajax = AjaxResult.success();
        // 生成令牌
        String token = loginService.noCodeLogin(loginBody.getUsername(), loginBody.getPassword());
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }

    /**
     * 获取用户信息
     *
     * @return 用户信息
     */
    @GetMapping("getInfo")
    public AjaxResult getInfo()
    {
        SysUser user = SecurityUtils.getLoginUser().getUser();
        if(user.getPartDeptId() != null && !"".equals(user.getPartDeptId())){
            SysDept dept = sysDeptMapper.selectDeptById(Long.valueOf(user.getPartDeptId()));
            String ancestors = dept.getAncestors()+","+user.getDept().getAncestors()+","+user.getPartDeptId()+","+user.getDept().getDeptId();
            user.setAncestors(ancestors);
        } else {
            if(user.getDeptId() != null){
                user.setAncestors(user.getDept().getAncestors()+","+user.getDept().getDeptId());
            }
        }
        // 角色集合
        Set<String> roles = permissionService.getRolePermission(user);
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(user);
        AjaxResult ajax = AjaxResult.success();
        if(!ObjectUtils.isEmpty(user)){
            SysCompany sysCompany = sysCompanyMapper.selectSysCompanyById(user.getComId());
            if(!ObjectUtils.isEmpty(sysCompany)){
                user.setAnnex(sysCompany.getAnnex());
            }
        }
        ajax.put("user", user);
        ajax.put("roles", roles);
        ajax.put("permissions", permissions);
        return ajax;
    }

    /**
     * 获取路由信息
     *
     * @return 路由信息
     */
    @GetMapping("getRouters")
    public AjaxResult getRouters()
    {
        Long userId = SecurityUtils.getUserId();
        SysMenu sysMenu = new SysMenu();
        sysMenu.setUserId(userId);
        List<SysMenu> menus = menuService.selectMenuTreeByUserId(sysMenu);
        return AjaxResult.success(menuService.buildMenus(menus));
    }

    /**
     * 拼接首页工作台
     */
    @GetMapping("/homePage")
    @ApiOperation(value = "首页工作台")
    public AjaxResult homePage()
    {
        Long userId = SecurityUtils.getUserId();
        SysMenu sysMenu = new SysMenu();
        sysMenu.setUserId(userId);
        sysMenu.setVisible("0");
        sysMenu.setParentId(0L);
        List<SysMenu> menus = menuService.selectMenuTreeByUserId(sysMenu);
        for (SysMenu menu:menus) {
                sysMenu.setParentId(menu.getMenuId());
                List<SysMenu> sysMenus = menuService.menuTreeByUserId(sysMenu);
                for (SysMenu sysMenu1:sysMenus) {
                    sysMenu1.setPath(menu.getPath()+"/"+sysMenu1.getPath());
                }
                menu.setChildren(sysMenus);
                for (SysMenu menu1:sysMenus) {
                    sysMenu.setParentId(menu1.getMenuId());
                    List<SysMenu> sysMenuList = menuService.menuTreeByUserId(sysMenu);
                    for (SysMenu sysMenu1:sysMenuList) {
                        sysMenu1.setPath(menu1.getPath()+"/"+sysMenu1.getPath());
                    }
                    menu1.setChildren(sysMenuList);
                    for (SysMenu sysMenu1:sysMenuList) {
                        sysMenu.setParentId(sysMenu1.getMenuId());
                        List<SysMenu> sysMenus1 = menuService.menuTreeByUserId(sysMenu);
                        for (SysMenu sysMenu2:sysMenus1) {
                            sysMenu2.setPath(sysMenu1.getPath()+"/"+sysMenu2.getPath());
                        }
                        sysMenu1.setChildren(sysMenus1);
                    }
                }
            }
        //剔除待办管理
        for (SysMenu menu:menus) {
            if(menu.getMenuId() == 2023){
                menus.remove(menu);
                break;
            }
        }
        return AjaxResult.success(menus);
    }

    /**
     * 下拉菜单
     */
    @GetMapping("/pullDown")
    @ApiOperation(value = "首页下拉菜单")
    public AjaxResult pullDown(){
        Long userId = SecurityUtils.getUserId();
        SysMenu sysMenu = new SysMenu();
        sysMenu.setUserId(userId);
        List<SysMenu> sysMenus = menuService.pullDown(sysMenu);
        for (SysMenu sysMenu1:sysMenus) {
            if(sysMenu1.getPath().equals("train")){
                sysMenu1.setMenuName("已发火运");
            }
            if(sysMenu1.getPath().equals("automobile")){
                sysMenu1.setMenuName("已发汽运");
            }
        }
        return AjaxResult.success(sysMenus);
    }
}
