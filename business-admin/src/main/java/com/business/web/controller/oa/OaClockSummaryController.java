package com.business.web.controller.oa;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.dto.DayClockListDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.OaClockSummary;
import com.business.system.service.IOaClockSummaryService;
/**
 * 打卡汇总Controller
 *
 * @author single
 * @date 2023-12-11
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/clockSummary")
@Api(tags ="打卡汇总接口")
public class OaClockSummaryController extends BaseController
{
    @Autowired
    private IOaClockSummaryService oaClockSummaryService;

    /**
     * 查询打卡汇总列表
     */
    @GetMapping("/list")
    @ApiOperation(value = "查询打卡汇总列表接口")
    public TableDataInfo list(OaClockSummary oaClockSummary)
    {
        //租户ID
        oaClockSummary.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<OaClockSummary> list = oaClockSummaryService.selectOaClockSummaryList(oaClockSummary);
        return getDataTable(list);
    }


    /**
     * 导出打卡汇总列表
     */
    @PostMapping("/export")
    @ApiOperation(value = "导出打卡汇总列表接口")
    public void export(HttpServletResponse response, OaClockSummary oaClockSummary) throws IOException
    {
        //租户ID
        oaClockSummary.setTenantId(SecurityUtils.getCurrComId());
        List<OaClockSummary> list = oaClockSummaryService.selectOaClockSummaryList(oaClockSummary);
        ExcelUtil<OaClockSummary> util = new ExcelUtil<OaClockSummary>(OaClockSummary.class);
        util.exportExcel(response, list, "clockSummary");
    }


    /**
     * 获取打卡汇总详细信息
     */
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取打卡汇总详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaClockSummaryService.selectOaClockSummaryById(id));
    }

    /**
     * 新增打卡汇总
     */
    @PostMapping
    @ApiOperation(value = "新增打卡汇总接口")
    public AjaxResult add(@RequestBody OaClockSummary oaClockSummary)
    {
        return toAjax(oaClockSummaryService.insertOaClockSummary(oaClockSummary));
    }

    /**
     * 修改打卡汇总
     */
    @PutMapping
    @ApiOperation(value = "修改打卡汇总接口")
    public AjaxResult edit(@RequestBody OaClockSummary oaClockSummary)
    {
        return toAjax(oaClockSummaryService.updateOaClockSummary(oaClockSummary));
    }

    /**
     * 删除打卡汇总
     */
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除打卡汇总接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(oaClockSummaryService.deleteOaClockSummaryByIds(ids));
    }

    /**
     * 获取当月打卡数据统计
     */
    @GetMapping("/sameMonth")
    @ApiOperation(value = "获取当月打卡数据统计")
    public AjaxResult sameMonth()
    {
        return toAjax(oaClockSummaryService.sameMonth());
    }


    @GetMapping("/dailyStatistics")
    @ApiOperation(value = "查询打卡日统计列表")
    public TableDataInfo dailyStatistics(DayClockListDTO dayClockListDTO)
    {
        //租户ID
        dayClockListDTO.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<DayClockListDTO> list = oaClockSummaryService.dailyStatistics(dayClockListDTO);
        return getDataTable(list);
    }

    /**
     * 导出打卡汇总列表
     */
    @PostMapping("/dailyStatisticsExport")
    @ApiOperation(value = "导出打卡汇总列表接口")
    public void dailyStatisticsExport(HttpServletResponse response, DayClockListDTO dayClockListDTO) throws IOException
    {
        //租户ID
        dayClockListDTO.setTenantId(SecurityUtils.getCurrComId());
        List<DayClockListDTO> list = oaClockSummaryService.dailyStatistics(dayClockListDTO);
        ExcelUtil<DayClockListDTO> util = new ExcelUtil<DayClockListDTO>(DayClockListDTO.class);
        util.exportExcel(response, list, "日统计导出");
    }

}
