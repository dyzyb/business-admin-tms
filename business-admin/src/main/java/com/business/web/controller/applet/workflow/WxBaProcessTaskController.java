package com.business.web.controller.applet.workflow;

import com.business.common.constant.BusinessConstant;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.PageDomain;
import com.business.common.core.page.TableDataInfo;
import com.business.common.core.page.TableSupport;
import com.business.common.utils.SecurityUtils;
import com.business.system.domain.dto.BaProcessInstanceRelatedQueryDTO;
import com.business.system.domain.dto.ProcessTaskDTO;
import com.business.system.domain.dto.ProcessTaskQueryDTO;
import com.business.system.service.IBaProcessTaskService;
import com.business.system.service.ISysUserService;
import com.business.system.service.wx.IWxBaProcessTaskService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * @version 1.0
 * @Author: js
 * @Description: 小程序审批任务前端控制器
 * @Date: 2023/04/25 16:44
 */
@RestController
@RequestMapping("/wx/workflow/task")
@Api(tags = "审批任务接口")
public class WxBaProcessTaskController extends BaseController {

    @Autowired
    private IWxBaProcessTaskService iWxBaProcessTaskService;

    @Autowired
    private ISysUserService iSysUserService;

    @ApiOperation("查询我的申请列表")
    @GetMapping("/myApplyList")
    public TableDataInfo myApplyList(BaProcessInstanceRelatedQueryDTO queryDTO) {
        startPage();
        //设置发起人ID
        queryDTO.setCreateBy(String.valueOf(SecurityUtils.getUsername()));
        return getDataTable(iWxBaProcessTaskService.myApplyList(queryDTO));
    }
}
