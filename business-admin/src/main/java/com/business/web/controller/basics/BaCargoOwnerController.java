package com.business.web.controller.basics;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.BaBillingInformation;
import com.business.system.domain.BaCargoOwner;
import com.business.system.service.IBaBillingInformationService;
import com.business.system.service.IBaCargoOwnerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * 货主档案Controller
 *
 * @author ljb
 * @date 2023-12-22
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/cargoOwner")
@Api(tags ="货主档案接口")
public class BaCargoOwnerController extends BaseController
{
    @Autowired
    private IBaCargoOwnerService baCargoOwnerService;

    @Autowired
    private IBaBillingInformationService baBillingInformationService;

    /**
     * 查询货主档案列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:cargoOwner:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询货主档案列表接口")
    public TableDataInfo list(BaCargoOwner baCargoOwner)
    {
        //租户ID
        baCargoOwner.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<BaCargoOwner> list = baCargoOwnerService.selectBaCargoOwnerList(baCargoOwner);
        return getDataTable(list);
    }

    /**
     * 导出货主档案列表
     */
    @PreAuthorize("@ss.hasPermi('admin:cargoOwner:export')")
    @Log(title = "货主档案", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出货主档案列表接口")
    public void export(HttpServletResponse response, BaCargoOwner baCargoOwner) throws IOException
    {
        //租户ID
        baCargoOwner.setTenantId(SecurityUtils.getCurrComId());
        List<BaCargoOwner> list = baCargoOwnerService.selectBaCargoOwnerList(baCargoOwner);
        ExcelUtil<BaCargoOwner> util = new ExcelUtil<BaCargoOwner>(BaCargoOwner.class);
        util.exportExcel(response, list, "cargoOwner");
    }

    /**
     * 获取货主档案详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:cargoOwner:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取货主档案详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baCargoOwnerService.selectBaCargoOwnerById(id));
    }

    /**
     * 新增货主档案
     */
    @PreAuthorize("@ss.hasPermi('admin:cargoOwner:add')")
    @Log(title = "货主档案", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增货主档案接口")
    public AjaxResult add(@RequestBody BaCargoOwner baCargoOwner)
    {
        int result = baCargoOwnerService.insertBaCargoOwner(baCargoOwner);
        if(result == -1){
            return AjaxResult.error(-1,"新增用户'" + baCargoOwner.getUser().getUserName() + "'失败，登录账号已存在");
        }else if (result == -2) {
            return AjaxResult.error(-2,"新增用户失败");
        }else if(result == 0){
            return AjaxResult.error(0,"新增部门失败");
        }
        return toAjax(result);
    }

    /**
     * 修改货主档案
     */
    @PreAuthorize("@ss.hasPermi('admin:cargoOwner:edit')")
    @Log(title = "货主档案", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改货主档案接口")
    public AjaxResult edit(@RequestBody BaCargoOwner baCargoOwner)
    {
        int result = baCargoOwnerService.updateBaCargoOwner(baCargoOwner);
        if(result == 0){
            return AjaxResult.error(0,"启动流程失败");
        }else if(result == -1){
            return AjaxResult.error(-1,"简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * 更新货主档案
     */
    @PreAuthorize("@ss.hasPermi('admin:cargoOwner:renew')")
    @Log(title = "货主档案", businessType = BusinessType.UPDATE)
    @PostMapping("/renew")
    @ApiOperation(value = "更新货主档案接口")
    public AjaxResult renew(@RequestBody BaCargoOwner baCargoOwner)
    {
        int result = baCargoOwnerService.updateBaCargoOwner(baCargoOwner);
        if(result == -1){
            return AjaxResult.error(-1,"简称不能重复");
        }else if(result == -2){
            return AjaxResult.error(-2,"名称不能重复");
        }
        return toAjax(result);
    }

    /**
     * 删除货主档案
     */
    @PreAuthorize("@ss.hasPermi('admin:cargoOwner:remove')")
    @Log(title = "货主档案", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除货主档案接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baCargoOwnerService.deleteBaCargoOwnerByIds(ids));
    }

    /**
     * 撤销按钮
     */
    @PreAuthorize("@ss.hasPermi('admin:cargoOwner:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id)
    {
        Integer result = baCargoOwnerService.revoke(id);
        if(result == 0){
            return AjaxResult.error("撤销流程失败");
        }
        return toAjax(result);
    }

    /**
     * 新增开票信息
     */
    @PreAuthorize("@ss.hasPermi('admin:billingInformation:add')")
    @PostMapping("/billingInformation")
    @ApiOperation(value = "新增开票信息")
    public AjaxResult add(@RequestBody BaBillingInformation baBillingInformation)
    {
        int result = baBillingInformationService.insertBaBillingInformation(baBillingInformation);
        return toAjax(result);
    }
}
