package com.business.web.controller.work;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.domain.entity.SysUser;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.service.ISysUserService;
import com.business.system.util.ProcessMapUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.BaComment;
import com.business.system.service.IBaCommentService;

/**
 * 评论Controller
 *
 * @author ljb
 * @date 2023-06-08
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/baComment")
@Api(tags ="评论接口")
public class BaCommentController extends BaseController
{
    @Autowired
    private IBaCommentService baCommentService;

    @Autowired
    private ISysUserService userService;

    /**
     * 查询评论列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:baComment:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询评论列表接口")
    public TableDataInfo list(BaComment baComment)
    {
        startPage();
        List<BaComment> list = baCommentService.selectBaCommentList(baComment);
        return getDataTable(list);
    }

    /**
     * 导出评论列表
     */
    @PreAuthorize("@ss.hasPermi('admin:baComment:export')")
    @Log(title = "评论", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出评论列表接口")
    public void export(HttpServletResponse response, BaComment baComment) throws IOException
    {
        List<BaComment> list = baCommentService.selectBaCommentList(baComment);
        ExcelUtil<BaComment> util = new ExcelUtil<BaComment>(BaComment.class);
        util.exportExcel(response, list, "baComment");
    }

    /**
     * 获取评论详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:baComment:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取评论详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(baCommentService.selectBaCommentById(id));
    }

    /**
     * 新增评论
     */
//    @PreAuthorize("@ss.hasPermi('admin:baComment:add')")
    @Log(title = "评论", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增评论接口")
    public AjaxResult add(@RequestBody BaComment baComment)
    {
        return toAjax(baCommentService.insertBaComment(baComment));
    }

    /**
     * 修改评论
     */
//    @PreAuthorize("@ss.hasPermi('admin:baComment:edit')")
    @Log(title = "评论", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改评论接口")
    public AjaxResult edit(@RequestBody BaComment baComment)
    {
        return toAjax(baCommentService.updateBaComment(baComment));
    }

    /**
     * 删除评论
     */
//    @PreAuthorize("@ss.hasPermi('admin:baComment:remove')")
    @Log(title = "评论", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除评论接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(baCommentService.deleteBaCommentByIds(ids));
    }

    /**
     * 查询该评论下的所有评论
     */
    @GetMapping("/commentList")
    @ApiOperation(value = "查询该评论下的所有评论")
    public TableDataInfo commentList(BaComment baComment)
    {
        List<BaComment> baComments = baCommentService.selectBaCommentList(baComment);

        List<BaComment> list = ProcessMapUtil.processComments(baComments);
        for (BaComment comment:list) {
            SysUser sysUser = userService.selectUserById(comment.getUserId());
            comment.setUserName(sysUser.getNickName());
            comment.setAvatar(sysUser.getAvatar());
            //下级回复姓名
            if(!CollectionUtils.isEmpty(comment.getChild()) && comment.getChild().size() > 0){
                for (BaComment child:comment.getChild()) {
                    SysUser user = userService.selectUserById(child.getUserId());
                    child.setUserName(user.getNickName());
                }
            }
        }
        return getDataTable(list);
    }
}
