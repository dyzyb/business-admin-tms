package com.business.web.controller.oa;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import com.business.system.domain.OaAssigned;
import com.business.system.service.IOaAssignedService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.OaSupervising;
import com.business.system.service.IOaSupervisingService;

/**
 * 督办Controller
 *
 * @author ljb
 * @date 2023-11-20
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/supervising")
@Api(tags ="督办接口")
public class OaSupervisingController extends BaseController
{
    @Autowired
    private IOaSupervisingService oaSupervisingService;

    @Autowired
    private IOaAssignedService oaAssignedService;

    /**
     * 查询督办列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:supervising:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询督办列表接口")
    public TableDataInfo list(OaSupervising oaSupervising)
    {
        //租户
        oaSupervising.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<OaSupervising> list = oaSupervisingService.selectOaSupervisingList(oaSupervising);
        return getDataTable(list);
    }

    /**
     * 导出督办列表
     */
    @PreAuthorize("@ss.hasPermi('admin:supervising:export')")
    //@Log(title = "督办", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出督办列表接口")
    public void export(HttpServletResponse response, OaSupervising oaSupervising) throws IOException
    {
        //租户
        oaSupervising.setTenantId(SecurityUtils.getCurrComId());
        List<OaSupervising> list = oaSupervisingService.selectOaSupervisingList(oaSupervising);
        ExcelUtil<OaSupervising> util = new ExcelUtil<OaSupervising>(OaSupervising.class);
        util.exportExcel(response, list, "supervising");
    }

    /**
     * 获取督办详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:supervising:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取督办详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaSupervisingService.selectOaSupervisingById(id));
    }

    /**
     * 新增督办
     */
    /*@PreAuthorize("@ss.hasPermi('admin:supervising:add')")
    @Log(title = "督办", businessType = BusinessType.INSERT)*/
    @PostMapping
    @ApiOperation(value = "新增督办接口")
    public AjaxResult add(@RequestBody OaSupervising oaSupervising)
    {
        return toAjax(oaSupervisingService.insertOaSupervising(oaSupervising));
    }

    /**
     * 修改督办
     */
    /*@PreAuthorize("@ss.hasPermi('admin:supervising:edit')")
    @Log(title = "督办", businessType = BusinessType.UPDATE)*/
    @PutMapping
    @ApiOperation(value = "修改督办接口")
    public AjaxResult edit(@RequestBody OaSupervising oaSupervising)
    {
        return toAjax(oaSupervisingService.updateOaSupervising(oaSupervising));
    }

    /**
     * 删除督办
     */
    /*@PreAuthorize("@ss.hasPermi('admin:supervising:remove')")
    @Log(title = "督办", businessType = BusinessType.DELETE)*/
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除督办接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(oaSupervisingService.deleteOaSupervisingByIds(ids));
    }

    /**
     * 交办我的
     */
    @GetMapping("/assignedList")
    @ApiOperation(value = "查询交办列表接口")
    public TableDataInfo assignedList(OaAssigned oaAssigned)
    {
        //租户
        oaAssigned.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<OaAssigned> list = oaAssignedService.selectOaAssignedList(oaAssigned);
        return getDataTable(list);
    }

    /**
     * 交办详情
     */
    @GetMapping(value = "assigned/{id}")
    @ApiOperation(value = "交办详情")
    public AjaxResult assigned(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaAssignedService.selectOaAssignedById(id));
    }

    /**
     * 回告
     */
     @PostMapping(value = "/reply")
     @ApiOperation(value = "回告")
     public AjaxResult reply(@RequestBody OaSupervising oaSupervising)
     {
         return toAjax(oaSupervisingService.reply(oaSupervising));
     }

    /**
     * 撤销按钮
     */
    //@PreAuthorize("@ss.hasPermi('admin:oaFee:revoke')")
    @GetMapping(value = "/revoke/{id}")
    @ApiOperation(value = "撤销按钮")
    public AjaxResult revoke(@PathVariable("id") String id) {
        Integer result = oaSupervisingService.revoke(id);
        if (result == 0) {
            return AjaxResult.error("撤回失败");
        }
        return toAjax(result);
    }

    /**
     * 批复
     */
    @PostMapping(value = "/approval")
    @ApiOperation(value = "批复按钮")
    public AjaxResult approval(@RequestBody OaSupervising oaSupervising) {
        Integer result = oaSupervisingService.approval(oaSupervising);
        if (result == 0) {
            return AjaxResult.error("批复失败");
        }
        return toAjax(result);
    }

    /**
     * 办结
     */
    @PostMapping(value = "/completion")
    @ApiOperation(value = "办结按钮")
    public AjaxResult completion(@RequestBody OaSupervising oaSupervising) {
        Integer result = oaSupervisingService.completion(oaSupervising);
        if (result == 0) {
            return AjaxResult.error("办结失败");
        }
        return toAjax(result);
    }

    /**
     * 督办事项统计
     */
    @PostMapping(value = "/oaSupervisingStat")
    @ApiOperation(value = "督办事项统计")
    public AjaxResult oaSupervisingStat(@RequestBody OaSupervising oaSupervising)
    {
        //租户ID
        oaSupervising.setTenantId(SecurityUtils.getCurrComId());
        return AjaxResult.success(oaSupervisingService.oaSupervisingStat(oaSupervising));
    }

    /**
     * 人员督办统计
     */
    @PostMapping(value = "/userOaSupervisingStat")
    @ApiOperation(value = "人员督办统计")
    public AjaxResult userOaSupervisingStat(@RequestBody OaSupervising oaSupervising)
    {
        //租户ID
        oaSupervising.setTenantId(SecurityUtils.getCurrComId());
        return AjaxResult.success(oaSupervisingService.userOaSupervisingStat(oaSupervising));
    }
}
