package com.business.web.controller.file;

import com.business.common.annotation.Anonymous;
import com.business.common.core.domain.UR;
import com.business.common.core.redis.GetRedisIncreID;
import com.business.system.service.MsService;
import com.business.system.util.RandomUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 短信/邮件发送
 *
 * @author jiahe
 * @date 2020.7
 */
@RestController
@RequestMapping("/msService")
@CrossOrigin //跨域
@Api(tags = "短信发送接口")
public class MsController {

    //短信业务层
    @Autowired
    private MsService msService;
    //redis缓存业务层
    @Autowired
    private RedisTemplate<String, String> redisTemplate;



    //发送修改密码验证码接口
    @ApiOperation("发送修改密码验证码接口")
    @GetMapping("updatePassword/{phone}")
    @Anonymous
    public UR updatePassword(@PathVariable String phone){
        String templateCode = "SMS_174895022";
        //生成随机值发送给阿里云
        String code1 = RandomUtils.getFourBitRandom();
        String phoneCode = "6"+code1;
        Map<String, Object> param = new HashMap<>();
        param.put("code",phoneCode);
        //调用service发送短信的方法
        boolean isSend = msService.send(phone,param,templateCode);
        if(isSend){
            String key = phone+"updatePassword";
            redisTemplate.opsForValue().set(key,phoneCode,15, TimeUnit.MINUTES);
            return UR.ok();
        }else {
            return UR.error().message("短信发送失败");
        }
    }

    //发送用户注册短信验证码
    @ApiOperation("发送实名认证短信接口")
    @GetMapping("sendphone/{phone}")
    @Anonymous
    public UR sendRegisterMs(@PathVariable String phone){
        //生成随机值发送给阿里云
        String code1 = RandomUtils.getFourBitRandom();
        String phoneCode = "6"+code1;
        Map<String, Object> param = new HashMap<>();
        param.put("code",phoneCode);
        String templateCode = "SMS_174895023";
        //调用service发送短信的方法
        boolean isSend = msService.send(phone,param,templateCode);
        if(isSend){
            redisTemplate.opsForValue().set(phone,phoneCode,15, TimeUnit.MINUTES);
            return UR.ok();
        }else{
            return UR.error().message("短信发送失败");
        }
    }


    //验证短信验证码
    @ApiOperation("验证修改密码短信验证码接口")
    @GetMapping("phoneCode")
    @Anonymous
    public UR phoneCode (String phone, String code){
        Boolean flag = msService.checkCode(phone,code);
        if (flag){
            return UR.ok().data("code",code);
        }else{
            return UR.error().message("验证码不正确");
        }
    }

    /**
     * 验证码校验
     */
    @PostMapping("/code")
    @ApiOperation(value = "验证码校验")
    @Anonymous
    public UR code(String code,String phone){
        //获取短信验证码并验证
        if (code==null){
            return UR.error().code(-1).message("验证码不存在");
        }else {
            String redisCode = String.valueOf(redisTemplate.opsForValue().get(phone));
            if (!code.equals(redisCode)) {
                return UR.error().code(-1).message("验证码不正确");
            }

        }
        return UR.ok().code(1).message("验证码校验成功");
    }

//    //验证短信验证码并修改手机号
//    @ApiOperation("验证短信验证码并修改手机号接口")
//    @GetMapping("checkCodeUpdatePhone")
//    public UR checkCodeUpdatePhone (String phone, String code, HttpServletRequest request){
//        Boolean flag = msService.checkCodeUpdatePhone(phone,code,request);
//        if (flag){
//            return UR.ok().data("code",code);
//        }else{
//            return UR.error().message("验证码不正确");
//        }
//    }

//    //验证短信验证码
//    @ApiOperation("验证邮箱验证码接口")
//    @GetMapping("emailCode")
//    public UR emailCode (String email, String code){
//
//        Boolean flag = msService.checkEmailCode(email,code);
//        if (flag){
//            return UR.ok().data("code",code);
//        }else{
//            return UR.error().message("验证码不正确");
//        }
//    }


}
