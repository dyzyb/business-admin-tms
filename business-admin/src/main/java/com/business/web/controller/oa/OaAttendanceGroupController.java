package com.business.web.controller.oa;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.business.common.annotation.Log;
import com.business.common.core.controller.BaseController;
import com.business.common.core.domain.AjaxResult;
import com.business.common.core.page.TableDataInfo;
import com.business.common.enums.BusinessType;
import com.business.common.utils.SecurityUtils;
import com.business.common.utils.poi.ExcelUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.business.system.domain.OaAttendanceGroup;
import com.business.system.service.IOaAttendanceGroupService;

/**
 * 考勤组Controller
 *
 * @author single
 * @date 2023-11-24
 */
@RestController
@CrossOrigin //跨域
@RequestMapping("/attendanceGroup")
@Api(tags ="考勤组接口")
public class OaAttendanceGroupController extends BaseController
{
    @Autowired
    private IOaAttendanceGroupService oaAttendanceGroupService;

    /**
     * 查询考勤组列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:attendanceGroup:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询考勤组列表接口")
    public TableDataInfo list(OaAttendanceGroup oaAttendanceGroup)
    {
        //租户ID
        oaAttendanceGroup.setTenantId(SecurityUtils.getCurrComId());
        startPage();
        List<OaAttendanceGroup> list = oaAttendanceGroupService.selectOaAttendanceGroupList(oaAttendanceGroup);
        return getDataTable(list);
    }

    /**
     * 导出考勤组列表
     */
    //@PreAuthorize("@ss.hasPermi('admin:attendanceGroup:export')")
    @Log(title = "考勤组", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ApiOperation(value = "导出考勤组列表接口")
    public void export(HttpServletResponse response, OaAttendanceGroup oaAttendanceGroup) throws IOException
    {
        //租户ID
        oaAttendanceGroup.setTenantId(SecurityUtils.getCurrComId());
        List<OaAttendanceGroup> list = oaAttendanceGroupService.selectOaAttendanceGroupList(oaAttendanceGroup);
        ExcelUtil<OaAttendanceGroup> util = new ExcelUtil<OaAttendanceGroup>(OaAttendanceGroup.class);
        util.exportExcel(response, list, "attendanceGroup");
    }

    /**
     * 获取考勤组详细信息
     */
    //@PreAuthorize("@ss.hasPermi('admin:attendanceGroup:query')")
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "获取考勤组详细信息接口")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(oaAttendanceGroupService.selectOaAttendanceGroupById(id));
    }

    /**
     * 新增考勤组
     */
    //@PreAuthorize("@ss.hasPermi('admin:attendanceGroup:add')")
    @Log(title = "考勤组", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增考勤组接口")
    public AjaxResult add(@RequestBody OaAttendanceGroup oaAttendanceGroup)
    {
        return toAjax(oaAttendanceGroupService.insertOaAttendanceGroup(oaAttendanceGroup));
    }

    /**
     * 延迟生效
     */
    @PostMapping("/delayOaAttendanceGroup")
    @ApiOperation(value = "新增延迟生效数据")
    public AjaxResult delayOaAttendanceGroup(@RequestBody OaAttendanceGroup oaAttendanceGroup)
    {
        return toAjax(oaAttendanceGroupService.delayOaAttendanceGroup(oaAttendanceGroup));
    }


    /**
     * 修改考勤组
     */
    //@PreAuthorize("@ss.hasPermi('admin:attendanceGroup:edit')")
    @Log(title = "考勤组", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改考勤组接口")
    public AjaxResult edit(@RequestBody OaAttendanceGroup oaAttendanceGroup)
    {
        return toAjax(oaAttendanceGroupService.updateOaAttendanceGroup(oaAttendanceGroup));
    }

    /**
     * 删除考勤组
     */
    //@PreAuthorize("@ss.hasPermi('admin:attendanceGroup:remove')")
    @Log(title = "考勤组", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    @ApiOperation(value = "删除考勤组接口")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(oaAttendanceGroupService.deleteOaAttendanceGroupByIds(ids));
    }

    /**
     * 获取当前登陆人的打卡信息
     */
    @GetMapping(value = "/CheckRules")
    @ApiOperation(value = "获取当前登陆人的打卡信息")
    public AjaxResult CheckRules(String userId)
    {
        return AjaxResult.success(oaAttendanceGroupService.CheckRules(userId));
    }
}
