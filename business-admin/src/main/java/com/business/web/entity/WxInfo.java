package com.business.web.entity;

import lombok.Data;

import java.util.Map;

@Data
public class WxInfo {

    private static final long serialVersionUID = 1L;

    private String nickName ;

    private String gender;

    private String avatarUrl;

    private String signature;

    private String encryptedData;

    private String iv;

    private Map<String,String> userInfo;

    private String phoneNumber;

    private String userName;

    private String password;

}
